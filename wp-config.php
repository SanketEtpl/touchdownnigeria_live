<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'daoceans_tdn');

/** MySQL database username */
define('DB_USER', 'daoceans_tdn');

/** MySQL database password */
define('DB_PASSWORD', '123etpl');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7QdGZE}c.`r+@7S!!V>B#U>VJO[v}to-!xk,/mBr>p]_AHkR!|Y<YmzeV69]/%Jx');
define('SECURE_AUTH_KEY',  'YO5<:a9T#ZSvf`sa!oHqvk!]We0onO@(+Z1<78t-e.DZwvjD*%=zq%$pt >_3T0E');
define('LOGGED_IN_KEY',    '$Ufq+OWy}jzO:DGf^?i,C/i:38u!T1QpZKLD}9tj(orw@U=>Ife0K6l yAWI}a7z');
define('NONCE_KEY',        'd/B&|?KE<KHF6W!oM9@6l]5N2U`E#K|vQD:tNp.vtwi2m.O]bBX$Qrz|$Jhc&#]J');
define('AUTH_SALT',        'wC-I)YZ6=` 7y~`ou^oY1lo2yJnXaPey8[%*)*m0J}vd}cMycHJRo^K#4A#G7s&#');
define('SECURE_AUTH_SALT', '8|K|aKgk)Lur!-P3?*/}Ly>ByW(NDs73^#8q$;Dkmrhe11rf3<XX*$alVyv# fD&');
define('LOGGED_IN_SALT',   '2/TN<26200=<N*NDN<9ZGw`mCvy>`L@z9h{AON{Q}sjqvli `xgP4|>j;?`UXX`H');
define('NONCE_SALT',       '(`|Uc4qwZ!`E0J41%*s6,3)?-sUxFkKsQ/LWuW?3.Z=ZcK^T)44-g~4j7q3pM>sP');
define('FS_METHOD', 'direct');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
