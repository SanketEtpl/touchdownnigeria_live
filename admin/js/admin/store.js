var pagenumber = 0;
var inprocess = null;
$("#listStoreCategory th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listStoreCategory th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listStoreCategory th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listStoreCategory(0,column,order);
  }
});


var pagenumber = 0;
var inprocess = null;
function listStore(page,column,order){
  $(".loadingBackground, .loadingImg").show();
  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"store/listStore",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listStore tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
        $(".loadingBackground, .loadingImg").hide();
      }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'store_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listStore(0,column,order);

function getStore($this){

  $("#storeForm")[0].reset();
  $('#store_thumb_img').css('display','none');
  $('#store_thumb_img').attr("src","");
  $("#storeForm .form-control").removeClass('error-block');
  $("#storeForm span.error-block").hide();
  $('#ImageHtml').html("");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/getStore",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      $("#action").html('Edit');
          var alert_type='danger';
          if(json.status == "success"){
            if (json.storeData.store_thumb) {
              $("[name=store_thumb]").each(function(){
                  $(this).rules("add", {
                  required: false,
                   });
                 });
            }
            $("#store_key").val($($this).attr("data-row-id"));
            $("#store_name").val(json.storeData.store_name);
            $("#store_desc").val(json.storeData.store_desc);
            $("#offer_desc").val(json.storeData.offer_desc);
            $("#store_category_id").val(json.storeData.store_category_id);
            $("#airport_id").val(json.storeData.airport_id);
            getTerminal(json.storeData.terminal_id);
            $("#terminal_id").val(json.storeData.terminal_id);

             // ---------- multiple images code here -----------
             // 
             console.log('######'+json.storeData.store_thumb);
             
             if("" != json.storeData.store_thumb)
             {

                var imgarr = json.storeData.store_thumb.split(",");
                $("#ImageHtml").html("");
                $(".store_one").hide();
                var icnt=1;
                $.each(imgarr, function(index, value){
                     $("#ImageHtml").append('<div class="col-md-6">\n\
                    <div class="form-group">\n\
                      <label class="control-label" for="store_thumb">Store Image</label>\n\
                       &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
                      <br/>\n\
                      <img src="'+BASEURL+'images/store/'+value+'" id="store_thumb_img" width="120" height="120" style="padding: 7px;">\n\
                      <input type="hidden" name="store_thumb_old[]" value="'+value+'">\n\
                    </div>\n\
                  </div>');

                     $("#imgcnt").val(icnt++);
                });
             }


            // exit;

            // $('#store_thumb_img').css('display','');
            // $('#store_thumb_img').attr("src",json.storeData.store_thumb);
            // $('#img').val(json.storeData.store_thumb);
            jQuery('#storeModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#store_key").val('');
    $("[name=store_thumb]").each(function(){
                $(this).rules("add", {
                required: true,
                 });
               });
    if ($('#airport_id').val()) {
        getTerminal();
    }
    jQuery('#storeModal').modal('show', {backdrop: 'static'});
  }
}

function deleteStore($this)
{
  var flag=confirm('Do you want to delete this store?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/deleteStore",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listStore(pagenumber,ordercolumn,orderby);
           toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listStore th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listStore th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listStore th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listStore(0,column,order);
  }
});


function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listStore(0,column,order); 
}
$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listStore(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#storeForm").validate({
        rules: {
          store_name: {
                required: true,
                NoSpace: true,
                maxlength: 100,
            },
          store_desc: {
              required: true,
          },
          store_thumb: {
              required: true,
          },
          airport_id: {
              required: true,
          },
          /*terminal_id: {
              required: true,
          },*/
          store_category_id: {
              required: true,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          store_name:{
                required:  "Please enter store name.",
                maxlength:  "Store name is too long.",
            },
          store_desc: "Please enter store detail.",
          store_thumb: "Please upload store image.",
          airport_id: "Please select airport.",
         // terminal_id: "Please select terminal.",
          store_category_id: "Please select store category.",
        }
    });

    $("#storeForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          //$("#storeForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#storeModal').modal('hide');
            if(json.action == "add"){
              listStore(0,'store_id','DESC');
            }else{
              listStore(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          //$("#storeForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            //$("#storeForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            //$("#storeForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });


    $("#storeFaciForm").validate({
        ignore: [],
        rules: {
          'facility[]': {
                required: true,
            },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          'facility[]': "Please enter facility name.",
        }
    });

    $("#storeFaciForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#storeFaciForm").find('[type="submit"]').toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#storeFaciModal').modal('hide');
            if(json.action == "add"){
              listStore(0,'store_id','DESC');
            }else{
              listStore(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#storeFaciForm").find('[type="submit"]').removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#storeFaciForm").find('[type="submit"]').removeClass('sending').blur();
        },
        error: function()
        {
            $("#storeFaciForm").find('[type="submit"]').removeClass('sending').blur();
        }
    });


    $("#storeOfferForm").validate({
        ignore: [],
        rules: {
          'offer[]': {
                required: true,
            },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          'offer[]': "Please enter offer.",
        }
    });

    $("#storeOfferForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#storeOfferForm").find('[type="submit"]').toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#storeOfferModal').modal('hide');
            if(json.action == "add"){
              listStore(0,'store_id','DESC');
            }else{
              listStore(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#storeOfferForm").find('[type="submit"]').removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#storeOfferForm").find('[type="submit"]').removeClass('sending').blur();
        },
        error: function()
        {
            $("#storeOfferForm").find('[type="submit"]').removeClass('sending').blur();
        }
    });
});

function getStoreFacility($this) {
  //jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
  $("#storeFaciForm")[0].reset();
  $("#storeFaciForm .form-control").removeClass('error-block');
  $("#storeFaciForm span.error-block").css("display","none");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/getStoreFacility",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#fstore_key").val($($this).attr("data-row-id"));
            if (json.storeFaciData) {

              var count=Object.keys(json.storeFaciData).length;
              //$("#facility").val(json.storeFaciData[0].facility_name);
              $("#facilitis").html('');
              for(i=0;i<count;i++)
              {
                var newrow=Math.floor((Math.random() * 100) + 1);
                var total=$('input[name="facility[]"]').length;
                if (total) {
                    var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" value="'+json.storeFaciData[i].facility_name+'" placeholder="Facility" id="facility" name="facility[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"><button id="remove'+newrow+'" class="btn btn-info" onclick="removeone('+newrow+');" style="display:" type="button"><i class="fa fa-fw fa-times"></i></button></div></div></div>';
                }
                else
                {
                  var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" value="'+json.storeFaciData[i].facility_name+'" placeholder="Facility" id="facility" name="facility[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"></div></div></div>';
                }

                $('#facilitis').append(str);
              }
            }
            jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
      $("#action").html('Add');
    $("#fstore_key").val('');
    jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
  }
}

function addone() {
  var newrow=Math.floor((Math.random() * 100) + 1);
  var total=$('input[name="facility[]"]').length;
  var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" placeholder="Facility" id="facility" name="facility[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"><button id="remove'+newrow+'" class="btn btn-info" onclick="removeone('+newrow+');" style="display:" type="button"><i class="fa fa-fw fa-times"></i></button></div></div></div>';
  //$('#addmore'+cnt).css('display','none');
  //$('#remove'+cnt).css('display','');
  $('#facilitis').append(str);
  if (total==9)
  {
    $('#addmore').css('display','none');
  }
}
function removeone(cnt) {
    $('#row'+cnt).remove();
    var total=$('input[name="facility[]"]').length;
    if (total>=9)
    {
      $('#addmore').css('display','');
    }
}

function getStoreOffer($this) {
  //jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
  $("#storeOfferForm")[0].reset();
  $("#storeOfferForm.form-control").removeClass('error-block');
  $("#storeOfferForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/getStoreOffer",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#ostore_key").val($($this).attr("data-row-id"));
            if (json.storeOfferData) {

              var count=Object.keys(json.storeOfferData).length;
              //$("#facility").val(json.storeFaciData[0].facility_name);
              $("#offers").html('');
              for(i=0;i<count;i++)
              {
                var newrow=Math.floor((Math.random() * 100) + 1);
                var total=$('input[name="offer[]"]').length;
                if (total) {
                    var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" value="'+json.storeOfferData[i].offer_name+'" placeholder="Offer" id="offer" name="offer[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"><button id="remove'+newrow+'" class="btn btn-info" onclick="removeoneoffer('+newrow+');" style="display:" type="button"><i class="fa fa-fw fa-times"></i></button></div></div></div>';
                }
                else
                {
                  var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" value="'+json.storeOfferData[i].offer_name+'" placeholder="Offer" id="offer" name="offer[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"></div></div></div>';
                }

                $('#offers').append(str);
              }
            }
            jQuery('#storeOfferModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
          }
          else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#ostore_key").val('');
    jQuery('#storeOfferModal').modal('show', {backdrop: 'static'});
  }
}

function addoneoffer() {
  var newrow=Math.floor((Math.random() * 100) + 1);
  var total=$('input[name="offer[]"]').length;
  var str='<div class="row" id="row'+newrow+'"><div class="col-md-10"><div class="form-group"><input type="text" placeholder="Offer" id="offer" name="offer[]" class="form-control" autocomplete="off"></div></div><div class="col-md-2"><div class="form-group"><button id="remove'+newrow+'" class="btn btn-info" onclick="removeoneoffer('+newrow+');" style="display:" type="button"><i class="fa fa-fw fa-times"></i></button></div></div></div>';
  $('#offers').append(str);
  if (total==9)
  {
    $('#addmore').css('display','none');
  }
}
function removeoneoffer(cnt) {
    $('#row'+cnt).remove();
    var total=$('input[name="offer[]"]').length;
    if (total<=9)
    {
      $('#addoneoffer').css('display','');
    }
}
function getTerminal(terminal_id) {
    if($('#airport_id').val())
    {
      $.ajax({
            type: "POST",
            dataType: "json",
            url: BASEURL+"subadmin/getTerminal",
            data: {"airport_id":$('#airport_id').val()},
        }).success(function (json) {
        if(json.status == "success"){

              var myOptions =json.terminalData;
              var mySelect = $('#terminal_id');
              mySelect.html('');
              mySelect.append(
                      $('<option></option>').val('').html('Select Terminal')
                  );
              $.each(myOptions, function(val, text) {
                if(val==terminal_id)
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(true)
                  );
                }
                else
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(false)
                  );
                }
              });
              //listTerminalAdmins(pagenumber,ordercolumn,orderby);
              //toastr.success(json.msg,"Success:");
            }else{
              toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
            }
        });
    }
    else
    {
      toastr.error("Please select airport.","Error:");
    }
}


var icnt=1;
function addImage(){
  var imgcnt = $("#imgcnt").val();
 /* if(imgcnt < 5){*/
  $("#ImageHtml").append('<div class="col-md-6">\n\
    <div class="form-group">\n\
      <label class="control-label" for="store_thumb">Store Image</label> &nbsp; (Size : 300X300) \n\
      &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
      <br/>\n\
      <img src="" id="store_thumb_img" width="120px" style="padding: 7px;display:none">\n\
      <input type="file" placeholder="Store Image" name="store_thumb[]" class="" autocomplete="off">\n\
      Upload upto 5 Mb image.\n\
    </div>\n\
  </div>');
  var newcnt = parseInt(imgcnt) + 1;
  $("#imgcnt").val(newcnt);
/*}else{

}*/
}

$("body").on("click", ".remove_field", function (e) {
  $(this).parents("div .col-md-6").remove();
    });
