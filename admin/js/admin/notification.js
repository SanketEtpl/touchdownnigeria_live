
var pagenumber = 0;
var inprocess = null;
var func = 'listNotification';
var column = 'notification_id';
var order = 'DESC';
if (module=='history') {
    func = 'listSentNotification';
    var column = 'created_date';
var order = 'DESC';
}else{
    func = 'listNotification';
column = 'notification_id';
order = 'DESC';
}
function listNotification(page,column,order){
  $(".loadingBackground, .loadingImg").show();
  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"notification/"+func,
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listNotification tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
        $(".loadingBackground, .loadingImg").hide();
      }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var ordercolumn = column;
var orderby = order;
listNotification(0,column,order);
function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listNotification(0,column,order); 
}

$(document).ready(function()
{
    $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listNotification(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

$("#select_all").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    if(status)
    {
      // $('.checkbox1').attr("disabled", true);
       $('.checkbox1').each(function(){
        this.checked = true;
        });
    }
    else
    {
        //$('.checkbox1').attr("disabled", false);
        $('.checkbox1').each(function(){
        this.checked = false;
        });
    }
    //$('.checkbox1').each(function(){ //iterate all listed checkbox items
    //    this.checked = status; //change ".checkbox" checked status
    //});
});
$('.checkbox2').click(function(){
                                   alert('dffg');
});
$('.checkbox1').change(function(){ //".checkbox" change
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(this.checked == false){ //if this item is unchecked
        $("#select_all")[0].checked = false; //change "select all" checked status to false
    }

    //check "select all" if all checkbox items are checked
    if ($('.checkbox:checked').length == $('.checkbox1').length ){
        $("#select_all")[0].checked = true; //change "select all" checked status to true
    }
});

$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#notificationForm").validate({
        rules: {
            notification_text: {
                required: true,
                NoSpace: true,
                maxlength: 200,
            },

        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
            notification_text:{
                required:  "Please enter notification text.",
                maxlength:  "Notification text is too long.",
            },
        }
    });

    $("#notificationForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#notificationModal').modal('hide');
            if(json.action == "add"){
              listNotification(0,'notification_id','DESC');
            }else{
              listNotification(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
            $("#notificationForm")[0].reset();
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
           $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });

    $("#notificationForm1").validate({
        rules: {
          notification: {
                required: true,
                NoSpace: true,
                maxlength: 200,
            },
             airport_3: {
                required: true,
            },


        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          notification:{
                required:  "Please enter notification text.",
                maxlength:  "Notification text is too long.",
            },
            airport_3:{
                required:  "Please select airport.",
            },

        }
    });

$('.send_conf').click(function(){
  if($('#notification').val())
  {
   var flag=confirm('Do you want to send this notification?');


    if(!flag)
    {
      toastr.warning("You have cancelled your request.","Info:");
      return false;
    }else
    {
      var chk = $('input[type=checkbox]:checked').length;

      if(chk > 0)
      {
        return true;
      }
      else
      {

        toastr.error("Please select at least one airport.","Error:");
        return false;  
      }

      //return true;
      
    }
  }
  });

    $("#notificationForm1").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#save_btn").toggleClass('sending').blur();
          $(".loadingBackground, .loadingImg").show();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {
          
        },
        success: function(json)
        {
          if(json.status == "success"){
            //if(json.action == "add"){
            //  listStore(0,'store_name','ASC');
            //}else{
            //  listStore(pagenumber,ordercolumn,orderby);
            //}
            toastr.success(json.msg,"Success:");
            $("#notificationForm1")[0].reset();
            $(".loadingBackground, .loadingImg").hide();
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
           $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });

    $("#notificationAlertForm").validate({
        rules: {
            gota: {
                required: true,
                NoSpace: true,
                maxlength: 200,
            },

        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
            gota:{
                required:  "Please enter flight number.",
                maxlength:  "flight number is too long.",
            },
        }
    });

    $("#notificationAlertForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#notificationAlertModal').modal('hide');
            if(json.action == "add"){
              listNotification(0,'notification_id','DESC');
            }else{
              listNotification(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
            $("#notificationAlertForm")[0].reset();
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
           $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });


    $("#notificationForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
            //$("#airportForm").find('[type="submit"]')
            $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {
            $(".loadingBackground, .loadingImg").show();
        },
        success: function(json)
        {
            if(json.status == "success")
            {
                $(".loadingBackground, .loadingImg").hide();
                jQuery('#notificationModal').modal('hide');
                toastr.success(json.msg,"Success:");
                $("#notificationForm")[0].reset();
                listNotification(0,'notification_id','DESC');
            }
            else
            {
                toastr.error(json.msg,"Error:");
                $('#store_thumb').val('');
                if(json.status == "logout")
                {
                    window.location=BASEURL+"/logout";
                }
            }
            $("#save_notification").removeClass('sending').blur();
        },
        complete: function(json)
        {


        },
        error: function()
        {


        }
    });




});


$("#listNotification th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listNotification th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listNotification th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listNotification(0,column,order);
  }
});

function getNotification($this){
  $("#notificationForm")[0].reset();
  $("#notificationForm .form-control").removeClass('error-block');
  $("#notificationForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/getNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      $("#action").html('Edit');
          var alert_type='danger';
          if(json.status == "success"){
            $("[name=store_thumb]").each(function(){
                $(this).rules("add", {
                required: false,
                 });
               });
            $("#notification_key").val($($this).attr("data-row-id"));
            $("#notification_text").val(json.notificationData.notification_text);
            if(json.notificationData.replace_array)
            {
              var str='';
              var arr=json.notificationData.replace_array;
              $('#replace_array').show();
              for(i=0;i<arr.length;i++)
              {
                str+='<span onclick="putMe(this);" class="btn btn-block btn-default" id="span'+(i+1)+'">'+arr[i]+'</span><br/>';
              }
              $('#replace_array').html(str);
            }
            else
            {
              $('#replace_array').hide();
            }
            jQuery('#notificationModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#notification_key").val('');
    jQuery('#notificationModal').modal('show', {backdrop: 'static'});
  }
}
function putMe($this) {
    var str =$('#'+$this.id).html();
    str=str.replace(' ','-');
    var text='**'+str+'**';
    //alert(text);
    var txtarea = document.getElementById('notification_text');
    var scrollPos = txtarea.scrollTop;
    var caretPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0, caretPos);
    var back = (txtarea.value).substring(txtarea.selectionEnd, txtarea.value.length);
    txtarea.value = front +text+ back;
    caretPos = caretPos + text.length;
    txtarea.selectionStart = caretPos;
    txtarea.selectionEnd = caretPos;
    txtarea.focus();
    txtarea.scrollTop = scrollPos;
}
function sendNotification($this)
{
    var flag=confirm('Do you want to send this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/sendbyId",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listNotification(pagenumber,ordercolumn,orderby);
           toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

function sendNotificationAlert($this)
{
    var flag=confirm('Do you want to send this notification?');
    if(!flag)
    {
      toastr.warning("You have cancelled your request.","Info:");
      return false;
    }
    $("#notificationAlertForm")[0].reset();
  $("#notificationAlertForm .form-control").removeClass('error-block');
  $("#notificationAlertForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/getNotificationValues",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      $("#action").html('Edit');
          var alert_type='danger';
          if(json.status == "success"){
            if(json.flag!="NO_FORM")
            {
                jQuery('#notificationAlertModal').modal('show', {backdrop: 'static'});
            }
            else
            {
                if(json.status == "success")
                {
                   listNotification(pagenumber,ordercolumn,orderby);
                   toastr.success(json.msg,"Success:");
                }else
                {
                   toastr.error(json.msg,"Error:");
                 if(json.status == "logout")
                 {
                  window.location=BASEURL+"/logout";
                 }
                }
            }

            $('#htmldata').html(json.htmlData);

            for(i=0;i<json.replace_array.length;i++)
            {
                var token=json.replace_array[i];
                var token1=token.replace(' ','-');
                //alert(token);
                $("[name="+token1+"]").each(function(){
                $(this).rules("add", {
                NoSpace: true,
                maxlength: 20,
                required: true,
                messages: {
                    required: "Please enter "+token,
                    maxlength:"Too long "+token,
                  }
                 });
               });
            }
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#notification_key").val('');
    jQuery('#notificationAlertModal').modal('show', {backdrop: 'static'});
  }

}

function deleteSentNotification($this)
{
  var flag=confirm('Do you want to delete this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/deleteSentNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listNotification(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}
function deletePendingNotification($this)
{
  var flag=confirm('Do you want to delete this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/deletePendingNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listNotification(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

function addNewNotifications()
{
    $('#notificationModal').modal('show');
}


function deleteNotification($this)
{
  var flag=confirm('Do you want to delete this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  else
  {
    if($($this).attr("data-row-id")){
      $.ajax({
            type: "POST",
            dataType: "json",
            url: BASEURL+"notification/deleteNotification",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
        if(json.status == "success")
        {
            listNotification(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
        }
        else
        {
            toastr.error(json.msg,"Error:");
            if(json.status == "logout")
            {
              window.location=BASEURL+"/logout";
            }
        }
        });
      }
    }
}
