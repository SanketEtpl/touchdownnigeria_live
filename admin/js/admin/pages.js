var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('page_detail');

var editorElement = CKEDITOR.document.getById( 'page_detail' );
CKEDITOR.instances.page_detail.on('change', function() {
         $("#page_detail").val(CKEDITOR.instances['page_detail'].getData());
    //alert(CKEDITOR.instances['page_detail'].getData());
      });
function listPages(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"pages/listPages",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listPages tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }      
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

function getPage($this){
  $("#pageForm")[0].reset();
  $("#pageForm .form-control").removeClass('error-block');
  $("#pageForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"pages/getPage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#page_key").val($($this).attr("data-row-id"));
            $("#type").val(json.pageData.type);
            $("#page_title_txt").html(json.pageData.page_title);
            if (json.pageData.type==1)
            {
                $("#row_page_detail").hide();
                $("#row_office_address").show();
                $("#row_call_us_on").show();
                $("#row_write_to_us_at").show();
                $("#office_address").val(json.pageData.office_address);
                $("#call_us_on").val(json.pageData.call_us_on);
                $("#write_to_us_at").val(json.pageData.write_to_us_at);
            }
            else
            {
              $("#row_page_detail").show();
              $("#row_office_address,#row_call_us_on,#row_write_to_us_at").hide();
              $("#page_detail").val(json.pageData.page_detail);
              CKEDITOR.instances['page_detail'].setData(json.pageData.page_detail)
              //var editorElement = CKEDITOR.document.getById( 'page_detail' );
              //editorElement.setHtml(json.pageData.page_detail);
              //alert('sd');
            }
            jQuery('#pageModal').modal('show', {backdrop: 'static'});
            //toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
  }else{
    $("#action").html('Add');
    $("#page_key").val('');
    jQuery('#pageModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'page_title';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listPages(0,column,order);

$("#listPages th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listPages th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listPages th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPages(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listPages(0,column,order); 
}

$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listPages(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#pageForm").validate({
      rules: {
        page_title: {
              required: true,
              NoSpace: true,
              maxlength: 50,
          },
        page_detail: {
              required: true,
              NoSpace: true,
          },
        office_address:{
            required: true,
            NoSpace: true,
        },
        call_us_on:{
            required: true,
            maxlength: 15,
            number: true,
            NoSpace:true,
        },
        write_to_us_at:{
            required: true,
            email: true,
            maxlength: 50,
            NoSpace:true,
        },
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        page_title: {
                required: "Please enter title.",
                maxlength: "Title is too long."
            },
        page_detail: {
                required: "Please enter detail.",
            },
        office_address:{
            required: "Please enter office address.",
        },
        call_us_on:{
            required: "Please enter phone number.",
            number: "Please enter valid phone number.",
            maxlength: "Phone number is too long."
        },
        write_to_us_at:{
            required: "Please enter email id.",
            email : "Please enter valid email id.",
            maxlength: "Email id is too long."
        },
      }
  });
  $("#pageForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#save_btn").toggleClass('sending').blur();
       /*var editorElement = CKEDITOR.document.getById( 'page_detail' );

       console.log(editorElement);*/
       var data = CKEDITOR.instances['page_detail'].getData();

       if("" == data)
       {
          alert("Please enter text");
          return false;
       }
   //$("#page_detail").val(editorElement.getHtml());
   //alert(editorElement.getHtml());
   
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
     //   var editorElement = CKEDITOR.document.getById( 'page_detail' );
   //$("#page_detail").val(editorElement.getHtml());
   //alert(editorElement.getHtml());
   //$("#page_detail").val(CKEDITOR.instances['page_detail'].getData());
  
    },
    success: function(json) 
    {
      if(json.status == "success"){  
        jQuery('#pageModal').modal('hide');
        if(json.action == "add"){
          listPages(0,'page_title','DESC'); 
        }else{
          listPages(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      }
      $("#save_btn").removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#save_btn").removeClass('sending').blur();
    },
    error: function()
    {
        $("#save_btn").removeClass('sending').blur();
        toastr.error("Something went wrong.","Error:");
    }
  });
});