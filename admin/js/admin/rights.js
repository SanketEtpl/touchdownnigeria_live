var pagenumber = 0;
var inprocess = null;
function listRights(page,column,order){
   $(".loadingBackground, .loadingImg").show();
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"rights/listRights",
      data: {"page":page,"column":column,"order":order,"key":$("#role_id").val()},
  }).success(function (json) {
      inprocess = null;
      var alert='danger';
      if(json.status == "success"){
        
          $("#listRights tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }      
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      $('.checkBox').bootstrapToggle({
        //on: 'On',
        //off: 'Off'
      });
           $(".loadingBackground, .loadingImg").hide();
  });
}

function updatePermission($this){
  //$("#permissionForm")[0].reset();
   $(".loadingBackground, .loadingImg").show();
  if($("#role_id").val()){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"rights/updatePermission",
          data: {"key":$("#role_id").val(),"status":$($this).attr("data-row-status"),"module":$($this).attr("data-row-module"),"sub_function":$($this).attr("data-row-subfunction")},
      }).success(function (json) {
          if(json.status == "success"){
            listRights(0,column,order);
            $($this).attr("data-row-status",json.rowval)
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
           $(".loadingBackground, .loadingImg").hide();
      });
  }else{
    toastr.error("Please select role.","Error:");
     $(".loadingBackground, .loadingImg").hide();
  }
}

function updatePermissionAccess($this) {
   $(".loadingBackground, .loadingImg").show();
    //alert($this.id);
    if($("#role_id").val()){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"rights/updatePermissionAccess",
          data: {"key":$("#role_id").val(),"status":$($this).attr("data-row-status"),"module":$($this).attr("data-row-module"),"sub_function":$($this).attr("data-row-subfunction")},
      }).success(function (json) {
          if(json.status == "success"){
            listRights(0,column,order);
            $($this).attr("data-row-status",json.rowval)
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
           //$(".loadingBackground, .loadingImg").hide();
      });
  }else{
    toastr.error("Please select role.","Error:");
     $(".loadingBackground, .loadingImg").hide();
  }
    
}
var column = 'cat';
var order = 'ASC';
var ordercolumn = column;
var orderby = order;
listRights(0,column,order);

$("#listRights th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listRights th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listRights th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listRights(0,column,order);
  }
});
