var pagenumber = 0;
var inprocess = null;
function listPermission(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"permission/listPermission",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      var alert='danger';
      if(json.status == "success"){
          $("#listPermission tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
      $('.checkBox').bootstrapToggle({
        on: 'Yes',
        off: 'No'
      });
  });
}

function updatePermission($this){
  $("#permissionForm")[0].reset();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"permission/updatePermission",
          data: {"key":$($this).attr("data-row-id"),"status":$($this).attr("data-row-status")},
      }).success(function (json) {
          if(json.status == "success"){
            $($this).attr("data-row-status",json.rowval)
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          }
      });
  }else{
    toastr.error("Please try again.","Error:");
  }
}

var column = 'cat';
var order = 'ASC';
var ordercolumn = column;
var orderby = order;
listPermission(0,column,order);

$("#listPermission th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listPermission th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listPermission th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPermission(0,column,order);
  }
});