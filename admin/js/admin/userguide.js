/*
Author: Ram K
Page : userguide.js
Description: User guide details page.
*/

var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('ug_desc');
var editorElement = CKEDITOR.document.getById( 'ug_desc' );
CKEDITOR.instances.ug_desc.on('change', function() {
         $("#ug_desc").val(CKEDITOR.instances['ug_desc'].getData());
    //alert(CKEDITOR.instances['page_detail'].getData());
      });
function listUserGuide(page,column,order){
  $(".loadingBackground, .loadingImg").show();
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
    type: "POST",
    dataType: "json",
    url: BASEURL+"User_guide/listUserGuide",
    data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    inprocess = null;
    if(json.status == "success"){
      $("#listUserGuide tbody").html(json.rows);
      $("#paginate_links").html(json.pagelinks);
      $("#paginate_entries").html(json.entries);
      $(".loadingBackground, .loadingImg").hide();
    }
    if(json.status == "logout"){
      window.location=BASEURL+"/logout";
    }
  });
}

var column = 'ug_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listUserGuide(0,column,order);
function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listUserGuide(0,column,order); 
}

function getUserGuide($this){
  $("#userGuideForm")[0].reset();
  
  $("#userGuideForm .form-control").removeClass('error-block');
  $("#userGuideForm span.error-block").hide();
  if($($this).attr("data-row-id")){

    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"User_guide/getUserGuide",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
        //console.log(json);
        $("#action").html('Edit');
        var alert_type='danger';
        var opt = '<select name="airportData" id="airportData" class="form-control">';
        if(json.status == "success"){

          $("#ug_key").val($($this).attr("data-row-id"));
          $("#ug_title").val(json.ugData.ug_title);
          $("#ug_desc").val(json.ugData.ug_details);
          CKEDITOR.instances['ug_desc'].setData(json.ugData.ug_details)

            //$("#airportData").val(json.ugData.airport_id);

            $(json.airport).each(function(i,val){

              if(val.id == json.ugData.airport_id)
              {
                opt +='<option value="'+ val.id+'" selected>'+val.name+'</option>';  
              }
              else
              {
                opt +='<option value="'+ val.id+'">'+val.name+'</option>';   
              }


            });

            opt+='</option>';

            //console.log(opt);
            $('#airportData').remove();

            $('#airportOptions').before(opt);

            jQuery('#UserGuideModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
            window.location=BASEURL+"/logout";
          }
        });
  }else{
    $("#action").html('Add');
    $("#ug_key").val('');

    jQuery('#UserGuideModal').modal('show', {backdrop: 'static'});
  }
}

function deleteGuide($this)
{
  var flag=confirm('Do you want to delete this Guide?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"User_guide/deleteGuide",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
      if(json.status == "success"){             
       toastr.success(json.msg,"Success:");
       listUserGuide(pagenumber,ordercolumn,orderby); 
     }else{
      toastr.error(json.msg,"Error:");
      if(json.status == "logout"){
        window.location=BASEURL+"/logout";
      }
    }
  });
  }else{
  }
}

$("#listUserGuide th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listUserGuide th").removeClass("sorting_asc").removeClass("sorting_desc");
    column = $(this).attr("data-name");
    order = $(this).attr("data-order");
    ordercolumn = column;
    orderby = order;
    $("#listUserGuide th").attr("data-order","ASC");
    if(order.toLowerCase() == 'asc'){
     $(this).attr("data-order","DESC");
   }else{
    $(this).attr("data-order","ASC");
  }
  $(this).addClass("sorting_"+order.toLowerCase());
  listUserGuide(0,column,order);
}
});

$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
     if($(this).val() != ""){
      $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
    } 
  }
  if($(this).val() == "") {
    $("#clear").remove(); 
    listUserGuide(0,column,order);        
  } 

}); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });

  $.validator.addMethod(
    "NoSpace",
    function(value, element) {
      return $.trim(value);
    },
    "No white space allowed."
    );
  $("#userGuideForm").validate({
    rules: {
      ug_title: {
        required: true,
        NoSpace: true,
        maxlength: 100,
      },
      ug_desc: {
        required: true,
      },          
    },
    errorClass: "error-block",
    errorElement: "span",
    messages: {
      ug_title:{
        required:  "Please enter user guide name.",
        maxlength:  "Guide name is too long.",
      },
      ug_desc: "Please enter Guide detail.",          
    }
  });

  $("#userGuideForm").ajaxForm({
    dataType: 'json',
    beforeSend: function() 
    {
          //$("#storeForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
          var data = CKEDITOR.instances['ug_desc'].getData();

       if("" == data)
       {
          alert("Please enter User guide details.");
          return false;
       }
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {

        },
        success: function(json) 
        {
          if(json.status == "success"){           
            jQuery('#UserGuideModal').modal('hide');
            if(json.action == "add"){
              listUserGuide(0,'ug_id','DESC'); 
            }else{
              listUserGuide(pagenumber,ordercolumn,orderby);  
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
           // $('#store_thumb').val('');
           if(json.status == "logout"){
            window.location=BASEURL+"/logout";
          }
        }
          //$("#storeForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json) 
        {
            //$("#storeForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
          },
          error: function()
          {
            //$("#storeForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");            
          }
        });



});



// $("body").on("click", ".remove_field", function (e) {
//   $(this).parents("div .col-md-6").remove();            
//     });