
var pagenumber = 0;
var inprocess = null;
function listApproveNotification(page,column,order){
  $(".loadingBackground, .loadingImg").show();
  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"notification/listApproveNotification",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listApproveNotification tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
        $(".loadingBackground, .loadingImg").hide();
      }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'created_date';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listApproveNotification(0,column,order);

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listApproveNotification(0,column,order); 
}

$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listApproveNotification(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#notificationForm").validate({
        rules: {
            notification_text: {
                required: true,
                NoSpace: true,
                maxlength: 200,
            },

        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
            notification_text:{
                required:  "Please enter notification text.",
                maxlength:  "Notification text is too long.",
            },
        }
    });

    $("#notificationForm").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            jQuery('#notificationModal').modal('hide');
            if(json.action == "add"){
              listApproveNotification(0,'notification_text','ASC');
            }else{
              listApproveNotification(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
            $("#notificationForm")[0].reset();
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
           $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });

    $("#notificationForm1").validate({
        rules: {
          notification: {
                required: true,
                NoSpace: true,
                maxlength: 200,
            },


        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          notification:{
                required:  "Please enter notification text.",
                maxlength:  "Notification text is too long.",
            },

        }
    });

    $("#notificationForm1").ajaxForm({
        dataType: 'json',
        beforeSend: function()
        {
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {
          if(json.status == "success"){
            //if(json.action == "add"){
            //  listStore(0,'store_name','ASC');
            //}else{
            //  listStore(pagenumber,ordercolumn,orderby);
            //}
            toastr.success(json.msg,"Success:");
            $("#notificationForm1")[0].reset();
          }else{
            toastr.error(json.msg,"Error:");
            $('#store_thumb').val('');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
           $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });

});

$("#listApproveNotification th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listApproveNotification th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listApproveNotification th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listApproveNotification(0,column,order);
  }
});

$("#editNotification").click(function(){

  var val = $('#updateExistingNotification').val();

  if("" == val || undefined === val)
  {
      $('#txtError').text('Please enter notification');
      $('#txtError').show();
  }
  else
  {
      $('#txtError').hide();
      $.ajax({

        type : "POST",
        url : BASEURL+"notification/saveUpdateNotification",
        data :
        {
            "key": $('#editId').val(),
            "notification" : $('#updateExistingNotification').val()
        },
        success:function(json)
        {
          var obj = JSON.parse(json);
          if(obj.status == "success")
          {
              $('#updateNotificationModel').modal('hide');
              listApproveNotification(pagenumber,ordercolumn,orderby);
              toastr.success(obj.msg,"Success:");
              //window.location.href=BASEURL+"notification/approveAndSend"
          }
          else
          {
            $('#updateNotificationModel').modal('hide');
            toastr.success(obj.msg,"Error:");
          }

        }

      }); 
  }

   

});



function getNotification($this){
  $("#notificationForm")[0].reset();
  $("#notificationForm .form-control").removeClass('error-block');
  $("#notificationForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/getNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      $("#action").html('Edit');
          var alert_type='danger';
          if(json.status == "success"){
            $("[name=store_thumb]").each(function(){
                $(this).rules("add", {
                required: false,
                 });
               });
            $("#notification_key").val($($this).attr("data-row-id"));
            $("#notification_text").val(json.notificationData.notification_text);
            if(json.notificationData.replace_array)
            {
              var str='';
              var arr=json.notificationData.replace_array;
              $('#replace_array').show();
              for(i=0;i<arr.length;i++)
              {
                str+='<span onclick="putMe(this);" id="span'+(i+1)+'">'+arr[i]+'</span><br/>';
              }
              $('#replace_array').html(str);
            }
            else
            {
              $('#replace_array').hide();
            }
            jQuery('#notificationModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#notification_key").val('');
    jQuery('#notificationModal').modal('show', {backdrop: 'static'});
  }
}
function putMe($this) {
    var text=$('#'+$this.id).html();
    //alert(text);
    var txtarea = document.getElementById('notification_text');
    var scrollPos = txtarea.scrollTop;
    var caretPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0, caretPos);
    var back = (txtarea.value).substring(txtarea.selectionEnd, txtarea.value.length);
    txtarea.value = front +" "+text+ back;
    caretPos = caretPos + text.length;
    txtarea.selectionStart = caretPos;
    txtarea.selectionEnd = caretPos;
    txtarea.focus();
    txtarea.scrollTop = scrollPos;
}
function approveAndSend($this)
{
    var flag=confirm('Do you want to approve this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/approveAndSend",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listApproveNotification(pagenumber,ordercolumn,orderby);
           toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

function updateNotification($this)
{
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/editNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            /*listApproveNotification(pagenumber,ordercolumn,orderby);
           toastr.success(json.msg,"Success:");*/

           //alert(json.notification_text);

           $('#editId').val($($this).attr("data-row-id"));
           $('#updateExistingNotification').val(json.data[0].notification_text);
           $('#updateNotificationModel').modal('show');

          }else{
            //toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
}





function deletePendingNotification($this)
{
  var flag=confirm('Do you want to reject this notification?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"notification/deletePendingNotification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listApproveNotification(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}
