var pagenumber = 0;
var inprocess = null;
function listAirportCsv(page,column,order){


  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"airportScript/listAirportCsv",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
          $("#listAirportCsv tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
       if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'flight_number';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAirportCsv(0,column,order);



function deleteCSVFile(id)
{
  var flag=confirm('Do you want to delete this csv file?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }

  $.ajax({

            type : "POST",
            url : BASEURL+"airportScript/deleteCSVFile",
            data :
            {
                'id' : id
            },
            success : function(json)
            {
                var obj = JSON.parse(json);
                if(obj.status == "success")
                {
                    listAirportCsv(pagenumber,ordercolumn,orderby);
                    toastr.success(obj.msg,"Success:");
                }
                else
                {
                    toastr.error(obj.msg,"Error:");
                    if(obj.status == "logout")
                    {
                        window.location=BASEURL+"/logout";
                    }
                }
            }

        });
}


