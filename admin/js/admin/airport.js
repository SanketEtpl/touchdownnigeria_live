var pagenumber = 0;
var inprocess = null;
function listAirports(page,column,order){

  //alert(column);
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"airports/listAirports",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
          $("#listAirports tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
       if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

function getAirport($this){
  $("#airportForm")[0].reset();
  // $('#interior_thumb_img').css('display','none');
  // $('#interior_thumb_img').attr("src","");
  // $('#exterior_thumb_img').css('display','none');
  // $('#exterior_thumb_img').attr("src","");
  $("#latitude").val('');
  $("#longitude").val('');
  $("#interior").val('');
  $("#exterior").val('');
  $("#zipcode,#city,#state,#country,#airport_name,#address").removeAttr('readonly');
  $("#airportForm span.error-block").css("display","none");
  $("#airportForm .form-control").removeClass("error-block");

  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"airports/getAirport",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            
            if (json.airportData.interior) {
              $("[name=interior]").each(function(){
                $(this).rules("add", {
                required: false,
                 });   
               });
              // $('#interior_thumb_img').css('display','');
              // $('#interior_thumb_img').attr("src",json.airportData.interior);
            }else
            {
              $("[name=interior]").each(function(){
                $(this).rules("add", {
                required: true,
                 });   
               });
            }
            if (json.airportData.exterior) {
              $("[name=exterior]").each(function(){
                $(this).rules("add", {
                required: false,
                 });   
               });
              // $('#exterior_thumb_img').css('display','');
              // $('#exterior_thumb_img').attr("src",json.airportData.exterior);
            }else
            {
              $("[name=exterior]").each(function(){
                $(this).rules("add", {
                required: true,
                 });   
               });
            }

          //  console.log(json.airportData);
            $("#action").html('Edit');
            $("#airport_key").val($($this).attr("data-row-id"));
            $("#airport_name").val(json.airportData.airport_name);
            $("#contact_number").val(json.airportData.contact_number);
            $("#email_address").val(json.airportData.email_address);
            $("#address").val(json.airportData.address);
            // -------- interior & exterior data here -------------
            $("#interior").val(json.airportData.interior);
            $("#exterior").val(json.airportData.exterior);
            $("#fs_code").val(json.airportData.fs);

            $("#iata_code").val(json.airportData.iata);
            $("#icao_code").val(json.airportData.icao);
            
            /*if (json.airportData.airport_name!="")
            {
                $("#airport_name").attr('readonly','readonly');
            }
            if (json.airportData.address!="")
            {
                $("#address").attr('readonly','readonly');
            }*/
            if (json.airportData.city!="")
            {
                $("#city").attr('readonly','readonly');
                $("#city").val(json.airportData.city);
            }
            if (json.airportData.state!="")
            {
                $("#state").attr('readonly','readonly');
                $("#state").val(json.airportData.state);
            }
            if (json.airportData.country!="")
            {
                $("#country").attr('readonly','readonly');
                $("#country").val(json.airportData.country);
            }
            if (json.airportData.zipcode!="")
            {
                //$("#zipcode").attr('readonly','readonly');
                $("#zipcode").val(json.airportData.zipcode);
            }
            $("#bio").val(json.airportData.bio);
            $("#latitude").val(json.airportData.latitude);
            $("#longitude").val(json.airportData.longitude);
            
            $.ajax({
                type: "POST",
                dataType: "json",
                url: BASEURL+"airports/getaddress/"+json.airportData.latitude+"/"+json.airportData.longitude,
                data: {"key":$($this).attr("data-row-id")},
            }).success(function (json1) {
            if(json1.status == "success"){
                 $("#address").val(json1.address);
                }else{
                  toastr.error(json1.msg,"Error:");
                  if(json1.status == "logout"){
                window.location=BASEURL+"/logout";
                }
                }
            });
            
            
            jQuery('#airportModal').modal('show', {backdrop: 'static'});
          }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#airport_key").val('');
    $("[name=interior]").each(function(){
                $(this).rules("add", {
                required: true,
                 });   
               });
    $("[name=exterior]").each(function(){
                $(this).rules("add", {
                required: true,
                 });   
               });
    jQuery('#airportModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAirports(0,column,order);

$("#listAirports th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAirports th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listAirports th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAirports(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();  
  listAirports(0,column,order);  
}

$(document).ready(function()
{
    $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove();  
      listAirports(0,column,order); 
    }  
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $('input').each(function(){
    $(this).focusout(function() {
    //$(this).val($.trim($(this).val()));
    if(this.id=='address')
    {
      if ($(this).val()!="")
      {
          $("[name=latitude]").each(function(){
                $(this).rules("add", {
                required: true,
                 });   
               });
      }
      else
      {
        $("[name=latitude]").each(function(){
                $(this).rules("add", {
                required: false,
                 });   
               });
      }
    }
  });



});
$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  
  $("#airportForm").validate({
      ignore: "",
      rules: {
        airport_name: {
              required: true,
              maxlength: 100,
              NoSpace: true,
          },
        contact_number: {
              required: true,
              maxlength: 15,
              number: true,
              NoSpace: true,
          },
        email_address: {
              required: true,
              email: true,
              maxlength: 50,
              NoSpace: true,
          },
        address: {
              required: true,
              NoSpace: true,
          },
        //latitude: {
        //      required: true,
        //  },
        //longitude: {
        //      required: true,
        //  }, 
        city: {
              required: true,
              maxlength: 30,
              NoSpace: true,
          },
        state: {
              required: true,
              maxlength: 50,
              NoSpace: true,
          },
        country: {
              required: true,
              maxlength: 50,
              NoSpace: true,
          },
        zipcode: {
              required: true,
              minlength: 5,
              maxlength: 6,
              NoSpace: true,
          },
        interior: {
            required: true,
        },
        exterior: {
            required: true,
        },
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        airport_name: {
              required: "Please enter your airport name.",
              maxlength: "Airport name is too long.",
          },
        contact_number: {
                required: "Please enter phone number.",
                number: "Please enter valid phone number.",
                maxlength: "Phone number is too long."
            },
        email_address: {
                required: "Please enter your email id.",
                email : "Please enter valid email id.",
                maxlength: "Email id is too long."
            },
        address: "Please enter your address.",
        latitude: "Please select valid address from dropdown.",
        //longitude: "Please select address from dropdown.",
        city: {
              required: "Please enter city.",
              maxlength: "City name is too long.",
          },
        state: {
              required: "Please enter state.",
              maxlength: "State name is too long.",
          },
        country: {
          required:"Please enter country.",
          maxlength: "Country name is too long."
        },
        zipcode:{
          required:"Please enter zip code.",
          minlength: "Please enter valid zip code.",
          maxlength: "Please enter valid zip code.",
          },
       interior: "Please enter interior.",
        exterior: "Please enter exterior.",
      }
  });
  $("#airportForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          //$("#airportForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
          toastr.success(json.msg,'Success:');
          jQuery('#airportModal').modal('hide');
          if(json.action == "add"){
            listAirports(0,'id','DESC'); 
          }else{
            listAirports(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
        }
        //$("#airportForm").find('[type="submit"]')
        $("#save_btn").removeClass('sending').blur();
      },
      complete: function(json) 
      {
          //$("#airportForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
      },
      error: function()
      {
          //$("#airportForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
          toastr.error("Something went wrong.","Error:");
      }
  });
/*$('#address').keydown(function(event)
{
  if((event.keyCode>=65 && event.keyCode<=90) || (event.keyCode>=45 && event.keyCode<=57) || (event.keyCode>=96 && event.keyCode<=111) || (event.keyCode>=186 && event.keyCode<=192) || (event.keyCode>=219 && event.keyCode<=222) || event.keyCode==8)
  {
    if (event.ctrlKey)
    {
        return true;
    }
    $("#zipcode,#city,#state,#country,#latitude,#longitude").val('');
    $("#zipcode,#city,#state,#country").removeAttr('readonly');
  }
  if(event.keyCode==13)
  {
    return false;
  }
});*/
  function init() {
var options = {
              types: ['(cities)'],
              componentRestrictions: { country: "NG" }
                }       
    var input = document.getElementById('address');
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    google.maps.event.addListener(autocomplete, 'place_changed',
       function() {
        /*$("#zipcode,#city,#state,#country,#latitude,#longitude").val('');
        $("#zipcode,#city,#state,#country").removeAttr('readonly');*/
          var place = autocomplete.getPlace();
          
          //console.log(place);
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
             var loc1 = place;
             var county, city, state,landmark,district,postal_code;
               $.each(loc1, function(k1,v1) {
                  if (k1 == "address_components") {
                     for (var i = 0; i < v1.length; i++) {
                        for (k2 in v1[i]) {
                           if (k2 == "types") {
                              var types = v1[i][k2];
                              if (types[0] =="country") {
                                  county = v1[i].long_name;
                                  $('#country').val(county);
                                  $("#country").attr('readonly','readonly');          
                              } 
                              if (types[0] =="locality") {
                                  city = v1[i].long_name;
                                  $('#city').val(city);
                                  $("#city").attr('readonly','readonly');
                             }
                             if(types[0] =="administrative_area_level_1"){
                                  state = v1[i].long_name;
                                  $('#state').val(state);
                                  $("#state").attr('readonly','readonly');
                             }
                             if(types[0] =="postal_code"){
                                  postal_code = v1[i].long_name;
                                  $('#zipcode').val(postal_code);
                                  $("#zipcode").attr('readonly','readonly');
                             }
                           }
                        }          
                     }
                  }
               });
          document.getElementById("latitude").value = lat;
          document.getElementById("longitude").value = lng;
       }
    );
}
google.maps.event.addDomListener(window, 'load', init);
});
function deleteAirport($this)
{
  var flag=confirm('Do you want to delete this airport?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"airports/deleteAirport",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listAirports(pagenumber,ordercolumn,orderby);  
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
            if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}
