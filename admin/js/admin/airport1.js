var pagenumber = 0;
var inprocess = null;
function listAirports(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"airports/listAirports",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
          $("#listAirports tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

function getAirport($this){
  $("#airportForm")[0].reset();
  $("#latitude").val('');
  $("#longitude").val('');
  $("#airportForm span.error-block").css("display","none");
  $("#airportForm .form-control").removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"airports/getAirport",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#airport_key").val($($this).attr("data-row-id"));
            $("#airport_name").val(json.airportData.airport_name);
            $("#contact_number").val(json.airportData.contact_number);
            $("#email_address").val(json.airportData.email_address);
            $("#address").val(json.airportData.address);
            $("#city").val(json.airportData.city);
            $("#state").val(json.airportData.state);
            $("#country").val(json.airportData.country);
            $("#zipcode").val(json.airportData.zipcode);
            $("#bio").val(json.airportData.bio);
            $("#latitude").val(json.airportData.latitude);
            $("#longitude").val(json.airportData.longitude);
            jQuery('#airportModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#airport_key").val('');
    jQuery('#airportModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAirports(0,column,order);

$("#listAirports th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAirports th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listAirports th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAirports(0,column,order);
  }
});

$(document).ready(function()
{
  $("#airportForm").validate({
      rules: {
        airport_name: {
              required: true,
          },
        contact_number: {
              required: true,
          },
        email_address: {
              required: true,
          },
        address: {
              required: true,
          },
        latitude: {
              required: true,
          },
        longitude: {
              required: true,
          }, 
        city: {
              required: true,
          },
        state: {
              required: true,
          },
        country: {
              required: true,
          },
        zipcode: {
              required: true,
          }
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        airport_name: "Please enter your airport name.",
        contact_number: "Please enter your phone.",
        email_address: "Please enter your email.",
        address: "Please enter your address.",
        latitude: "Please select address from dropdown.",
        longitude: "Please select address from dropdown.",
        city: "Please enter your city.",
        state: "Please enter your state.",
        country: "Please enter your country.",
        zipcode: "Please enter your zip code.",
      }
  });
  $("#airportForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#airportForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
          toastr.success(json.msg,'Success:');
          jQuery('#airportModal').modal('hide');
          if(json.action == "add"){
            listAirports(0,'airport','DESC'); 
          }else{
            listAirports(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
        }
        $("#airportForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#airportForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#airportForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
  
  function init() {
var options = {
              //types: ['(cities)'],
              //componentRestrictions: { country: "GR" }
                }       
    var input = document.getElementById('address');
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    google.maps.event.addListener(autocomplete, 'place_changed',
       function() {
          var place = autocomplete.getPlace();
          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();
          console.log(place);
          document.getElementById("city").value = place.address_components[2].long_name;
          document.getElementById("state").value=place.address_components[3].long_name;
          document.getElementById("country").value=place.address_components[4].long_name;
          document.getElementById("latitude").value = lat;
          document.getElementById("longitude").value = lng;
       }
    );
}
google.maps.event.addDomListener(window, 'load', init);
});

