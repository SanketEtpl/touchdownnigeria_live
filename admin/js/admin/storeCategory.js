var pagenumber = 0;
var inprocess = null;
function listStoreCategory(page,column,order){
  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"store/listStoreCategory",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listStoreCategory tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'category_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listStoreCategory(0,column,order);

function deleteStoreCategory($this)
{
  var flag=confirm('Do you want to delete this product category?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/deleteStoreCategory",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listStoreCategory(pagenumber,ordercolumn,orderby);
           toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listStoreCategory th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listStoreCategory th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listStoreCategory th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listStoreCategory(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listStoreCategory(0,column,order); 
}

$(document).ready(function()
{

  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listStoreCategory(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#storeCatiForm").validate({
        rules: {

          category_name: {
              required: true,
              NoSpace: true,
              maxlength: 100,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
        category_name: {
              required: "Please enter store category.",
              maxlength: "Store category name is too long.",
          },
        }
    });

    $("#storeCatiForm").ajaxForm({


      //return false;
        dataType: 'json',
        beforeSend: function()
        {
          //$("#storeFaciForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();


        },
        uploadProgress: function(event, position, total, percentComplete)
        {

        },
        success: function(json)
        {

          if(json.status == "success"){
            jQuery('#storeCategoryModal').modal('hide');
            if(json.action == "add"){
              listStoreCategory(0,'category_id','DESC');
            }else{
              listStoreCategory(pagenumber,ordercolumn,orderby);
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          //$("#storeFaciForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json)
        {
            //$("#storeFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            //$("#storeFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });
});

function getStoreCategory($this) {
  //jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
  $("#storeCatiForm")[0].reset();
  $("#storeCatiForm .form-control").removeClass('error-block');
  $("#storeCatiForm span.error-block").css("display","none");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/getStoreCategory",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#storefacility_key").val($($this).attr("data-row-id"));
            if (json.storeFaciData) {

              var count=Object.keys(json.storeFaciData).length;

              $("#category_name").val(json.storeFaciData.category);
              // $("#store_id").val(json.storeFaciData.store_id);

              console.log(json);
            }
            jQuery('#storeCategoryModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){

          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#storefacility_key").val('');
    jQuery('#storeCategoryModal').modal('show', {backdrop: 'static'});
  }
}
