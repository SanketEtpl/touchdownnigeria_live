var pagenumber = 0;
var inprocess = null;
function listTerminalAdmins(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"subadmin/listTerminalAdmins",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listAdmins tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
      if(json.status == "logout"){
      window.location=BASEURL+"/logout";
      }
  });
}

function getTerminalUser($this){
  $("#userForm")[0].reset();
  var mySelect = $('#terminal_id');
              mySelect.html('');
              mySelect.append(
                      $('<option></option>').val('').html('Select Terminal')
                  );
  $("#userForm .form-control").removeClass('error-block');
  $("#userForm span.error-block").hide();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"subadmin/getTerminalUser",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#password_note").show();
              $("[name=password]").each(function(){
                $(this).rules("add", {
                required: false,
                NoSpace: false,
                 });   
               });                        
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(json.userData.fname);
            $("#last_name").val(json.userData.lname);

            $("#username").val(json.userData.username);
            $("#email_id").val(json.userData.email_id);

            $("#phone").val(json.userData.phone);
            $("#airport").val(json.userData.airport);
            //alert('sdf');
            getTerminal(json.userData.terminal_id);
            $("#terminal_id").val(json.userData.terminal_id);
            if(json.userData.is_active === 1){
              $('#active').prop('checked', true);
            }else{
              console.log(json.userData.is_active);
              $('#active').prop('checked', false);              
            }
            
              jQuery('#userModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
              
          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
    $("#password_note").hide();
    $("[name=password]").each(function(){
                $(this).rules("add", {
                required: true,
                NoSpace: true,
                 });   
               });
    if ($('#airport').val()) {
        getTerminal();
    }
  }
}

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listTerminalAdmins(0,column,order);

$("#listAdmins th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAdmins th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAdmins th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listTerminalAdmins(0,column,order);
  }
});
function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listTerminalAdmins(0,column,order); 
}

$(document).ready(function()
{

    $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  {
      $("#clear").remove();    
      listTerminalAdmins(0,column,order);   
    }

    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
//$('input').each(function(){
//    $(this).focusout(function() {
//    $(this).val($.trim($(this).val()));
//  });
//});

//$('input').each(function(){
//    $(this).keypress(function(e) {
//    if (e.keyCode==32) {
//        return false;
//    }
//  });
//});

$('#password').keypress(function(e) {
  if ($('#password').val()) {
      $("[name=password]").each(function(){
                $(this).rules("add", {
                required: true,
                NoSpace: true,
                 });   
               });
  }
});



$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);

  $("#userForm").validate({
      rules: {
        first_name: {
              required: true,
              maxlength: 50,
              NoSpace:true,
              //regex:'[a-zA-Z0-9]',
          },
        last_name: {
              required: true,
              maxlength: 50,
              NoSpace:true,
          },
        username: {
              required: true,
              maxlength: 30,
              NoSpace:true,
          },
        password: {
              required: true,
              minlength: 5,
              maxlength: 20,
              NoSpace:true,
              //regex:'[a-zA-Z0-9]',
          },
        email_id: {
              required: true,
              email: true,
              maxlength: 50,
              NoSpace:true,
          },
        phone: {
              required: true,
              maxlength: 15,
              number: true,
              NoSpace:true,
          },
        airport: {
              required: true,
          },
        terminal_id:{
              required: true,
          },
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: {
                required: "Please enter your first name.",
                maxlength: "First name is too long.",
                //regex:"First name should not contain only white space.",
            },
        last_name: {
                required: "Please enter your last name.",
                maxlength: "Last name is too long."
            },
        username: {
                required: "Please enter your username.",
                maxlength: "Username is too long."
            },
        password: {
                required: "Please enter your password.",
                minlength: "Your password must be at least 5 characters long.",
                maxlength: "Password is too long.",
                //regex:"Password should not contain only white space.",
            },
        email_id: {
                required: "Please enter email id.",
                email : "Please enter valid email id.",
                maxlength: "Email id is too long."
            },
        phone: {
                required: "Please enter phone number.",
                number: "Please enter valid phone number.",
                maxlength: "Phone number is too long."
            },
        airport: "Please select airport.",
        terminal_id: "Please select terminal.",
      }
  });
  $("#userForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          //$("#userForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){           
          jQuery('#userModal').modal('hide');
          if(json.action == "add"){
            listTerminalAdmins(0,'user','DESC'); 
          }else{
            listTerminalAdmins(pagenumber,ordercolumn,orderby);  
          }
          toastr.success(json.msg,"Success:");
        }else{
          toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
        }
        //$("#userForm").find('[type="submit"]')
        $("#save_btn").removeClass('sending').blur();
      },
      complete: function(json) 
      {
          //$("#userForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
      },
      error: function()
      {
          //$("#userForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
          toastr.error("Something went wrong.","Error:");
      }
  });
});
$(function() {
  $('#active').bootstrapToggle({
    on: 'Yes',
    off: 'No'
  });
})

function deleteUser($this)
{
  var flag=confirm('Do you want to delete this terminal admin?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"subadmin/deleteTerminalUser",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listTerminalAdmins(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}
function changeStatus($this)
{
  var flag=confirm('Do you want to '+$($this).attr("title").toLowerCase()+' this terminal admin?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"subadmin/changeTerminalStatus",
          data: {"key":$($this).attr("data-row-id"),"status":$($this).attr("data-row-status")},
      }).success(function (json) {
      if(json.status == "success"){
            listTerminalAdmins(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}
function getTerminal(terminal_id) {
  //alert($('#airport').val());
    if($('#airport').val())
    {
      $.ajax({
            type: "POST",
            dataType: "json",
            url: BASEURL+"subadmin/getTerminal/main",
            data: {"airport_id":$('#airport').val()},
        }).success(function (json) {
        if(json.status == "success"){
              
              var myOptions =json.terminalData;
              var mySelect = $('#terminal_id');
              mySelect.html('');
              mySelect.append(
                      $('<option></option>').val('').html('Select Terminal')
                  );
              $.each(myOptions, function(val, text) {
                if(val==terminal_id)
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(true)
                  );
                }
                else
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(false)
                  ); 
                }
              });
              //listTerminalAdmins(pagenumber,ordercolumn,orderby);
              //toastr.success(json.msg,"Success:");
            }else{
              toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
            }
        });
    }
    else
    {
      toastr.error("Please select airport.","Error:");
    }
}