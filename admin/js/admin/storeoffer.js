var pagenumber = 0;
var inprocess = null;
function listStoreOffer(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"store/listStoreOffer",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listStoreOffer tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
       if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'store_offer_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listStoreOffer(0,column,order);

function deleteStoreOffer($this)
{
  var flag=confirm('Do you want to delete this offer?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/deleteStoreOffer",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listStoreOffer(pagenumber,ordercolumn,orderby);  
           toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listStoreOffer th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listStoreOffer th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listStoreOffer th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listStoreOffer(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listStoreOffer(0,column,order); 
}

$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listStoreOffer(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#storeOfferForm").validate({
        rules: {
          store_id: {
                required: true,
            },
          offer_name: {
              required: true,
              NoSpace: true,
              maxlength: 100,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          store_id: "Please select store.",
          offer_name: {
              required: "Please enter offer.",
              maxlength: "Offer name is too long.",
          },
        }
    });

    $("#storeOfferForm").ajaxForm({
        dataType: 'json',
        beforeSend: function() 
        {
          //$("#storeOfferForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {
            
        },
        success: function(json) 
        {
          if(json.status == "success"){           
            jQuery('#storeOfferModal').modal('hide');
            if(json.action == "add"){
              listStoreOffer(0,'store_offer_id','DESC'); 
            }else{
              listStoreOffer(pagenumber,'store_offer_id',orderby);  
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          //$("#storeOfferForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json) 
        {
            //$("#storeOfferForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            //$("#storeOfferForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });
});

function getStoreOffer($this) {
  //jQuery('#storeFaciModal').modal('show', {backdrop: 'static'});
  $("#storeOfferForm")[0].reset();
  $("#storeOfferForm .form-control").removeClass('error-block');
  $("#storeOfferForm span.error-block").css("display","none");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"store/getStoreOffer",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#storeoffer_key").val($($this).attr("data-row-id"));
            if (json.storeOfferData) {
                
              var count=Object.keys(json.storeOfferData).length;
              $("#offer_name").val(json.storeOfferData.offer_name);
              $("#store_id").val(json.storeOfferData.store_id);
            }
            jQuery('#storeOfferModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
              
          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#storeoffer_key").val('');
    jQuery('#storeOfferModal').modal('show', {backdrop: 'static'});
  }
}