var pagenumber = 0;
var inprocess = null;
function listTerminal(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"terminal/listTerminal",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listTerminal tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'tb_terminal.terminal_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listTerminal(0,column,order);

function deleteTerminal($this)
{
  var flag=confirm('Do you want to delete this terminal?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"terminal/deleteTerminal",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listTerminal(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listTerminal th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listTerminal th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listTerminal th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listTerminal(0,column,order);
  }
});


function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();  
  listTerminal(0,column,order);  
}

$(document).ready(function()
{
    $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {  
      $("#clear").remove(); 
      listTerminal(0,column,order);     
    }  
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $.validator.addMethod(
          "NoSpace",
          function(value, element) {
              return $.trim(value);
          },
          "No white space allowed."
  );
  $("#terminalForm").validate({
        rules: {
          airport_id: {
                required: true,
            },
          terminal_name: {
              required: true,
              NoSpace: true,
              maxlength: 100,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          airport_id: "Please select airport.",
          terminal_name: {
              required: "Please enter terminal.",
              maxlength: "Terminal name is too long.",
          },
        }
    });

    $("#terminalForm").ajaxForm({
        dataType: 'json',
        beforeSend: function() 
        {
          //$("#airportFaciForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {
            
        },
        success: function(json) 
        {
          if(json.status == "success"){
            jQuery('#terminalModal').modal('hide');
            if(json.action == "add"){
              listTerminal(0,'terminal_id','DESC'); 
            }else{
              listTerminal(pagenumber,ordercolumn,orderby);  
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          //$("#airportFaciForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json) 
        {
            //$("#airportFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            //$("#airportFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });
});

function getTerminal($this) {
  $("#terminalForm")[0].reset();
  $("#terminalForm .form-control").removeClass('error-block');
  $("#terminalForm span.error-block").css("display","none");
  $("#ImageHtml").html("");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"terminal/getTerminal",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#terminal_key").val($($this).attr("data-row-id"));
            if (json.terminalData) {
                
              var count=Object.keys(json.terminalData).length;
              $("#terminal_name").val(json.terminalData.terminal_name);
              $("#terminal_desc").val(json.terminalData.terminal_desc);
              $("#airport_id").val(json.terminalData.airport_id);

               // ---------- multiple images code here -----------
                 if(json.terminalData.terminal_images != ""){
                  var imgarr = json.terminalData.terminal_images.split(",");
                  $("#ImageHtml").html("");
                  $(".fac_one").hide();
                  var icnt=1;
                  $("#ImageHtml").append('<div class="clearfix"></div>');
                  $.each(imgarr, function(index, value){
                       $("#ImageHtml").append('<div class="col-md-6">\n\
                      <div class="form-group">\n\
                        <label class="control-label" for="terminal_thumb">Terminal Image</label>\n\
                         &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
                        <br/>\n\
                        <img src="'+BASEURL+'images/terminal/'+value+'" id="terminal_thumb_img" width="120" height="120" style="padding: 7px;">\n\
                        <input type="hidden" name="terminal_thumb_old[]" value="'+value+'">\n\
                      </div>\n\
                    </div>');

                       $("#imgcnt").val(icnt++);
                  });
                }
            }
            jQuery('#terminalModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
              
          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#terminal_key").val('');
    jQuery('#terminalModal').modal('show', {backdrop: 'static'});
  }
}

//-------  multiple facility images -------------
var icnt=1;
function addFacImage()
{
  var imgcnt = $("#imgcnt").val();
  // alert(imgcnt);
 /* if(imgcnt < 6)
  {*/
    $("#ImageHtml").append('<div class="col-md-6 terminalImages">\n\
      <div class="form-group">\n\
        <label class="control-label" for="terminal_thumb">Terminal Image</label> (Size: 300X300)\n\
        &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
        <br/>\n\
        <img src="" id="terminal_thumb_img" width="120px" style="padding: 7px;display:none">\n\
        <input type="file" placeholder="Terminal Image" name="terminal_thumb[]" class="" autocomplete="off">\n\
        Upload upto 5 Mb image.\n\
      </div>\n\
    </div>');
    var newcnt = parseInt(imgcnt) + 1;
  /*  $("#imgcnt").val(newcnt);
  }
  else
  {
    alert('You can upload max 6 image');
  }*/
}

$("body").on("click", ".remove_field", function (e) {

  var len = $('.terminalImages').length;
  if(len == 0)
  {
      $("#ImageHtml").append('<div class="col-md-6 terminalImages">\n\
      <div class="form-group">\n\
        <label class="control-label" for="terminal_thumb">Terminal Image</label> (Size: 300X300)'+
        /*&nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\*/
        '<br/>\n\
        <img src="" id="terminal_thumb_img" width="120px" style="padding: 7px;display:none">\n\
        <input type="file" placeholder="Terminal Image" name="terminal_thumb[]" class="form-control" autocomplete="off">\n\
        Upload upto 5 Mb image.\n\
      </div>\n\
      </div>');    
  }
  $(this).parents("div .col-md-6").remove();            
});