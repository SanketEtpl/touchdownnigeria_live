var pagenumber = 0;
var inprocess = null;
function listAirportData(page,column,order){


  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"airportScript/listAirportData",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
          $("#listCSVData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
       if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'flight_number';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAirportData(0,column,order);

function updateStatus(id,$this)
{
   // var status = ;
   var status = $($this).val();
    
    $.ajax({

        type : "POST",
        url : BASEURL+"airportScript/updateFlightStatus",
        data :
        {
            'id' : id,
            'status' : status
        },
        success : function(json)
        {
            console.log(json);
            var obj = JSON.parse(json);
            if(obj.status == "success")
            {
              $('#checkAll').prop('checked',false);
                listAirportData(pagenumber,ordercolumn,orderby);
                toastr.success(obj.msg,"Success:");
            }
            else
            {
                toastr.error(obj.msg,"Error:");
                if(obj.status == "logout")
                {
                    window.location=BASEURL+"/logout";
                }
            }
        }
    });

}

function changeFlightStatus($this)
{

    var select = [];
   
    $('input[name="airportData"]:checked').each(function() {
        select.push(this.value);
    });

    
    if(select.length > 0)
    {

        $.ajax({

            type : "POST",
            url : BASEURL+"airportScript/updateFlightStatus",
            data :
            {
                'id' : select,
                'status' : $($this).val()
            },
            success : function(json)
            {
                var obj = JSON.parse(json);
                if(obj.status == "success")
                {
                    $('#checkAll').prop('checked',false);
                    listAirportData(pagenumber,ordercolumn,orderby);
                    toastr.success(obj.msg,"Success:");
                }
                else
                {
                    toastr.error(obj.msg,"Error:");
                    if(obj.status == "logout")
                    {
                        window.location=BASEURL+"/logout";
                    }
                }
            }

        });
    }
    else
    {
        alert('Please select at lease 1 airport to change status')    ;
    }

}




function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listAirportData(0,column,order); 
}





$(document).ready(function(){
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listAirportData(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
	$('.csvAlert').show();
    $('#uploadData').click(function(){

        if("" == $('#fileToUpload').val())
        {
            alert('Please select file to upload');
        }
        else
        {
            $("#uploadCSVScript").submit();
        }

    });


    setTimeout(function(){

        $('.csvAlert').hide();
    },2000);

    $('#checkAll').click(function(){

      if ($('#checkAll').is(':checked')) {

        $("input:checkbox").prop("checked", true);

        $(this).prop("checked", true);

      }
      else
      {
          $("input:checkbox").prop("checked", false);

          $(this).prop("checked", false);
      }

    });

});