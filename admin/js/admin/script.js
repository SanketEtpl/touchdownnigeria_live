// $("#uploadCSVScript").ajaxForm({
//     dataType: 'json',
//     beforeSend: function()
//     {
//         //$("#airportForm").find('[type="submit"]')
//         //$("#save_btn").toggleClass('sending').blur();
//     },
//     uploadProgress: function(event, position, total, percentComplete)
//     {
//
//     },
//     success: function(json)
//     {
//         console.log(json);
//     },
//     complete: function(json)
//     {
//         //$("#airportForm").find('[type="submit"]')
//         //$("#save_btn").removeClass('sending').blur();
//     },
//     error: function()
//     {
//         //$("#airportForm").find('[type="submit"]')
//         // $("#save_btn").removeClass('sending').blur();
//         // toastr.error("Something went wrong.","Error:");
//     }
// });

var pagenumber = 0;
var inprocess = null;
function listCSVFile(page,column,order){


  pagenumber = page;
  if(inprocess){
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"script/listCSVFile",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
          $("#listCSVData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
       if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listCSVFile(0,column,order);


function deleteCSVFile(id)
{
    var flag=confirm('Do you want to delete this file?');
    if(!flag)
    {
      toastr.warning("You have cancelled your request.","Info:");
      return false;
    }

    if(id)
    {
        $.ajax({
              type: "POST",
              dataType: "json",
              url: BASEURL+"script/deleteCSVFile",
              data: {
                'id' : id
              },
          }).success(function (json) {
          if(json.status == "success"){
                listCSVFile(pagenumber,ordercolumn,orderby);
                toastr.success(json.msg,"Success:");
              }else{
                toastr.error(json.msg,"Error:");
                if(json.status == "logout"){
              window.location=BASEURL+"/logout";
              }
              }
          });
    }


}



function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listCSVFile(0,column,order); 
}

$(document).ready(function(){
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listCSVFile(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
    
    $('.csvAlert').show();
    $('#uploadData').click(function(){

        if("" == $('#fileToUpload').val())
        {
            alert('Please select file to upload');
        }
        else
        {
            $("#uploadCSVScript").submit();
        }

    });


    setTimeout(function(){

        $('.csvAlert').hide();
    },2000);




    $('#checkAirline').click(function(){

      if ($('#checkAirline').is(':checked')) {

        $("input:checkbox").prop("checked", true);

        $(this).prop("checked", true);

      }
      else
      {
          $("input:checkbox").prop("checked", false);

          $(this).prop("checked", false);
      }

    });


    $('#deleteAirline').click(function(){

        //alert('ddddddd');
        var select = [];
   
      $('input[name="airlineData"]:checked').each(function() {
          select.push(this.value);
      });

      console.log(select);

      if(select.length > 0)
      {
         
          $.ajax({

            type : "POST",
            url : BASEURL+"script/deleteMultipleAirlineFile",
            data :
            {
                'id' : select
                
            },
            success : function(json)
            {
                var obj = JSON.parse(json);
                if(obj.status == "success")
                {
                    listCSVFile(pagenumber,ordercolumn,orderby);
                    toastr.success(obj.msg,"Success:");
                }
                else
                {
                    toastr.error(obj.msg,"Error:");
                    if(obj.status == "logout")
                    {
                        window.location=BASEURL+"/logout";
                    }
                }

            }

        });

      }
      else
      {
          alert('Please select at least 1 csv file');
      }

    });


});
