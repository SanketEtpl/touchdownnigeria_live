var pagenumber = 0;
var inprocess = null;
function listHomescreen(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"homescreen/listHomescreen",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listHomescreen tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
      if(json.status == "logout"){
      window.location=BASEURL+"/logout";
      }
  });
}

var column = 'id';
var order = 'ASC';
var ordercolumn = column;
var orderby = order;
listHomescreen(0,column,order);

function deleteHomescreen($this)
{
  var flag=confirm('Do you want to delete this image?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"homescreen/deleteHomescreen",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listHomescreen(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listHomescreen th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listHomescreen th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listHomescreen th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listHomescreen(0,column,order);
  }
});

$(document).ready(function()
{
$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#homescreenForm").validate({
        rules: {
          airport_id: {
                required: true,
            },
          facility_name: {
              required: true,
              NoSpace: true,
              maxlength: 100,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          airport_id: "Please select airport.",
          facility_name: {
              required: "Please enter facility.",
              maxlength: "Facility name is too long.",
          },
        }
    });

    $("#homescreenForm").ajaxForm({
        dataType: 'json',
        beforeSend: function() 
        {
          $("#save_btn").toggleClass('sending').blur();
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {
            
        },
        success: function(json) 
        {
          if(json.status == "success"){
            jQuery('#homescreenModal').modal('hide');
            if(json.action == "add"){
              listHomescreen(0,'facility_name','ASC'); 
            }else{
              listHomescreen(pagenumber,ordercolumn,orderby);  
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json) 
        {
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });
});

function getHomescreen($this) {
  $("#homescreenForm")[0].reset();
  $("#homescreenForm .form-control").removeClass('error-block');
  $("#homescreenForm span.error-block").css("display","none");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"homescreen/getHomescreen",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json){
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
            $("#homescreen_key").val($($this).attr("data-row-id"));
            if (json.airportFaciData) {
                
              var count=Object.keys(json.airportFaciData).length;
              if (json.airportFaciData.image!="") {
                $("#image_thumb").attr('src',BASEURL+'images/homescreen/'+json.airportFaciData.image);  
              }
              else
              {
                $("#image_thumb").attr('src',BASEURL+'images/no-image.png');
              }
              //$("#airport_id").val(json.airportFaciData.airport_id);
            }
            jQuery('#homescreenModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
              
          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{
    $("#action").html('Add');
    $("#homescreen_key").val('');
    jQuery('#homescreenModal').modal('show', {backdrop: 'static'});
  }
}