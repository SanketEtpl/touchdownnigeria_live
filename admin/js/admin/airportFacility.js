var pagenumber = 0;
var inprocess = null;

CKEDITOR.replace('facility_desc');

var editorElement = CKEDITOR.document.getById( 'facility_desc' );
CKEDITOR.instances.facility_desc.on('change', function() {
         $("#facility_desc").val(CKEDITOR.instances['facility_desc'].getData());
    //alert(CKEDITOR.instances['facility_desc'].getData());
      });




function listAirportFacility(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"facility/listAirportFacility",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listAirportFacility tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
      if(json.status == "logout"){
      window.location=BASEURL+"/logout";
      }
  });
}

var column = 'facility_id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAirportFacility(0,column,order);

function deleteAirportFacility($this)
{
  var flag=confirm('Do you want to delete this facility?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"facility/deleteAirportFacility",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listAirportFacility(pagenumber,ordercolumn,orderby);
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}

$("#listAirportFacility th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAirportFacility th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAirportFacility th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAirportFacility(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listAirportFacility(0,column,order); 
}

$(document).ready(function()
{

    $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listAirportFacility(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
$.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#airportFaciForm").validate({
        rules: {
          airport_id: {
                required: true,
            },
          /*terminal_id: {
              required: true,
          },*/
          facility_name: {
              required: true,
              NoSpace: true,
              maxlength: 100,
          },
          facility_desc: {
              required: true,
              NoSpace: true,
              maxlength: 300,
          },
        },
        errorClass: "error-block",
        errorElement: "span",
        messages: {
          airport_id: "Please select airport.",
          //terminal_id: "Please select terminal.",
          facility_name: {
              required: "Please enter facility.",
              maxlength: "Facility name is too long.",
          },
          facility_desc: {
              required: "Please enter facility description.",
              maxlength: "Facility description is too long.",
          },
        }
    });

    $("#airportFaciForm").ajaxForm({
        dataType: 'json',
        beforeSend: function() 
        {
          //$("#airportFaciForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();

          var data = CKEDITOR.instances['facility_desc'].getData();

          //$('#faciDescError').text('Please enter description');

          if("" == data)
          {
            $('#faciDescError').text('Please enter description');
            return false;
          }
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {
            
        },
        success: function(json) 
        {
          if(json.status == "success"){
            jQuery('#airportFacilityModal').modal('hide');
            if(json.action == "add"){
              listAirportFacility(0,'facility_id','DESC'); 
            }else{
              listAirportFacility(pagenumber,ordercolumn,orderby);  
            }
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
          //$("#airportFaciForm").find('[type="submit"]')
          $("#save_btn").removeClass('sending').blur();
        },
        complete: function(json) 
        {
            //$("#airportFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
        },
        error: function()
        {
            //$("#airportFaciForm").find('[type="submit"]')
            $("#save_btn").removeClass('sending').blur();
            toastr.error("Something went wrong.","Error:");
        }
    });
});

function getAirportFacility($this) {
  $("#airportFaciForm")[0].reset();

  CKEDITOR.instances['facility_desc'].setData("");

  /*if($('.ImageHtml').has('div'))
  {
    console.log('found');
  }
  else
  {
    console.log('not found');
  }*/



  var mySelect = $('#terminal_id');
              mySelect.html('');
              mySelect.append(
                      $('<option></option>').val('').html('Select Terminal')
                  );
  $("#airportFaciForm .form-control").removeClass('error-block');
  $("#airportFaciForm span.error-block").css("display","none");

  /*console.log($($this));

  return false;*/

  if($($this).attr("data-row-id")){
    $('.fac_one').css('display','none');
    $('#ImageHtml').show();
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"Facility/getAirportFacility",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json){

        console.log(json);
          var alert_type='danger';
          if(json.status == "success"){

           getTerminal(json.airportFaciData.terminal_id,json.airportFaciData.airport_id);
            $("#terminal_id").val(json.airportFaciData.terminal_id);


            $("#action").html('Edit');
            $("#airportfacility_key").val($($this).attr("data-row-id"));
            if (json.airportFaciData) {
                
              var count=Object.keys(json.airportFaciData).length;
              $("#facility_name").val(json.airportFaciData.facility_name);
              $("#facility_desc").val(json.airportFaciData.facility_desc);
              $("#airport_id").val(json.airportFaciData.airport_id);
              // ---------- multiple images code here -----------
                 if(json.airportFaciData.facility_images != ""){
            var imgarr = json.airportFaciData.facility_images.split(",");
            $("#ImageHtml").html("");
            $(".fac_one").hide();
            var icnt=1;
            $("#ImageHtml").append('<div class="clearfix"></div>');
            $.each(imgarr, function(index, value){
                 $("#ImageHtml").append('<div class="col-md-6">\n\
                <div class="form-group">\n\
                  <label class="control-label" for="facility_thumb">Facility Image</label>\n\
                   &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
                  <br/>\n\
                  <img src="'+BASEURL+'images/facility/'+value+'" id="facility_thumb_img" width="120" height="120" style="padding: 7px;">\n\
                  <input type="hidden" name="facility_thumb_old[]" value="'+value+'">\n\
                </div>\n\
              </div>');

                 $("#imgcnt").val(icnt++);
            });
          }
          else
          {
              $("#ImageHtml").html("");
              $('.fac_one').show();
          }

            }
            jQuery('#airportFacilityModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
              
          }else if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      });
  }else{

    $('.fac_one').css('display','block');
    $('#ImageHtml').html("");    
    $("#action").html('Add');
    $("#airportfacility_key").val('');
    //getTerminal();
    jQuery('#airportFacilityModal').modal('show', {backdrop: 'static'});
  }
}

//-------  multiple facility images -------------
var icnt=1;
function addFacImage(){
  $('#ImageHtml').show();
  var imgcnt = $("#imgcnt").val();
  /*if(imgcnt < 5){*/
  $("#ImageHtml").append('<div class="col-md-6">\n\
    <div class="form-group">\n\
      <label class="control-label" for="facility_thumb">Facility Image</label> (Size: 300X300)\n\
      &nbsp; <a href="javascript:void(0)"><label class="remove_field">Remove</label></a>\n\
      <br/>\n\
      <img src="" id="facility_thumb_img" width="120px" style="padding: 7px;display:none">\n\
      <input type="file" placeholder="Store Image" name="facility_thumb[]" class="" autocomplete="off">\n\
      Upload upto 5 Mb image.\n\
    </div>\n\
  </div>');
  var newcnt = parseInt(imgcnt) + 1;
  $("#imgcnt").val(newcnt);
/*}else{

}*/
}

function getTerminal(terminal_id,airport_id) {
  // alert(terminal_id);
  // alert(airport_id);
    if($('#airport_id').val() || airport_id)
    {
      $.ajax({
            type: "POST",
            dataType: "json",
            url: BASEURL+"Facility/getTerminal/main",
            data: {"airport_id":$('#airport_id').val()+airport_id},
        }).success(function (json) {
        if(json.status == "success"){
              
              var myOptions =json.terminalData;
              var mySelect = $('#terminal_id');
              mySelect.html('');
              mySelect.append(
                      $('<option></option>').val('').html('Select Terminal')
                  );
              $.each(myOptions, function(val, text) {
                if(val==terminal_id)
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(true)
                  );
                }
                else
                {
                  mySelect.append(
                      $('<option></option>').val(val).html(text).selected(false)
                  ); 
                }
              });
              //listTerminalAdmins(pagenumber,ordercolumn,orderby);
              //toastr.success(json.msg,"Success:");
            }else{
              toastr.error(json.msg,"Error:");
          if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
            }
        });
    }
    else
    {
      toastr.error("Please select airport.","Error:");
    }
}

$("body").on("click", ".remove_field", function (e) {
  $(this).parents("div .col-md-6").remove();            
    });