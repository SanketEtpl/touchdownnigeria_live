var pagenumber = 0;
var inprocess = null;
function listUsers(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"users/listUsers",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
      inprocess = null;
      if(json.status == "success"){
        $("#listUsers tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
      if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
  });
}

//function getUser($this){
//  $("#userForm")[0].reset();
//  $("#userForm .form-control").removeClass('error-block');
//  $("#userForm span.error-block").hide();
//  if($($this).attr("data-row-id")){
//    $.ajax({
//          type: "POST",
//          dataType: "json",
//          url: BASEURL+"users/getUser",
//          data: {"key":$($this).attr("data-row-id")},
//      }).success(function (json) {
//          if(json.status == "success"){
//            $("#action").html('Edit');
//            $("#user_key").val($($this).attr("data-row-id"));
//            $("#first_name").val(json.userData.first_name);
//            $("#last_name").val(json.userData.last_name);
//            $("#email_id").val(json.userData.email_id);
//            $("#phone").val(json.userData.phone);
//            jQuery('#userModal').modal('show', {backdrop: 'static'});
//            //toastr.success(json.msg,"Success:");
//          }else{
//              toastr.error(json.msg,"Error:");
//              if(json.status == "logout"){
//          window.location=BASEURL+"/logout";
//          }
//          }
//      });
//  }else{
//    $("#action").html('Add');
//    $("#user_key").val('');
//    jQuery('#userModal').modal('show', {backdrop: 'static'});
//  }
//}

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listUsers(0,column,order);

$("#listUsers th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listUsers th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listUsers th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listUsers(0,column,order);
  }
});

function clearSearch() 
{ 
  $("#txt_search").val("");
  $("#clear").remove();   
  listUsers(0,column,order); 
}


$(document).ready(function()
{
  $("#txt_search").on("keyup",function(event) {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#txt_search").parent().append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "") {
      $("#clear").remove(); 
      listUsers(0,column,order);        
    } 
    
  }); 
  $("#txt_search").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $.validator.addMethod(
        "NoSpace",
        function(value, element) {
            return $.trim(value);
        },
        "No white space allowed."
);
  $("#userForm").validate({
      rules: {
        first_name: {
              required: true,
              NoSpace: true,
              maxlength: 50,
          },
        last_name: {
              required: true,
              maxlength: 50,
              NoSpace: true,
          },
        email_id: {
              required: true,
              email: true,
              maxlength: 50,
              NoSpace: true,
          },
        phone: {
              required: true,
              maxlength: 15,
              number: true,
              NoSpace: true,
          },
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: {
                required: "Please enter your first name.",
                maxlength: "First name is too long."
            },
        last_name: {
                required: "Please enter your last name.",
                maxlength: "Last name is too long."
            },
        email_id: {
                required: "Please enter your email id.",
                email : "Please enter valid email id.",
                maxlength: "Email id is too long."
            },
        phone: {
                required: "Please enter your phone.",
                maxlength: "Phone is too long.",
                number: "Please enter valid phone."
            },
      }
  });
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        //$("#userForm").find('[type="submit"]')
        $("#save_btn").toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listUsers(0,'user','DESC'); 
        }else{
          listUsers(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
        if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
      }
      //$("#userForm").find('[type="submit"]')
      $("#save_btn").removeClass('sending').blur();
    },
    complete: function(json) 
    {
        //$("#userForm").find('[type="submit"]')
        $("#save_btn").removeClass('sending').blur();
    },
    error: function()
    {
        //$("#userForm").find('[type="submit"]')
        $("#save_btn").removeClass('sending').blur();
        toastr.error("Something went wrong.","Error:");
    }
  });
});

function deleteUser($this)
{
  var flag=confirm('Do you want to delete this user?');
  if(!flag)
  {
    toastr.warning("You have cancelled your request.","Info:");
    return false;
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"users/deleteUser",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
            listUsers(pagenumber,ordercolumn,orderby);  
            toastr.success(json.msg,"Success:");
          }else{
            toastr.error(json.msg,"Error:");
            if(json.status == "logout"){
          window.location=BASEURL+"/logout";
          }
          }
      });
    }else{
  }
}