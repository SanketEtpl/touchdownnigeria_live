<?php
class Common_model extends CI_Model{
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

    function insert($table,$data)
    {
		$this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    function insert_batch($table,$data)
    {
        try {
    		$this->db->insert_batch($table,$data);
    		return true;
        }
        catch(Exception $e) {
          return false;
        }
    }

    function update($table,$where=array(),$data)
    {
        try {
            $this->db->update($table,$data,$where);
            if($this->db->affected_rows() == 0){
                return true;
            }
            return $this->db->affected_rows();
        }
        catch(Exception $e) {
          return false;
        }
    }

    function updateIncrement($table,$where=array(),$data)
    {
        foreach ($data AS $k => $v)
		{
			$this->db->set($k, $v, FALSE);
		}
        foreach ($where AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->update($table);
		return $this->db->affected_rows();
    }

    function update_batch($table,$where,$data)
    {
        try {
            $this->db->_protect_identifiers=true;
            $this->db->update_batch($table,$data,$where);
            if($this->db->affected_rows() == 0){
                return true;
            }
            return $this->db->affected_rows();
        }
        catch(Exception $e) {
          return false;
        }
	}

    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        return $this->db->affected_rows();
    }

    function deleteAll($table)
    {
        $this->db->from($table); 
        $this->db->truncate(); 
        
    }

    function delete_batch($table,$key,$ids=array())
    {
		$this->db->where_in($key,$ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }

    function select($sel,$table,$cond = array())
	{
        $this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
        $query = $this->db->get();
       // echo $this->db->last_query(); exit;

        return $query->result_array();
	}

	function selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
        foreach($join as $key => $val)
        {
            if(!empty($joinType) && $joinType[$key]!=""){
                $this->db->join($key, $val,$joinType[$key]);
            }else{
                $this->db->join($key, $val);
            }
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        // exit;

		return $query->result_array();
	}


    function getRowsPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array(),$perPage=PER_PAGE)
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val);
        }
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        $this->db->limit($perPage,($page*$perPage));
        $query = $this->db->get();

        return $query->result_array();
    }


    function getCount($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join=array())
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }

        foreach($join as $key => $val)
        {

            $this->db->join($key, $val);
        }
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


   function getCount_1($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join=array())
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }

        foreach($join as $key => $val)
        {

            $this->db->join($key, $val,"LEFT");
        }

        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result_array();
    }

    function getData($airport_id,$terminal_id)
    {

        if(0 == $terminal_id)
        {
            $sql = "SELECT * FROM `tbl_facility` JOIN `tb_terminal` ON `tb_terminal`.`terminal_id` = `tbl_facility`.`terminal_id` WHERE `tbl_facility`.`airport_id` = ".$airport_id." GROUP BY `tbl_facility`.`terminal_id` ORDER BY `facility_id` DESC";
           
        }
        else
        {
            $sql = "SELECT * FROM `tbl_facility` JOIN `tb_terminal` ON `tb_terminal`.`terminal_id` = `tbl_facility`.`terminal_id` WHERE `tbl_facility`.`airport_id` = ".$airport_id." and `tb_terminal`.`terminal_id`= ".$terminal_id." GROUP BY `tbl_facility`.`terminal_id` ORDER BY `facility_id` DESC";
       
        }
        
        $query = $this->db->query($sql);
        return $query->result_array();
        //print_r($query->result_array());
    }

    function select_where_in($sel,$table,$key,$ids = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->where_in($key,$ids);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;

        return $query->result_array();
    }

    function select_limit($sel,$table,$start,$end)
    {
        $this->db->select($sel,FALSE);
        $this->db->from($table);
        $this->db->limit($start,$end);
        $query = $this->db->get();

        return $query->result_array();
    }

    function truncate($table)
    {
        if($this->db->truncate($table))
        {
           return true;
        }
        else
        {
            return false;
        }
    }

    function update_where_in($table,$data,$key,$ids=array())
    {
        try {
            $this->db->set($data);
            $this->db->where_in($key,$ids);
            $this->db->update($table);
            if($this->db->affected_rows() == 0){
                return true;
            }
            return $this->db->affected_rows();

            //echo $this->db->last_query();
        }
        catch(Exception $e) {
          return false;
        }
    }

    function selectWhereIn($sel,$table,$where=NULL,$in=array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->where_in($where,$in);
        /*foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }*/
        $query = $this->db->get();
        return $query->result_array();
    }
    function getSubData($sel,$table1,$cond = array(),$field1,$table2,$cond1 = array(),$field2)
    {
        $where1 = '';

        if(count($cond) > 0)
        {
            $where.='and ';
            foreach ($cond as $key => $value) {
                $where.= $key.' = '.$value;
            }    
        }
        else
        {
            $where.="";
        }


        if(count($cond1) > 0)
        {
            foreach ($cond1 as $key => $value) {
                $where1.= $key.' = '.$value;
            }    
        }
        

        
         
        $query = $this->db->query("select * from $table1 where $field1 in (select $field2 from $table2 where $where1) $where");
        //$query = $this->db->get();
        //
        /*echo $this->db->last_query();

        exit();*/
        return $query->result_array();
    }



}
?>
