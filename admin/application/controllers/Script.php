<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Script extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('csvimport');
    $this->load->library('session');
    date_default_timezone_set("Africa/Lagos");
  }

  public function index()
  {
        // $this->load->view('header');
    $data = array();
    if($this->session->userdata('csvSuccess'))
    {
            //echo $this->session->userdata('csvSuccess');
      $data['status'] = 'success';
      $data['message'] = $this->session->userdata('csvSuccess');
    }
    if($this->session->userdata('csvError'))
    {
            //echo $this->session->userdata('csvError');
      $data['status'] = 'error';
      $data['message'] = $this->session->userdata('csvError');
      $this->session->unset_userdata('csvError');
      $this->load->view('script',$data);
    }
    else
    {

      $data['pageTitle'] = 'Airlines - TDN';

          /*print_r($dtRight);

          exit();*/
      //$dtRight=checkRight($role,1);
          if($this->session->userdata('csvSuccess'))
          {
            $this->session->unset_userdata('csvSuccess');
          }
          if($this->session->userdata('csvError'))
          {
            $this->session->unset_userdata('csvError');
          }
          $this->load->view('script',$data);
        }

        //$this->load->view('footer');
      }

      public function uploadFile()
      {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		
		// Check if image file is a actual image or fake image
        if($_FILES['fileToUpload']['type'] != 'text/csv' && $_FILES['fileToUpload']['type'] != 'text/comma-separated-values' && $_FILES['fileToUpload']['type'] != 'application/octet-stream' && $_FILES['fileToUpload']['type'] != 'text/plain')
        {
			
          $mimes = array('text/csv');
          $uploadOk = 0;
            /*$this->session->set_flashdata('csvError','Sorry, mime type not allowed.');
            redirect('/airportScript');*/
            $this->session->set_userdata('csvError','Sorry, file type not allowed.');
            redirect('/script');


          }

          if ($_FILES["fileToUpload"]["size"] > 500000)
          {
            $this->session->set_flashdata('csvError','Sorry, your file is too large.');
            //echo "Sorry, your file is too large.";
            redirect('/script');
            $uploadOk = 0;
          }
          if ($uploadOk == 0)
          {
            echo "Sorry, your file was not uploaded.";
            redirect('/script');
        // if everything is ok, try to upload file
          }
          else
          {
            if($_FILES['fileToUpload']['type'] == 'text/csv' || $_FILES['fileToUpload']['type'] == 'text/comma-separated-values' || $_FILES['fileToUpload']['type'] == 'application/octet-stream' || $_FILES['fileToUpload']['type'] == 'text/plain')
            {
              $flag = 0;
              $fileName = "flightData_".date('Y-m-d h:i:s');
              $target_file = $target_dir.$fileName.".csv";

              if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
              {
                error_reporting(E_ALL);
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                $file_data = $this->csvimport->get_array("uploads/".$fileName.".csv");

                  if(count($file_data) > 0)  // if csv file is empty
                  {
                    $data = array();
                      // echo "<pre>";print_r($file_data);die;

                      // foreach($file_data as $key => $data)
                      // {
                      //   echo "...".$data['Airline'].".";
                      //   if($data['Airline'] == "")
                      //   {
                      //     echo "empty";
                      //   }else
                      //   {
                      //     echo "not";
                      //   }


                      // }
                      // die;


                    foreach($file_data as $key => $data)
                    {

                      if(("" != trim($data['Flight Number'])) && ("" != trim($data['Airline'])) && 
                        ("" != trim($data['Weekdays'])) && ("" != trim($data['Departure From'])) && 
                        ("" != trim($data['Arrival At'])) && ("" != trim($data['Departure Time'])) && 
                        ("" != trim($data['Arrival Time'])))
                      {
                       $val[] = array(
                        'flight_number' => $data['Flight Number'],
                        'airline' => $data['Airline'],
                        'weekdays' => $data['Weekdays'],
                        'departure_from' => $data['Departure From'],
                        'arrival_at' => $data['Arrival At'],
                              //'journey_hrs' => $data['Journey hrs'],
                        'departure_time' => $data['Departure Time'],
                        'arrival_time' => $data['Arrival Time'],
                        'terminal' => $data['Terminal'],
                        'no_of_stops' => $data['No Of Stops'],
                             // 'created_date' => date('Y-m-d h:i:s'),
                        'created_date' => date('Y-m-d',strtotime($data['Date'])),
                        'valid_upto' => date('Y-m-d', strtotime('+1 months',strtotime($data['Date'])))
                        );
                     }
                     else
                     {
                       $flag = 1;
                           // break;
                     }
                   }


                   if(empty($val))
                   {
                    $this->session->set_flashdata('csvError','Data could not be inserted');
                    redirect('/script');
                  }else
                  {
                   $csvData = $this->common->select('count(*)',TB_AIRLINE_CSV_UPLOAD);
                   if($csvData[0]['count(*)'] > 0)
                   {
                    $this->common->truncate(TB_AIRLINE_CSV_UPLOAD);
                  }                 
                  $insert = $this->common->insert_batch(TB_AIRLINE_CSV_UPLOAD,$val);
                  if($insert)
                  {
                   $data = array('name' => "flightData_".date('Y-m-d h:i:s').".csv",'created_date' => date('Y-m-d h:i:s'));
                   $upload = $this->common->insert(TB_CSV_AIRLINE,$data);
                   $this->session->set_userdata('csvSuccess','Data inserted sucessfully');
                   redirect('/script');
                 }
                 else
                 {
                            //$this->session->set_flashdata('uploadCSVData','error');
                  $this->session->set_flashdata('csvError','Data could not be inserted');
                  redirect('/script');
                            //echo "Problem in Insertion";
                }
              }                     
            }
            else
            {
              $this->session->set_userdata('csvError','File is empty or wrong format');
              redirect('/script');
            }

          }
          else
          {
            $this->session->set_flashdata('csvError','Sorry, there was an error uploading your file.');
            redirect('/script');
                //echo "Sorry, there was an error uploading your file.";
          }
        }
        else
        {


          $mimes = array('text/csv');
          $uploadOk = 0;
              /*$this->session->set_flashdata('csvError','Sorry, mime type not allowed.');
              redirect('/airportScript');*/
              $this->session->set_userdata('csvError','Sorry, file type not allowed.');
              redirect('/script');
            }
          }

        }


        public function updateAirlineData()
        {
          $validUpto = $this->common->select_limit('valid_upto',TB_AIRLINE_CSV_UPLOAD,1,0);

          if(date('Y-m-d') > $validUpto[0]['valid_upto'])
          {
            $data = $this->common->select('*',TB_AIRLINE_CSV_UPLOAD);
            $insertData = array();

            foreach($data as $val)
            {
              $insertData[] = array(
                'id' => $val['id'],
                'flight_number' => $val['flight_number'],
                'weekdays' => $val['weekdays'],
                'departure_from' => $val['departure_from'],
                'arrival_at' => $val['arrival_at'],
                'departure_time' => $val['departure_time'],
                'arrival_time' => $val['arrival_time'],
                'terminal' => $val['terminal'],
                'no_of_stops' => $val['no_of_stops'],
                'created_date' => date('Y-m-d h:i:s'),
                'valid_upto' => date('Y-m-d', strtotime('+1 months'))
                );
            }

            if($this->common->truncate(TB_AIRLINE_CSV_UPLOAD))
            {
              $this->common->insert_batch(TB_AIRLINE_CSV_UPLOAD,$insertData);
            }

          }
          else
          {
            echo "small";
          }

        //print_r($validUpto);


        }

        public function listCSVFile(){
          if(is_ajax_request())
          {
           if(is_user_logged_in()){
            $postData = $this->input->post();

            $arrayColumn = array("airport"=>"id","fname"=>"name","date" =>"date");
            $arrayStatus["is_active"] = array();
            $arrayColumnOrder = array("ASC","asc","DESC","desc");
            $where=array();
            $result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'date','id',TB_CSV_AIRLINE,'*','listCSVFile',$where);


            $rows = '';
            if(!empty($result['rows']))
            {
             $i=1;
             foreach ($result['rows'] as $csvData) {
              $id = $this->encrypt->encode($csvData['id']);
              $rows .= '<tr id="'.$id.'">
              <td class="text-left"><input type="checkbox" name="airlineData" value="'.$csvData['id'].'"></td>
              <td class="text-left"><a href="'.base_url().'script/downloadScript/'.$csvData['id'].'">'.$csvData['name'].'</a></td>
              <td class="text-left">'.$csvData['created_date'].'</td>';

              $rows .= '<td><a data-id="'.$i.'" data-row-id="'.$csvData['id'].'" class="" onclick="deleteCSVFile('.$csvData['id'].')" title="Delete" href="javascript:void(0)">
              <i class="fa fa-fw fa-close"></i></a></td>';

              $rows .= '</tr>';
            }
          }
          else
          {
           $rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
         }
         $data["rows"] = $rows;

         $data["pagelinks"] = $result["pagelinks"];
         $data["entries"] = $result['entries'];
         $data["status"] = "success";

         echo json_encode($data);

       }else{
        echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
      }
    }
  }


  public function downloadScript($id)
  {
    if($id)
    {
      $this->load->helper('download');
      $fileInfo = $this->common->select('name',TB_CSV_AIRLINE,array('id' => $id));

      $file = 'uploads/'.$fileInfo[0]['name'];

      force_download($file, NULL);

      redirect('/script');
    }

  }


  public function deleteCSVFile()
  {
    $id = $this->input->post('id');
    if($id)
    {
            //$this->load->helper("file");
          //  unlink('uploads/data.xlsx');

      $file = $this->common->select('*',TB_CSV_AIRLINE,array('id' => $id));

      if(count($file) > 0)
      {
        $deleteFile = $this->common->delete(TB_CSV_AIRLINE,array('id' => $id));
        unlink('uploads/'.$file[0]['name']);
        echo json_encode(array("status" => "success","msg"=>"File deleted sucessfully"));
      }
      else {
        echo json_encode(array("status" => "error","msg"=>"Something went wrong"));
      }

    }

  }


  public function deleteMultipleAirlineFile()
  {
    $id = $this->input->post('id');
    $status = 0;

    foreach($id as $val)
    {
      $file = $this->common->select('*',TB_CSV_AIRLINE,array('id' => $val));

      if(count($file) > 0)
      {
        $deleteFile = $this->common->delete(TB_CSV_AIRLINE,array('id' => $val));
        unlink('uploads/'.$file[0]['name']);
        $status = 1;
      }
    }

    if(1 == $status)
    {
      echo json_encode(array("status" => "success","msg"=>"File deleted sucessfully"));
    }
    else
    {
      echo json_encode(array("status" => "error","msg"=>"Something went wrong"));
    }
  }





}
