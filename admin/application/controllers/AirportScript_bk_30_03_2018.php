<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AirportScript extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('csvimport');
        $this->load->library('session');
		//Do your magic here
	}

	public function index()
	{

		$data = array();
        if($this->session->userdata('csvSuccess'))
        {
            //echo $this->session->userdata('csvSuccess');
            $data['status'] = 'success';
            $data['message'] = $this->session->userdata('csvSuccess');
        }
        if($this->session->userdata('csvError'))
        {
            //echo $this->session->userdata('csvError');
            $data['status'] = 'error';
            $data['message'] = $this->session->userdata('csvError');
        }
        else
        {
            if($this->session->userdata('csvSuccess'))
            {
                $this->session->unset_userdata('csvSuccess');
            }
            if($this->session->userdata('csvError'))
            {
                $this->session->unset_userdata('csvError');
            }

            $data['flight_status'] = $this->common->select('*',TB_FLIGHT_STATUS);

            $this->load->view('header');
      			$this->load->view('airportScript',$data);
      			$this->load->view('footer');
        }


		 
	}

	 public function uploadFile()
    {
        $target_dir = "uploads/airports/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"]))
        {



            $mimes = array('text/csv');

            if(!in_array($_FILES['fileToUpload']['type'],$mimes))
            {


                $uploadOk = 0;


                $this->session->set_flashdata('csvError','Sorry, mime type not allowed.');
                //die("Sorry, mime type not allowed");
            }

        }

        if ($_FILES["fileToUpload"]["size"] > 500000)
        {
          $this->session->set_flashdata('csvError','Sorry, your file is too large.');
            //echo "Sorry, your file is too large.";
            redirect('/script');
            $uploadOk = 0;
        }
        if ($uploadOk == 0)
        {
            echo "Sorry, your file was not uploaded.";
            redirect('/script');
        // if everything is ok, try to upload file
        }
        else
        {
          $target_file = $target_dir."airportData_".date('Y-m-d h:i:s').".csv";
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
            {
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                  $file_data = $this->csvimport->get_array("uploads/airports/airportData_".date('Y-m-d h:i:s').".csv");

                  if(count($file_data) > 0)  // if csv file is empty
                  {
                      $data = array();
                      foreach($file_data as $key => $data)
                      {
                         $val[] = array(
                              'flight_number' => $data['Flight Number'],
                              'airline' => $data['Airline'],
                              'weekdays' => $data['Weekdays'],
                              'departure_from' => $data['Departure From'],
                              'arrival_at' => $data['Arrival At'],
                              //'journey_hrs' => $data['Journey hrs'],
                              'schedule' => $data['Schedule'],
                              'terminal' => $data['Terminal'],
                              'fs_code' => $data['Fs Code'],
                              'status' => $data['Status'],
                              'airport_status' => $data['Airport Status'],
                              'created_date' => date('Y-m-d')
                         );
                      }

                      $csvData = $this->common->select('count(*)',TB_AIRPORT_CSV_UPLOAD);

                      if($csvData[0]['count(*)'] > 0)
                      {
                          $this->common->truncate(TB_AIRPORT_CSV_UPLOAD);

                      }
                      
                     $insert = $this->common->insert_batch(TB_AIRPORT_CSV_UPLOAD,$val);
                     if($insert)
                     {
	                     	$data = array('name' => "airportData_".date('Y-m-d h:i:s').".csv");
	                        $upload = $this->common->insert(TB_CSV_AIRPORT,$data);
	                        $this->session->set_userdata('csvSuccess','Data inserted sucessfully');
	                        redirect('/airportScript');
                     		
                     }
                     else
                     {
                     		$this->session->set_flashdata('csvError','Data could not be inserted');
                            redirect('/airportScript');
                     }
                      
                  }
                  else
                  {
                        $this->session->set_userdata('csvSuccess','Data inserted sucessfully');
                        redirect('/script');
                  }

            }
            else
            {
              $this->session->set_flashdata('csvError','Sorry, there was an error uploading your file.');
              redirect('/script');
                //echo "Sorry, there was an error uploading your file.";
            }
        }

    }

    public function listAirportData()
    {

        if(is_ajax_request())
      {
        if(is_user_logged_in()){
          $postData = $this->input->post();

          /*print_r($postData);

          exit();*/

          $arrayColumn = array("airport"=>"id","flight_number"=>"flight_number","airline" =>"airline","weekdays" =>"weekdays","departure_from" => "departure_from","arrival_at" => "arrival_at","schedule" => "schedule","terminal" => "terminal","status" => "status" ,"airport_status" => "airport_status");

          $arrayStatus["is_active"] = array();
          $arrayColumnOrder = array("ASC","asc","DESC","desc");
          $where=array();
          $result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'flight_number','id',TB_AIRPORT_CSV_UPLOAD,'*','listAirportData',$where);

          $status = $this->common->select('*',TB_FLIGHT_STATUS); 


          $rows = '';
          if(!empty($result['rows']))
          {
            $i=1;
            foreach ($result['rows'] as $key => $airportData) {
              $id = $this->encrypt->encode($airportData['id']);
              $rows .= '<tr id="'.$id.'">
                                <td class="text-left"><input type="checkbox" name="airportData" value="'.$airportData['id'].'"></td>
                                <td class="text-left">'.$airportData['flight_number'].'</td>
                                <td class="text-left">'.$airportData['airline'].'</td>
                                <td class="text-left">'.$airportData['weekdays'].'</td>
                                <td class="text-left">'.$airportData['departure_from'].'</td>
                                <td class="text-left">'.$airportData['arrival_at'].'</td>
                                <td class="text-left">'.$airportData['schedule'].'</td>
                                <td class="text-left">'.$airportData['terminal'].'</td>
                                <td class="text-left"><select name="" id="flightStatus_'.$key.'" class="form-control flightStatus" onchange="updateStatus('.$airportData['id'].',this)">';
                                foreach($status as $val){ 
                                  $rows.='<option value="'.$val["name"].'"';
                                  if($val["name"] == $airportData["status"])
                                  {
                                    $rows.=' selected>'.$val["name"].'</option>';  
                                  }
                                  else
                                  {
                                      $rows.='>'.$val["name"].'</option>'; 
                                  }
                                  
                                }
                                $rows.='</select></td>';
                                $rows.='<td class="text-left">'.$airportData['airport_status'].'</td>';

              $rows .= '</tr>';
            }
          }
          else
          {
            $rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
          }
          $data["rows"] = $rows;

          $data["pagelinks"] = $result["pagelinks"];
          $data["entries"] = $result['entries'];
          $data["status"] = "success";

          echo json_encode($data);

        }else{
          echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
        }
      }


    }

    public function updateFlightStatus()
    {
        if(is_ajax_request())
        {
          if(is_user_logged_in())
          {

            if("" != $this->input->post('id') && "" != $this->input->post('status'))
            {
                $data = array(

                    'status' => $this->input->post('status')
                );

                if($this->common->update_where_in(TB_AIRPORT_CSV_UPLOAD,$data,'id',$this->input->post('id')))
                {
                    //echo json_encode(array("status"=>"success","msg"=>"Status updated successfully")); exit;
                    echo json_encode(array("status" => "success","msg"=>"Status updated successfully"));
                }
                else
                {
                    echo json_encode(array("status"=>"error","msg"=>"problem in status updation")); exit;
                }
            }
            else
            {
                echo json_encode(array("status"=>"error","msg"=>"Something went wrong !!")); exit;
            }
          }
          else
          {
              echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
          }
        }
    }




}

/* End of file airportScript.php */
/* Location: .//tmp/fz3temp-1/airportScript.php */