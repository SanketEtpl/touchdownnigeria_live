<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homescreen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
    
    public function index()
    {
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$widthArr=array('image'=>90,'action'=>10);
			$data['pageTitle']="App background - TDN";
			$data['widthArr']=$widthArr;
			if($role==1)
			{
				$this->load->view('homescreen/listHomescreen',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
			
		}else{
			redirect("login");
			exit;
		}
	}
	
	function listHomescreen()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","image"=>"image");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'image','id',TBL_HOMESCREEN,'id,image','listHomescreen',$where,array());
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $image) {
						$image_id = $this->encrypt->encode($image['id']);
						$rows .= '<tr facility_id="'.$image_id.'">';
						if($image['image']!="")
						{
							$rows .= '<td class="text-left"><center><img width="30%" src="'.base_url().'images/homescreen/'.$image['image'].'"></center></td>';
						}
						else
						{
							$rows .= '<td class="text-left"><center><img width="30%" src="'.base_url().'images/no-image.png'.'"></center></td>';
						}
						
								
							$rows .= '<td class="text-left">';
								
									$rows .= '	<a data-id="'.$i.'" data-row-id="'.$image_id.'" class="" onclick="getHomescreen(this)" title="Edit" href="javascript:void(0)">
													<i class="fa fa-fw fa-edit"></i>
												</a>';
									$rows .= '    <a data-id="'.$i.'" data-row-id="'.$image_id.'" class="" onclick="deleteHomescreen(this)" title="Delete" href="javascript:void(0)">
													<i class="fa fa-fw fa-close"></i>
												</a>';
								$rows .= ' </td>';
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;	
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function getHomescreen()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportFaciData = $this->common->selectQuery("*",TBL_HOMESCREEN,array('id'=>$this->encrypt->decode($postData['key'])));
				if($airportFaciData){
					echo json_encode(array("status"=>"success","airportFaciData"=>$airportFaciData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities found.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function saveHomescreen()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr = array();
				$files=$_FILES['image'];
				if($files['name'])
				{
					$userfile_extn = explode(".", strtolower($files['name']));
					$ext=$userfile_extn[1];
					$allowed=array('gif','jpg','jpeg','png');
					if(!in_array($ext,$allowed))
					{
						echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"image"));	 exit;	
					}
					if($files['size'] > 5242880)//2097152
					{
						echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"image"));exit;		
					}
					$new_image_name=uniqid(rand(), true).time().'.'.$ext;
					$quality=40;
					$source=$files['tmp_name'];
					$destination='./images/homescreen/'.$new_image_name;
					$info = getimagesize($source);
					if ($info['mime'] == 'image/jpeg')
						$image = imagecreatefromjpeg($source); 
					elseif ($info['mime'] == 'image/gif')
						$image = imagecreatefromgif($source); 
					elseif ($info['mime'] == 'image/png')
						$image = imagecreatefrompng($source); 
					imagejpeg($image, $destination, $quality);
					$insertArr['image']=$new_image_name;
				}
				if($postData["homescreen_key"]){
					$insertcategory_id = $this->common->update(TBL_HOMESCREEN,array('id'=>$this->encrypt->decode($postData['homescreen_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Image has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}/*else{
					$insertcategory_id = $this->common->insert(TBL_HOMESCREEN,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Facility has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}*/
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function deleteHomescreen()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportFacilityData = $this->common->update(TBL_HOMESCREEN,array('id'=>$this->encrypt->decode($postData['key'])),array('image'=>''));
				if($airportFacilityData){
					echo json_encode(array("status"=>"success","msg"=>"Facility deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
}