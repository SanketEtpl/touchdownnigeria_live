<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Airports extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,1);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($editRight)/*|| $deleteRight*/
			{
				$widthArr=array('fname'=>20,'city'=>15,'email'=>20,'phone'=>15,'action'=>10);
			}
			else
			{
				$widthArr=array('fname'=>23,'city'=>17,'email'=>23,'phone'=>17);
			}
			$data['widthArr']=$widthArr;
			$data['pageTitle']='Airports - Airport management - TDN';
			if($accessRight)
			{
				$this->load->view('airports/listAirports',$data);
			}
			else
			{
				redirect("login");
				exit;
			}

		}else{
			redirect("login");
			exit;
		}
	}

	public function listAirports(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,1);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];

				$postData = $this->input->post();



				$arrayColumn = array("airport"=>"id","fname"=>"name","city"=>"city","email"=>"email","phone"=>"phone");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array();
				if($role!=1)
				{
					$where=array('id'=>get_user_data('air_port_id'));
				}
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','id',TB_AIRPORTS,'*','listAirports',$where,array(),'created_at');

				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $airport) {
						$airportId = $this->encrypt->encode($airport['id']);
						$rows .= '<tr id="'.$airportId.'">
	                            <td class="text-left">'.$airport['name'].'</td>
	                            <td class="text-left">'.$airport['city'].'</td>
	                            <td class="text-left">'.$airport['email'].'</td>
	                            <td class="text-left">'.$airport['phone'].'</td>';
						if($edit )/*|| $delete*/
						{
							$rows .= '<td class="text-left">';
							if($edit)
							{
								$rows .= '	<a data-id="'.$i.'" data-row-id="'.$airportId.'" class="" onclick="getAirport(this)" title="Edit" href="javascript:void(0)">
												<i class="fa fa-fw fa-edit"></i>
											</a>';
							}
							if($delete)
							{
								$rows .= '    <a data-id="'.$i.'" data-row-id="'.$airportId.'" class="" onclick="deleteAirport(this)" title="Delete" href="javascript:void(0)">
												<i class="fa fa-fw fa-close"></i>
											</a>';
							}
							$rows .= '</td>';
						}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function getAirport(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportData = $this->common->selectQuery("name AS airport_name,phone as contact_number,email as email_address,address,state,country,zipcode,bio,city,fs,iata,icao,latitude,longitude,interior,exterior",TB_AIRPORTS,array('id'=>$this->encrypt->decode($postData['key'])));
				// $airportData = $this->common->selectQuery("name AS airport_name,phone as contact_number,email as email_address,address,state,country,zipcode,bio,city,latitude,longitude,CONCAT('".base_url()."images/airport/interior/',interior) as interior,CONCAT('".base_url()."images/airport/exterior/',exterior) as exterior",TB_AIRPORTS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($airportData){
					echo json_encode(array("status"=>"success","airportData"=>$airportData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}

	}

	public function saveAirport(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				// $files1=$_FILES['interior'];
				// $files2=$_FILES['exterior'];
				$dateTime = date("Y-m-d H:i:s");
				$insertArr = array(
								'name'=>$postData["airport_name"],
								"phone"=>$postData["contact_number"],
								"email"=>$postData["email_address"],
								"address"=>$postData["address"],
								"city"=>$postData["city"],
								"state"=>$postData["state"],
								"country"=>$postData["country"],
								"zipcode"=>$postData["zipcode"],
								"interior"=>$postData["interior"],
								"exterior"=>$postData["exterior"],
								"fs"=>$postData["fs_code"],
								"iata"=>$postData["iata_code"],
								"icao"=>$postData["icao_code"],
								"latitude"=>$postData["latitude"],
								"longitude"=>$postData["longitude"],
								"bio"=>$postData["bio"],
								"updated_at" => $dateTime,
								);
				// if($files1['name'])
				// {
				// 	$userfile_extn = explode(".", strtolower($files1['name']));
				// 	$ext=$userfile_extn[1];
				// 	$allowed=array('gif','jpg','jpeg','png');
				// 	if(!in_array($ext,$allowed))
				// 	{
				// 		echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"interior"));	 exit;
				// 	}
				// 	if($files1['size'] > 5242880)//2097152
				// 	{
				// 		echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"interior"));exit;
				// 	}
				// 	$new_image_name=uniqid(rand(), true).time().'.'.$ext;
				// 	$quality=40;
				// 	$source=$files1['tmp_name'];
				// 	$destination='./images/airport/interior/'.$new_image_name;
				// 	$info = getimagesize($source);
				// 	if ($info['mime'] == 'image/jpeg')
				// 		$image = imagecreatefromjpeg($source);
				// 	elseif ($info['mime'] == 'image/gif')
				// 		$image = imagecreatefromgif($source);
				// 	elseif ($info['mime'] == 'image/png')
				// 		$image = imagecreatefrompng($source);
				// 	imagejpeg($image, $destination, $quality);
				// 	$insertArr['interior']=$new_image_name;
				// }
				// if($files2['name'])
				// {
				// 	$userfile_extn = explode(".", strtolower($files2['name']));
				// 	$ext=$userfile_extn[1];
				// 	$allowed=array('gif','jpg','jpeg','png');
				// 	if(!in_array($ext,$allowed))
				// 	{
				// 		echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"exterior"));	 exit;
				// 	}
				// 	if($files2['size'] > 5242880)//2097152
				// 	{
				// 		echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"exterior"));exit;
				// 	}
				// 	$new_image_name=uniqid(rand(), true).time().'.'.$ext;
				// 	$quality=40;
				// 	$source=$files2['tmp_name'];
				// 	$destination='./images/airport/exterior/'.$new_image_name;
				// 	$info = getimagesize($source);
				// 	if ($info['mime'] == 'image/jpeg')
				// 		$image = imagecreatefromjpeg($source);
				// 	elseif ($info['mime'] == 'image/gif')
				// 		$image = imagecreatefromgif($source);
				// 	elseif ($info['mime'] == 'image/png')
				// 		$image = imagecreatefrompng($source);
				// 	imagejpeg($image, $destination, $quality);
				// 	$insertArr['exterior']=$new_image_name;
				// }
				if($postData["airport_key"]){
					$insertId = $this->common->update(TB_AIRPORTS,array('id'=>$this->encrypt->decode($postData['airport_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Airport has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_AIRPORTS,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Airport has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteAirport()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airport_id=$this->encrypt->decode($postData['key']);
				$admin=$this->common->selectQuery("count(id) as cnt",TB_ADMIN,array('air_port_id'=>$airport_id));
				$store=$this->common->selectQuery("count(store_id) as cnt",TB_STORES,array('airport_id'=>$airport_id));
				$terminal=$this->common->selectQuery("count(terminal_id) as cnt",TB_TERMINAL,array('airport_id'=>$airport_id));
				$facility=$this->common->selectQuery("count(facility_id) as cnt",TB_FACILITY,array('airport_id'=>$airport_id));
				if($admin[0]['cnt'] || $store[0]['cnt'] || $terminal[0]['cnt'] || $facility[0]['cnt'])
				{
					echo json_encode(array("status"=>"error","msg"=>"Can not delete airport, there are airport admins,terminals,facilites or stores available under this airport.")); exit;
				}
				else
				{
					$subadminData = $this->common->delete(TB_AIRPORTS,array('id'=>$airport_id));
					if($subadminData){
						echo json_encode(array("status"=>"success","msg"=>"Airport deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function getaddress($lat,$lng)
	{
	   $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'';
	   $json = @file_get_contents($url);
	   $data=json_decode($json);
	   $status = $data->status;
	   $address=$data->results[0]->formatted_address;
	//   $arr=explode(',',$address);
	//	foreach($arr as $k => $a)
	//   {
	//		$arr1=array();
	//		$arr1=explode(' ',$a);
	//		foreach($arr1 as $k1 => $a1)
	//		{
	//			if($a1=='Unnamed')
	//			{
	//				unset($arr[$k]);
	//			}
	//		}
	//		//Unnamed
	//   }
	//   $address=implode(',',$arr);
	   if($status=="OK")
	   {
		 echo json_encode(array("status"=>"success","address"=>trim($address)));
	   }
	   else
	   {
		 echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
	   }
	}
	function getIata(){
	$curl_handle=curl_init();

	curl_setopt($curl_handle,CURLOPT_URL,"https://api.flightstats.com/flex/airports/rest/v1/json/countryCode/NG?appId=".FLIGHT_STAT_appId."&appKey=".FLIGHT_STAT_appKey);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	//curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	$dt=json_decode($buffer,'ASSOC');
	// echo "<pre>";print_r($dt['airports']);
  //
	// exit;
	$insertArr=array();
	foreach($dt['airports'] as $k => $v)
	{
		$insertArr=array(
						'fs'=>$v['fs'],
						'iata'=>$v['iata'],
						'icao'=>$v['icao'],
						'name'=>$v['name'],
						'city'=>$v['city'],
						'cityCode'=>$v['cityCode'],
						'countryCode'=>$v['countryCode'],
						'country'=>$v['countryName'],
						'region'=>$v['regionName'],
						'timezone'=>$v['timeZoneRegionName'],
						'latitude'=>$v['latitude'],
						'longitude'=>$v['longitude'],
		);
		$insertId = $this->common->insert(TB_AIRPORTS,$insertArr);
	}

	}
	function getAirlineCode(){
	$curl_handle=curl_init();

curl_setopt($curl_handle,CURLOPT_URL,"https://api.flightstats.com/flex/airlines/rest/v1/json/active?appId=".FLIGHT_STAT_appId."&appKey=".FLIGHT_STAT_appKey);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	//curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	//$dt=json_decode($buffer,'ASSOC');
	//echo "<pre>";print_r($dt);print_r($buffer);
	//die('hah');
	$dt=$this->common->update(TBL_AIRLINE_CODE,array("id"=>1),array('details'=>$buffer));
	}
	function getsl()
	{
		$address='Unnamed Road, Kaduna, Nigeria';
		$arr=explode(',',$address);
		foreach($arr as $k => $a)
	   {
			$arr1=array();
			$arr1=explode(' ',$a);
			foreach($arr1 as $k1 => $a1)
			{
				if($a1=='Unnamed')
				{
					unset($arr[$k]);
				}
			}
			//Unnamed
	   }
	   $address=implode(',',$arr);
	   echo trim($address);
	}
}
