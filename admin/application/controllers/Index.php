<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	function index()
    {
        if(is_user_logged_in())
		{
			//$whereuser=array();
            $whereairport=array();
            $whereadmin=array();
            if(get_user_data('role')==1)
            {
               //$whereuser=array('airport_id'=>get_user_data('air_port_id'));
                $whereadmin=array("role"=>"2");
				$wherestore=array("store_category_id"=>"1");
				$whererest=array("store_category_id"=>"2");
				$storeData = $this->common->selectQuery("count(store_id) as cnt",TB_STORES,$wherestore);
				$restData = $this->common->selectQuery("count(store_id) as cnt",TB_STORES,$whererest);

				$data["charts"] = array("donut"=>array(
							array("label"=> "Stores","value"=>$storeData[0]['cnt']),
							array("label"=> "Restaurants","value"=>$restData[0]['cnt']),
							));
            }
            else
            {
               $whereadmin=array('air_port_id'=>get_user_data('air_port_id'),"role"=>"3");
               $whereairport=array('id'=>get_user_data('air_port_id'));
			   //$wherestore=array("store_category_id"=>"1");
			   //$whererest=array("store_category_id"=>"2","airport_id");
               $terminalData = $this->common->selectQuery("count(terminal_id) as cnt",TB_TERMINAL,array('airport_id'=>get_user_data('air_port_id')));
               $data['terminal']= $terminalData[0]['cnt'];
            }
            $userData = $this->common->selectQuery("count(id) as cnt",TB_USERS);
            $airportData = $this->common->selectQuery("count(id) as cnt",TB_AIRPORTS,$whereairport);
            $adminData = $this->common->selectQuery("count(".TB_ADMIN.".id) as cnt",TB_ADMIN,$whereadmin,array(),array(TB_AIRPORTS=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
			$notificationData = $this->common->selectQuery("count(pending_id) as cnt",TBL_PENDING_NOTIFICATION,array('status'=>2));
            $data['users']=$userData[0]['cnt'];
            $data['admins']=$adminData[0]['cnt'];
            $data['airports']=$airportData[0]['cnt'];
			$data['notifications']=$notificationData[0]['cnt'];
            $data['pageTitle']='Touchdown nigeria - Admin panel - TDN';

			//$terminaladmins

            /*echo "<pre>";

			print_r($data);
      
			exit();*/
            $this->load->view('home',$data);
		}
        else
        {
            redirect('login');
            exit;
        }
    }
}
?>
