<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,9);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($editRight || $deleteRight)
			{
				$widthArr=array('page_title'=>35,'page_detail'=>55,'action'=>10);
			}
			else
			{
				$widthArr=array('page_title'=>40,'page_detail'=>60);
			}
			$data['widthArr']=$widthArr;
			$data['pageTitle'] = 'Pages - TDN';
			if($accessRight)
			{
				$this->load->view('pages/listPages',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	public function listPages(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,9);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$postData = $this->input->post();
				
				$arrayColumn = array("page_id"=>"page_id","page_title"=>"page_title","page_detail"=>"page_detail");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'page_title','page_id',TB_PAGES,'*','listPages');
				$lables=lang('CONSTANT_LABLES_ARRAY');
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $page) {
						$pageId = $this->encrypt->encode($page['page_id']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$page['page_title'].'</td>';
	                    if($page['type']==1)
						{
							$rows .= '<td class="text-left">';
							$page_detail=json_decode($page['page_detail'],'ASSOC');
							foreach($page_detail as $k => $v)
							{
								if(is_array($v))
								{
									$rows .= implode(',',$v);
								}
								else
								{
									$rows .= '<b>'.$lables[$k].'</b> : '.$v.'<br/>';	//$lables[$k].' : '.
								}
							}
							$rows .= '</td>';
						}
						else
						{
							$rows .= '<td class="text-left">'.$page['page_detail'].'</td>';
						}
								if($edit || $delete)
								{
									$rows .= '<td class="text-left">';
								if($edit)
								{
									$rows .= '<a data-id="'.$i.'" data-row-id="'.$pageId.'" class="" onclick="getPage(this)" title="Edit" href="javascript:void(0)">
												   <i class="fa fa-fw fa-edit"></i>
											   </a>';
								}	
									$rows .= ' </td>';
								}
								$rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function getPage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$pageData = $this->common->selectQuery("page_title,page_detail,type",TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['key'])));
				if($pageData){
					if($pageData[0]['type']==1)
					{
						$page_detail=json_decode($pageData[0]['page_detail'],'ASSOC');
						$page_detail['page_title'] = $pageData[0]['page_title'];
						$page_detail['type'] = $pageData[0]['type'];	
					}
					else
					{
						$page_detail=$pageData[0];
					}
					///echo "<pre>";print_r($page_detail);die();
					echo json_encode(array("status"=>"success","pageData"=>$page_detail)); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function savePage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//echo "<pre>";print_r($postData);die();
				if($postData["type"]==1)
				{
					$page_detail=array('office_address'=>$postData["office_address"],'call_us_on' => $postData['call_us_on'],'write_to_us_at' => $postData['write_to_us_at']);
					$insertArr = array('page_detail' => json_encode($page_detail));
				}
				else
				{
					$insertArr = array('page_detail' => $postData['page_detail']);
				}
				
				if($postData["page_key"]){
					$insertId = $this->common->update(TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Page has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertId = $this->common->insert(TB_PAGES,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Page has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
}