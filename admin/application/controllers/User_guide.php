<?php 
/*
Author: Ram K
Page : User_guide.php
Description: User guide details page.
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_guide extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			
			$widthArr['action']=10;
			$data['pageTitle'] = 'User guide - TDN';
			
			$data['widthArr']=$widthArr;

			$data['airport'] = $this->common->select('*',TB_AIRPORTS);
			
			if(get_user_data('role')==1 || get_user_data('role')==2)
			{
				$this->load->view('user_guide/listUserGuide',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}
	function listUserGuide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,4);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				
				$where=array();
				
				$postData = $this->input->post();				
				$arrayColumn = array("ug_id"=>"ug_id","ug_title"=>"ug_title","ug_details"=>"ug_details","name"=>"name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");	
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'ug_title','ug_details',TB_USER_GUIDE,'*','listUserGuide',$where,array(TB_AIRPORTS=>TB_AIRPORTS.'.id = '.TB_USER_GUIDE.'.airport_id'));

				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $userguide) {
						$ug_id = $this->encrypt->encode($userguide['ug_id']);
						
						$rows .= '<tr category_id="'.$ug_id.'">
	                            <td class="text-left">'.$userguide['ug_title'].'</td>
	                             <td class="text-left">'.$userguide['ug_details'].'</td>
	                             <td class="text-left">'.$userguide['name'].'</td>';
								
						if($edit || $delete)
						{
							$rows.='<td class="text-left">';
										if($edit){
											$rows .= '<a data-id="'.$i.'" data-row-id="'.$ug_id.'" class="" onclick="getUserGuide(this)" title="Edit" href="javascript:void(0)">
												<i class="fa fa-fw fa-edit"></i>
											</a>';
										}
										if($delete)
										{
											$rows .= '<a data-id="'.$i.'" data-row-id="'.$ug_id.'" class="" onclick="deleteGuide(this)" title="Delete" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
											</a>';
										}
							$rows .= '</td>';
						}
	                    $rows.='</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;	
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function getUserGuide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$ugData = $this->common->selectQuery("*",TB_USER_GUIDE,array('ug_id'=>$this->encrypt->decode($postData['key'])));
			
				$airportData = $this->common->select('*',TB_AIRPORTS);
				if($ugData){
					echo json_encode(array("status"=>"success","ugData"=>$ugData[0],"airport" => $airportData)); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function deleteGuide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$ug_id=$this->encrypt->decode($postData['key']);
				
				
					$ugData = $this->common->delete(TB_USER_GUIDE,array('ug_id'=>$ug_id));

					if($ugData){
						echo json_encode(array("status"=>"success","msg"=>"Guide deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	public function saveUserGuide(){
		
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$store_images='';
				$postData = $this->input->post();
				$insertArr = array('ug_title'=>$postData["ug_title"],'airport_id'=>$postData['airportData'],'ug_details'=>$postData["ug_desc"]);

				if($postData["ug_key"]){

					$insertcategory_id = $this->common->update(TB_USER_GUIDE,array('ug_id'=>$this->encrypt->decode($postData['ug_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Guide has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertcategory_id = $this->common->insert(TB_USER_GUIDE,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Guide has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}	
	
}
