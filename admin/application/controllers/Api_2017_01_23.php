<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Common_model");
    }
    
    public function login_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }
        else if(trim($postData["password"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"password"=>md5(trim($postData["password"]))));//,"del_status"=>"no","verification_status"=>null//"user_status"=>"published"//,"user_status"=>"published"
		if(count($userArr)>0)
        {
				
				if(trim($postData["device_token"]) != ""){
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("device_token"=>trim($postData["device_token"]),"device_type"=>trim($postData["device_type"])));
				}//
				$this->response(array("status"=>"success", "msg"=> "Logged in successfully.", "user"=> $userArr[0]), 200); exit;
			
		}else{
    		$this->response(array("status"=>"error", "msg"=> "Email or password is incorrect."), 200);
    	}
    }
    function registerUser_post()
    {
        $postData = $_POST;
        if(trim($postData["first_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
        }/*else if(trim($postData["last_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
        }*/else if(trim($postData["country"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your country."), 200);
        }/*else if(trim($postData["mobile_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
        }*/else if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
        }
        
        $emailArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);
						
        }
        else
        {
            $date = date("Y-m-d H:i:s");
            $user_token = md5(uniqid(rand(), true));
			$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randstring = '';
			for ($i = 0; $i < 12; $i++) {
				$randstring.= $characters[rand(0, strlen($characters))];
			}
            $insertArr = array(
                                "password"      =>md5(trim($postData["password"])),
                                "user_token"    =>$user_token,
                                "first_name"    =>trim($postData["first_name"]),
								//"last_name"    =>trim($postData["last_name"]),
                                "email"         =>trim($postData["email"]),
                                "phone"  =>trim($postData["mobile_no"]),
                                "country"     =>trim($postData["country"]),
								"created_at"  => $date,
                                "updated_at"  => $date,
								"del_status"=>"no",
                            );
              
            if($postData["device_token"]!="")
			{
				$insertArr["device_token"] = trim($postData["device_token"]);
			}
			if($postData["device_type"]!="")
			{
				$insertArr["device_type"] = trim($postData["device_type"]);
			}
            $user = $this->Common_model->insert(TB_USERS,$insertArr);
        }
        if($user)
        {
				$subject="Welcome to Touchdown Nigeria";
				$message=getMailTemplate('welcome',array('name'=>$postData["first_name"]));
				$res=sentEmail(array('avadhut@exceptionaire.co','Touchdown Nigeria'), array($postData["email"]),array('avadhut@exceptionaire.co','Touchdown Nigeria'),$subject,$message);            
				$this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Your account has been created successfully."), 200);
        }else{
				$this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
        
    }
    public function getPassword_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
		if(count($userArr)>0)
        {
				
				$user_token = md5(uniqid(rand(), true));
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 7; $i++) {
                    $randstring.= $characters[rand(0, strlen($characters))];
                }
                $subject="Your forgot password.";
                $message='<br/>
                Dear '.$userArr[0]['first_name'].'<br/>
                <br/>
                Your new password is : '.$randstring.' <br/>
                =================================<br/>
                Touchdown Nigeria';
                $res=sentEmail(array('avadhut@exceptionaire.co','Touchdown Nigeria'), array($postData["email"]),array('avadhut@exceptionaire.co','Touchdown Nigeria'),$subject,$message);
                //echo "<pre>";print_r($res);
                $this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("password"=>md5($randstring)));
                $this->response(array("status"=>"success", "msg"=> "Password sent to your email, please check your inbox.",), 200); exit;
			
		}else{
    		$this->response(array("status"=>"error", "msg"=> "Email id is not registered."), 200);
    	}
    }
	
	function updateUser1_post()
    {
        $postData = $_POST;
        
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["first_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
				}/*else if(trim($postData["last_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
				}*/else if(trim($postData["country"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your country."), 200);
				}/*else if(trim($postData["mobile_no"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
				}*/else if(trim($postData["email"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
				}/*else if(trim($postData["gender"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your gender."), 200);
				}else if(trim($postData["date_of_birth"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your date_of_birth."), 200);
				}*/
				/*else if(trim($postData["password"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}*/
				
				$emailArr=$this->Common_model->select("*",TB_USERS,array("id != "=>trim($userArr[0]["id"]),"email"=>trim($postData["email"])));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);
								
				}
				else
				{
					$date = date("Y-m-d H:i:s");
					$insertArr = array(
										"first_name"    =>trim($postData["first_name"]),
										//"last_name"    =>trim($postData["last_name"]),
										"email"         =>trim($postData["email"]),
										//"gender"         =>trim($postData["gender"]),
										//"date_of_birth"  =>date('Y-m-d',strtotime($postData["date_of_birth"])),
										"phone"  =>trim($postData["mobile_no"]),
										"country"     =>trim($postData["country"]),
										"updated_at"  => $date,
									);
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Profile edited successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
        
    }
	function updateUser_post()
    {
        $postData = $_POST;
        
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["first_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
				}/*else if(trim($postData["last_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
				}*/else if(trim($postData["country"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your country."), 200);
				}/*else if(trim($postData["mobile_no"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
				}*/else if(trim($postData["email"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
				}/*else if(trim($postData["gender"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your gender."), 200);
				}else if(trim($postData["date_of_birth"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your date_of_birth."), 200);
				}*/
				/*else if(trim($postData["password"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}*/
				$insertArr = array(
										"first_name"    =>trim($postData["first_name"]),
										//"last_name"    =>trim($postData["last_name"]),
										"email"         =>trim($postData["email"]),
										//"gender"         =>trim($postData["gender"]),
										//"date_of_birth"  =>date('Y-m-d',strtotime($postData["date_of_birth"])),
										"phone"  =>trim($postData["mobile_no"]),
										"country"     =>trim($postData["country"]),
										"updated_at"  => $date,
									);
				//picture
				//echo "<pre>";print_r($_FILES);die();
				$files=$_FILES['picture'];
				if($files['name'])
				{
					$userfile_extn = explode(".", strtolower($files['name']));
					$ext=$userfile_extn[1];
					$allowed=array('gif','jpg','jpeg','png');
					if(!in_array($ext,$allowed))
					{
						$this->response(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"picture"));	 exit;	
					}
					if($files['size'] > 5242880)//2097152
					{
						$this->response(array("status"=>"error","msg"=> "Image size too big.","field"=>"picture"));exit;		
					}
					$new_image_name=uniqid(rand(), true).time().'.'.$ext;
					$quality=40;
					$source=$files['tmp_name'];
					$destination='./images/profile/'.$new_image_name;
					$info = getimagesize($source);
					if ($info['mime'] == 'image/jpeg')
						$image = imagecreatefromjpeg($source); 
					elseif ($info['mime'] == 'image/gif')
						$image = imagecreatefromgif($source); 
					elseif ($info['mime'] == 'image/png')
						$image = imagecreatefrompng($source); 
					imagejpeg($image, $destination, $quality);
					$insertArr['picture']=$new_image_name;
				}
				else
				{
					$this->response(array("status"=>"error", "msg"=> "Please upload profile image."), 200);
				}
				
				$emailArr=$this->Common_model->select("*",TB_USERS,array("id != "=>trim($userArr[0]["id"]),"email"=>trim($postData["email"])));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);
								
				}
				else
				{
					$date = date("Y-m-d H:i:s");
					
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Profile edited successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
        
    }
	function getUser_post()
    {
        $postData = $_POST;
        
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*,CONCAT('".base_url()."images/profile/',picture) as picture",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				$this->response(array("status"=>"success", "msg"=> "User found.","user"=>$userArr), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
        
    }
	
	function changePassword_post()
    {
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id,password",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			//echo md5($postData["current_password"]);
			if(count($userArr) > 0)
			{
				if(trim($postData["current_password"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your current password."), 200);
				}
				else if(trim($postData["password"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}
				else if(($userArr[0]["password"])==md5($postData["password"]))
				{
					$this->response(array("status"=>"error", "msg"=> "Your current password and new password should be different."), 200);
				}
				else if(($userArr[0]["password"])!=md5($postData["current_password"]))
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter correct old password."), 200);
				}
				else
				{
					$insertArr = array("password"=>md5(trim($postData["password"])));
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Password changed successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}   
    }
	
	function deactivateMe_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["reason"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your reason."), 200);
				}
				else
				{
					$insertArr = array("del_status"=>'yes');
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Your account deactivated successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}  
	}
	
	function logout_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				$insertArr = array("device_type"=>null,"device_token"=>null);
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
				$this->response(array("status"=>"success","msg"=> "Logout successful."), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}  
	}
	
	
	
	function getList_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);
				}
				else
				{
					$module=array(
								  'airport_dropdown'=>array('table'=>TB_AIRPORTS,'field'=>array('id'=>'DISTINCT '.TB_AIRPORTS.'.id','name'=>'name','fs'=>'fs','iata'=>'iata','icao'=>'icao','cityCode'=>'cityCode','countryCode'=>'countryCode','region'=>'region','latitude'=>'latitude','longitude'=>'longitude',),'where'=>array(TB_ADMIN.'.role'=>2),'order'=>array("name"=>"ASC"),'join'=>array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id')),
								  'airports_list'=>array('table'=>TB_AIRPORTS,'field'=>array('id'=>'DISTINCT '.TB_AIRPORTS.'.id','name'=>'name',"bio"=>"bio","address"=>"address","state"=>"state","city"=>"city","country"=>"country","zipcode"=>"zipcode","latitude"=>"latitude","longitude"=>"longitude","email"=>TB_AIRPORTS.".email","phone"=>TB_AIRPORTS.".phone"),'where'=>array(TB_ADMIN.'.role'=>2),'order'=>array("name"=>"ASC"),'join'=>array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id')),
								  'facility_list'=>array('table'=>'tbl_facility','field'=>array(),'default_column'=>'facility_id','column_name'=>'facility_id','where'=>array("airport_id"=>$postData['airport_id']),'order'=>array("facility_id"=>"DESC"),'join'=>array()),
								  'store_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array("store_category_id"=>1,"airport_id"=>$postData['airport_id']),'order'=>array("store_id"=>"DESC"),'join'=>array()),
								  'restorent_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array("store_category_id"=>2,"airport_id"=>$postData['airport_id']),'order'=>array("store_id"=>"DESC"),'join'=>array()),
								  "store_product_category_list"=>array(
																	 'table'=>TB_STORE_FACILITY,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),
																	 'order'=>array("store_facility_id"=>"DESC"),
																	 'join'=>array()
																	 ),
									"store_offer_list"=>array(
																	 'table'=>TB_STORE_OFFER,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),
																	 'order'=>array("store_offer_id"=>"DESC"),
																	 'join'=>array()
																	 ),
								  );
					$moduleArr=$module[$postData['module']];
					if(!isset($moduleArr))
					{
						$this->response(array("status"=>"error", "msg"=> "Invalid module name."), 200);
					}
					$select='';
					$where=$moduleArr['where'];
					$order=$moduleArr['order'];
					$default_column='';
					$column_name='';
					$column='';
					$order='';
					$default_order_column='';
					if(count($moduleArr['order']))
					{
						foreach($moduleArr['order'] as $k => $v)
						{
							$column=$k;
							$order=$v;
							$default_order_column=$k;
						}
					}
					if(count($moduleArr['field']))
					{
						foreach($moduleArr['field'] as $k => $v)
						{
							if($k)
							{
								$select.=','.$v;
								$default_column=$v;
								$column_name=$v;
							}
							else
							{
								
								$select.=$v;
							}
						}
					}
					else
					{
						$select="*";
						$default_column=$moduleArr['default_column'];
						$column_name=$moduleArr['column_name'];
					}
					$arrayColumn[] = $moduleArr['field'][0];
					$arrayStatus["is_active"] = array();
					$arrayColumnOrder = array("ASC","asc","DESC","desc");
					$post=array('page'=>$postData['page'],'column'=>$column,'order'=>$order);
					$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$moduleArr['join'],$column);//,array(),'',10
					//echo $this->db->last_query();
					//echo "<pre>";print_r($result['rows']);die();
					if(count($result['rows']))
					{
						$this->response(array("status"=>"success","msg"=> $result['entries'],'list'=>$result['rows']), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "No Record Found."), 200);
					}
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}
	
	function getDetail_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);exit;
				}
				else
				{
					if($postData["module"]=='homescreen_images1')
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>array(0=>base_url().'images/homescreen/1.jpg',1=>base_url().'images/homescreen/2.jpg',2=>base_url().'images/homescreen/3.jpg',3=>base_url().'images/homescreen/4.jpg',4=>base_url().'images/homescreen/5.jpg')), 200);
					}
					$module=array(
								'store_detail'=>array(
										'table'=>TB_STORES,
										'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"),
										'where'=>array("store_category_id"=>1,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								'restorent_detail'=>array(
										'table'=>TB_STORES,
										'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"),
										'where'=>array("store_category_id"=>2,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								'airport_images'=>array(
										'table'=>TB_AIRPORTS,
										'field'=>array("interior"=>"CONCAT('".base_url()."images/airport/interior/',interior) as interior","exterior"=>"CONCAT('".base_url()."images/airport/exterior/',exterior) as exterior"),
										'where'=>array("id"=>$postData['airport_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								//'homescreen_images'=>array(
								//		'table'=>TBL_HOMESCREEN,
								//		'field'=>array("image"=>"CONCAT('".base_url()."images/homescreen/',image) as image"),
								//		'where'=>array(),
								//		//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
								//		),
								);
					$moduleArr=$module[$postData['module']];
					$dataArr=getModuleData($moduleArr);
					if(count($moduleArr['subtable']))
					{
						foreach($moduleArr['subtable'] as $k => $v)
						{
							$dataArr[0][$k]=getModuleData($v);
						}
					}					
					//echo "<pre>";print_r($dataArr);
					//$arrayColumn = $moduleArr['field'];
					//$arrayStatus["is_active"] = array();
					//$arrayColumnOrder = array("ASC","asc","DESC","desc");
					//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where);
					if(count($dataArr))
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>$dataArr[0]), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "No Detail Found."), 200);
					}
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}
	function getHomescreen_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			
			$dataArr=$this->Common_model->select("CONCAT('".base_url()."images/homescreen/',image) as image",TBL_HOMESCREEN,array('image !='=>''));
			if(count($dataArr))
			{
				$this->response(array("status"=>"success","msg"=> "","homescreen_images"=>$dataArr), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "No Detail Found."), 200);
			}
		}
	}
	function getPage_post()
	{
		$postData = $_POST;
		if(trim($postData["page"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter page."), 200);
        }
		$dataArr = $this->Common_model->select('page_title,page_detail',TB_PAGES,array("page_name"=>$postData["page"]));
		//if($postData["page"]=='contact_us')
		//{
		//	//echo "<pre>";print_r($dataArr);die();
		//	$page_detail=json_decode($dataArr[0]['page_detail'],'ASSOC');
		//	//$page_detail['page_title'] = $dataArr[0]['page_title'];
		//	//echo "<pre>";print_r($page_detail);die();
		//	//$dataArr1=array('page_title'=>$dataArr[0]['page_title'],'page_detail'=>$page_detail);
		//	//$page_detail['page_title']=$dataArr[0]['page_title'];
		//	$this->response(array("status"=>"success","msg"=> "Page detail found.",'page'=>$page_detail), 200);
		//}
		//else
		{
			$this->response(array("status"=>"success","msg"=> "Page detail found.",'page'=>$dataArr), 200);
		}
	}
	function getNotifications_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			//TB_NOTIFICATION
			//view_status
			$notif = $this->Common_model->selectQuery("*",TB_NOTIFICATION_ALERT,array("userid"=>trim($user[0]["id"])),array("not_id"=>"desc"));
			$this->db->select('*, count(not_id) as cnt');//
			$this->db->where('userid',$user[0]["id"]); 
			$this->db->group_by('not_token'); 
			$this->db->order_by('not_id', 'desc'); 
			$query=$this->db->get(TB_NOTIFICATION_ALERT);
			$notifications=$query->result_array();
			//echo "<pre>";print_r($notifications);die();
			if(count($notifications))
			{
				//$this->Common_model->update(TB_NOTIFICATION_ALERT,array("userid"=>trim($user[0]["id"])),array("view_status"=>1));
				$this->response(array("status"=>"success","notification"=>$notif,"msg"=> "You have ".count($notif)." new notifications.","total_cnt"=>count($notif)), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "No new notifications.","total_cnt"=>0), 200);
			}
		}
	}
	function removeMySeenNotification_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		if(trim($postData["not_id"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter notification id."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			$cond=array("userid"=>$user[0]['id'],"not_id"=>$postData["not_id"]);			
			//if($postData["not_id"]!="")
			//{
			//$cond['not_id']=$postData["not_id"];
			//}
			//$this->Common_model->delete(TB_NOTIFICATION_ALERT,$cond);
			$this->Common_model->update(TB_NOTIFICATION_ALERT,$cond,array("view_status"=>1));
			$this->response(array("status"=>"success","msg"=> ""), 200);
			
		}
	}

	function getAirLineCode_get(){
		$dataArr = $this->Common_model->select('details',TBL_AIRLINE_CODE,array("id"=>1));
		$this->response(array("status"=>"success","details"=> $dataArr[0]['details']), 200);
	}
	
	function getFaq_post()
	{
		$postData = $_POST;
		$select='';
		$where=array();
		$order=array();
		$default_column='';
		$column_name='';
		$arrayColumn[] = array('faq_id'=>'faq_id','faq_title'=>'faq_title','faq_detail'=>'faq_detail');
		$arrayStatus["is_active"] = array();
		$arrayColumnOrder = array("ASC","asc","DESC","desc");
		$post=array();//array('page'=>$postData['page'],'column'=>($postData['column']!='')?$postData['column']:null,'order'=>($postData['order']!="")?$postData['order']:null);
		$result=$this->Common_model->select("*",TB_FAQ);
		
		//$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,'faq_id','faq_title',TB_FAQ,"*",null,$where);//,array(),'',10
		if(count($result))
		{
			$this->response(array("status"=>"success","msg"=> count($result)." records found.",'list'=>$result), 200);
		}
		else
		{
			$this->response(array("status"=>"error","msg"=> "No Record Found."), 200);
		}
	}
	function giveMeAirportAlerts_post()
	{
		$postData = $_POST;
		//echo "<pre>";print_r($postData);die();
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			if(!count($postData["airport_id"]))
			{
				$this->response(array("status"=>"error","msg"=> "Please enter airport id."), 200);
			}
			else
			{
				$this->Common_model->delete(TBL_AIRPORT_ALERT_REQUEST,array("user_id"=>$user[0]['id']));
				$r=0;
				foreach($postData["airport_id"] as $k => $v)
				{
					
					$insertArr = array(
                                "airport_id"  => $v,
								"user_id"=>$user[0]['id'],
                            );
					$g=$this->Common_model->insert(TBL_AIRPORT_ALERT_REQUEST,$insertArr);
					if($g)
					{
						$r++;
					}
					//echo "<pre>";print_r($insertArr);
		
				}
				if($r)
				{
					$this->response(array("status"=>"success","msg"=> "Request accepted."), 200);
				}
				else
				{
					$this->response(array("status"=>"error","msg"=> "Something went wrong."), 200);
				}
			}
		}
	}
	function myAirportAlertRequests_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			$airportData=$this->Common_model->select('airport_id',TBL_AIRPORT_ALERT_REQUEST,array("user_id"=>$user[0]['id']));
			if(count($airportData)>0)
			{
				$this->response(array("status"=>"success","msg"=> "Request found.","airport"=>$airportData), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "No record found."), 200);	
			}
		}
	}
	function giveMeFlightAlerts_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			if(trim($postData["flight_number"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter flight number."), 200);
			}
			else if(trim($postData["airport_id"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter airport id."), 200);
			}
			else if(trim($postData["flight_date"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter flight date."), 200);
			}
			else if(trim($postData["carrier"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter carrier."), 200);
			}
			else if(trim($postData["airport_code"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter airport code."), 200);
			}
			
			else
			{
				$insertArr = array(
							"flight_number"=>$postData["flight_number"],
							"airport_id"  => $postData["airport_id"],
							"user_id"=>$user[0]['id'],
							"flight_date"=>date("Y-m-d",strtotime($postData["flight_date"])),
							"carrier"=>$postData["carrier"],
							"airport_code"=>$postData["airport_code"],
							"depart_airport_code"=>$postData["depart_airport_code"],//$postData["type"],
						);
						
				$requestData=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,$insertArr);
				if(count($requestData)>0)
				{
					$this->response(array("status"=>"error","msg"=> "Already requested."), 200);
				}
				else
				{
					$cond=array(
							"flight_number"=>$postData["flight_number"],
							"airport_id"  => $postData["airport_id"],
							//"user_id"=>$user[0]['id'],
							"flight_date"=>date("Y-m-d",strtotime($postData["flight_date"])),
							"carrier"=>$postData["carrier"],
							"airport_code"=>$postData["airport_code"],
							"depart_airport_code"=>$postData["depart_airport_code"],//$postData["type"],
							);
					$requestExist=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,$cond);
					if(count($requestExist)>0)
					{
						$insertArr['alert_token']=$requestExist[0]['alert_token'];
					}
					else
					{
						$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$randstring = '';
						for ($i = 0; $i < 12; $i++) {
							$randstring.= $characters[rand(0, strlen($characters))];
						}
						$insertArr['alert_token']=$randstring;
					}
					//echo "<pre>";print_r($insertArr);die();
					$this->Common_model->insert(TBL_FLIGHT_ALERT_REQUEST,$insertArr);
					$this->response(array("status"=>"success","msg"=> "Request accepted."), 200);
				}
			}
		}
	}
}
?>
