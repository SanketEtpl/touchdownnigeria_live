<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subadmin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,2);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			$data['pageTitle']='Airport admin - Airport management - TDN';


			if($role==1)
			{
				$widthArr=array('fname'=>18,'lname'=>18,'email'=>23,'phone'=>13,'airport_name'=>18,'status'=>8,'action'=>10);
			}
			else
			{
				if($editRight || $deleteRight)
				{
					$widthArr=array('fname'=>23,'lname'=>23,'email'=>27,'phone'=>18,'action'=>15);
				}
				else
				{
					$widthArr=array('fname'=>27,'lname'=>27,'email'=>33,'phone'=>23);
				}
			}
			$data['widthArr']=$widthArr;
			if($accessRight)
			{
				$data["airports"] = $this->common->selectQuery("id as airport,name",TB_AIRPORTS,array());
				$this->load->view('subadmin/listAdmins',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	public function listAdmins(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,2);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];

				$postData = $this->input->post();
				$where=array();
				if($role!=1)
				{
					$where=array(TB_ADMIN.'.air_port_id'=>get_user_data('air_port_id'),"role"=>'2');
				}
				else
				{
					$where=array("role"=>'2');
				}

				$arrayColumn = array("user"=>TB_ADMIN.".id","fname"=>"first_name","lname"=>"last_name","email"=>TB_ADMIN.".email","phone"=>TB_ADMIN.".phone","is_active"=>TB_ADMIN.".is_active","name"=>"name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'user',TB_ADMIN.'.id',TB_ADMIN,TB_ADMIN.'.id as user_id,'.TB_ADMIN.'.first_name,'.TB_ADMIN.'.last_name,'.TB_ADMIN.'.email,'.TB_ADMIN.'.phone,'.TB_ADMIN.'.is_active,'.TB_AIRPORTS.'.name','listAdmins',$where,array(TB_AIRPORTS=>TB_ADMIN.".air_port_id=".TB_AIRPORTS.".id"));
				/*echo $this->db->last_query();
				exit;*/
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['user_id']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                            <td class="text-left">'.$user['last_name'].'</td>
	                            <td class="text-left">'.$user['email'].'</td>
	                            <td class="text-left">'.$user['phone'].'</td>';
								if(get_user_data('role')==1){
						$rows .= '<td class="text-left">'.$user['name'].'</td>';
								$rows .= '<td class="text-left">';
								if($user['is_active']=='1')
								{
									$status='0';
									$icon='on';
									$title='Inactive';
								}
								else
								{
									$status='1';
									$icon='off';
									$title='Active';
								}
								$rows .= '<a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-status="'.$status.'" title="Make '.$title.'" class="" onclick="changeStatus(this)" href="javascript:void(0)">
											<i class="fa fa-fw fa-toggle-'.$icon.'"></i>
										</a>';
								$rows .= '</td>';
								}
	                    if($edit || $delete)
						{
							$rows .= '<td class="text-left">';
							if($edit)
							{
							$rows .= '	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getUser(this)" title="Edit" href="javascript:void(0)">
											<i class="fa fa-fw fa-edit"></i>
										</a>';
							}
							if($delete)
							{
							$rows .= '   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteUser(this)" title="Delete" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
										</a>';
							}
							$rows .= '</td>';
						}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function getUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("first_name as fname,last_name AS lname,username,email as email_id,phone,air_port_id AS airport,is_active",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}

	}

	public function getMyProfile(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$user_auth=$this->session->userdata("auth_user");
				$userData = $this->common->selectQuery("first_name as fname,last_name AS lname,username,email as email_id,phone,air_port_id AS airport,is_active",TB_ADMIN,array('id'=>$user_auth['id']));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}

	}

	public function saveUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();

				
				$insertArr = array('first_name'=>$postData["first_name"],'last_name' => $postData['last_name'],'username' => $postData['username'],'email' => $postData['email_id'],'phone' => $postData['phone'],'created_id' => $this->session->userdata('auth_user')['id']);
				if(get_user_data('role')==1)
				{
					$insertArr["air_port_id"]=$postData["airport"];
				}
				else
				{
					$insertArr["air_port_id"]=get_user_data('air_port_id');
				}
				if($postData["email_id"]!="")
				{
					if($postData["user_key"]){
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$this->encrypt->decode($postData['user_key']),'email'=>$postData['email_id']));
					}
					else
					{
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('email'=>$postData['email_id']));
					}
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Email id already exists.")); exit;
					}
				}
				if($postData["username"]!="")
				{
					if($postData["user_key"]){
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$this->encrypt->decode($postData['user_key']),'username'=>$postData['username']));
					}
					else
					{
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('username'=>$postData['username']));
					}
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Username already exists.")); exit;
					}
				}


				if($postData["user_key"]){

					$userData = $this->common->selectQuery("*",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['user_key'])));

					if($userData[0]['id'] == $this->encrypt->decode($postData['user_key']))
					{
						if($postData["password"] != ""){
							$insertArr["password"] = md5($postData["password"]);
								$subject = "Your password changed by admin | Touchdown Nigeria";
								$email_content = '<p>Hello '.$postData["first_name"].'</p>
								<p>Your password has been changed by the admin.</p>
								<p>Your new password is: <b>'.$postData["password"].'</b></p>';
								$message = getMailTemplate('email_template',array('email_content'=>$email_content));
								$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
						}
						if($userData[0]['username']!=$postData["username"])
						{
							$subject = "Your username changed by admin | Touchdown Nigeria";
							$email_content = '<p>Hello '.$postData["first_name"].'</p>
							<p>Your username has been changed by the admin.</p>
							Your new username is: <b>'.$postData["username"].'</b></p>';
							$message = getMailTemplate('email_template',array('email_content'=>$email_content));
							$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
						}
					}
					$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Airport admin has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertArr["password"] = md5($postData["password"]);
					$insertArr["role"] = '2';
					$insertArr["is_active"] = '1';
					$subject = "Your account created by admin | Touchdown Nigeria";
					$email_content = '<p>Hello '.$postData["first_name"].'</p><p>Welcome to Touchdown Nigeria.</p><p>Your Touchdown Nigeria account has been created by the admin.</p>
					<p>Please <a href="'.base_url().'">Click here</a> to login your account or copy and paste the following link into your browser to enter your username and password.</p>
					Url : '.base_url().'<br>
					Username : '.$postData['username'].'<br>
					Password : '.$postData["password"].'<br>';

					$message = getMailTemplate('email_template',array('email_content'=>$email_content));
                	$res=sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

					$insertId = $this->common->insert(TB_ADMIN,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Airport admin has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveProfile()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$user_auth=$this->session->userdata("auth_user");
				$insertArr = array('first_name'=>$postData["myfirst_name"],'last_name' => $postData['mylast_name'],'username' => $postData['myusername'],'email' => $postData['myemail_id'],'phone' => $postData['myphone']);//,'air_port_id' => $postData['myairport']
				if($postData["email_id"]!="")
				{
					$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$user_auth['id'],'email'=>$postData['email_id']));
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Email id already exists.")); exit;
					}
				}
				if($postData["username"]!="")
				{
					$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$user_auth['id'],'username'=>$postData['username']));
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Username already exists.")); exit;
					}
				}

				if($postData["mypassword"] != ""){
					$insertArr["password"] = md5($postData["mypassword"]);
				}

				$insertId = $this->common->update(TB_ADMIN,array('id'=>$user_auth['id']),$insertArr);
				if($insertId){
				$result = $this->common->selectQuery("*",TB_ADMIN,array('id'=>$user_auth['id']));
				$this->session->set_userdata("auth_user", $result[0]);
					echo json_encode(array("status"=>"success","action"=>"update",'userData'=>$result[0],"msg"=>"Your profile has been updated successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteUser()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();

				/*echo $this->encrypt->decode($postData['key']);

				exit();*/

				// used subquery to get the terminal id from airport data
				$terminalData = $this->common->getSubData('*',TB_TERMINAL,array(),'airport_id',TB_ADMIN,array('id' => $this->encrypt->decode($postData['key'])),'air_port_id');

				if(count($terminalData) > 0)
				{
					echo json_encode(array('status' => 'error' , 'msg' => 'Can not delete airport admin,first delete terminal assign to this airport'));

					exit();
				}


				
				$adminData = $this->common->select('*',TB_ADMIN,array('created_id' => $this->encrypt->decode($postData['key'])));

				

				if(count($adminData) > 0)
				{
					echo json_encode(array('status' => 'error' , 'msg' => 'Please first delete terminal admin created by airport admin'));

					exit();
				}


				/*$adminStatus=$this->common->selectQuery("count(a1.id) as cnt",TB_ADMIN.' As a1',array('a1.id'=>$this->encrypt->decode($postData['key'])),array(),array(TB_ADMIN.' As a2'=>"a1.air_port_id = a2.air_port_id"));


				if($adminStatus[0]['cnt']<=1)
				{
						echo json_encode(array("status"=>"error","msg"=>"Can not delete this admin, there is only admin assigned to airport.")); exit;
				}
				else
				{*/
					$subadminData = $this->common->delete(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
					if($subadminData){
						echo json_encode(array("status"=>"success","msg"=>"Airport admin deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				//}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function deleteTerminalUser()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				/*$adminStatus=$this->common->selectQuery("count(a1.id) as cnt",TB_ADMIN.' As a1',array('a1.id'=>$this->encrypt->decode($postData['key'])),array(),array(TB_ADMIN.' As a2'=>"a1.terminal_id = a2.terminal_id"));
				if($adminStatus[0]['cnt']<=1)
				{
						echo json_encode(array("status"=>"error","msg"=>"Can not delete this admin, there is only admin assigned to terminal.")); exit;
				}
				else
				{*/
					$subadminData = $this->common->delete(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
					if($subadminData){
						echo json_encode(array("status"=>"success","msg"=>"Terminal admin deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				//}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function changeStatus()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr=array('is_active'=>$postData['status']);
				$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
				if($insertId)
				{
					// ------ Ashwini code here ----------
					$get_airid = $this->common->select("air_port_id",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
					$airport_id = $get_airid[0]['air_port_id'];

					//------ update termial status -----
					$uptermial=$this->common->update(TB_ADMIN,array('air_port_id'=>$airport_id),$insertArr);		

					echo json_encode(array("status"=>"success","msg"=>"Airport admin ".(($postData['status'])?'activated':'inactivated')." successfully.")); exit;
				}else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function changeTerminalStatus()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr=array('is_active'=>$postData['status']);
				$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
				if($insertId)
				{
					echo json_encode(array("status"=>"success","msg"=>"Terminal admin ".(($postData['status'])?'activated':'inactivated')." successfully.")); exit;
				}else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function terminalAdmin()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,11);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];

			if($role==1)
			{
				$widthArr=array('fname'=>18,'lname'=>18,'email'=>20,'phone'=>10,'terminal_name'=>15,'airport_name'=>15,'status'=>8,'action'=>10);
			}
			else
			{
				if($role==2){
					if($editRight || $deleteRight)
					{
						$widthArr=array('fname'=>15,'lname'=>15,'email'=>20,'phone'=>15,'terminal_name'=>20,'action'=>15);
					}
					else
					{
						$widthArr=array('fname'=>18,'lname'=>18,'email'=>20,'phone'=>19,'terminal_name'=>25);
					}
				}else{
					if($editRight || $deleteRight)
					{
						$widthArr=array('fname'=>23,'lname'=>23,'email'=>27,'phone'=>18,'action'=>15);
					}
					else
					{
						$widthArr=array('fname'=>27,'lname'=>27,'email'=>33,'phone'=>23);
					}
				}
			}
			$data['widthArr']=$widthArr;
			if($accessRight)
			{
				//$data["airports"] = $this->common->selectQuery("id as airport,name",TB_AIRPORTS,array());
				$data["airports"] = $this->common->selectQuery('DISTINCT '.TB_AIRPORTS.".id as airport,name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
				$this->load->view('subadmin/listTerminalAdmins',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}
	public function listTerminalAdmins(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,11);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$postData = $this->input->post();
				$where=array();
				if($role==2)
				{
					$where=array(TB_ADMIN.'.air_port_id'=>get_user_data('air_port_id'),"role"=>'3');
				}
				else if($role==3)
				{
					$where=array(TB_ADMIN.'.terminal_id'=>get_user_data('terminal_id'),"role"=>'3');
				}
				else
				{
					$where=array("role"=>'3');
				}

				$arrayColumn = array("user"=>TB_ADMIN.".id","fname"=>"first_name","lname"=>"last_name","email"=>TB_ADMIN.".email","phone"=>TB_ADMIN.".phone","is_active"=>TB_ADMIN.".is_active","name"=>"name","terminal_id"=>TB_TERMINAL.".terminal_id","terminal_name"=>"terminal_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname',TB_ADMIN.'.id',TB_ADMIN,TB_ADMIN.'.id as user_id,'.TB_ADMIN.'.first_name,'.TB_ADMIN.'.last_name,'.TB_ADMIN.'.email,'.TB_ADMIN.'.phone,'.TB_ADMIN.'.is_active,'.TB_AIRPORTS.'.name,'.TB_TERMINAL.'.terminal_name','listTerminalAdmins',$where,array(TB_AIRPORTS=>TB_ADMIN.".air_port_id=".TB_AIRPORTS.".id",TB_TERMINAL=>TB_ADMIN.".terminal_id=".TB_TERMINAL.".terminal_id"));//

				

				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['user_id']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                            <td class="text-left">'.$user['last_name'].'</td>
	                            <td class="text-left">'.$user['email'].'</td>
	                            <td class="text-left">'.$user['phone'].'</td>';
	                            if(get_user_data('role')!=3){
									$rows .= '<td class="text-left">'.$user['terminal_name'].'</td>';
								}
								if(get_user_data('role')==1)
								{
									$rows .= '<td class="text-left">'.$user['name'].'</td>';
									$rows .= '<td class="text-left">';
									if($user['is_active']=='1')
									{
										$status='0';
										$icon='on';
										$title='Inactive';
									}
									else
									{
										$status='1';
										$icon='off';
										$title='Active';
									}
									$rows .= '<a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-status="'.$status.'" title="'.$title.'" class="" onclick="changeStatus(this)" href="javascript:void(0)">
												<i class="fa fa-fw fa-toggle-'.$icon.'"></i>
											</a>';
									$rows .= '</td>';
								}
	                    if($edit || $delete)
						{
							$rows .= '<td class="text-left">';
							if($edit)
							{
							$rows .= '	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getTerminalUser(this)" title="Edit" href="javascript:void(0)">
											<i class="fa fa-fw fa-edit"></i>
										</a>';
							}
							if($delete)
							{
							$rows .= '   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteUser(this)" title="Delete" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
										</a>';
							}
							$rows .= '</td>';
						}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function getTerminalUser()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("first_name as fname,last_name AS lname,username,email as email_id,phone,air_port_id AS airport,is_active,terminal_id",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function saveTerminalAdmin(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();

				$insertArr = array('first_name'=>$postData["first_name"],'last_name' => $postData['last_name'],'username' => $postData['username'],'email' => $postData['email_id'],'phone' => $postData['phone'],"terminal_id"=>$postData['terminal_id'],'created_id' => $this->session->userdata('auth_user')['id']);
				if(get_user_data('role')==1)
				{
					$insertArr["air_port_id"]=$postData["airport"];
				}
				else
				{
					$insertArr["air_port_id"]=get_user_data('air_port_id');
				}
				if($postData["email_id"]!="")
				{
					if($postData["user_key"]){
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$this->encrypt->decode($postData['user_key']),'email'=>$postData['email_id']));
					}
					else
					{
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('email'=>$postData['email_id']));
					}
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Email id already exists.")); exit;
					}
				}
				if($postData["username"]!="")
				{
					if($postData["user_key"]){
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('id !='=>$this->encrypt->decode($postData['user_key']),'username'=>$postData['username']));
					}
					else
					{
						$userData = $this->common->selectQuery("*",TB_ADMIN,array('username'=>$postData['username']));
					}
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Username already exists.")); exit;
					}
				}

				//if($postData["password"] != ""){
				//	$insertArr["password"] = md5($postData["password"]);
				//	if($postData["user_key"]){
				//		$subject="Your password changed.";
				//		$message='
				//		Dear '.$postData["first_name"].'<br/>
				//		<br/>
				//		Your password has been changed.<br/>
				//		Password : '.$postData["password"].'<br/>
				//		=================================<br/>
				//		Touchdown Nigeria';
				//		$res=sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
				//	}
				//}
				if($postData["user_key"]){

					$userData = $this->common->selectQuery("*",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['user_key'])));

					if($userData[0]['id']==$this->encrypt->decode($postData['user_key']))
					{
						if($postData["password"] != ""){
							$insertArr["password"] = md5($postData["password"]);
								$subject="Your password changed by admin | Touchdown Nigeria";
								$email_content ='<p>Hello '.$postData["first_name"].'</p><p>Your password has been changed by the admin.</p>
								<p>Your new password is: <b>'.$postData["password"].'</b></p>';

								$message = getMailTemplate('email_template',array('email_content'=>$email_content));
								$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
						}
						if($userData[0]['username'] != $postData["username"])
						{
							$subject="Your username changed by admin | Touchdown Nigeria";
							$email_content ='<p>Hello '.$postData["first_name"].'</p><p>Your username has been changed by the admin.</p>
							<p>Your new username is : <b>'.$postData["username"].'</b></p>';

							$message = getMailTemplate('email_template',array('email_content'=>$email_content));
							$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
						}
					}
					$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Terminal admin has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertArr["password"] = md5($postData["password"]);
					$insertArr["role"] = '3';
					$insertArr["is_active"] = '1';
					$subject="Your account created by admin | Touchdown Nigeria";
					$email_content ='<p>Hello '.$postData["first_name"].'</p><p>Welcome to Touchdown Nigeria.</p><p>Your Touchdown Nigeria account has been created by the admin.</p>
					<p>Please <a href="'.base_url().'">Click here</a> to login your account or copy and paste the following link into your browser to enter your username and password.</p>
					Url : '.base_url().'<br>
					Username : '.$postData['username'].'<br>
					Password : '.$postData["password"].'<br>';

					$message = getMailTemplate('email_template',array('email_content'=>$email_content));
					$res=sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData['email_id']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

					/*print_r($insertArr);

					exit();*/

					$insertId = $this->common->insert(TB_ADMIN,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Terminal admin has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function getTerminal($flag='')
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();

				if($flag=='main')
				{
					$terminalData = $this->common->selectQuery("*",TB_TERMINAL,array('airport_id'=>$postData['airport_id']),array());
				}
				else
				{
					//$terminalData = $this->common->selectQuery("*",TB_TERMINAL,array('airport_id'=>$postData['airport_id'],TB_ADMIN.'.role'=>3),array(),array(TB_ADMIN=>TB_ADMIN.'.terminal_id = '.TB_TERMINAL.'.terminal_id'));
					$terminalData = $this->common->select("*",TB_TERMINAL,array('airport_id'=>$postData['airport_id']));
				}
				//$data["airports"] = $this->common->selectQuery(TB_AIRPORTS.".id as airport,name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
				//if($terminalData){
					$dta=array();
					//$dta['']="Select Terminal";
					foreach($terminalData as $k => $v)
					{
						$dta[$v['terminal_id']]=$v['terminal_name'];
					}
					//echo implode("#",$dta);die;
					echo json_encode(array("status"=>"success","terminalData"=>$dta)); exit;
				//}else{
				//	echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				//}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
}
