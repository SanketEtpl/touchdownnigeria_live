<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AirportScript extends CI_Controller {

	public function __construct()
	{
  		parent::__construct();
  		$this->load->library('csvimport');
      $this->load->library('session');     
	}

	public function index()
	{

		$data = array();
   /* print_r($this->session->userdata);
    

    exit();*/
        if($this->session->userdata('csvSuccess'))
        {
            //echo $this->session->userdata('csvSuccess');
            $data['status'] = 'success';
            $data['message'] = $this->session->userdata('csvSuccess');
        }
        if($this->session->userdata('csvError'))
        {
            //echo $this->session->userdata('csvError');
            $data['status'] = 'error';

            $data['message'] = $this->session->userdata('csvError');

            $this->session->unset_userdata('csvError');
            $role=get_user_data('role');
            $dtRight=checkRight($role,12);

            $data['addRight']=$dtRight['add_privilege'];
            $data['editRight']=$editRight=$dtRight['update_privilege'];
            $data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
            $data['pageTitle'] = 'Upload csv file - Airport - TDN';
            

            $this->load->view('header');
            $this->load->view('airportScript',$data);
            $this->load->view('footer');
        }
        else
        {
            $role=get_user_data('role');
            $dtRight=checkRight($role,12);
           // print_r($this->data['pageTitle']); exit();
            /*print_r($dtRight);

            exit();*/
            $data['addRight']=$dtRight['add_privilege'];
            $data['editRight']=$editRight=$dtRight['update_privilege'];
            $data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
            $data['pageTitle'] = 'Upload csv file - Airport - TDN';


            if($this->session->userdata('csvSuccess'))
            {
                $this->session->unset_userdata('csvSuccess');
            }
            if($this->session->userdata('csvError'))
            {
                $this->session->unset_userdata('csvError');
            }

            
            $data['flight_status'] = $this->common->select('*',TB_FLIGHT_STATUS);

            $this->load->view('header');
      			$this->load->view('airportScript',$data);
      			$this->load->view('footer');
        }


		 
	}

	 public function uploadFile()
    {
        $target_dir = "uploads/airports/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        if ($_FILES["fileToUpload"]["size"] > 500000)
        {
          $this->session->set_flashdata('csvError','Sorry, your file is too large.');
            //echo "Sorry, your file is too large.";
            redirect('/script');
            $uploadOk = 0;
        }
        if ($uploadOk == 0)
        {
            echo "Sorry, your file was not uploaded.";
            redirect('/script');
        // if everything is ok, try to upload file
        }


        if($_FILES['fileToUpload']['type'] == 'text/csv' || $_FILES['fileToUpload']['type'] == 'text/comma-separated-values' || $_FILES['fileToUpload']['type'] == 'application/octet-stream' || $_FILES['fileToUpload']['type'] == 'text/plain')
        {
            $flag = 0;

            $target_file = $target_dir."airportData_".date('Y-m-d h:i:s').".csv";
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
            {
                //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                  $file_data = $this->csvimport->get_array("uploads/airports/airportData_".date('Y-m-d h:i:s').".csv");

                  if(count($file_data) > 0)  // if csv file is not empty
                  {
                      $data = array();
                      foreach($file_data as $key => $data)
                      {

                        if("" !=$data['Flight Number'] && "" != $data['Airline'] && "" != $data['Weekdays'] && "" != $data['Departure From'] && "" != $data['Arrival At'] && "" != $data['Schedule'] && "" != $data['Fs Code'] && "" != $data['Status'])
                        {
                           $val[] = array(
                                'flight_number' => $data['Flight Number'],
                                'airline' => $data['Airline'],
                                'weekdays' => $data['Weekdays'],
                                'departure_from' => $data['Departure From'],
                                'arrival_at' => $data['Arrival At'],
                                //'journey_hrs' => $data['Journey hrs'],
                                'schedule' => $data['Schedule'],
                                'terminal' => $data['Terminal'],
                                'fs_code' => $data['Fs Code'],
                                'status' => $data['Status'],
                                'airport_status' => $data['Airport Status'],
                                'created_date' => date('Y-m-d')
                           );
                        }
                        else
                        {
                            $flag = 1;
                            break;
                        }
                      }


                      if(1 == $flag)
                      {
                          $this->session->set_flashdata('csvError','Missing data');
                            redirect('/airportScript');
                      }

                      $csvData = $this->common->select('count(*)',TB_AIRPORT_CSV_UPLOAD);

                      if($csvData[0]['count(*)'] > 0)
                      {
                          $this->common->truncate(TB_AIRPORT_CSV_UPLOAD);

                      }
                      
                     $insert = $this->common->insert_batch(TB_AIRPORT_CSV_UPLOAD,$val);
                     if($insert)
                     {
                        $data = array('name' => "airportData_".date('Y-m-d h:i:s').".csv");
                          $upload = $this->common->insert(TB_CSV_AIRPORT,$data);
                          $this->session->set_userdata('csvSuccess','Data inserted sucessfully');
                          redirect('/airportScript');
                        
                     }
                     else
                     {
                        $this->session->set_flashdata('csvError','Data could not be inserted');
                            redirect('/airportScript');
                     }
                      
                  }
                  else
                  {
                        $this->session->set_userdata('csvError','Sorry file is empty');
                        redirect('/script');
                  }

            }
            else
            {
              $this->session->set_flashdata('csvError','Sorry, there was an error uploading your file.');
              redirect('/script');
                //echo "Sorry, there was an error uploading your file.";
            }
           

        }
        else
        {
            

            $mimes = array('text/csv');
            $uploadOk = 0;
            /*$this->session->set_flashdata('csvError','Sorry, mime type not allowed.');
            redirect('/airportScript');*/
            $this->session->set_userdata('csvError','Sorry, file type not allowed.');
            redirect('/airportScript');
        }

    }

    public function listAirportData()
    {

        if(is_ajax_request())
      {
        if(is_user_logged_in()){
          $postData = $this->input->post();

          /*print_r($postData);

          exit();*/

          $arrayColumn = array("airport"=>"id","flight_number"=>"flight_number","airline" =>"airline","weekdays" =>"weekdays","departure_from" => "departure_from","arrival_at" => "arrival_at","schedule" => "schedule","terminal" => "terminal","status" => "status" ,"airport_status" => "airport_status");

          $arrayStatus["is_active"] = array();
          $arrayColumnOrder = array("ASC","asc","DESC","desc");
          $where=array();
          $result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'flight_number','id',TB_AIRPORT_CSV_UPLOAD,'*','listAirportData',$where);

          $status = $this->common->select('*',TB_FLIGHT_STATUS); 
/*          echo $this->db->last_query();
          print_r($result);
          exit;*/


          $rows = '';
          if(!empty($result['rows']))
          {
            $i=1;

            $role=get_user_data('role');
            $dtRight=checkRight($role,12);
            $rights['editRight'] = $dtRight['update_privilege'];
            foreach ($result['rows'] as $key => $airportData) {
              $id = $this->encrypt->encode($airportData['id']);
              $rows .= '<tr id="'.$id.'">
                                <td class="text-left"><input type="checkbox" name="airportData" value="'.$airportData['id'].'"></td>
                                <td class="text-left">'.$airportData['flight_number'].'</td>
                                <td class="text-left">'.$airportData['airline'].'</td>
                                <td class="text-left">'.$airportData['weekdays'].'</td>
                                <td class="text-left">'.$airportData['departure_from'].'</td>
                                <td class="text-left">'.$airportData['arrival_at'].'</td>
                                <td class="text-left">'.$airportData['schedule'].'</td>
                                <td class="text-left">'.$airportData['terminal'].'</td>';

                                if($rights['editRight'])
                                {

                                  $rows.= '<td class="text-left"><select name="" id="flightStatus_'.$key.'" class="form-control flightStatus" onchange="updateStatus('.$airportData['id'].',this)">';
                                    foreach($status as $val)
                                    { 
                                      $rows.='<option value="'.$val["name"].'"';
                                      if($val["name"] == $airportData["status"])
                                      {
                                        $rows.=' selected>'.$val["name"].'</option>';  
                                      }
                                      else
                                      {
                                          $rows.='>'.$val["name"].'</option>'; 
                                      }
                                      
                                    }
                                

                                  $rows.='</select></td>';
                                }
                                else
                                {
                                    $rows.= '<td class="text-left">-</td>';
                                }  
                                $rows.='<td class="text-left">'.$airportData['airport_status'].'</td>';

              $rows .= '</tr>';
            }
          }
          else
          {
            $rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
          }
          $data["rows"] = $rows;

          $data["pagelinks"] = $result["pagelinks"];
          $data["entries"] = $result['entries'];
          $data["status"] = "success";

          echo json_encode($data);

        }else{
          echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
        }
      }


    }

    public function updateFlightStatus()
    {
        if(is_ajax_request())
        {
          if(is_user_logged_in())
          {

            if("" != $this->input->post('id') && "" != $this->input->post('status'))
            {
                $data = array(

                    'status' => $this->input->post('status')
                );

                if($this->common->update_where_in(TB_AIRPORT_CSV_UPLOAD,$data,'id',$this->input->post('id')))
                {
                    //echo json_encode(array("status"=>"success","msg"=>"Status updated successfully")); exit;
                    echo json_encode(array("status" => "success","msg"=>"Status updated successfully"));
                }
                else
                {
                    echo json_encode(array("status"=>"error","msg"=>"problem in status updation")); exit;
                }
            }
            else
            {
                echo json_encode(array("status"=>"error","msg"=>"Something went wrong !!")); exit;
            }
          }
          else
          {
              echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
          }
        }
    }

    public function csvFiles()
    {
        $this->load->view('header');
        $this->load->view('airportcsvFiles');
        $this->load->view('footer');
    }

    public function listAirportCsv()
    {
      if(is_ajax_request())
      {
        if(is_user_logged_in()){
          $postData = $this->input->post();

          $arrayColumn = array("airport"=>"id","fname"=>"name","date" =>"created_date");
          $arrayStatus["is_active"] = array();
          $arrayColumnOrder = array("ASC","asc","DESC","desc");
          $where=array();
          $result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'date','id',TB_CSV_AIRPORT,'*','listAirportCsv',$where);

          $rows = '';
          if(!empty($result['rows']))
          {
            $i=1;
            $role=get_user_data('role');
            $dtRight=checkRight($role,12);
            $rights['deleteRight'] = $dtRight['delete_privilege'];


            foreach ($result['rows'] as $csvData) {
              $id = $this->encrypt->encode($csvData['id']);
              $rows .= '<tr id="'.$id.'">
                                <td class="text-left"><a href="'.base_url().'airportScript/downloadScript/'.$csvData['id'].'">'.$csvData['name'].'</a></td>
                                <td class="text-left">'.$csvData['created_date'].'</td>';

              if($rights['deleteRight'])
              {
                  $rows .= '<td><a data-id="'.$i.'" data-row-id="'.$csvData['id'].'" class="" onclick="deleteCSVFile('.$csvData['id'].')" title="Delete" href="javascript:void(0)">
                            <i class="fa fa-fw fa-close"></i></a></td>';
              }
              else
              {
                  $rows.= '<td class="text-left"> - </td>';
              }
             

              $rows .= '</tr>';
            }
          }
          else
          {
            $rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
          }
          $data["rows"] = $rows;

          $data["pagelinks"] = $result["pagelinks"];
          $data["entries"] = $result['entries'];
          $data["status"] = "success";

          echo json_encode($data);

        }else{
          echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
        }
      }
    }

    public function downloadScript($id)
    {
        if($id)
        {

            $this->load->helper('download');
            $fileInfo = $this->common->select('name',TB_CSV_AIRPORT,array('id' => $id));

            $file = 'uploads/airports/'.$fileInfo[0]['name'];

            force_download($file, NULL);

            redirect('/airportScript/csvFiles');
        }

    }



function deleteCSVFile()
{
  if(is_ajax_request())
  {
      $id = $this->input->post('id');
      if($id)
      {
          //$this->load->helper("file");
        //  unlink('uploads/data.xlsx');

        $file = $this->common->select('*',TB_CSV_AIRPORT,array('id' => $id));

        if(count($file) > 0)
        {
            $deleteFile = $this->common->delete(TB_CSV_AIRPORT,array('id' => $id));
            unlink('uploads/'.$file[0]['name']);
            echo json_encode(array("status" => "success","msg"=>"File deleted sucessfully"));
        }
        else {
            echo json_encode(array("status" => "error","msg"=>"Something went wrong"));
        }

      }
  }
}





}

/* End of file airportScript.php */
/* Location: .//tmp/fz3temp-1/airportScript.php */