<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Common_model");
    }

    public function login_post()
    {
        $postData = $this->input->post();

        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your registered email id."), 200);
        }
        else if(trim($postData["password"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter password for your registered email account."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"password"=>md5($postData["password"])));//,"del_status"=>"no","verification_status"=>null//"user_status"=>"published"//,"user_status"=>"published"
		if(count($userArr)>0)
        {
        	if($userArr[0]["email_verified"] == 0){
        		$this->response(array("status"=>"error", "msg"=> "Your account is not verified.","email_verify" => 0), 200);
        	}
			if($userArr[0]["picture"] != ""){
				$filename = 'images/profile/'.$userArr[0]["picture"];
				if(file_exists($filename)){
					$userArr[0]["picture"] = base_url()."images/profile/".$userArr[0]["picture"];
				}else{
					$userArr[0]["picture"] = base_url()."images/no-image-mob.png";
				}
			}else{
				$userArr[0]["picture"] = base_url()."images/no-image-mob.png";
			}
			if(trim($postData["device_token"]) != ""){
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("device_token"=>trim($postData["device_token"]),"device_type"=>trim($postData["device_type"])));
			}//

            $userArr1 = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"password"=>md5($postData["password"])));
			$this->response(array("status"=>"success", "msg"=> "You have successfully logged in.", "user"=> $userArr1[0],"email_verify" => 1), 200); exit;

		}else{
    		$this->response(array("status"=>"error", "msg"=> "Email or password is incorrect. Please check and re-enter."), 200);
    	}
    }

    public function verifyEmail_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your registered email id."), 200);
        }
        else if(trim($postData["otp"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter OTP."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
		if(count($userArr)>0)
        {
        	if($userArr[0]["email_verified"]){
        		$this->response(array("status"=>"error", "msg"=> "Your account is already verified."), 200);
        	}
        	else if($userArr[0]['otp'] == trim($postData["otp"])){
        		$current_time = date('Y-m-d H:i:s');
        		$otp_expiration =  date('Y-m-d H:i:s', strtotime($userArr[0]['otp_generated_at']." +2 days"));
        		if($current_time > $otp_expiration){
        			$this->response(array("status"=>"error", "msg"=> "OTP has been expired."), 200);
        		}else{
        			$this->Common_model->update(TB_USERS,array("id"=>$userArr[0]["id"]),array("email_verified"=>'1',"otp"=>''));
        			$subject = "Welcome | Touchdown Nigeria";
	        		$email_content = '<p>Hello '.$userArr[0]['first_name'].',</p>
	                <p>Welcome to Touchdown Nigeria.</p>
	                <p>Your account has been verified successfully.</p>';

					$message = getMailTemplate('email_template',array('email_content'=>$email_content));
					$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($userArr[0]['email']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

        			$this->response(array("status"=>"success", "msg"=> "Your account has been verified."), 200);
        		}
        	}else{
        		$this->response(array("status"=>"error", "msg"=> "Invalid OTP you have entered."), 200);
        	}
        }else{
        	$this->response(array("status"=>"error", "msg"=> "Email not registered with us."), 200);
        }
    }

    public function resendOtp_post()
    {
    	$postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your registered email id."), 200);
        }

		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));

		$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < 12; $i++) {
			$randstring.= $characters[rand(0, strlen($characters))];
		}
		$otp = generateOTP();


		$insertArr = array(
                            "otp" => $otp

                            );



		$update = $this->Common_model->update(TB_USERS,array("email"=>trim($userArr[0]["email"])),$insertArr);


		if($update)
        {
        		$subject = "Verification Email | Touchdown Nigeria";
        		$email_content = '<p>Hello '.$userArr[0]["first_name"].',</p>
                <p style="word-wrap:break-word;">Mail for Resend OTP.</p>
                <p style="word-wrap:break-word;">Thank you for showing an interest to create an account with Touchdown Nigeria.</p>
                <p style="word-wrap:break-word;">Please enter the OTP (<b>'.$otp.'</b>) to verify your email address.</p>
                <p style="word-wrap:break-word;">The OTP will expire in 48 hours.</p>';

				$message = getMailTemplate('email_template',array('email_content'=>$email_content));
				$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($postData["email"])),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

				$this->response(array("status"=>"success","userid"=>$userArr[0]['id'],"otp"=>$otp,"user"=>$userArr[0]['first_name'], "user_token"=>$userArr[0]['user_token'], "msg"=> "Your New OTP has been sent to your registered Email, Please verify your account."), 200);
        }else{
				$this->response(array("status"=>"error", "msg"=> "Oops! Something went wrong. Please try again."), 200);
        }


    }

    function registerUser_post()
    {
        $postData = $_POST;

        if(trim($postData["first_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
        }/*else if(trim($postData["last_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
        }*/else if(trim($postData["country"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please choose your country."), 200);
        }/*else if(trim($postData["mobile_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
        }*/else if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email id to register."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
        }
        $emailArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"email_verified"=>'0'));


        if(count($emailArr) > 0){
        	$ids = array_map(function($users) { return $users["id"]; },$emailArr);
        	$this->Common_model->delete_batch(TB_USERS,'id',$ids);
        }

        $emailArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "There is an account with this email address. Please share different email id."), 200);
        }
        else
        {
            $date = date("Y-m-d H:i:s");
            $user_token = md5(uniqid(rand(), true));
			$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randstring = '';
			for ($i = 0; $i < 12; $i++) {
				$randstring.= $characters[rand(0, strlen($characters))];
			}
			$otp = generateOTP();
            $insertArr = array(
                                "password"      =>md5($postData["password"]),
                                "user_token"    =>$user_token,
                                "first_name"    =>trim($postData["first_name"]),
								//"last_name"    =>trim($postData["last_name"]),
                                "email"         =>trim($postData["email"]),
                                "phone"  =>trim($postData["mobile_no"]),
                                "country_code"  =>trim($postData["country_code"]),
                                "country_isocode"  =>trim($postData["country_isocode"]),
                                "country"     =>trim($postData["country"]),
                                "otp" => $otp,
								"created_at"  => $date,
								"otp_generated_at"  => $date,
                                "updated_at"  => $date,
								"del_status"=>"no",
                            );

            if($postData["device_token"]!="")
			{
				$insertArr["device_token"] = trim($postData["device_token"]);
			}
			if($postData["device_type"]!="")
			{
				$insertArr["device_type"] = trim($postData["device_type"]);
			}
            $user = $this->Common_model->insert(TB_USERS,$insertArr);
        }
        if($user)
        {
        		$subject = "Verification Email | Touchdown Nigeria";
        		$email_content = '<p>Hello '.$postData["first_name"].',</p>

                <p style="word-wrap:break-word;">Thank you for showing an interest to create an account with Touchdown Nigeria.</p>
                <p style="word-wrap:break-word;">Please enter the OTP (<b>'.$otp.'</b>) to verify your email address.</p>
                <p style="word-wrap:break-word;">The OTP will expire in 48 hours.</p>';

				$message = getMailTemplate('email_template',array('email_content'=>$email_content));
				$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($postData["email"])),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

				$this->response(array("status"=>"success","userid"=>$user,"otp"=>$otp,"user"=>$user, "user_token"=>$user_token, "msg"=> "OTP has been sent on your registered Email, Please verify your account."), 200);
        }else{
				$this->response(array("status"=>"error", "msg"=> "Oops! Something went wrong. Please try again."), 200);
        }

    }
    public function getPassword_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your registered email id."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
		if(count($userArr)>0)
        {

            $emailVerified = $this->Common_model->select('*',TB_USERS,array("email"=>trim($postData["email"]),"email_verified" => '1'));

            /*print_r($emailVerified);

            exit();*/

            if(count($emailVerified) > 0)
            {



				$user_token = md5(uniqid(rand(), true));
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 7; $i++) {
                    $randstring.= $characters[rand(0, strlen($characters))];
                }
                $subject = "Requesting for reset password | Touchdown Nigeria";
                $email_content='<br/>
                Hello '.$userArr[0]['first_name'].'<br/>
                <p>You have requested for a change of password.</p>
                <p>Your new password is: <b>'.$randstring.'</b></p>';

                $message = getMailTemplate('email_template',array('email_content'=>$email_content));
                $res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($postData["email"]),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
                $this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("password"=>md5($randstring)));
                $this->response(array("status"=>"success", "msg"=> "Password sent to your email, please check your inbox.",), 200); exit;
            }
            else
            {
                $this->response(array("status"=>"error", "msg"=> "Sorry your email id is not verified"), 200);
            }

		}else{
    		$this->response(array("status"=>"error", "msg"=> "We did not find an account for this email id. Please check or click on register to sign up. "), 200);
    	}
    }

	function updateUser1_post()
    {
        $postData = $_POST;

		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["first_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
				}/*else if(trim($postData["last_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
				}*/else if(trim($postData["country"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please choose your country."), 200);
				}/*else if(trim($postData["mobile_no"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
				}*/else if(trim($postData["email"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
				}/*else if(trim($postData["gender"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your gender."), 200);
				}else if(trim($postData["date_of_birth"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your date_of_birth."), 200);
				}*/
				/*else if(trim($postData["password"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}*/

				$emailArr=$this->Common_model->select("*",TB_USERS,array("id != "=>trim($userArr[0]["id"]),"email"=>trim($postData["email"])));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);

				}
				else
				{
					$date = date("Y-m-d H:i:s");
					$insertArr = array(
										"first_name"    =>trim($postData["first_name"]),
										//"last_name"    =>trim($postData["last_name"]),
										"email"         =>trim($postData["email"]),
										//"gender"         =>trim($postData["gender"]),
										//"date_of_birth"  =>date('Y-m-d',strtotime($postData["date_of_birth"])),
										"phone"  =>trim($postData["mobile_no"]),
										"country"     =>trim($postData["country"]),
										"updated_at"  => $date,
									);
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Profile updated successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}

    }
	function updateUser_post()
    {
        $postData = $_POST;
         $emailMessage = '';
         $flag = 0;

		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));

            if(trim($postData["email"]) == $userArr[0]["email"])
            {
                $mailHeader = 'Mail for Profile Update';
                $emailMessage = '<p>Your profile has been updated.</p>';
                $flag = 0;
            }
            else
            { 
                $mailHeader = 'Mail for Profile Update';
                $emailMessage = '<p>Your Email Address has been updated.</p> <p> Name <b>'.$postData['first_name'].'</b></p>
                    <p>Country <b>'.$postData["country"].'</b></p>
                    <p>Email <b>'.$postData["email"].'</b></p>
                    <p>Mobile No. <b>'.$postData["mobile_no"].'</b></p>';

                $flag = 1;
            }
			if(count($userArr) > 0)
			{
				if(trim($postData["first_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
				}/*else if(trim($postData["last_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
				}*/else if(trim($postData["country"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please choose your country."), 200);
				}else if(trim($postData["mobile_no"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
				}else if(trim($postData["email"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
				}/*else if(trim($postData["gender"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your gender."), 200);
				}else if(trim($postData["date_of_birth"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your date_of_birth."), 200);
				}*/
				/*else if(trim($postData["password"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}*/
				$insertArr = array(
										"first_name"    =>trim($postData["first_name"]),
										//"last_name"    =>trim($postData["last_name"]),
										"email"         =>trim($postData["email"]),
										//"gender"         =>trim($postData["gender"]),
										//"date_of_birth"  =>date('Y-m-d',strtotime($postData["date_of_birth"])),
										"phone"  =>trim($postData["mobile_no"]),
										"country"     =>trim($postData["country"]),
										"updated_at"  => $date,
									);
				//picture
				//echo "<pre>";print_r($_FILES);die();
				$files=$_FILES['picture'];
				/*if($files['name'])
				{
					$userfile_extn = explode(".", strtolower($files['name']));
					$ext=$userfile_extn[1];
					$allowed=array('gif','jpg','jpeg','png');
					if(!in_array($ext,$allowed))
					{
						$this->response(array("status"=>"error","msg"=> "The file is invalid. Please choose different image.","field"=>"picture"));	 exit;
					}
					if($files['size'] > 5242880)//2097152
					{
						$this->response(array("status"=>"error","msg"=> "The file is invalid. Please choose different image.","field"=>"picture"));exit;
					}
					$new_image_name=uniqid(rand(), true).time().'.'.$ext;
					$quality=40;
					$source=$files['tmp_name'];
					$destination='./images/profile/'.$new_image_name;
					$info = getimagesize($source);
					if ($info['mime'] == 'image/jpeg')
						$image = imagecreatefromjpeg($source);
					elseif ($info['mime'] == 'image/gif')
						$image = imagecreatefromgif($source);
					elseif ($info['mime'] == 'image/png')
						$image = imagecreatefrompng($source);
					imagejpeg($image, $destination, $quality);
					$insertArr['picture']=$new_image_name;
				}
				else
				{
					$this->response(array("status"=>"error", "msg"=> "Please upload profile image."), 200);
				}*/

				$emailArr=$this->Common_model->select("*",TB_USERS,array("id != "=>trim($userArr[0]["id"]),"email"=>trim($postData["email"])));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "There is an account with this email address. Please share different email id. "), 200);

				}
				else
				{
					$date = date("Y-m-d H:i:s");


                    $email = $userArr[0]["email"];

					$update = $this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);

                    if($update)
                    {

                        $subject = $mailHeader." | Touchdown Nigeria";
                        $email_content = '<p>Hello '.$postData["first_name"].',</p>';
                        $email_content .= $emailMessage;
                        


                        $message = getMailTemplate('email_template',array('email_content'=>$email_content));
                        $res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($email)),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

                        if(1 == $flag)
                        {
                            $res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($postData["email"])),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);    
                        }

                        

                        $this->response(array("status"=>"success","userid"=>$userArr[0]["id"],"user"=>$postData["first_name"], "user_token"=>$userArr[0]["user_token"], "msg"=> "Profile updated successfully."), 200);

                    }


				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}

    }
	function getUser_post()
    {
        $postData = $_POST;

		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{


				/*if($userArr[0]["picture"] != ""){
					$filename = 'images/profile/'.$userArr[0]["picture"];
					if(file_exists($filename)){
						$userArr[0]["picture"] = base_url()."images/profile/".$userArr[0]["picture"];
					}else{
						$userArr[0]["picture"] = base_url()."images/no-image-mob.png";
					}
				}else{
					$userArr[0]["picture"] = base_url()."images/no-image-mob.png";
				}*/
				$this->response(array("status"=>"success", "msg"=> "User found.","user"=>$userArr), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Oops! Something went wrong. Please try again.","flag"=>"logout"), 200);
			}
		}

    }

	function changePassword_post()
    {
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id,password",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			//echo md5($postData["current_password"]);
			if(count($userArr) > 0)
			{

				if($postData["current_password"] == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter password for your registered email account."), 200);
				}

                else if(substr_count($postData["current_password"], ' ') > 2)
                {
                    $this->response(array("status"=>"error", "msg"=> "Please enter valid current password."), 200);
                }
				else if($postData["password"] == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter new password"), 200);
				}
                else if(substr_count($postData["password"], ' ') > 2)
                {
                    $this->response(array("status"=>"error", "msg"=> "Please enter valid new password."), 200);
                }
				else if(($userArr[0]["password"])==md5($postData["password"]))
				{
					$this->response(array("status"=>"error", "msg"=> "Your current password and new password should be different."), 200);
				}
				else if(($userArr[0]["password"])!=md5($postData["current_password"]))
				{
					$this->response(array("status"=>"error", "msg"=> "It doesn’t match to our records. Are you sure you entered the current password for your account?"), 200);
				}
				else
				{
					$insertArr = array("password"=>md5($postData["password"]));
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Password changed successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
    }

	function deactivateMe_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["reason"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your reason."), 200);
				}
				else
				{
					$insertArr = array("del_status"=>'yes');
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Your account deactivated successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}

	function logout_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				$insertArr = array("device_type"=>null,"device_token"=>null);
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
				$this->response(array("status"=>"success","msg"=> "Logged out successfully."), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
	}



	function getList_post()
	{
		$postData = $_POST;
        // echo "<pre>";print_r($_POST);die;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);
				}
				else
				{
					$module=array(
								  'airport_dropdown'=>array('table'=>TB_AIRPORTS,'field'=>array('id'=>'DISTINCT '.TB_AIRPORTS.'.id','name'=>'name','fs'=>'fs','iata'=>'iata','icao'=>'icao','cityCode'=>'cityCode','countryCode'=>'countryCode','region'=>'region','latitude'=>'latitude','longitude'=>'longitude',),'where'=>array(TB_ADMIN.'.role'=>2),'order'=>array("name"=>"ASC"),'join'=>array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id')),
								  'airports_list'=>array('table'=>TB_AIRPORTS,'field'=>array('id'=>'DISTINCT '.TB_AIRPORTS.'.id','name'=>'name',"bio"=>"bio","address"=>"address","state"=>"state","city"=>"city","country"=>"country","zipcode"=>"zipcode","latitude"=>"latitude","longitude"=>"longitude","email"=>TB_AIRPORTS.".email","phone"=>TB_AIRPORTS.".phone"),'where'=>array(TB_ADMIN.'.role'=>2),'order'=>array("name"=>"ASC"),'join'=>array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id')),
								  'facility_list'=>array('table'=>'tbl_facility','field'=>array(),'default_column'=>'facility_id','column_name'=>'facility_id','where'=>array(TB_FACILITY.".airport_id"=>$postData['airport_id'],TB_FACILITY.".terminal_id"=>$postData['terminal_id']),'order'=>array("facility_id"=>"DESC"),'join'=>array()),
								  'store_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array(/*"store_category_id"=>14,*/"airport_id"=>$postData['airport_id']),'order'=>array("store_id"=>"DESC"),'join'=>array()),
								  'restorent_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array("store_category_id"=>15,"airport_id"=>$postData['airport_id']),'order'=>array("store_id"=>"DESC"),'join'=>array()),
								  "store_product_category_list"=>array(
																	 'table'=>TB_STORE_FACILITY,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),
																	 'order'=>array("store_facility_id"=>"DESC"),
																	 'join'=>array()
																	 ),
									"store_offer_list"=>array(
																	 'table'=>TB_STORE_OFFER,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),
																	 'order'=>array("store_offer_id"=>"DESC"),
																	 'join'=>array(TB_STORES => TB_STORES.".store_id = ".TB_STORE_OFFER.".store_id")
																	 ),
								  );
					$moduleArr=$module[$postData['module']];

                    /*print_r($moduleArr);

                    exit();*/

                    if(!isset($moduleArr))
					{
						$this->response(array("status"=>"error", "msg"=> "Invalid module name."), 200);
					}
					$select='';
					$where=$moduleArr['where'];
					$order=$moduleArr['order'];
					$default_column='';
					$column_name='';
					$column='';
					$order='';
					$default_order_column='';
					if(count($moduleArr['order']))
					{
						foreach($moduleArr['order'] as $k => $v)
						{
							$column=$k;
							$order=$v;
							$default_order_column=$k;
						}
					}
					if(count($moduleArr['field']))
					{
						foreach($moduleArr['field'] as $k => $v)
						{
							if($k)
							{
								$select.=','.$v;
								$default_column=$v;
								$column_name=$v;
							}
							else
							{

								$select.=$v;
							}
						}
					}
					else
					{
						$select="*";
						$default_column=$moduleArr['default_column'];
						$column_name=$moduleArr['column_name'];
					}


                    $join = array();
					$arrayColumn[] = $moduleArr['field'][0];
					$arrayStatus["is_active"] = array();
					$arrayColumnOrder = array("ASC","asc","DESC","desc");
					$post=array('page'=>$postData['page'],'column'=>$column,'order'=>$order);
					if($postData['module'] == "airport_dropdown"){

                  $result['rows'] = $this->Common_model->select('*',TB_AIRPORTS);
                    //$result['rows'] = $this->Common_model->selectQuery('DISTINCT '.TB_AIRPORTS.'.id,name,fs,iata,icao,cityCode,countryCode,region,latitude,longitude',TB_AIRPORTS,array(),array("name" => "ASC"),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
                    // echo $this->db->last_query();
                    //
                    // exit();
            	   //  Query for Hit count Sort data

						//$result['rows'] = $this->Common_model->selectQuery('DISTINCT '.TB_AIRPORTS.'.id,name,fs,iata,icao,cityCode,countryCode,region,latitude,longitude,'.TBL_AIRPORT_COUNT.'.hit_count',TB_AIRPORTS,array(),array(TBL_AIRPORT_COUNT.".hit_count" => "DESC"),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id',TBL_AIRPORT_COUNT=>TBL_AIRPORT_COUNT.'.airport_id ='.TB_AIRPORTS.'.id'),array(TB_ADMIN => 'LEFT',TBL_AIRPORT_COUNT => 'LEFT'));
                    }

                    if($postData['module'] == "facility_list"){
                        //$join = array(TB_TERMINAL => TB_TERMINAL.".terminal_id = ".TB_FACILITY.".terminal_id");
                        //$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$join,$column);//,array(),'',10
                        //
                                           
                        $result['rows'] = $this->Common_model->getData($postData['airport_id'],$postData['terminal_id']);

                        //exit();
                       // 
                        /*if(0 == $postData['terminal_id'])
                        {
                            $result['rows'] = $this->Common_model->select('*',TB_FACILITY,array('airport_id' =>$postData['airport_id']));
                        }
                        else
                        {
                            $result['rows'] = $this->Common_model->select('*',TB_FACILITY,array('airport_id' =>$postData['airport_id'],'terminal_id' => $postData['terminal_id']));
                        }*/
                        

                        //echo $this->db->last_query();
                    }

                    if($postData['module'] == "airports_list" || $postData['module'] == "store_product_category_list"){


						$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$join,$column);//,array(),'',10
                        /*echo $this->db->last_query();
                        exit();*/
					}

                   elseif($postData['module'] == "restorent_list" || $postData['module'] == "store_list" || $postData['module'] == "store_list" || $postData['module'] == "store_offer_list")
                   {

                    if($postData['module'] == "store_list")
                    {
                        $where['store_category_id'] = 14;   
                    }
                    if(0 != $postData['terminal_id'])
                    {
                        $where['terminal_id'] = $postData['terminal_id'];

                    }

                    if($postData['module'] == "store_offer_list")
                    {
                        
                        $select = TB_STORE_OFFER.".store_id as id,offer_name";
                        $where['store_id'] = $moduleArr['where']['store_id'];

                        $join = $moduleArr['join']['tb_stores'];

                        $arrayColumn[] = $moduleArr['field'][0];
                        $arrayStatus["is_active"] = array();
                        $arrayColumnOrder = array("ASC","asc","DESC","desc");
                        $post=array('page'=>$postData['page'],'column'=>$column,'order'=>$order);

                        /*print_r($column);
                        exit();*/
                        
                    }
                    
                    
                     $result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$join,$column);//,array(),'',10
                        /*echo $this->db->last_query();
                        exit();*/
                    }
                    /*
                                        echo $this->db->last_query();

                                        exit();
                    */
                    /*print_r($result['rows']);

                    exit();*/

					if(count($result['rows']))
					{

						//$this->response(array("status"=>"success","msg"=> count($result['rows'])." records found.",'list'=>$result['rows']),200);




                        $resData =array();
                        $i = 0;
						// $this->response(array("status"=>"success","msg"=> $result['entries'],'list'=>$result['rows']), 200);
					   if($postData['module'] == "facility_list")
					    {


	                        $k = 0;
	                        $all_data=array();
                            foreach ($result['rows'] as $keyf => $valuef) {
                                $allheader[] = array("name"=>$valuef['terminal_name']);
                                $subitems = explode("###", $valuef['facility_name']);
                                $facility_desc = explode("###", $valuef['facility_desc']);
                                $facility_id = explode("###", $valuef['facility_id']);
                                $airport_id = explode("###", $valuef['airport_id']);
                                $terminal_id = explode("###", $valuef['terminal_id']);
                                $facility_images = explode("###", $valuef['facility_images']);

                                $cond = array('terminal_id'=>$valuef['terminal_id']);

                                $terminal = $this->Common_model->select("facility_id,airport_id,terminal_id,facility_name,facility_desc,facility_images",TB_FACILITY,$cond);
                                //echo "<pre>";print_r($terminal);//die;
                                $fac_data['facility_id']=array();
                                $fac_data['airport_id']=array();
                                $fac_data['terminal_id']=array();
                                $fac_data['facility_name']=array();
                                $fac_data['facility_desc']=array();
                                foreach ($terminal as $valueimg) {
                                	$fimages = explode(',', $valueimg['facility_images']);
                                	if($fimages !='')
                                	{$fac_data['facility_images']=array();
                                		foreach ($fimages as $valuei) {
                                			if($valuei !='')
                                			{
                                				$fac_images = base_url()."images/facility/".$valuei;
	                                			$fac_data['facility_images'][]=array("img"=>$fac_images);
                                			}


	                                	}

                                		$fac_data['facility_id']=$valueimg['facility_id'];
                                		$fac_data['airport_id']=$valueimg['airport_id'];
                                		$fac_data['terminal_id']=$valueimg['terminal_id'];
                                		$fac_data['facility_name']=$valueimg['facility_name'];
                                		$fac_data['facility_desc']=$valueimg['facility_desc'];
                                		$all_data[$valueimg['terminal_id']][]=$fac_data;
                                	}else{
                                		$fac_data['facility_images'] = '';
                                		$fac_data['images']=array();
                                		$fac_data['facility_id']=$valueimg['facility_id'];
                                		$fac_data['airport_id']=$valueimg['airport_id'];
                                		$fac_data['terminal_id']=$valueimg['terminal_id'];
                                		$fac_data['facility_name']=$valueimg['facility_name'];
                                		$fac_data['facility_desc']=$valueimg['facility_desc'];
                                	}

                                }


                                $k++;
                            }
                            $tes = array();
                           foreach($all_data as $key => $val){
						    $tes[$key]['terminal'] = $val;
							}
							$t = array();
							foreach ($tes as $key => $value) {
								$t[] =  $value;
							}

                            $subpoints= array('terminal'=>$all_data);
                            $ugdata=array("header_data"=>$allheader,"subdata"=>$t);

                            /*print_r($ugdata);

                            exit();*/
                            // $faqdata = array_merge($headerdata,$subpoints);


                            $this->response(array("status"=>"success","msg"=> count($result)." records found.",'data'=>$ugdata),200);
                        }
                        else
                        {
                            $this->response(array("status"=>"success","msg"=> $result['entries'],'list'=>$result['rows']), 200);
                        }

                    }
					else
					{
						$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
					}
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}

	function getDetail_post()
	{
		$postData = $_POST;

		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));

			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);exit;
				}
				else
				{
					if($postData["module"]=='homescreen_images1')
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>array(0=>base_url().'images/homescreen/1.jpg',1=>base_url().'images/homescreen/2.jpg',2=>base_url().'images/homescreen/3.jpg',3=>base_url().'images/homescreen/4.jpg',4=>base_url().'images/homescreen/5.jpg')), 200);
					}
					$module=array(
								'store_detail'=>array(
										'table'=>TB_STORES,
                                        'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>
                                            "store_thumb"),
										// 'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"                                          ),
										'where'=>array("store_category_id"=>14,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								'restorent_detail'=>array(
										'table'=>TB_STORES,
                                        'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>
                                            "store_thumb"),
										// 'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"),
										'where'=>array("store_category_id"=>15,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								'airport_images'=>array(
										'table'=>TB_AIRPORTS,
                                        'field'=>array("interior"=>"interior","exterior"=>"exterior"),
										// 'field'=>array("interior"=>"CONCAT('".base_url()."images/airport/interior/',interior) as interior","exterior"=>"CONCAT('".base_url()."images/airport/exterior/',exterior) as exterior"),
										'where'=>array("id"=>$postData['airport_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								//'homescreen_images'=>array(
								//		'table'=>TBL_HOMESCREEN,
								//		'field'=>array("image"=>"CONCAT('".base_url()."images/homescreen/',image) as image"),
								//		'where'=>array(),
								//		//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
								//		),
								);


					$moduleArr=$module[$postData['module']];
					$dataArr=getModuleData($moduleArr);

                    /*echo $this->db->last_query();

                    exit();*/


                    // ---- static code for images ---
                    // $st_images=array("homescreen_images"=>array(
                    //     array("image"=>"http://touchdownnigeria.net/admin/images/homescreen/11712127645979c76ae6be01.690413531501153130.jpeg"),
                    //     array("image"=>"http://touchdownnigeria.net/admin/images/homescreen/10491612635979c778c91839.758231211501153144.jpeg"),
                    //     array("image"=>"http://touchdownnigeria.net/admin/images/homescreen/6407245835979c78cecb938.929561971501153164.jpeg"),
                    //     array("image"=>"http://touchdownnigeria.net/admin/images/homescreen/13133270825971a4362a8e73.222880731500619830.jpeg"),
                    //     array("image"=>"http://touchdownnigeria.net/admin/images/homescreen/18725866355979c7a0ed1417.093388271501153184.jpeg")));

                    // $new_dataArr = array_merge($dataArr[0],$st_images);


                    // ------------ multiple images here ----------------
                    $store_image = explode(",", $dataArr[0]['store_thumb']);
                    foreach ($store_image as $keyi => $valuei) {
                        $allimg[] = array("image"=>base_url()."images/store/".$valuei);
                    }
                    $st_images=array("homescreen_images"=>$allimg);
                    // $new_dataArr = array_merge($dataArr[0],$st_images);

                    if($postData["module"]=='airport_images'){
                        $new_dataArr = $dataArr[0];
                    }else{
                         $new_dataArr = array_merge($dataArr[0],$st_images);
                    }

					// Vadhaman's Code

					/*if(count($moduleArr['subtable']))
					{
						foreach($moduleArr['subtable'] as $k => $v)
						{
							$dataArr[0][$k]=getModuleData($v);
						}
					}*/

					// Vadhaman's Code end

					/*print_r($moduleArr['subtable']);

					exit;*/
					//echo "<pre>";print_r($dataArr);
					//$arrayColumn = $moduleArr['field'];
					//$arrayStatus["is_active"] = array();
					//$arrayColumnOrder = array("ASC","asc","DESC","desc");
					//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where);
					//if(count($dataArr))

					// Vardhamans Code

					/*if($dataArr[0]["interior"] != "" && $dataArr[0]["exterior"] != "")
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>$dataArr[0]), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
					}*/

					// Vadhaman's Code end

                    /*echo '<pre>';

                    print_r($new_dataArr);

                    exit();*/

					if($new_dataArr)
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>$new_dataArr), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
					}




				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}
	function getHomescreen_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{

			$dataArr=$this->Common_model->select("CONCAT('".base_url()."images/homescreen/',image) as image",TBL_HOMESCREEN,array('image !='=>''));
			if(count($dataArr))
			{
				$this->response(array("status"=>"success","msg"=> "","homescreen_images"=>$dataArr), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
			}
		}
	}
	function getPage_post()
	{
		$postData = $_POST;
		if(trim($postData["page"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter page."), 200);
        }
		$dataArr = $this->Common_model->select('page_title,page_detail',TB_PAGES,array("page_name"=>$postData["page"]));
		//if($postData["page"]=='contact_us')
		//{
		//	//echo "<pre>";print_r($dataArr);die();
		//	$page_detail=json_decode($dataArr[0]['page_detail'],'ASSOC');
		//	//$page_detail['page_title'] = $dataArr[0]['page_title'];
		//	//echo "<pre>";print_r($page_detail);die();
		//	//$dataArr1=array('page_title'=>$dataArr[0]['page_title'],'page_detail'=>$page_detail);
		//	//$page_detail['page_title']=$dataArr[0]['page_title'];
		//	$this->response(array("status"=>"success","msg"=> "Page detail found.",'page'=>$page_detail), 200);
		//}
		//else
		{
			$this->response(array("status"=>"success","msg"=> "Nothing here yet. Please check back soon!",'page'=>$dataArr), 200);
		}
	}
	function getNotifications_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			//TB_NOTIFICATION
			//view_status
			$notifications = $this->Common_model->selectQuery("*",TB_NOTIFICATION,array(TB_USER_NOTIFICATION.'.user_id' => $user[0]['id']),array(),array(TB_USER_NOTIFICATION=>TB_USER_NOTIFICATION.'.notification_id ='.TB_NOTIFICATION.'.notification_id'));

            /*echo $this->db->last_query();

            exit();*/
              //echo "<pre>";print_r($notifications);die;
			// $this->db->select('*, count(not_id) as cnt');//
			// $this->db->where('userid',$user[0]["id"]);
   //          $this->db->where('view_status',0);
			// $this->db->group_by('not_token');
			// $this->db->order_by('not_id', 'desc');
			// $query=$this->db->get(TB_NOTIFICATION_ALERT);
   //          // echo $this->db->last_query();die;
			// $notifications=$query->result_array();
            $not_array = array();
              foreach ($notifications as $key => $value) {
                  $not_array[] = array('not_id'=>$value['notification_id'],
                    'not_title'=>$value['notification_text'],
                    'view_status'=>$value['view_status'],
                    'type'=>$value['noti_type'],
                    'create_date'=>date('m-d-Y h:i:s',strtotime($value['created_date']))
                  );
              }

			//echo "<pre>";print_r($not_array);die();
			if(count($not_array))
			{
				//$this->Common_model->update(TB_NOTIFICATION_ALERT,array("userid"=>trim($user[0]["id"])),array("view_status"=>1));
				$this->response(array("status"=>"success","notification"=>$not_array,"msg"=> "You have ".count($not_array)." new notifications.","total_cnt"=>count($not_array)), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!","total_cnt"=>0), 200);
			}
		}
	}
	function removeMySeenNotification_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		if(trim($postData["not_id"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter notification id."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			$cond=array("notification_id"=>$postData["not_id"],"user_id" =>$user[0]['id']);
			//if($postData["not_id"]!="")
			//{
			//$cond['not_id']=$postData["not_id"];
			//}
			//$this->Common_model->delete(TB_NOTIFICATION_ALERT,$cond);
			$this->Common_model->delete(TB_USER_NOTIFICATION,$cond);
			$this->response(array("status"=>"success","msg"=> "Notification deleted successfully."), 200);

		}
	}


    function deleteAllNotification_post()
    {        $postData = $_POST;
        if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
        $user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(!count($user))
        {
            $this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
        }
        else
        {
            $deleteNotification = $this->common->deleteAll(TB_NOTIFICATION);
            $this->response(array("status"=>"success","msg"=> "Notification deleted successfully."), 200);

        }
    }

    function updateMyNotification_post()
    {
        $postData = $_POST;
        if("" == $postData['user_token'])
        {
            echo $this->response(array('status' => 'error' ,'msg' => 'Please enter user token'));
        }
        if("" == $postData['noti_id'])
        {
            echo $this->response(array('status' => 'error' ,'msg' => 'Please enter notification id'));
        }
        else
        {
            $user = $this->Common_model->select('id',TB_USERS,array('user_token' => $postData['user_token']));




           if(count($user) > 0)
           {

                $update = $this->Common_model->update(TB_USER_NOTIFICATION,array('notification_id' => $postData['noti_id'],'user_id' => $user[0]['id']),array('view_status' => '1'));

                if($update)
                {
                    echo $this->response(array('status' => 'success','msg' => 'Status updated sucessfully'));
                }
                else
                {
                    echo $this->response(array('status' => 'error','msg' => 'Something went wrong'));   
                }
           }
           else
           {
                echo $this->response(array('status' => 'error' , 'msg' => 'no user found'));
           }
        }
    }


    function removeMyAllNotifications_post()
    {

        $postData = $_POST;
        if("" == $postData['user_token'])
        {
            echo $this->response(array('status' => 'error' ,'msg' => 'Please enter user token'));
        }
        
        else
        {
             $user = $this->Common_model->select('id',TB_USERS,array('user_token' => $postData['user_token']));
        }

        if(count($user) > 0)
           {
                $notificationId = $this->Common_model->select("*",TB_USER_NOTIFICATION,array('user_id' => $user[0]['id']));

                $remove = $this->Common_model->delete(TB_USER_NOTIFICATION,array('user_id' => $user[0]['id']));


                if($remove > 0)
                {
                    echo $this->response(array('status' => 'success','msg' => 'Status deleted sucessfully'));
                }
                else
                {
                    echo $this->response(array('status' => 'error','msg' => 'Something went wrong'));   
                }
           }
           else
           {
                echo $this->response(array('status' => 'error' , 'msg' => 'no user found'));
           }


    }

	function getAirLineCode_get(){

		$dataArr = $this->Common_model->select('details',TBL_AIRLINE_CODE,array("id"=>1));
		$this->response(array("status"=>"success","details"=> $dataArr[0]['details']), 200);
	}

	function getFaq_post()
	{
		$postData = $_POST;
		$select='';
		$where=array();
		$order=array();
		$default_column='';
		$column_name='';
		$arrayColumn[] = array('faq_id'=>'faq_id','faq_title'=>'faq_title','faq_detail'=>'faq_detail');
		$arrayStatus["is_active"] = array();
		$arrayColumnOrder = array("ASC","asc","DESC","desc");
		$post=array();//array('page'=>$postData['page'],'column'=>($postData['column']!='')?$postData['column']:null,'order'=>($postData['order']!="")?$postData['order']:null);
		$result=$this->Common_model->select("*",TB_FAQ);

		//$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,'faq_id','faq_title',TB_FAQ,"*",null,$where);//,array(),'',10
		if(count($result))
		{
            foreach ($result as $keyf => $valuef) {
                $allheader[] = array("name"=>$valuef['faq_title']);
                $subitems = explode("###", $valuef['faq_detail']);
                $subarray=array();
                foreach ($subitems as $keys => $values) {
                    $subarray[]=array("name"=>$values);
                }
                $subpoints[] = array('travel'=>$subarray);

            }
            $faqdata=array("header_data"=>$allheader,"subdata"=>$subpoints);
            // $faqdata = array_merge($headerdata,$subpoints);


            $this->response(array("status"=>"success","msg"=> count($result)." records found.",'data'=>$faqdata),200);

			//$this->response(array("status"=>"success","msg"=> count($result)." records found.",'list'=>$result), 200);



            // $json =  json_encode(array("status"=>"success","msg"=> count($result)." records found.",'list'=>$result));

            // //$json = json_encode($result[0]['faq_detail'],JSON_UNESCAPED_SLASHES);

            // $json = preg_replace('/(\s+)?\\\t(\s+)?/', '', $json);
            // $json = preg_replace('/(\s+)?\\\n(\s+)?/', '', $json);
            // //$json = str_replace("\\","",$json);
            // //$json = str_ireplace('\r\n', '', urldecode($json));
            // echo $json;

		}
		else
		{
			$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
		}
	}
	function giveMeAirportAlerts_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			/*if(!count($postData["airport_id"]))
			{
				$this->response(array("status"=>"error","msg"=> "Please enter airport id."), 200);
			}
			else
			{*/
				$this->Common_model->delete(TBL_AIRPORT_ALERT_REQUEST,array("user_id"=>$user[0]['id']));
				$r=0;
				$insertArr = array();
				foreach($postData["airport_id"] as $k => $v)
				{
					$insertArr[] = array("airport_id"  => $v,"user_id"=>$user[0]['id']);

					/*$insertArr = array(
                                "airport_id"  => $v,
								"user_id"=>$user[0]['id'],
                            );
					$g=$this->Common_model->insert(TBL_AIRPORT_ALERT_REQUEST,$insertArr);
					if($g)
					{
						$r++;
					}*/
				}
				if(count($insertArr) > 0){
					$r = $this->Common_model->insert_batch(TBL_AIRPORT_ALERT_REQUEST,$insertArr);
					if($r)
					{
						$this->response(array("status"=>"success","msg"=> "Thank you. We have received your request for airport alert. "), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "Oops! Something went wrong. Please try again."), 200);
					}
				}else{
					$this->response(array("status"=>"success","msg"=> "Thank you. We have received your request for airport alert."), 200);
				}
			//}
		}
	}
	function myAirportAlertRequests_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			$airportData=$this->Common_model->select('airport_id',TBL_AIRPORT_ALERT_REQUEST,array("user_id"=>$user[0]['id']));
			if(count($airportData)>0)
			{
				$this->response(array("status"=>"success","msg"=> "Request found.","airport"=>$airportData), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
			}
		}
	}
	function giveMeFlightAlerts_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			if(trim($postData["flight_number"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter flight number."), 200);
			}
			else if(trim($postData["airport_id"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please choose airport."), 200);
			}
			else if(trim($postData["flight_date"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please choose flight date."), 200);
			}
			else if(trim($postData["carrier"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please choose Airline."), 200);
			}
			else if(trim($postData["airport_code"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter airport code."), 200);
			}

			else
			{
				$insertArr = array(
							"flight_number"=>$postData["flight_number"],
							"airport_id"  => $postData["airport_id"],
							"user_id"=>$user[0]['id'],
							"flight_date"=>date("Y-m-d",strtotime($postData["flight_date"])),
							"carrier"=>$postData["carrier"],
							"airport_code"=>$postData["airport_code"],
							"depart_airport_code"=>$postData["depart_airport_code"],//$postData["type"],
						);

				$requestData=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,$insertArr);
				if(count($requestData)>0)
				{
					$this->response(array("status"=>"error","msg"=> "There is an active alert request for this flight number with us, Please enter a different flight number for alert."), 200);
				}
				else
				{
					$cond=array(
							"flight_number"=>$postData["flight_number"],
							"airport_id"  => $postData["airport_id"],
							//"user_id"=>$user[0]['id'],
							"flight_date"=>date("Y-m-d",strtotime($postData["flight_date"])),
							"carrier"=>$postData["carrier"],
							"airport_code"=>$postData["airport_code"],
							"depart_airport_code"=>$postData["depart_airport_code"],//$postData["type"],
							);
					$requestExist=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,$cond);
					if(count($requestExist)>0)
					{
						$insertArr['alert_token']=$requestExist[0]['alert_token'];
					}
					else
					{
						$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$randstring = '';
						for ($i = 0; $i < 12; $i++) {
							$randstring.= $characters[rand(0, strlen($characters))];
						}
						$insertArr['alert_token']=$randstring;
						//$url="https://api.flightstats.com/flex/alerts/rest/v1/json/create/".$postData["carrier"]."/".$postData["flight_number"]."/to/".$postData["airport_code"]."/arriving/".date("Y",strtotime($postData["flight_date"]))."/".date("m",strtotime($postData["flight_date"]))."/".date("d",strtotime($postData["flight_date"]))."?appId=".FLIGHT_STAT_appId."&appKey=".FLIGHT_STAT_appId."&type=JSON&deliverTo=".base_url()."cron";

						$url="https://api.flightstats.com/flex/alerts/rest/v1/json/create/".$postData["carrier"]."/".$postData["flight_number"]."/from/".$postData["depart_airport_code"]."/departing/".date("Y",strtotime($postData["flight_date"]))."/".date("m",strtotime($postData["flight_date"]))."/".date("d",strtotime($postData["flight_date"]))."?appId=".FLIGHT_STAT_appId."&appKey=".FLIGHT_STAT_appKey."&type=JSON&deliverTo=".base_url()."cron";
						$curl_handle=curl_init();
						curl_setopt($curl_handle,CURLOPT_URL,$url);
						curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
						curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'GET');
						curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,true);
						curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
						$buffer = curl_exec($curl_handle);
						$arr=json_decode($buffer);
						curl_close($curl_handle);
						//echo "<pre>";print_r($arr->error);
						if(isset($arr->error))
						{
							$this->response(array("status"=>"error","msg"=> $arr->error->errorMessage), 200);
						}

					}
					//echo "<pre>";print_r($insertArr);die();
					$r=$this->Common_model->insert(TBL_FLIGHT_ALERT_REQUEST,$insertArr);
					if($r)
					{
						$this->response(array("status"=>"success","msg"=> "Thank you. We have received your request for flight alert."), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "Oops! Something went wrong. Please try again."), 200);
					}
				}
			}
		}
	}
	function myFlightAlerts_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			$requestData=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,array('user_id'=>$user[0]['id']));
			if(count($requestData)>0)
			{
				$this->response(array("status"=>"success","msg"=> "Request found.","request"=>$requestData), 200);
			}
			else
			{
				$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
			}
		}
	}

	function removeMyFlightAlert_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
		$user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		if(!count($user))
		{
			$this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
		}
		else
		{
			if(trim($postData["id"])=="")
			{
				$this->response(array("status"=>"error","msg"=> "Please enter flight request id."), 200);
			}
			else
			{
				$requestData=$this->Common_model->select("*",TBL_FLIGHT_ALERT_REQUEST,array('id'=>$postData['id']));
				if(count($requestData)>0)
				{
					$this->Common_model->delete(TBL_FLIGHT_ALERT_REQUEST,array('id'=>$postData['id']));
					$this->response(array("status"=>"success","msg"=> "Request deleted successfully."), 200);
				}
				else
				{
					$this->response(array("status"=>"error","msg"=> "Invalid request id."), 200);
				}
			}
		}
	}


	function getFLightalerts_post()
	{
		$postData = $_POST;

		$dt=$this->common->insert(TBL_AIRLINE_CODE,array('details'=>json_encode($postData)));
		//$dataArr = $this->Common_model->select('details',TBL_AIRLINE_CODE,array("id"=>1));
		//$token_array1[0] = array('id'=>'112','device_token' => 'cD8fouQyyl8:APA91bF-yyxK3nZrUuVCMF_IdbvnovHQWioBNGC5uamBXCyXZ0YO-o39nvsRky6LX8lf3cUhqGXuofxdvFF7ejeshMGL7GBwS4ZVyQDPXRRDhXg8ZP_l0o__g_Z56wivp3GAdVeI4_hC');
		//$r = sendAndroidNotification($token_array1, "via api.");
		$res=sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array("vardhaman@exceptionaire.co"),array(ADMIN_EMAIL,ADMIN_NAME),"FLight API hit",json_encode($postData));
		$this->response(array("status"=>"success","msg"=> ""), 200);

	}


	function airportHitCount_post()
	{
		$postData = $_POST;
		if(trim($postData["airport_id"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please select airport."), 200);
        }





        $user = $this->Common_model->select("*",TBL_AIRPORT_COUNT,array("airport_id"=>trim($postData["airport_id"])));

        if($user)
        {

        	$count = $user[0]['hit_count'] + 1;

        	$this->Common_model->update(TBL_AIRPORT_COUNT,array("airport_id"=>$postData["airport_id"]),array("hit_count"=>$count));


        }
        else
        {
        	$data = array('airport_id'=>$postData["airport_id"],
        				   'hit_count' =>1
        		);

        	$dt=$this->common->insert(TBL_AIRPORT_COUNT,$data);
        	//echo $this->db->last_query();
        }

        $airport = $this->Common_model->selectQuery("*",TBL_AIRPORT_COUNT,array(),array("hit_count"=>"desc"),array(),array());


        for($i=0;$i<count($airport);$i++)
        {
        	if($airport[$i]['hit_count'] > 5)
	    	{

	    		$airport_data = $this->Common_model->select("id,name",TB_AIRPORTS,array("id"=>trim($airport[$i]['airport_id'])));

	    		$res[] = array("id" => $airport_data[0]['id'],
	    			"airport"=> $airport_data[0]['name'],
	    			"hit_count" =>$airport[$i]['hit_count']);


	    	}
        }

        $this->response(array("status"=>"success","result" => $res), 200);





	}

    public function addContact_post()
    {
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'subject' => $_POST['subject'],
            'comments' => $_POST['comments']
        );



       $insertData = $this->Common_model->insert(TBL_ADDCONTACT,$data);

        if($insertData)
        {
            $subject = "Contact Us | Touchdown Nigeria";
                $email_content = '<p>Hello '.$_POST['name'].',</p>

                <p>Thank you for contacting us.</p>
                ';

                $message = getMailTemplate('email_template',array('email_content'=>$email_content));
                $res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($_POST['email'])),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

                if($res)
                {
                    $this->response(array('status'=>'success',"msg" => "Information save sucessfully."));
                }

        }
        else
        {
            $this->response(array('status'=>'error',"msg" => "Oops something went wrong!"));
        }
    }


    public function addFeedback_post()
    {
        $postData = $_POST;

        if("" == $postData['user_id'])
        {
            $this->response(array('status' => 'error','message' => 'please enter user id'),200);
        }

        if("" == $postData['airport_id'])
        {
            $this->response(array('status' => 'error','message' =>'please enter airport id'),200);
        }

        if("" == $postData['name'])
        {
            $this->response(array('status' => 'error','message' =>'please enter user name'),200);
        }

        if("" == $postData['subject'])
        {
            $this->response(array('status' => 'error','message' =>'please enter subject'),200);
        }

        if("" == $postData['description'])
        {
            $this->response(array('status' => 'error','message' =>'please enter description'),200);
        }

        // check user is exist

        $validUser = $this->Common_model->select('id,email,first_name,last_name',TB_USERS,array('id' => $postData['user_id']));

        if(count($validUser) > 0)
        {

            //check Airport Exist

            $airportExist = $this->Common_model->select('id',TB_AIRPORTS,array('id' => $postData['airport_id']));

            if(count($airportExist) > 0)
            {


                $data = array(

                            'user_id' => $postData['user_id'],
                            'airport_id' => $postData['airport_id'],
                            'name' => $postData['name'],
                            'subject' => $postData['subject'],
                            'description' => $postData['description']
                    );

                $insertData = $this->Common_model->insert(TBL_FEEDBACK,$data);

                if($insertData)
                {

                    $subject = "Feedback | Touchdown Nigeria";
                    $email_content = '<p>Hello '.$validUser[0]["first_name"].',</p>

                    <p>Thank you for contacting us.</p>
                    ';

                    $message = getMailTemplate('email_template',array('email_content'=>$email_content));
                    $res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(trim($validUser[0]['email'])),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);



                    $subject = "Feedback | Touchdown Nigeria";
                    $email_content = '<p>Hello  Admin,</p>

                    <p>'.$validUser[0]["first_name"].', has submitted the feedback</p>
                    <p> Name : '.$validUser[0]["first_name"].' </p>
                    <p> Email : '.$validUser[0]["email"].' </p>
                    <p> Subject : '.$postData['subject'].' </p>
                    <p> Description : '.$postData['description'].' </p>
                    ';

                    $message = getMailTemplate('email_template',array('email_content'=>$email_content));

                    //Patch by Ram K
                     $adminRes = sentAdminEmail(array(trim($validUser[0]['email']),ADMIN_NAME), array(ADMIN_EMAIL),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);

                   // $adminRes = sentAdminEmail(array(trim($validUser[0]['email']),trim($validUser[0]["first_name"])), array(ADMIN_EMAIL),array(trim($validUser[0]['email']),trim($validUser[0]["first_name"])),$subject,$message);

                    if($res)
                    {
                        $this->response(array('status' => 'success' , 'message' => 'Feedback added Successfully'),200);
                    }


                }
                else
                {
                    $this->response(array('status' => 'error' , 'message' => 'something went wrong !!'),200);
                }
            }
            else
            {
                $this->response(array('status' => 'error','message' => 'airport does not exist'));
            }
        }
        else
        {
            $this->response(array('status' => 'error' , 'message' => 'User does not exist'),200);
        }


    }

    // ----------- get facility images -----------
    function getFacilityImages_post()
    {
        $postData = $_POST;
        if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
        $user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(!count($user))
        {
            $this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
        }
        else
        {
            if(trim($postData["facility_id"])=="")
            {
                $this->response(array("status"=>"error","msg"=> "Please enter facility id."), 200);
            }
            else
            {
                $requestData=$this->Common_model->select("facility_name,facility_images",TB_FACILITY,array('facility_id'=>$postData['facility_id']));
                if(count($requestData)>0)
                {
                    $fac_image = explode(",", $requestData[0]['facility_images']);
                    foreach ($fac_image as $keyf => $valuef) {
                        $allimg[] = array("image"=>base_url()."images/facility/".$valuef);
                    }
                    $facility_images=array("facility_images"=>$allimg);
                    $fac_data = array_merge($requestData[0],$facility_images);

                    $this->response(array("status"=>"success","status"=>"success","msg"=> "Request found.","data"=>$fac_data), 200);
                }
                else
                {
                    $this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
                }
            }
        }
    }


    function getUserGuide_post()
    {
        $postData = $_POST;
        $select='';
        $where=array();
        $order=array();
        $default_column='';
        $column_name='';
        $arrayColumn[] = array('ug_id'=>'ug_id','ug_title'=>'ug_title','ug_details'=>'ug_details');
        $arrayStatus["is_active"] = array();
        $arrayColumnOrder = array("ASC","asc","DESC","desc");
        $post=array();//array('page'=>$postData['page'],'column'=>($postData['column']!='')?$postData['column']:null,'order'=>($postData['order']!="")?$postData['order']:null);
        $result=$this->Common_model->select("*",TB_USER_GUIDE,array('airport_id'=>$postData['airport_id']));

        //$result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,'faq_id','faq_title',TB_FAQ,"*",null,$where);//,array(),'',10
        if(count($result))
        {
            foreach ($result as $keyf => $valuef) {
                $allheader[] = array("name"=>$valuef['ug_title']);
                $subitems = explode("###", $valuef['ug_details']);
                $subarray=array();
                foreach ($subitems as $keys => $values) {
                    $subarray[]=array("name"=>$values);
                }
                $subpoints[] = array('terminal'=>$subarray);

            }
            $ugdata=array("header_data"=>$allheader,"subdata"=>$subpoints);
            // $faqdata = array_merge($headerdata,$subpoints);


            $this->response(array("status"=>"success","msg"=> count($result)." records found.",'data'=>$ugdata),200);

            //$this->response(array("status"=>"success","msg"=> count($result)." records found.",'list'=>$result), 200);



            // $json =  json_encode(array("status"=>"success","msg"=> count($result)." records found.",'list'=>$result));

            // //$json = json_encode($result[0]['faq_detail'],JSON_UNESCAPED_SLASHES);

            // $json = preg_replace('/(\s+)?\\\t(\s+)?/', '', $json);
            // $json = preg_replace('/(\s+)?\\\n(\s+)?/', '', $json);
            // //$json = str_replace("\\","",$json);
            // //$json = str_ireplace('\r\n', '', urldecode($json));
            // echo $json;

        }
        else
        {
            $this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
        }
    }


    public function addInterest_post()
    {
        $postData = $_POST;
        if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
        $user = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(!count($user))
        {
            $this->response(array("status"=>"error","msg"=> "Invalid access token.","flag"=>"logout"), 200);
        }
        else
        {
            if($postData !='' || $postData['user_id'] || $postData['airport_id'])
            {
                $data = array(
                        'user_id' => $postData['user_id'],
                        'airport_id' => $postData['airport_id'],
                        // 'terminal_id' => $postData['terminal_id'],
                        );

                    $insertData = $this->Common_model->insert(TB_INTEREST,$data);
                    if($insertData)
                    {
                     $this->response(array('status' => 'success' , 'message' => 'Your interest added Successfully'),200);
                    }else{
                         $this->response(array('status'=>'error',"msg" => "Oops something went wrong!"));
                    }
            }
            else
            {
                $this->response(array('status'=>'error',"msg" => "Oops something went wrong!"));
            }
        }
    }


    function getTerminalList_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);
				}
				else
				{
					$module=array(

								  'airports_list'=>array('table'=>TB_AIRPORTS,'field'=>array('id'=>'DISTINCT '.TB_AIRPORTS.'.id','name'=>'name',"bio"=>"bio","address"=>"address","state"=>"state","city"=>"city","country"=>"country","zipcode"=>"zipcode","latitude"=>"latitude","longitude"=>"longitude","email"=>TB_AIRPORTS.".email","phone"=>TB_AIRPORTS.".phone"),'where'=>array(TB_ADMIN.'.role'=>2),'order'=>array("name"=>"ASC"),'join'=>array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id')),
								  'terminal_list'=>array('table'=>'tb_terminal','field'=>array(),'default_column'=>'terminal_id','column_name'=>'terminal_id','where'=>array(TB_TERMINAL.".airport_id"=>$postData['airport_id']),'order'=>array("terminal_id"=>"DESC"),'join'=>array()),

								  );
					$moduleArr=$module[$postData['module']];
					if(!isset($moduleArr))
					{
						$this->response(array("status"=>"error", "msg"=> "Invalid module name."), 200);
					}
					$select='';
					$where=$moduleArr['where'];
					$order=$moduleArr['order'];
					$default_column='';
					$column_name='';
					$column='';
					$order='';
					$default_order_column='';
					if(count($moduleArr['order']))
					{
						foreach($moduleArr['order'] as $k => $v)
						{
							$column=$k;
							$order=$v;
							$default_order_column=$k;
						}
					}
					if(count($moduleArr['field']))
					{
						foreach($moduleArr['field'] as $k => $v)
						{
							if($k)
							{
								$select.=','.$v;
								$default_column=$v;
								$column_name=$v;
							}
							else
							{

								$select.=$v;
							}
						}
					}
					else
					{
						$select="*";
						$default_column=$moduleArr['default_column'];
						$column_name=$moduleArr['column_name'];
					}
                    $join = array();
					$arrayColumn[] = $moduleArr['field'][0];
					$arrayStatus["is_active"] = array();
					$arrayColumnOrder = array("ASC","asc","DESC","desc");
					$post=array('page'=>$postData['page'],'column'=>$column,'order'=>$order);


                    if($postData['module'] == "terminal_list"){

                        if($postData['terminal_id'] == 0)
                        {
                            $result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$join,$column);//,array(),'',10    
                        }
                        else
                        {
                            $where = array('terminal_id' => $postData['terminal_id']);

                            /*print_r($where);
                            exit();*/
                            $result = pagination_data($arrayColumn,$arrayStatus,$post,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where,$join,$column);//,array(),'',10 



                        }
                        
                    }

					if(count($result['rows']))
					{
                        $resData =array();
                        $i = 0;
					   if($postData['module'] == "terminal_list"){
                        $k = 0;
                        $all_data=array();
                        //print_r($result['rows']);exit;
                            foreach ($result['rows'] as $keyf => $valuef) {
                                $allheader[] = array("name"=>$valuef['terminal_name'],"terminal_id"=>$valuef['terminal_id']);
                                $subitems = explode("###", $valuef['terminal_name']);
                                $facility_desc = explode("###", $valuef['terminal_desc']);

                                $airport_id = explode("###", $valuef['airport_id']);
                                $terminal_id = explode("###", $valuef['terminal_id']);
                                $terminal_images = explode("###", $valuef['terminal_images']);

                                 if($terminal_images[0] !='')
                                {
                                    $terminal_img = explode(',', $terminal_images[0]);
                                    foreach ($terminal_img as $key => $value) {
                                       $test[] = array('img'=>base_url()."images/terminal/".$value);
                                    }

                                    $subarray[$k]['terminal']=array("name"=>$subitems[0],
                                    "desc"=>$facility_desc[0],
                                    "airport_id"=>$airport_id[0],
                                   "terminal_id"=>$terminal_id[0],
                                    "terminal_images"=>$test);
                                }
                                else{
                                    $subarray[$k]['terminal']=array("name"=>$subitems[0],
                                    "desc"=>$facility_desc[0],
                                    "airport_id"=>$airport_id[0],
                                    "terminal_id"=>$terminal_id[0],
                                    "terminal_images"=>$facility_images[0]);
                                }


                                $k++;
                            }
                            $ugdata=array("header_data"=>$allheader,"subdata"=>$subarray);



                            $this->response(array("status"=>"success","msg"=> count($result['rows'])." records found.",'data'=>$ugdata),200);
                        }else{
                            $this->response(array("status"=>"success","msg"=> $result['entries'],'list'=>$result['rows']), 200);
                        }

                    }
					else
					{
						$this->response(array("status"=>"error","msg"=> "Nothing here yet. Please check back soon!"), 200);
					}
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token.","flag"=>"logout"), 200);
			}
		}
	}


  function getAirPeaceFlightData_post()
  {

      $postData = $this->post();
      //print_r($this->post());

      if("" == $postData['flight_number'])
      {
          $this->response(array('status' => 'error' , 'msg' => 'Please enter Flight Number'));
      }
      if("" == $postData['airline'])
      {
          $this->response(array('status' => 'error' , 'msg' => 'Please enter Airline'));
      }
      if("" == $postData['date'])
      {
          $this->response(array('status' => 'error' , 'msg' => 'Please enter Date'));
      }
      $date = $this->post('date');

      $dt = new DateTime($this->post('date'));

      // compare posted date with current date
      // If posted date one day before the current date then fetch the data otherwise not
      if(strtotime($date) < strtotime(date('Y-m-d h:i:s')) || strtotime($date) > strtotime(date('Y-m-d h:i:s')))
      {
          $diff=date_diff(date_create($dt->format('Y-m-d')),date_create(date('Y-m-d')));
          $dateDiff  =  $diff->format("%R%a");

          // print_r($dateDiff);
          //
          // exit();

          if(1 >= $dateDiff)
          {
              $nameOfDay = date('l', strtotime($date));

              // get the date from the date parameter

              $departureDate = $dt->format('Y-m-d');

              $flightData = $this->Common_model->select('*',TB_AIRLINE_CSV_UPLOAD,array('flight_number' => $postData['flight_number'],'airline' => $postData['airline'],'weekdays' => $nameOfDay));

              if(count($flightData) > 0)
              {

                  $arrive_date = date("Y-m-d", strtotime('+'.$flightData[0]['journey_hrs'].'hours'));

                  foreach($flightData as $val)
                  {
                      $result[] = array(

                          'flightId' => "",
                          'carrierFsCode' => $val['airline'],
                          'flightNumber' => $val['flight_number'],
                          'departureAirportFsCode' => $val['departure_from'],
                          'arrivalAirportFsCode' => $val['arrival_at'],
                          'departureTime' => $departureDate.'T'. $val['departure_time'],
                          'arrivalTime' => $arrive_date.'T'.$val['arrival_time'],
                          'terminal' => $val['terminal'],
                          'no_of_stops' => $val['no_of_stops'],

                      );
                  }



                  $this->response(array('status' => 'success' , 'scheduledFlights' => $result));
              }
              else
              {
                  $this->response(array('status' => 'error' , 'msg' => 'no data found'));
              }
          }
          else
          {
              $this->response(array('status' => 'error' , 'msg' => "Date before yesterday not allowed"));
          }
      }



      // get the day name from date


  }


  function getFlightData_post()
  {

  		$postData = $this->post();

  	  if("" == $postData['airport_code'])
      {
          $this->response(array('status' => 'error' , 'msg' => 'Please enter airport code'));
      }

      if("" == $postData['airport_status'])
      {
      	  $this->response(array('status' => 'error' , 'msg' => 'Please enter airport status'));
      }

      if("" == $postData['date'])
      {
      	  $this->response(array('status' => 'error' , 'msg' => 'Please enter date'));
      }

      $date = $this->post('date');

      $dt = new DateTime($this->post('date'));

      $nameOfDay = date('l', strtotime($date));

      if("" == $postData['flight_number'])
      {
            $airportData = $this->Common_model->select('*',TB_AIRPORT_CSV_UPLOAD,array('weekdays' => $nameOfDay,'fs_code' =>$postData['airport_code'],'airport_status' => $postData['airport_status']));
      }
      else
      {
            $airportData = $this->Common_model->select('*',TB_AIRPORT_CSV_UPLOAD,array('weekdays' => $nameOfDay,'fs_code' =>$postData['airport_code'],'airport_status' => $postData['airport_status'],'flight_number' => $postData['flight_number']));  
      }

      

     /* echo $this->db->last_query();
      exit();
*/
      if(count($airportData) > 0)
      {


      		foreach($airportData as $res)
      		{
      			$status_code = '';
      			switch($res['status'])
      			{
      				case 'Scheduled':
      					$status_code = 'S';
      					break;

      				case 'InTransit':
      					$status_code = 'A';
      					break;

      				case 'Unknown':
      					$status_code = 'U';
      					break;

      				case 'Redirected':
      					$status_code = 'R';
      					break;

      				case 'Landed':
      					$status_code = 'L';
      					break;

      				case 'Diverted':
      					$status_code = 'D';
      					break;

      				case 'Cancelled':
      					$status_code = 'C';
      					break;

      				case 'Not Operational':
      					$status_code = 'NO';
      					break;

      				case 'Data source needed':
      					$status_code = 'DN';
      					break;


      			}
      			$result[] = array(

      					'id' => $res['id'],
      					'flight_number' => $res['flight_number'],
      					'airline' => $res['airline'],
      					'weekdays' => $res['weekdays'],
      					'departure_from' => $res['departure_from'],
      					'arrival_at' => $res['arrival_at'],
      					'schedule_date' => date('Y-m-d',strtotime($postData['date'])),
      					'schedule_time' => date('h:i A',strtotime($res['schedule'])),
      					'terminal' => $res['terminal'],
      					'fs_code' => $res['fs_code'],
      					'status' => $res['status'],
      					'status_code' => $status_code,
      					'airport_status' => $res['airport_status']
      			);
      		}

      		$this->response(array('status' => 'success','data' => $result));
      }
      else
      {
      		$this->response(array('status' => 'success','msg'=> 'no data found'));
      }



  }


}
?>
