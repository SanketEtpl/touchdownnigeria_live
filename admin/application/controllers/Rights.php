<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rights extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{
		if(is_user_logged_in())
		{
			$user_auth=$this->session->userdata("auth_user");
			if(checkRight($user_auth['id'],'admin_right','access'))
			{
				$data['id']=$this->encrypt->encode($id);
				$roles[]=array(
				"role_id"=>"2",
				"role_title"=>"Airport admin",
				);
				$roles[]=array(
				"role_id"=>"3",
				"role_title"=>"Terminal admin",
				);
				$data['pageTitle'] = 'Settings - TDN';
				$data['roles']=$roles;
				$this->load->view('rights/listRights',$data);
				
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	public function listRights(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$arrayColumn = array("cat"=>"module");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$admin=$postData['key'];
				$modules=lang('MODULE_RIGHTS');

				/*print_r($modules);

				exit();*/
				$rows = '';
				$i=1;
				foreach($modules as $module => $title)
				{
					$rows .= '<tr right_id="'.$module.'">
	                            <td class="text-left">'.$title.'</td>';
								$arrPrivilege=array();
								$arrPrivilege=$this->common->select('*',TB_RIGHTS,array("role_id"=>$admin,'module_id'=>$module));
								$arrPrivilege=$arrPrivilege[0];
	                $rows.=      '<td class="text-left">
	                            	<input'.(($arrPrivilege['access_privilege'])?' checked':'').' id="'.$i.'" data-toggle="toggle" class="checkBox" type="checkbox" data-row-module="'.$module.'" data-row-subfunction="access_privilege" data-row-status="'.(($arrPrivilege['access_privilege'])?'0':'1').'" onchange="updatePermissionAccess(this)">
	                            </td>';
								
					$rows.=      '<td class="text-left">';
									if($module!='1' && $module!='7' && $module!='9')
									{
										$rows .= '<input'.(($arrPrivilege['add_privilege'])?' checked':'').' '.(($arrPrivilege['access_privilege'])?' ':' disabled').' id="add'.$i.'" data-toggle="toggle" class="checkBox" type="checkbox" data-row-module="'.$module.'" data-row-subfunction="add_privilege" data-row-status="'.(($arrPrivilege['add_privilege'])?'0':'1').'" onchange="updatePermission(this)">';
									}
									else
									{
										$rows .= '<span class="disabled_checkBox">-</span>';
									}
	                    $rows .=  '</td>
								<td class="text-left">
	                            	<input'.(($arrPrivilege['update_privilege'])?' checked':'').' '.(($arrPrivilege['access_privilege'])?' ':' disabled').' id="edit'.$i.'" data-toggle="toggle" class="checkBox" type="checkbox" data-row-module="'.$module.'" data-row-subfunction="update_privilege" data-row-status="'.(($arrPrivilege['update_privilege'])?'0':'1').'" onchange="updatePermission(this)">
	                            </td>			
								<td class="text-left">';
	                    if($module!='9' && $module!='1')
						{
						$rows .=  '	<input'.(($arrPrivilege['delete_privilege'])?' checked':'').' '.(($arrPrivilege['access_privilege'])?' ':' disabled').' id="delete'.$i.'" data-toggle="toggle" class="checkBox" type="checkbox" data-row-module="'.$module.'" data-row-subfunction="delete_privilege" data-row-status="'.(($arrPrivilege['delete_privilege'])?'0':'1').'" onchange="updatePermission(this)">';
						}
						else
						{
							$rows .= '<span class="disabled_checkBox">-</span>';
						}
						$rows .=  '</td>';'
	                </tr>';
					$i++;
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function updatePermission(){
		if(is_ajax_request())
		{
			
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//echo "<pre>";print_r($postData);//die();
				if($postData["status"] == "1"){
					$rowval = "0";
				}else{
					$rowval = "1";
					$postData["status"] = "0";
				}
				$admin=$postData['key'];
				$insertArr = array($postData["sub_function"]=>$postData["status"]);
				if($postData["key"]){
				
					$dataRights=$this->common->select('*',TB_RIGHTS,array('role_id'=>$admin,'module_id'=>$postData["module"]));
					if(count($dataRights))
					{
						$insertpermission_id = $this->common->update(TB_RIGHTS,array('privilege_id'=>$dataRights[0]['privilege_id']),$insertArr);
					}
					else
					{
						$insertArr['role_id']=$admin;
						$insertArr['module_id']=$postData["module"];
						$insertpermission_id = $this->common->insert(TB_RIGHTS,$insertArr);	
					}					
					if($insertpermission_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Permission has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","rowval"=>$rowval,"action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	public function updatePermissionAccess(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
	
				$admin=$postData['key'];
				if($postData["status"] == "1"){
					$rowval = "1";
				}else{
					//$rowval = "1";
					//$postData["status"] = "0";					
				}
				$insertArr=array();
				$insertArr = array($postData["sub_function"]=>$postData["status"]);
				if($postData["key"]){
				
					$dataRights=array();
					$dataRights=$this->common->select('*',TB_RIGHTS,array('role_id'=>$admin,'module_id'=>$postData["module"]));
					if(count($dataRights))
					{
						if(!$postData["status"])
						{
							$insertArr['add_privilege']='0';
							$insertArr['update_privilege']='0';
							$insertArr['delete_privilege']='0';
						}
						$insertpermission_id = $this->common->update(TB_RIGHTS,array('privilege_id'=>$dataRights[0]['privilege_id']),$insertArr);
					}
					else
					{
						$insertArr['role_id']=$admin;
						$insertArr['module_id']=$postData["module"];
						$insertpermission_id = $this->common->insert(TB_RIGHTS,$insertArr);	
					}
					
					if($insertpermission_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Permission has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","rowval"=>$rowval,"action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

}