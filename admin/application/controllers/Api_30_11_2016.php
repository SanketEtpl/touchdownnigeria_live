<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Common_model");
    }
    
    public function login_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }
        else if(trim($postData["password"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"del_status"=>"no","password"=>md5(trim($postData["password"]))));//,"del_status"=>"no","verification_status"=>null//"user_status"=>"published"//,"user_status"=>"published"
		if(count($userArr)>0)
        {
				
				if(trim($postData["device_token"]) != ""){
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("device_token"=>trim($postData["device_token"]),"device_type"=>trim($postData["device_type"])));
				}//
				$this->response(array("status"=>"success", "msg"=> "Login successfully.", "user"=> $userArr[0]), 200); exit;
			
		}else{
    		$this->response(array("status"=>"error", "msg"=> "Email or password is incorrect."), 200);
    	}
    }
    function registerUser_post()
    {
        $postData = $_POST;
        if(trim($postData["first_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
        }/*else if(trim($postData["last_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
        }*/else if(trim($postData["country"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your country."), 200);
        }/*else if(trim($postData["mobile_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
        }*/else if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
        }
        
        $emailArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);
						
        }
        else
        {
            $date = date("Y-m-d H:i:s");
            $user_token = md5(uniqid(rand(), true));
			$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randstring = '';
			for ($i = 0; $i < 12; $i++) {
				$randstring.= $characters[rand(0, strlen($characters))];
			}
            $insertArr = array(
                                "password"      =>md5(trim($postData["password"])),
                                "user_token"    =>$user_token,
                                "first_name"    =>trim($postData["first_name"]),
								//"last_name"    =>trim($postData["last_name"]),
                                "email"         =>trim($postData["email"]),
                                "phone"  =>trim($postData["mobile_no"]),
                                "country"     =>trim($postData["country"]),
								"created_at"  => $date,
                                "updated_at"  => $date,
								"del_status"=>"no",
                            );
              
            if($postData["device_token"]!="")
			{
				$insertArr["device_token"] = trim($postData["device_token"]);
			}
			if($postData["device_type"]!="")
			{
				$insertArr["device_type"] = trim($postData["device_type"]);
			}
            $user = $this->Common_model->insert(TB_USERS,$insertArr);
        }
        if($user)
        {
                $subject="Signup successful.";
                $message='
                Dear '.$postData["name"].'
                
                Your account has been created successfully.
                =================================
                Les Flammant';
                $res=sentEmail(array('avadhut@exceptionaire.co','Les Flammant'), array($postData["email"]),array('avadhut@exceptionaire.co','Les Flammant'),$subject,$message);            
            $this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Your account has been created successfully."), 200);
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
        
    }
    public function getPassword_post()
    {
        $postData = $this->input->post();
        if(trim($postData["email"]) == "")
        {
            $this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
        }
		$userArr = $this->Common_model->select("*",TB_USERS,array("email"=>trim($postData["email"]),"del_status"=>"no"));
		if(count($userArr)>0)
        {
				
				$user_token = md5(uniqid(rand(), true));
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 7; $i++) {
                    $randstring.= $characters[rand(0, strlen($characters))];
                }
                $subject="Your forgot password.";
                $message='
                Dear '.$userArr[0]['first_name'].'
                
                Your new password is : '.$randstring.' 
                =================================
                Les Flammant';
                $res=sentEmail(array('avadhut@exceptionaire.co','Les Flammant'), array($postData["email"]),array('avadhut@exceptionaire.co','Les Flammant'),$subject,$message);
                //echo "<pre>";print_r($res);
                $this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),array("password"=>md5($randstring)));
                $this->response(array("status"=>"success", "msg"=> "Password sent to your email, please check your inbox.",), 200); exit;
			
		}else{
    		$this->response(array("status"=>"error", "msg"=> "Email id is not registered."), 200);
    	}
    }
	
	function updateUser_post()
    {
        $postData = $_POST;
        
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"]),"del_status"=>"no"));
			if(count($userArr) > 0)
			{
				if(trim($postData["first_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your first name."), 200);
				}/*else if(trim($postData["last_name"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your last name."), 200);
				}*/else if(trim($postData["country"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your country."), 200);
				}/*else if(trim($postData["mobile_no"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your mobile number."), 200);
				}*/else if(trim($postData["email"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your email id."), 200);
				}/*else if(trim($postData["gender"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your gender."), 200);
				}else if(trim($postData["date_of_birth"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your date_of_birth."), 200);
				}*/
				/*else if(trim($postData["password"]) == ""){
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}*/
				
				$emailArr=$this->Common_model->select("*",TB_USERS,array("id != "=>trim($userArr[0]["id"]),"email"=>trim($postData["email"])));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "There is already an account with this email address."), 200);
								
				}
				else
				{
					$date = date("Y-m-d H:i:s");
					$insertArr = array(
										"first_name"    =>trim($postData["first_name"]),
										//"last_name"    =>trim($postData["last_name"]),
										"email"         =>trim($postData["email"]),
										//"gender"         =>trim($postData["gender"]),
										//"date_of_birth"  =>date('Y-m-d',strtotime($postData["date_of_birth"])),
										"phone"  =>trim($postData["mobile_no"]),
										"country"     =>trim($postData["country"]),
										"updated_at"  => $date,
									);
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","userid"=>$user,"user"=>$user_data, "user_token"=>$user_token, "msg"=> "Profile edited successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
        
    }
	
	function getUser_post()
    {
        $postData = $_POST;
        
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"]),"del_status"=>"no"));
			if(count($userArr) > 0)
			{
				$this->response(array("status"=>"success", "msg"=> "User found.","user"=>$userArr), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
        
    }
	
	function changePassword_post()
    {
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id,password",TB_USERS,array("user_token"=>trim($postData["user_token"]),"del_status"=>"no"));
			if(count($userArr) > 0)
			{
				if(trim($postData["current_password"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your current password."), 200);
				}
				else if(trim($postData["password"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your password."), 200);
				}
				else if(md5($userArr[0]["password"])==md5($postData["current_password"]))
				{
					$this->response(array("status"=>"error", "msg"=> "Your old password and new password should be diffrent."), 200);
				}
				else
				{
					$insertArr = array("password"=>md5(trim($postData["password"])));
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Password changed successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}   
    }
	
	function deactivateMe_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["reason"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter your reason."), 200);
				}
				else
				{
					$insertArr = array("del_status"=>'yes');
					$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
					$this->response(array("status"=>"success","msg"=> "Your account deactivated successfully."), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}  
	}
	
	function logout_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				$insertArr = array("device_type"=>null,"device_token"=>null);
				$this->Common_model->update(TB_USERS,array("id"=>trim($userArr[0]["id"])),$insertArr);
				$this->response(array("status"=>"success","msg"=> "Logout successful."), 200);
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}  
	}
	
	
	
	function getList_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);
				}
				else
				{
					$module=array(
								  'airport_dropdown'=>array('table'=>'tbl_airports','field'=>array('id'=>'id','name'=>'name'),'where'=>array(),'order'=>array()),
								  'airports_list'=>array('table'=>'tbl_airports','field'=>array('id'=>'id','name'=>'name',"bio"=>"bio","address"=>"address","state"=>"state","city"=>"city","country"=>"country","zipcode"=>"zipcode","latitude"=>"latitude","longitude"=>"longitude","email"=>"email","phone"=>"phone"),'where'=>array(),'order'=>array()),
								  'facility_list'=>array('table'=>'tbl_facility','field'=>array(),'default_column'=>'facility_id','column_name'=>'facility_id','where'=>array("airport_id"=>$postData['airport_id']),'order'=>array()),
								  'store_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array("store_category_id"=>1,"airport_id"=>$postData['airport_id']),'order'=>array()),
								  'restorent_list'=>array('table'=>TB_STORES,'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb","store_desc"=>"store_desc"),'where'=>array("store_category_id"=>2,"airport_id"=>$postData['airport_id']),'order'=>array()),
								  "store_product_category_list"=>array(
																	 'table'=>TB_STORE_FACILITY,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),
																	 'order'=>array(),
																	 ),
									"store_offer_list"=>array(
																	 'table'=>TB_STORE_OFFER,
																	 'where'=>array("store_id"=>$postData['store_id']),
																	 'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),
																	 ),
								  );
					$moduleArr=$module[$postData['module']];
					$select='';
					$where=$moduleArr['where'];
					$order=$moduleArr['order'];
					$default_column='';
					$column_name='';
					if(count($moduleArr['field']))
					{
						foreach($moduleArr['field'] as $k => $v)
						{
							if($k)
							{
								$select.=','.$v;
								$default_column=$v;
								$column_name=$v;
							}
							else
							{
								
								$select.=$v;
							}
						}
					}
					else
					{
						$select="*";
						$default_column=$moduleArr['default_column'];
						$column_name=$moduleArr['column_name'];
					}
					$arrayColumn[] = $moduleArr['field'][0];
					$arrayStatus["is_active"] = array();
					$arrayColumnOrder = array("ASC","asc","DESC","desc");
					$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where);//,array(),'',10
					
					$this->response(array("status"=>"success","msg"=> (count($result['rows']))?$result['entries']:"No Record Found.",'list'=>$result['rows']), 200);
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
	}
	
	function getDetail_post()
	{
		$postData = $_POST;
		if(trim($postData["user_token"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter user token."), 200);
        }
		else
		{
			$userArr = $this->Common_model->select("id",TB_USERS,array("user_token"=>trim($postData["user_token"])));
			if(count($userArr) > 0)
			{
				if(trim($postData["module"]) == "")
				{
					$this->response(array("status"=>"error", "msg"=> "Please enter module name."), 200);exit;
				}
				else
				{
					$module=array(
								'store_detail'=>array(
										'table'=>TB_STORES,
										'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"),
										'where'=>array("store_category_id"=>1,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								'restorent_detail'=>array(
										'table'=>TB_STORES,
										'field'=>array('store_id'=>'store_id','airport_id'=>'airport_id',"store_name"=>"store_name","store_desc"=>"store_desc","store_thumb"=>"CONCAT('".base_url()."images/store/',store_thumb) as store_thumb"),
										'where'=>array("store_category_id"=>2,"store_id"=>$postData['store_id']),
										//'subtable'=>array("store_product_category"=>array('table'=>TB_STORE_FACILITY,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_facility_id'=>'store_facility_id','store_id'=>'store_id',"facility_name"=>"facility_name"),),"store_offer"=>array('table'=>TB_STORE_OFFER,'where'=>array("store_id"=>$postData['store_id']),'field'=>array('store_offer_id'=>'store_offer_id','store_id'=>'store_id',"offer_name"=>"offer_name"),),),
										),
								);
					$moduleArr=$module[$postData['module']];
					$dataArr=getModuleData($moduleArr);
					if(count($moduleArr['subtable']))
					{
						foreach($moduleArr['subtable'] as $k => $v)
						{
							$dataArr[0][$k]=getModuleData($v);
						}
					}					
					//echo "<pre>";print_r($dataArr);
					//$arrayColumn = $moduleArr['field'];
					//$arrayStatus["is_active"] = array();
					//$arrayColumnOrder = array("ASC","asc","DESC","desc");
					//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,$default_column,$column_name,$moduleArr['table'],$select,null,$where);
					if(count($dataArr))
					{
						$this->response(array("status"=>"success","msg"=> "",$postData["module"]=>$dataArr[0]), 200);
					}
					else
					{
						$this->response(array("status"=>"error","msg"=> "No Detail Found."), 200);
					}
				}
			}
			else
			{
				$this->response(array("status"=>"error", "msg"=> "Invalid user token."), 200);
			}
		}
	}
	
	function getPage_post()
	{
		$postData = $_POST;
		if(trim($postData["page"]) == "")
		{
            $this->response(array("status"=>"error", "msg"=> "Please enter page."), 200);
        }
		$dataArr = $this->Common_model->select('page_title,page_detail',TB_PAGES,array("page_name"=>$postData["page"]));
		if($postData["page"]=='contact_us')
		{
			//echo "<pre>";print_r($dataArr);
			
			$page_detail=json_decode($dataArr[0]['page_detail'],'ASSOC');
			$page_detail['page_title'] = $dataArr[0]['page_title'];
			//$dataArr1=array('page_title'=>$dataArr[0]['page_title'],'page_detail'=>$page_detail);
			//$page_detail['page_title']=$dataArr[0]['page_title'];
			$this->response(array("status"=>"success","msg"=> "Page detail found.",'page'=>$page_detail), 200);
		}
		else
		{
			$this->response(array("status"=>"success","msg"=> "Page detail found.",'page'=>$dataArr), 200);
		}
	}
	
}
?>