<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			//$dtRight=checkRight($role,4);
			//$data['addRight']=$dtRight['add_privilege'];
			//$data['editRight']=$editRight=$dtRight['update_privilege'];
			//$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			//$accessRight=$dtRight['access_privilege'];
			//if($role==1)
			{
				$widthArr=array('notification'=>90,'action'=>10);
			}

			//else
			//{
			//	if($role==2)
			//	{
			//		$widthArr=array('store_name'=>30,'terminal_name'=>30,'store_thumb'=>30,'action'=>10);
			//	}
			//	if($role==3)
			//	{
			//		$widthArr=array('store_name'=>45,'store_thumb'=>45,'action'=>10);
			//	}
			//	if($editRight || $deleteRight)
			//	{
			//		$widthArr['action']=10;
			//	}
			//}


			$data['widthArr']=$widthArr;
			//$data["airports"] = $this->common->selectQuery("id as airport_id,name",TB_AIRPORTS,array());
			//$data["category"] = $this->common->selectQuery("category_id,category",TB_STORE_CATEGORY,array());
			if(get_user_data('role')==1 || get_user_data('role')==2)//if($accessRight)
			{
				$this->load->view('notification/listNotification',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}
	function listNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				//$role=get_user_data('role');
				//$dtRight=checkRight($role,4);
				//$add=$dtRight['add_privilege'];
				//$edit=$dtRight['update_privilege'];
				//$delete=$dtRight['delete_privilege'];

				if(get_user_data('role') == 1)
				{
						$where=array();
				}
				else
				{
						$where=array('created_by' => $this->session->userdata('auth_user')['id']);
				}


				//if($role!=1)
				//{
				//	$where=array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'));
				//	$terminal_id=get_user_data('terminal_id');
				//	if($role==3 && $terminal_id)
				//	{
				//		$where[TB_TERMINAL.'.terminal_id']=$terminal_id;
				//	}
				//}
				$postData = $this->input->post();
				$arrayColumn = array("notification_id"=>"notification_id","notification_text"=>"notification_text");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'notification_text','notification_text',TB_NOTIFICATION,'*','listNotification',$where);

				// echo $this->db->last_query();
				// exit();
				//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_FACILITY,'store_facility_id,store_name,facility_name','listStoreFacility',array(),array(TB_STORES=>TB_STORE_FACILITY.".store_id=".TB_STORES.".store_id"));
				//echo "<pre>";print_r($result);die();
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $store) {
						$store_id = $this->encrypt->encode($store['notification_id']);
						$rows .= '<tr category_id="'.$store_id.'">
	                            <td class="text-left">'.$store['notification_text'].'</td>';

	                    //$rows .= '<td class="text-left"><img src="'.base_url().'images/store/'.$store['store_thumb'].'" width="100"></td>';
						//if($edit || $delete)
						{
							$rows.='<td class="text-left">';
										//if($edit)
										{
											// $rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="getNotification(this)" title="Edit" href="javascript:void(0)">
											// 	<i class="fa fa-fw fa-edit"></i>
											// </a>';
										}
										//if($delete)
										//{
											//$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="sendNotification(this)" title="Send Notification" href="javascript:void(0)">
											//<i class="fa fa-fw fa-paper-plane"></i>
											//</a>';

											// $rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="sendNotificationAlert(this)" title="Send Notification" href="javascript:void(0)">
											// <i class="fa fa-fw fa-paper-plane-o"></i>
											// </a>';

											$rows .= '    <a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="deleteNotification(this)" title="Delete" href="javascript:void(0)">
															<i class="fa fa-fw fa-close"></i>
														</a>';
										//}
							$rows .= '</td>';
						}
	                    $rows.='</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function getNotificationValues()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//echo "<pre>";print_r($postData);
				$notificationData = $this->common->selectQuery("*",TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['key'])));


				$notificationData[0]['replace_array']=json_decode($notificationData[0]['replace_array'],'ASSOC');
				$replace_array=$notificationData[0]['replace_array'];
				//echo "<pre>";print_r($replace_array);die();
				$htmlStr='';
				if(count($replace_array))
				{
					$htmlStr='<input type="hidden" id="notification_key" name="notification_key" value="'.$postData['key'].'">';
					foreach($replace_array as $k => $v)
					{
						$str='<div class="row">
								<div class="col-md-12">
								  <div class="form-group">
									<label class="control-label" for="notification_text">'.ucfirst($v).'</label>
									<input type="text" class="form-control" rows="2" id="'.str_replace(" ", "-", $v).'" name="'.str_replace(" ", "-", $v).'" placeholder="'.ucfirst($v).'"></textarea>
								  </div>
								</div>
							  </div>';
					$htmlStr.=$str;
					}
				}
				if($htmlStr!='')
				{

					echo json_encode(array("status"=>"success","msg"=>"","htmlData"=>$htmlStr,"replace_array"=>$replace_array)); exit;
				}
				else
				{
					$notificationData = $this->common->selectQuery("*",TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['key'])));
				//echo "<pre>";print_r($notificationData);
					if($notificationData){
						if(get_user_data('role')==2)
						{
							$insertArr=array('notification_text'=>$notificationData[0]['notification_text'],"admin_id"=>get_user_data('id'),'status'=>0);
							$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
							echo json_encode(array("status"=>"success","flag"=>'NO_FORM',"msg"=>"Notification submited to admin for approval successfully.")); exit;
						}
						else
						{
							$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
							$insertArr=array('notification_text'=>$notificationData[0]['notification_text'],"admin_id"=>get_user_data('id'),'status'=>2);
							$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
							sendAndroidNotification($token_array,$notificationData[0]['notification_text']);
							echo json_encode(array("status"=>"success","flag"=>'NO_FORM',"msg"=>"Notification sent successfully.")); exit;
						}
					}
				}
				if($notificationData){
					echo json_encode(array("status"=>"success","notificationData"=>$notificationData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function sendNotificationAlert()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$notificationData = $this->common->selectQuery("*",TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['notification_key'])));
				//echo "<pre>";print_r($notificationData);die();
				$notification_text=$notificationData[0]['notification_text'];
				$replace_array=json_decode($notificationData[0]['replace_array'],'ASSOC');


				$arr=array();
				$arr1=array();
				foreach($replace_array as $k => $v)
				{
					$key=str_replace(" ","-", $v);
					$arr[]='**'.$key.'**';
					$arr1[]=$postData[$key];
				}




				$msg=str_replace($arr,$arr1,$notification_text);


				if($msg){

					/*print_r($msg);

					exit();*/

					if(get_user_data('role')==2)
					{

						/*print_r($msg);
						exit();*/
						$insertArr=array('notification_text'=>$msg,"admin_id"=>get_user_data('id'),'status'=>0);
						/*$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
						echo json_encode(array("status"=>"success","msg"=>"Notification submited to admin for approval successfully.")); exit;*/

						$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
						sendAndroidNotification($token_array,$msg);
						echo json_encode(array("status"=>"success","msg"=>"Notification submited.")); exit;
					}
					else
					{
						$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
						$insertArr=array('notification_text'=>$msg,"admin_id"=>get_user_data('id'),'status'=>2);
						$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
						sendAndroidNotification($token_array,$msg);
						echo json_encode(array("status"=>"success","msg"=>"Notification sent successfully.")); exit;
					}
				}
			}
		}
	}
	function getNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$notificationData = $this->common->selectQuery("*",TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['key'])));
				$notificationData[0]['replace_array']=json_decode($notificationData[0]['replace_array'],'ASSOC');
				if($notificationData){
					echo json_encode(array("status"=>"success","notificationData"=>$notificationData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function sendbyId()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//$storeData = $this->common->delete(TB_STORES,array('store_id'=>$this->encrypt->decode($postData['key'])));
				$notificationData = $this->common->selectQuery("*",TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['key'])));
				//echo "<pre>";print_r($notificationData);
				if($notificationData){
					if(get_user_data('role')==2)
					{
						$insertArr=array('notification_text'=>$notificationData[0]['notification_text'],"admin_id"=>get_user_data('id'),'status'=>0);
						$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
						echo json_encode(array("status"=>"success","msg"=>"Notification submited to admin for approval successfully.")); exit;
					}
					else
					{
						$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
						$insertArr=array('notification_text'=>$notificationData[0]['notification_text'],"admin_id"=>get_user_data('id'),'status'=>2);
						$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
						sendAndroidNotification($token_array,$notificationData[0]['notification_text']);
						echo json_encode(array("status"=>"success","msg"=>"Notification sent successfully.")); exit;
					}
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	public function saveNotification(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{


				$postData = $this->input->post();
				$insertArr = array('notification_text'=>$postData["notification_text"]);
				if($postData["notification_key"]){
					$insertcategory_id = $this->common->update(TB_NOTIFICATION,array('notification_id'=>$this->encrypt->decode($postData['notification_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Notification text has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					//$insertcategory_id = $this->common->insert(TB_STORES,$insertArr);
					//if($insertcategory_id){
					//	echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Store has been added successfully.")); exit;
					//}else{
					//	echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					//}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function emergency()
	{
		if(is_user_logged_in())
		{
			if(get_user_data('role')==1 || get_user_data('role')==2)
			{
				$widthArr=array('notification'=>30,'first_name'=>30,'created_date'=>30,'action'=>10);
				$data['widthArr']=$widthArr;
				$data["airports"] = $this->common->selectQuery('DISTINCT '.TB_AIRPORTS.".id as airport_id,name as airport_name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
				$this->load->view('notification/emergencyNotification',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}
		else
		{
			redirect("login");
			exit;
		}
	}
	function sendNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{

				$postData = $this->input->post();
				$airport_ids  = implode(",",$postData['airport_id']);


				if(1 == $this->session->userdata("auth_user")['role'])  // check for admin user
				{
					$tmp = 0;
					$insertArr=array('notification_text'=>$postData['notification'],'airport_id' => $airport_ids,"admin_id"=>get_user_data('id'),'status' => 2);

					$token_array=array();
					$str= array();
					$airportId = "";

					foreach($postData['airport_id'] as $ids)
					{
						$str[] =  $ids;
						$airportId .= $ids.",";
					}

					$airportId = rtrim($airportId,",");

					// get distinct user based on airport id
					$users = $this->common->selectWhereIn("DISTINCT(user_id)",TBL_AIRPORT_ALERT_REQUEST,"airport_id",$str);

					if(count($users) < 1)
					{
						echo json_encode(array('status' => 'error','msg' => 'no user assigned to any airports'));

						exit();
					}

					$data = array(

						'notification_text' => $postData['notification'],
						'noti_type' => 'emergency',
						'airport_id' => $airportId,
						'created_by' => 1,
						'created_date' => date('Y-m-d h:i:s')
					);

					$insert = $this->common->insert(TB_NOTIFICATION,$data);

					if($insert)
					{

						foreach($users as $user)
						{

							$userNotiData[] = array(

								'notification_id' => $insert,
								'user_id' => $user['user_id'],
								'view_status' => '0'
							);
						}

						$userNotiRes = $this->common->insert_batch(TB_USER_NOTIFICATION,$userNotiData);

						if($userNotiRes)
						{
							$tmp = 1;
						}	
						else
						{
							echo json_encode(array('status' => 'error' ,'msg' => 'Something went wrong.'));
						}

					}
					else
					{
						echo json_encode(array('status' => 'error' ,'msg' => 'Something went wrong.'));
					}

					if(1 == $tmp)					
					{
						$flag = 0;

						if($postData['select_all']!="")
						{
							$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
						}
						else
						{
							//selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())
							foreach($postData['airport_id'] as $v)
							{
								//echo $v;
								$arr=array();
								//$arr=$this->common->selectQuery("DISTINCT ".TB_USERS.".id,".TB_USERS.".device_token",TB_USERS,array(TBL_AIRPORT_ALERT_REQUEST.'.airport_id'=>$v),array(),array(TBL_AIRPORT_ALERT_REQUEST=>TB_USERS.".id = ".TBL_AIRPORT_ALERT_REQUEST.".user_id"));

								$arr=$this->common->selectQuery("DISTINCT ".TB_USERS.".device_token,".TB_USERS.".id",TB_USERS,array(TBL_AIRPORT_ALERT_REQUEST.'.airport_id'=>$v),array(),array(TBL_AIRPORT_ALERT_REQUEST=>TB_USERS.".id = ".TBL_AIRPORT_ALERT_REQUEST.".user_id"));

								
								foreach($arr as $k => $vl)
								{
									if(!in_array($vl,$token_array))
									{
										$token_array[]=$vl;
									}
								}

								
							}
							//$token_array=$this->common->selectQuery("DISTINCT ".TB_USERS.".id,".TB_USERS.".device_token",TB_USERS,array(),array(),array(TBL_AIRPORT_ALERT_REQUEST=>TB_USERS.".id = ".TBL_AIRPORT_ALERT_REQUEST.".user_id"));
						}
						//echo "<pre>";print_r($token_array);die();
						if(sendAndroidNotification($token_array,$postData['notification']))
						{
							$flag = 1;
						}
					}
				}
				else
				{
					$insertArr=array('notification_text'=>$postData['notification'],'airport_id' => $airport_ids,"admin_id"=>get_user_data('id'));

					$subject = "Notification alert | Touchdown Nigeria";
	        		$email_content = '<p>Hello Admin,</p>
	                <p>'.$this->session->userdata("auth_user")['first_name']. ' '. $this->session->userdata("auth_user")['last_name'].', has created the Emergency Notification.</p>';

					$message = getMailTemplate('email_template',array('email_content'=>$email_content));
					$res = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(ADMIN_EMAIL),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
					//$res = sentAdminEmail(array(ADMIN_EMAIL,ADMIN_NAME), array(ADMIN_EMAIL),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);


				}
				//$insertcategory_id = $this->common->insert(TBL_SENT_NOTIFICATION,$insertArr);
				//
				//
				
				$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
				
				/*if(1 == $flag)
				{
					$insertcategory_id = $this->common->insert(TBL_PENDING_NOTIFICATION,$insertArr);
	
				}
				else
				{
					echo json_encode(array('status' => 'error' ,'msg' => 'Something went wrong'));
				}*/
				
				/*$token_array=array();
				if($postData['select_all']!="")
				{
					$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
				}
				else
				{
					//selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())
					foreach($postData['airport_id'] as $v)
					{
						//echo $v;
						$arr=array();
						$arr=$this->common->selectQuery("DISTINCT ".TB_USERS.".id,".TB_USERS.".device_token",TB_USERS,array(TBL_AIRPORT_ALERT_REQUEST.'.airport_id'=>$v),array(),array(TBL_AIRPORT_ALERT_REQUEST=>TB_USERS.".id = ".TBL_AIRPORT_ALERT_REQUEST.".user_id"));
						foreach($arr as $k => $vl)
						{
							if(!in_array($vl,$token_array))
							{
								$token_array[]=$vl;
							}
						}
					}
					//$token_array=$this->common->selectQuery("DISTINCT ".TB_USERS.".id,".TB_USERS.".device_token",TB_USERS,array(),array(),array(TBL_AIRPORT_ALERT_REQUEST=>TB_USERS.".id = ".TBL_AIRPORT_ALERT_REQUEST.".user_id"));
				}
				//echo "<pre>";print_r($token_array);die();
				sendAndroidNotification($token_array,$postData['notification'],"AIRPORT");*/

				echo json_encode(array("status"=>"success","msg"=>"Notification sent successfully.")); exit;
			}
			else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	public function approveNotification()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			//$dtRight=checkRight($role,4);
			//$data['addRight']=$dtRight['add_privilege'];
			//$data['editRight']=$editRight=$dtRight['update_privilege'];
			//$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			//$accessRight=$dtRight['access_privilege'];
			//if($role==1)
			{
				$widthArr=array('notification'=>30,'first_name'=>20,'created_date'=>15,'action'=>35);
			}
			//else
			//{
			//	if($role==2)
			//	{
			//		$widthArr=array('store_name'=>30,'terminal_name'=>30,'store_thumb'=>30,'action'=>10);
			//	}
			//	if($role==3)
			//	{
			//		$widthArr=array('store_name'=>45,'store_thumb'=>45,'action'=>10);
			//	}
			//	if($editRight || $deleteRight)
			//	{
			//		$widthArr['action']=10;
			//	}
			//}
			$data['widthArr']=$widthArr;
			//$data["airports"] = $this->common->selectQuery("id as airport_id,name",TB_AIRPORTS,array());
			//$data["category"] = $this->common->selectQuery("category_id,category",TB_STORE_CATEGORY,array());
			if(get_user_data('role')==1)
			{

				$this->load->view('notification/approveNotification',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}
	function listApproveNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				//$role=get_user_data('role');
				//$dtRight=checkRight($role,4);
				//$add=$dtRight['add_privilege'];
				//$edit=$dtRight['update_privilege'];
				//$delete=$dtRight['delete_privilege'];

				$where=array();
				//if($role!=1)
				//{
				//	$where=array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'));
				//	$terminal_id=get_user_data('terminal_id');
				//	if($role==3 && $terminal_id)
				//	{
				//		$where[TB_TERMINAL.'.terminal_id']=$terminal_id;
				//	}
				//}
				$postData = $this->input->post();
				$arrayColumn = array("pending_id"=>"pending_id","notification_text"=>"notification_text","first_name"=>"first_name","created_date"=>TBL_PENDING_NOTIFICATION.".created_date");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'notification_text','notification_text',TBL_PENDING_NOTIFICATION,TBL_PENDING_NOTIFICATION.'.*,'.TB_ADMIN.'.first_name','listApproveNotification',$where,array(TB_ADMIN=>TB_ADMIN.'.id = '.TBL_PENDING_NOTIFICATION.'.admin_id'));



				//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_FACILITY,'store_facility_id,store_name,facility_name','listStoreFacility',array(),array(TB_STORES=>TB_STORE_FACILITY.".store_id=".TB_STORES.".store_id"));
				//echo "<pre>";print_r($result);die();
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $store) {
						$store_id = $this->encrypt->encode($store['pending_id']);
						$rows .= '<tr category_id="'.$store_id.'">
	                            <td class="text-left">'.$store['notification_text'].'</td>
								<td class="text-left">'.$store['first_name'].'</td>
								<td class="text-left">'.date('d-m-Y',strtotime($store['created_date'])).'</td>';

	                    //$rows .= '<td class="text-left"><img src="'.base_url().'images/store/'.$store['store_thumb'].'" width="100"></td>';
						//if($edit || $delete)
						{
							$rows.='<td class="text-left">';
										//if($edit)
										{
											//$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="getNotification(this)" title="Edit" href="javascript:void(0)">
											//	<i class="fa fa-fw fa-edit"></i>
											//</a>';
										}
										//if($delete)
										//{

										//}
										if($store['status']==1)
										{
											$rows .= '<button type="button" class="btn btn-block btn-default btn-sm">Rejected</button>';
										}
										else if($store['status']==2)
										{
											$rows .= '<button type="button" class="btn btn-block btn-success btn-sm" disabled>Approve</button>';
										}
										else
										{


											$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="updateNotification(this)" title="Edit Notification" href="javascript:void(0)">
												<button type="button" class="btn btn-block btn-info btn-sm">Edit</button>
												</a>';


											$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="approveAndSend(this)" title="Approve Notification" href="javascript:void(0)">
												<button type="button" class="btn btn-block btn-info btn-sm">Approve</button>
												</a>';
												$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="deletePendingNotification(this)" title="Reject Notification" href="javascript:void(0)">
												<button type="button" class="btn btn-block btn-danger btn-sm">Reject</button>
												</a>';
										}
							$rows .= '</td>';
						}
	                    $rows.='</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function sentEmergency()
	{
		if(is_user_logged_in())
		{
			if(get_user_data('role')==1)
			{
				$widthArr=array('notification'=>33,'first_name'=>20,'airport'=>20,'created_date'=>20);
				$data['widthArr']=$widthArr;
				$this->load->view('notification/sentEmergencyNotification',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}
		else
		{
			redirect("login");
			exit;
		}
	}
	function listSentNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$where=array();
				$postData = $this->input->post();
				/*$arrayColumn = array("sent_id"=>"sent_id","notification_text"=>"notification_text","first_name"=>TB_ADMIN.".first_name","created_date"=>TBL_SENT_NOTIFICATION.".created_date");*/

				$arrayColumn = array("sent_id"=>"sent_id","notification_text"=>"notification_text","first_name"=>TB_ADMIN.".first_name","created_date"=>TB_NOTIFICATION.".created_date");

				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'notification_text','notification_text',TB_NOTIFICATION,TB_NOTIFICATION.'.*,'.TB_ADMIN.".first_name",'listNotification',$where,array(TB_ADMIN=>TB_ADMIN.'.id = '.TB_NOTIFICATION.'.created_by'));


				/*echo $this->db->last_query();

				exit();*/
				$rows = '';
				//echo "<pre>";print_r($this->db->last_query());die();
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $store) {

						$ids = explode(",",$store['airport_id']);

						$airportId  = $this->common->select_where_in('name',TB_AIRPORTS,'id',$ids);

						$airport = implode(",",$airportId);
						$airportData = '';

						if(!empty($airportId))
						{
							foreach($airportId as $val)
							{
								$airportData .= $val['name'].',';
							}
						}




						$store_id = $this->encrypt->encode($store['sent_id']);
						$rows .= '<tr category_id="'.$store_id.'">
	                            <td class="text-left">'.$store['notification_text'].'</td>
								<td class="text-left">'.$store['first_name'].'</td>
								<td class="text-left">'.rtrim($airportData,',').'</td>
								<td class="text-left">'.date('d-m-Y h:i:s',strtotime($store['created_date'])).'</td>';

						//$rows .= '<td class="text-left"><a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="deleteSentNotification(this)" title="Delete" href="javascript:void(0)">
						//					<i class="fa fa-fw fa-close"></i>
						//					</a></td>';
						$rows.='</tr>';
					}


				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function approveAndSend()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//$storeData = $this->common->delete(TB_STORES,array('store_id'=>$this->encrypt->decode($postData['key'])));
				$notificationData = $this->common->selectQuery("*",TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($postData['key'])));

				$users = $this->common->select('*',TBL_AIRPORT_ALERT_REQUEST,array('airport_id' => $notificationData[0]['airport_id']));



				if($notificationData){

						$token_array=$this->common->selectQuery("device_token,id",TB_USERS);


						sendAndroidNotification($token_array,$notificationData[0]['notification_text']);
						//$this->Common_model->delete(TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($postData['key'])));
						
						$data = array(

							'notification_text' => $notificationData[0]['notification_text'],
							'noti_type' => 'emergency',
							'airport_id' => $notificationData[0]['airport_id'],
							'created_by' => $notificationData[0]['admin_id'],
							'created_date' => $notificationData[0]['created_date']
						);

						$insert = $this->common->insert(TB_NOTIFICATION,$data);
						if($insert)
						{
							foreach($users as $user)
							{
								$userNotiData[] = array(

									'notification_id' => $insert,
									'user_id' => $user['user_id'],
									'view_status' => '0'
								);
							}	
						}

						$inserUserNoti = $this->common->insert_batch(TB_USER_NOTIFICATION,$userNotiData);

						
						


						$insertcategory_id = $this->common->update(TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($postData['key'])),array('status'=>2));

						/*$data = array(

						);
						$insert = $this->common->insert(TB_NOTIFICATION,$data);*/

						echo json_encode(array("status"=>"success","msg"=>"Notification approved successfully.")); exit;

				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function editNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();

				$data = $this->common->select('*',TBL_PENDING_NOTIFICATION,array('pending_id' => $this->encrypt->decode($postData['key'])));

				if(count($data) > 0)
				{
					echo json_encode(array('status' => 'success', 'data' => $data));
				}
				else
				{
					echo json_encode(array('status' => 'error', 'msg' => 'no data found'));
				}



			}
		}
	}



	function deleteSentNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportFacilityData = $this->common->delete(TBL_SENT_NOTIFICATION,array('sent_id'=>$this->encrypt->decode($postData['key'])));
				if($airportFacilityData){
					echo json_encode(array("status"=>"success","msg"=>"Notification deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function deletePendingNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//$airportFacilityData = $this->common->delete(TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($postData['key'])));
				$insertArr=array('status'=>1);
				$insertcategory_id = $this->common->update(TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
				if($insertcategory_id){
					echo json_encode(array("status"=>"success","msg"=>"Notification rejected successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveUpdateNotification()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$data = array(

						'notification_text' => $this->input->post('notification')
						//'status' => 2
					);
					$update = $this->common->update(TBL_PENDING_NOTIFICATION,array('pending_id'=>$this->encrypt->decode($this->input->post('key'))),$data);

					if($update)
					{
						 	echo json_encode(array("status"=>"success","msg"=>"Notification updated sucessfully.")); exit;
					}
					else
					{
							echo json_encode(array("status"=>"error","msg"=>"Something went wrong !!")); exit;
					}

			}
		}
	}


	function addNotification()
	{
			if(is_ajax_request())
	 		{
		 			if(is_user_logged_in())
		 			{


		 				$userId = $this->common->select('id',TB_USERS);
		 				
		 				$ids;

						$data = array(

							'notification_text' => $this->input->post('notification_text'),
							'noti_type' => 'normal',
							'created_by' => $this->session->userdata('auth_user')['id'],
							'created_date' => date('Y-m-d h:i:s')
							//'status' => 2
						);
						$insert = $this->common->insert(TB_NOTIFICATION,$data);

						if($insert)
						{
							foreach($userId as $val)
							{
								$ids[] = array(
									'notification_id' => $insert,
									'user_id' => $val['id']
								);
							}

							$this->common->insert_batch(TB_USER_NOTIFICATION,$ids);

							/*print_r();*/
							$msgforNotification = $this->input->post('notification_text');
								$token_array=$this->common->selectQuery("device_token,id",TB_USERS);
								$registrationIds = array();
								foreach ($token_array as $key => $value) {
									
									if($value['device_token']!="")
									{
										array_push($registrationIds,$value['device_token']);
									}	
								}
										$msg = array(
											'message' 	=> $msgforNotification,
											 'data'			=> $msg,
											'action_by' =>'',
											'action_by_profile_pic' =>'',
											'action' => '',
											'action_time' => trim(date('Y-m-d H:i:s'))
										);

										define( 'API_ACCESS_KEY', 'AIzaSyBv4UO0BFVn7rx_XLYy3XqHJSbVLItfPB0' );
										$fields = array
										(	
											'registration_ids' 	=> $registrationIds,
											'data'			=> $msg
										);
								 
										$headers = array
										(
											'Authorization: key=' . API_ACCESS_KEY,
											'Content-Type: application/json'
										);
								 
										$ch = curl_init();
										curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
										curl_setopt( $ch,CURLOPT_POST, true );
										curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
										curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
										curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
										curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
										$result = curl_exec($ch );
										curl_close( $ch );
										
								//sendAndroidNotification($registrationIds,$msg);
								echo json_encode(array("status"=>"success","msg"=>"Notification sent successfully.")); exit;
						}
						else
						{
								echo json_encode(array("status"=>"error","msg"=>"Something went wrong !!")); exit;
						}
		 			}
	 		}
	}


	function deleteNotification()
	{
			if(is_ajax_request())
			{
					if(is_user_logged_in())
					{

							$postData = $this->input->post();

							$notification_id=$this->encrypt->decode($postData['key']);

							$deleteNotification = $this->common->delete(TB_NOTIFICATION,array('notification_id'=>$notification_id));

							if($deleteNotification)
							{
									echo json_encode(array('status' => 'success','msg' => 'Notification deleted successfully'));
							}
							else
							{
									echo json_encode(array('status' => 'error','msg' =>'Something went wrong !!'));
							}


					}
			}
	}


	function sendAndroidNotification($registrationIds,$msg) {
		// define( 'API_ACCESS_KEY', 'AIzaSyAqILqwjh8c2ulVLyDHw_nLrtsmCQFjXAY' );
		define( 'API_ACCESS_KEY', 'AIzaSyBv4UO0BFVn7rx_XLYy3XqHJSbVLItfPB0' );

		// $registrationid = array('duu6gpGc5WM:APA91bG7S80ajmeUSxKNnKaV0usB2XctJV0Fforn9iJNU-9bRJU_X_angLku_KRLnKlK2iYZzXk-W1H2BNgfD6mQIrWJcEo2VDCgY_ujWzOBlRmExBufJDfHAcUMZmiRvCTREjG9WJwM');
 	// 	$msgforNotification = 'Notification added by admin';
		// $msg = array(
		// 		'message' 	=>  $msgforNotification,
		// 		'registration_ids' 	=> $registrationIds,
		// 		'data'			=> $msg,
		// 		'action_by' =>'',
		// 		'action_by_profile_pic' =>'',
		// 		'action' => '',
		// 		'action_time' => trim(date('Y-m-d H:i:s'))
		// 	);
		$fields = array
		(	
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);
 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		// echo "<pre>";print_r($result);die;
		return $result;		

	}


}
