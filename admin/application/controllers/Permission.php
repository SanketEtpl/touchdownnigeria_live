<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('permission/listPermission');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listPermission(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				
				$arrayColumn = array("cat"=>"module");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'module,has_permission','permission_id',TB_ACCESS_PERMISSION,'*','listPermission');
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $permission) {
						$permission_id = $this->encrypt->encode($permission['permission_id']);
						if($permission['has_permission'] == "yes"){$checked = " checked"; $status = 'no';}else{$checked=""; $status = 'yes';}
						$rows .= '<tr permission_id="'.$permission_id.'">
	                            <td class="text-left">'.$permission['module'].'</td>
	                            <td class="text-left">
	                            	<input'.$checked.' data-toggle="toggle" class="checkBox" type="checkbox" data-row-status="'.$status.'" data-row-id="'.$permission_id.'" onchange="updatePermission(this)">
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	/*public function updatePermission(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$permissionData = $this->common->selectQuery("module",TB_ACCESS_PERMISSION,array('permission_id'=>$this->encrypt->decode($postData['key'])));
				if($permissionData){
					echo json_encode(array("status"=>"success","moduleData"=>$permissionData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}*/

	public function updatePermission(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				if($postData["status"] == "yes"){
					$rowval = "no";
				}else{
					$rowval = "yes";
					$postData["status"] = "no";
				}
				$insertArr = array('has_permission'=>$postData["status"]);
				if($postData["key"]){
					$insertpermission_id = $this->common->update(TB_ACCESS_PERMISSION,array('permission_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					if($insertpermission_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Permission has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","rowval"=>$rowval,"action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

}