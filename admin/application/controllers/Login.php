<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if(is_user_logged_in())
		{
			redirect("index");
			exit;
		}else{
			$this->load->view('login');
		}
	}

	public function checkLogin(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"]),"password"=>md5(trim($postData["password"]))));
			if($result)
			{
				if($result[0]["is_active"]){
					if($result[0]["role"] == 3){
						$airport_admin_result = $this->common->select("*",TB_ADMIN,array("air_port_id"=>$result[0]["air_port_id"],'is_active'=>'1',"role"=>2));
						if(!$airport_admin_result[0]["is_active"]){
							echo json_encode(array("status" => "error","message"=>"Your account has been inactive.")); exit;		
						}
					}
					$this->session->set_userdata("auth_user", $result[0]);
					echo json_encode(array("status" => "success")); exit;
				}else{
					echo json_encode(array("status" => "error","message"=>"Your account has been inactive.")); exit;
				}
			}
			else
			{
				echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.")); exit;
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("login"); exit;
	}

	public function forgetPassword()
	{
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			if($postData["username_f"]=="")
			{
				echo json_encode(array("status" => "error","message"=>"Please enter your username.")); exit;
			}
			else
			{
				$result = $this->common->select("id,email,is_active,username,first_name",TB_ADMIN,array("username"=>trim($postData["username_f"])));
				if($result)
				{
					if(!$result[0]['is_active'])
					{
						echo json_encode(array("status" => "error","message"=>"Your account has been inactive.")); exit;
					}
					$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$randstring = '';
					for ($i = 0; $i < 12; $i++) {
						$randstring.= $characters[rand(0, strlen($characters))];
					}
					$link = base_url().'login/reset_password/'.$randstring;
					$subject="Your reset password link | Touchdown Nigeria";
					$email_content = "<p>Dear ".$result[0]['first_name']."</p>";
					$email_content .= '<p>You have requested for a change of password. To change your Touchdown Nigeria admin password please <a href="'.$link.'">click here</a> or copy and paste the following link into your browser to enter your new password.</p><p>Link: '.$link.'<br><br>This link will expire in the next 48 hours.</p><p>If you have not requested a password change, please ignore this email.</p>';
					
					$message = getMailTemplate('email_template',array('email_content'=>$email_content));
					$this->common->update(TB_ADMIN,array("id"=>$result[0]['id']),array("verification_code"=>$randstring));
					$r = sentEmail(array(ADMIN_EMAIL,ADMIN_NAME), array($result[0]['email']),array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message);
					
					//$r = sentEmail();
					if($r["status"] == "success"){
						
						echo json_encode(array("status" => "success","message"=>"The reset password link has been sent on your registered email, please check your email to reset your password.")); exit;	
					}else{
						echo json_encode(array("status" => "error","message"=>"Please try again.")); exit;	
					}
				}
				else
				{
					echo json_encode(array("status" => "error","message"=>"The username you have entered is not present.")); exit;
				}
			}
		}
	}
	function reset_password($verification_code=null)
	{
		if($verification_code!="")
		{
			$userData=$this->common->select("id",TB_ADMIN,array("verification_code"=>$verification_code));
			if(count($userData))
			{
				$data['show_change_password']=1;	
			}
			else
			{
				$data['show_change_password']=2;
			}
			$data['verification_code']=$verification_code;
			$this->load->view('login',$data);
		}
		else
		{
		
		}
	}
	function changepass()
	{
		if(is_ajax_request())
		{
			$this->load->model("Common_model");
			$postData = $this->input->post();
			if($postData["new_password"]=="")
			{
				echo json_encode(array("status" => "error","message"=>"Please enter password.")); exit;
			}
			else if($postData["conf_password"]=="")
			{
				echo json_encode(array("status" => "error","message"=>"Please enter confirm password.")); exit;
			}
			else if($postData["new_password"]!=$postData["conf_password"])
			{
				echo json_encode(array("status" => "error","message"=>"Confirm password does not match to password.")); exit;
			}
			else
			{
				$userData=$this->common->select("id",TB_ADMIN,array("verification_code"=>trim($postData["verification_code"])));
				$r=count($userData);
				if($r)
				{
					$this->Common_model->update(TB_ADMIN,array("verification_code"=>trim($postData["verification_code"])),array("password"=>md5($postData["new_password"]),"verification_code"=>null));
					echo json_encode(array("status" => "success","message"=>"Password has been changed successfully.")); exit;
				}
				else
				{
					echo json_encode(array("status" => "error","message"=>"Your change password link has been expired.")); exit;
				}
			}
		}
	}
}
