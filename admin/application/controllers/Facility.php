<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facility extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
    
    public function index()
    {
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,3);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$data['pageTitle']='Airport facilities - Airport management - TDN';

			$accessRight=$dtRight['access_privilege'];
			if($role==1)
			{
				$widthArr=array('facility_name'=>25,'facility_desc'=>25,'airport_name'=>20,'terminal_name'=>20,'action'=>10);
			}
			else
			{
				if($editRight || $deleteRight)
				{
					$widthArr=array('facility_name'=>90,'action'=>10);
					$widthArr=array('facility_dec'=>90,'action'=>10);
				}
				else
				{
					$widthArr=array('facility_name'=>100);
					$widthArr=array('facility_dec'=>100);
				}
			}
			$data['widthArr']=$widthArr;
			// echo "<pre>";print_r($widthArr);die;
			if($accessRight)
			{
				//$data["airports"] = $this->common->selectQuery("id as airport_id,name as airport_name",TB_AIRPORTS,array());
				$data["airports"] = $this->common->selectQuery('DISTINCT '.TB_AIRPORTS.".id as airport_id,name as airport_name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));

				$this->load->view('airports/listAirportFacility',$data);

				/*echo "testttttt";
				print_r($data["airports"]);

				exit();*/
			}
			else
			{
				redirect("login");
				exit;
			}
			
		}else{
			redirect("login");
			exit;
		}
	}
	
	function listAirportFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,3);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$postData = $this->input->post();				
				$arrayColumn = array("name"=>"name","facility_name"=>"facility_name","facility_desc"=>"facility_desc");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array();
				if($role!=1)
				{
					$where=array('airport_id'=>get_user_data('air_port_id'));
				}
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'name','name',TB_FACILITY,'facility_id,facility_name,facility_desc, name,facility_images','listAirportFacility',$where,
					array(TB_AIRPORTS=>TB_FACILITY.".airport_id=".TB_AIRPORTS.".id"),TB_FACILITY.'.created_at');
				//echo $this->db->last_query();die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $facility) {
						$terminalData = $this->common->selectQuery("terminal_name",TB_TERMINAL,array('terminal_id'=>$facility['terminal_id']));
						// echo $this->db->last_query();die;
						// 
						$imgarr = explode(",", $facility['facility_images']);
						$facilityImg = $imgarr[0];

						$facility_id = $this->encrypt->encode($facility['facility_id']);
						$rows .= '<tr facility_id="'.$facility_id.'">
	                            <td class="text-left">'.$facility['facility_name'].'</td>';
	                    $rows .= '<td class="text-left">'.$facility['facility_desc'].'</td>';
	                    
								if($role==1)
								{
						$rows .= '<td class="text-left">'.$facility['name'].'</td>';

						$rows .= '<td class="text-left">'.$facility['terminal_name'].'</td>';

						if("" != $facilityImg)
						{

						 $rows .= '<td class="text-left"><img src="'.base_url().'images/facility/'.$facilityImg.'" width="100"></td>';
						}
						else
						{
							 $rows .= '<td class="text-left">Image not available</td>';
						}

						 
						/*if($terminalData[0])
						{
							$rows .= '<td class="text-left">'.$terminalData[0]['terminal_name'].'</td>';
						}else{
							$rows .= '<td class="text-left"></td>';
						}*/
						
								}
							if($edit || $delete)
							{	
								$rows .= '<td class="text-left">';
								if($edit)
								{
									$rows .= '	<a data-id="'.$i.'" data-row-id="'.$facility_id.'" class="" onclick="getAirportFacility(this)" title="Edit" href="javascript:void(0)">
													<i class="fa fa-fw fa-edit"></i>
												</a>';
								}
								if($delete)
								{
									$rows .= '    <a data-id="'.$i.'" data-row-id="'.$facility_id.'" class="" onclick="deleteAirportFacility(this)" title="Delete" href="javascript:void(0)">
													<i class="fa fa-fw fa-close"></i>
												</a>';
								}
								$rows .= ' </td>';
							}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;	
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function getAirportFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportFaciData = $this->common->selectQuery("*",TB_FACILITY,array('facility_id'=>$this->encrypt->decode($postData['key'])));
				// echo "<pre>";print_r($airportFaciData);die;
				
				if($airportFaciData){
					echo json_encode(array("status"=>"success","airportFaciData"=>$airportFaciData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities found.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	function saveAirportFacility()
	{
		// echo "<pre>";print_r($_POST);die;
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$fac_images='';
				$postData = $this->input->post();
				// ------ images ------
				$files=$_FILES['facility_thumb'];
				$cpt = count($_FILES['facility_thumb']['name']);

				$insertArr = array('facility_name'=>$postData["facility_name"],'facility_desc'=>$postData["facility_desc"]);
				// echo "<pre>";print_r($insertArr);die;
				// $insertArr = array('facility_desc'=>$postData["facility_description"]);
				if(get_user_data('role')==1)
				{
					$insertArr["airport_id"]=$postData["airport_id"];
					$insertArr["terminal_id"]=$postData["terminal_id"];
				}
				else
				{
					$insertArr["airport_id"]=get_user_data('air_port_id');
					$insertArr["terminal_id"]=get_user_data('terminal_id');
				}


				// -------- images code here------------
				if($cpt==0){
					foreach ($_POST['facility_thumb_old'] as $key => $value) {
						$all_img_old[]=$value;
					}
					$fac_images.= implode(",", $all_img_old);
					$insertArr['facility_images']=$fac_images;
				}else{
					$fac_images='';
					foreach ($_POST['facility_thumb_old'] as $key => $value) {
						$all_img_old[]=$value;
					}
					if(!(empty($all_img_old))){
					$fac_images.= implode(",", $all_img_old).",";
				}else{
					$fac_images.='';
				}

			
				for($i=0; $i<$cpt; $i++){     
				if($files['name'][$i])
				{					
					$userfile_extn = explode(".", strtolower($files['name'][$i]));
					$ext=$userfile_extn[1];
					$allowed=array('gif','jpg','jpeg','png');
					if(!in_array($ext,$allowed))
					{
						echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"facility_thumb"));	 exit;	
					}
					if($files['size'][$i] > 5242880)//2097152
					{
						echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"facility_thumb"));exit;		
					}

					// ------ check image dimension ---------
					$image_info = getimagesize($_FILES["facility_thumb"]['tmp_name'][$i]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];					
					if ($image_width > 300 || $image_height > 300)
					{
					echo json_encode(array("status"=>"error","msg"=> "Please upload image of 300X300.","field"=>"facility_thumb"));exit;
					}

					$new_image_name=uniqid(rand(), true).time().'.'.$ext;					
					$all_img[]=$new_image_name;

					$quality=40;
					$source=$files['tmp_name'][$i];
					$destination='./images/facility/'.$new_image_name;
					$info = getimagesize($source);
					if ($info['mime'] == 'image/jpeg')
						$image = imagecreatefromjpeg($source); 
					elseif ($info['mime'] == 'image/gif')
						$image = imagecreatefromgif($source); 
					elseif ($info['mime'] == 'image/png')
						$image = imagecreatefrompng($source); 
					imagejpeg($image, $destination, $quality);
					
				}
			}
			$fac_images.= implode(",", $all_img);
			$insertArr['facility_images']=$fac_images;
		}



				if($postData["airportfacility_key"]){
					$insertcategory_id = $this->common->update(TB_FACILITY,array('facility_id'=>$this->encrypt->decode($postData['airportfacility_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Facility has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertcategory_id = $this->common->insert(TB_FACILITY,$insertArr);
					// echo $this->db->last_query();die;
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Facility has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	
	
	function deleteAirportFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$airportFacilityData = $this->common->delete(TB_FACILITY,array('facility_id'=>$this->encrypt->decode($postData['key'])));
				if($airportFacilityData){
					echo json_encode(array("status"=>"success","msg"=>"Facility deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function getTerminal($flag='')
	{
		//echo "<pre>";print_r($_POST);die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				
				if($flag=='main')
				{
					$terminalData = $this->common->selectQuery("*",TB_TERMINAL,array('airport_id'=>$postData['airport_id']),array());
				}
				else
				{
					$terminalData = $this->common->selectQuery("*",TB_TERMINAL,array('airport_id'=>$postData['airport_id'],TB_ADMIN.'.role'=>3),array(),array(TB_ADMIN=>TB_ADMIN.'.terminal_id = '.TB_TERMINAL.'.terminal_id'));
				}
				
				//if($terminalData){
					$dta=array();
					//$dta['']="Select Terminal";
					foreach($terminalData as $k => $v)
					{
						$dta[$v['terminal_id']]=$v['terminal_name'];
					}
					//echo implode("#",$dta);die;
					echo json_encode(array("status"=>"success","terminalData"=>$dta)); exit;
				//}else{
				//	echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				//}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
}