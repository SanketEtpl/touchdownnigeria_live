<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(-1);
	}
    
    public function index()
    {

    	$json = '{"alert":{"event":{"type":"EQUIPMENT_ADJUSTMENT"},"dataSource":"Airport","dateTimeRecorded":"2017-01-18T10:10:21.141Z","rule":{"id":"436218055","name":"Default","carrierFsCode":"AA","flightNumber":"100","departureAirportFsCode":"JFK","arrivalAirportFsCode":"LHR","departure":"2017-01-20T18:25:00.000","arrival":"2017-01-18T06:20:00.000","ruleEvents":{"ruleEvent":{"type":"ALL_CHANGES"}},"nameValues":null,"delivery":{"format":"json","destination":"smtp://vardhaman@exceptionaire.co"}},"flightStatus":{"flightId":"836943608","carrierFsCode":"AA","flightNumber":"100","departureAirportFsCode":"JFK","arrivalAirportFsCode":"LHR","departureDate":{"dateLocal":"2017-01-20T18:25:00.000","dateUtc":"2017-01-17T23:25:00.000Z"},"arrivalDate":{"dateLocal":"2017-01-18T06:20:00.000","dateUtc":"2017-01-18T06:20:00.000Z"},"status":"L","schedule":{"flightType":"J","serviceClasses":"FJY","restrictions":"","uplines":{"upline":[{"fsCode":"ORD","flightId":"836964282"},{"fsCode":"CLT","flightId":"836919171"}]}},"operationalTimes":{"publishedDeparture":{"dateLocal":"2017-01-17T18:25:00.000","dateUtc":"2017-01-17T23:25:00.000Z"},"publishedArrival":{"dateLocal":"2017-01-18T06:20:00.000","dateUtc":"2017-01-18T06:20:00.000Z"},"scheduledGateDeparture":{"dateLocal":"2017-01-17T18:25:00.000","dateUtc":"2017-01-17T23:25:00.000Z"},"estimatedGateDeparture":{"dateLocal":"2017-01-17T21:45:00.000","dateUtc":"2017-01-18T02:45:00.000Z"},"actualGateDeparture":{"dateLocal":"2017-01-17T21:45:00.000","dateUtc":"2017-01-18T02:45:00.000Z"},"flightPlanPlannedDeparture":{"dateLocal":"2017-01-17T21:59:00.000","dateUtc":"2017-01-18T02:59:00.000Z"},"estimatedRunwayDeparture":{"dateLocal":"2017-01-17T22:10:00.000","dateUtc":"2017-01-18T03:10:00.000Z"},"actualRunwayDeparture":{"dateLocal":"2017-01-17T22:10:00.000","dateUtc":"2017-01-18T03:10:00.000Z"},"scheduledGateArrival":{"dateLocal":"2017-01-18T06:20:00.000","dateUtc":"2017-01-18T06:20:00.000Z"},"estimatedGateArrival":{"dateLocal":"2017-01-18T09:44:00.000","dateUtc":"2017-01-18T09:44:00.000Z"},"actualGateArrival":{"dateLocal":"2017-01-18T09:44:00.000","dateUtc":"2017-01-18T09:44:00.000Z"},"flightPlanPlannedArrival":{"dateLocal":"2017-01-18T09:16:00.000","dateUtc":"2017-01-18T09:16:00.000Z"},"estimatedRunwayArrival":{"dateLocal":"2017-01-18T09:37:00.000","dateUtc":"2017-01-18T09:37:00.000Z"},"actualRunwayArrival":{"dateLocal":"2017-01-18T09:37:00.000","dateUtc":"2017-01-18T09:37:00.000Z"}},"codeshares":{"codeshare":[{"fsCode":"AY","flightNumber":"4012","relationship":"L"},{"fsCode":"BA","flightNumber":"1511","relationship":"L"},{"fsCode":"GF","flightNumber":"6654","relationship":"L"},{"fsCode":"IB","flightNumber":"4218","relationship":"L"},{"fsCode":"LY","flightNumber":"8051","relationship":"L"},{"fsCode":"MH","flightNumber":"9410","relationship":"L"},{"fsCode":"QR","flightNumber":"5290","relationship":"L"}]},"delays":{"departureGateDelayMinutes":"200","departureRunwayDelayMinutes":"11","arrivalGateDelayMinutes":"204","arrivalRunwayDelayMinutes":"21"},"flightDurations":{"scheduledBlockMinutes":"415","blockMinutes":"419","scheduledAirMinutes":"377","airMinutes":"387","scheduledTaxiOutMinutes":"214","taxiOutMinutes":"25","taxiInMinutes":"7"},"airportResources":{"departureTerminal":"8","departureGate":"6","arrivalTerminal":"3","arrivalGate":"36","baggage":"03"},"flightEquipment":{"scheduledEquipmentIataCode":"77W","tailNumber":"N728AN"},"flightStatusUpdates":{"flightStatusUpdate":[{"updatedAt":{"dateUtc":"2017-01-14T23:10:37.084Z"},"source":"Innovata","updatedTextFields":{"updatedTextField":[{"field":"STS","newText":"S"},{"field":"SQP","newText":"77W"},{"field":"ATM","newText":"3"},{"field":"DTM","newText":"8"}]},"updatedDateFields":{"updatedDateField":[{"field":"SGA","newDateLocal":"2017-01-18T06:20:00.000","newDateUtc":"2017-01-18T06:20:00.000Z"},{"field":"SGD","newDateLocal":"2017-01-17T18:25:00.000","newDateUtc":"2017-01-17T23:25:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-16T17:26:30.061Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"DGT","newText":"14"}}},{"updatedAt":{"dateUtc":"2017-01-16T23:26:14.046Z"},"source":"ASDI","updatedDateFields":{"updatedDateField":[{"field":"SRA","newDateLocal":"2017-01-18T05:58:00.000","newDateUtc":"2017-01-18T05:58:00.000Z"},{"field":"SRD","newDateLocal":"2017-01-17T18:31:00.000","newDateUtc":"2017-01-17T23:31:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-17T00:07:27.602Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"DGT","originalText":"14","newText":"16"}}},{"updatedAt":{"dateUtc":"2017-01-17T00:46:34.024Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"DGT","originalText":"16","newText":"14"}}},{"updatedAt":{"dateUtc":"2017-01-17T05:25:55.129Z"},"source":"ASDI","updatedDateFields":{"updatedDateField":[{"field":"SRA","originalDateLocal":"2017-01-18T05:58:00.000","newDateLocal":"2017-01-18T06:11:00.000","originalDateUtc":"2017-01-18T05:58:00.000Z","newDateUtc":"2017-01-18T06:11:00.000Z"},{"field":"SRD","originalDateLocal":"2017-01-17T18:31:00.000","newDateLocal":"2017-01-17T18:54:00.000","originalDateUtc":"2017-01-17T23:31:00.000Z","newDateUtc":"2017-01-17T23:54:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-17T11:35:47.047Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"DGT","originalText":"14","newText":"6"}}},{"updatedAt":{"dateUtc":"2017-01-17T20:51:18.028Z"},"source":"Airline","updatedDateFields":{"updatedDateField":[{"field":"EGA","newDateLocal":"2017-01-18T07:55:00.000","newDateUtc":"2017-01-18T07:55:00.000Z"},{"field":"EGD","newDateLocal":"2017-01-17T20:00:00.000","newDateUtc":"2017-01-18T01:00:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-17T20:55:40.007Z"},"source":"ASDI","updatedDateFields":{"updatedDateField":[{"field":"SRA","originalDateLocal":"2017-01-18T06:11:00.000","newDateLocal":"2017-01-18T09:16:00.000","originalDateUtc":"2017-01-18T06:11:00.000Z","newDateUtc":"2017-01-18T09:16:00.000Z"},{"field":"SRD","originalDateLocal":"2017-01-17T18:54:00.000","newDateLocal":"2017-01-17T21:59:00.000","originalDateUtc":"2017-01-17T23:54:00.000Z","newDateUtc":"2017-01-18T02:59:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-17T20:56:24.078Z"},"source":"Airline","updatedDateFields":{"updatedDateField":[{"field":"EGA","originalDateLocal":"2017-01-18T07:55:00.000","newDateLocal":"2017-01-18T09:25:00.000","originalDateUtc":"2017-01-18T07:55:00.000Z","newDateUtc":"2017-01-18T09:25:00.000Z"},{"field":"EGD","originalDateLocal":"2017-01-17T20:00:00.000","newDateLocal":"2017-01-17T21:30:00.000","originalDateUtc":"2017-01-18T01:00:00.000Z","newDateUtc":"2017-01-18T02:30:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-17T22:50:43.030Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"TAL","newText":"N728AN"}}},{"updatedAt":{"dateUtc":"2017-01-18T00:45:32.033Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:25:00.000","newDateLocal":"2017-01-18T10:04:00.000","originalDateUtc":"2017-01-18T09:25:00.000Z","newDateUtc":"2017-01-18T10:04:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T02:07:17.136Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGD","originalDateLocal":"2017-01-17T21:30:00.000","newDateLocal":"2017-01-17T21:57:00.000","originalDateUtc":"2017-01-18T02:30:00.000Z","newDateUtc":"2017-01-18T02:57:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T02:43:16.221Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"STS","originalText":"S","newText":"A"}},"updatedDateFields":{"updatedDateField":[{"field":"EGA","originalDateLocal":"2017-01-18T10:04:00.000","newDateLocal":"2017-01-18T09:49:00.000","originalDateUtc":"2017-01-18T10:04:00.000Z","newDateUtc":"2017-01-18T09:49:00.000Z"},{"field":"EGD","originalDateLocal":"2017-01-17T21:57:00.000","newDateLocal":"2017-01-17T21:42:00.000","originalDateUtc":"2017-01-18T02:57:00.000Z","newDateUtc":"2017-01-18T02:42:00.000Z"},{"field":"AGD","newDateLocal":"2017-01-17T21:42:00.000","newDateUtc":"2017-01-18T02:42:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-18T02:47:11.082Z"},"source":"Airline","updatedDateFields":{"updatedDateField":[{"field":"EGA","originalDateLocal":"2017-01-18T09:49:00.000","newDateLocal":"2017-01-18T09:52:00.000","originalDateUtc":"2017-01-18T09:49:00.000Z","newDateUtc":"2017-01-18T09:52:00.000Z"},{"field":"EGD","originalDateLocal":"2017-01-17T21:42:00.000","newDateLocal":"2017-01-17T21:45:00.000","originalDateUtc":"2017-01-18T02:42:00.000Z","newDateUtc":"2017-01-18T02:45:00.000Z"},{"field":"AGD","originalDateLocal":"2017-01-17T21:42:00.000","newDateLocal":"2017-01-17T21:45:00.000","originalDateUtc":"2017-01-18T02:42:00.000Z","newDateUtc":"2017-01-18T02:45:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-18T03:10:54.945Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:52:00.000","newDateLocal":"2017-01-18T09:48:00.000","originalDateUtc":"2017-01-18T09:52:00.000Z","newDateUtc":"2017-01-18T09:48:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T03:11:20.004Z"},"source":"Airline","updatedDateFields":{"updatedDateField":[{"field":"ERD","newDateLocal":"2017-01-17T22:10:00.000","newDateUtc":"2017-01-18T03:10:00.000Z"},{"field":"ARD","newDateLocal":"2017-01-17T22:10:00.000","newDateUtc":"2017-01-18T03:10:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-18T03:25:45.040Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:48:00.000","newDateLocal":"2017-01-18T09:40:00.000","originalDateUtc":"2017-01-18T09:48:00.000Z","newDateUtc":"2017-01-18T09:40:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T04:29:06.086Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"AGT","newText":"36"}}},{"updatedAt":{"dateUtc":"2017-01-18T05:22:30.096Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:40:00.000","newDateLocal":"2017-01-18T09:37:00.000","originalDateUtc":"2017-01-18T09:40:00.000Z","newDateUtc":"2017-01-18T09:37:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T05:32:18.034Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:37:00.000","newDateLocal":"2017-01-18T09:38:00.000","originalDateUtc":"2017-01-18T09:37:00.000Z","newDateUtc":"2017-01-18T09:38:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T07:04:49.104Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:38:00.000","newDateLocal":"2017-01-18T09:39:00.000","originalDateUtc":"2017-01-18T09:38:00.000Z","newDateUtc":"2017-01-18T09:39:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T07:40:05.988Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:39:00.000","newDateLocal":"2017-01-18T09:38:00.000","originalDateUtc":"2017-01-18T09:39:00.000Z","newDateUtc":"2017-01-18T09:38:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T08:21:24.023Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:38:00.000","newDateLocal":"2017-01-18T09:39:00.000","originalDateUtc":"2017-01-18T09:38:00.000Z","newDateUtc":"2017-01-18T09:39:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T08:47:46.997Z"},"source":"Airline","updatedDateFields":{"updatedDateField":{"field":"EGA","originalDateLocal":"2017-01-18T09:39:00.000","newDateLocal":"2017-01-18T09:40:00.000","originalDateUtc":"2017-01-18T09:39:00.000Z","newDateUtc":"2017-01-18T09:40:00.000Z"}}},{"updatedAt":{"dateUtc":"2017-01-18T09:40:21.167Z"},"source":"Airline","updatedTextFields":{"updatedTextField":{"field":"STS","originalText":"A","newText":"L"}},"updatedDateFields":{"updatedDateField":[{"field":"ERA","newDateLocal":"2017-01-18T09:37:00.000","newDateUtc":"2017-01-18T09:37:00.000Z"},{"field":"ARA","newDateLocal":"2017-01-18T09:37:00.000","newDateUtc":"2017-01-18T09:37:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-18T09:50:09.074Z"},"source":"Airline","updatedDateFields":{"updatedDateField":[{"field":"EGA","originalDateLocal":"2017-01-18T09:40:00.000","newDateLocal":"2017-01-18T09:44:00.000","originalDateUtc":"2017-01-18T09:40:00.000Z","newDateUtc":"2017-01-18T09:44:00.000Z"},{"field":"AGA","newDateLocal":"2017-01-18T09:44:00.000","newDateUtc":"2017-01-18T09:44:00.000Z"}]}},{"updatedAt":{"dateUtc":"2017-01-18T10:10:21.141Z"},"source":"Airport","updatedTextFields":{"updatedTextField":{"field":"BGG","newText":"03"}}}]},"operatingCarrierFsCode":"AA","primaryCarrierFsCode":"AA"}},"appendix":{"airlines":{"airline":[{"fs":"AA","iata":"AA","icao":"AAL","name":"American Airlines","phoneNumber":"08457-567-567","active":"true"},{"fs":"QR","iata":"QR","icao":"QTR","name":"Qatar Airways","phoneNumber":"+1 877 777 2827","active":"true"},{"fs":"LY","iata":"LY","icao":"ELY","name":"El Al","phoneNumber":"+ 972-3-9771111","active":"true"},{"fs":"AY","iata":"AY","icao":"FIN","name":"Finnair","phoneNumber":"+ 358 600 140 140","active":"true"},{"fs":"IB","iata":"IB","icao":"IBE","name":"Iberia","phoneNumber":"1800 772 4642","active":"true"},{"fs":"MH","iata":"MH","icao":"MAS","name":"Malaysia Airlines","phoneNumber":"+603 7843 3000","active":"true"},{"fs":"BA","iata":"BA","icao":"BAW","name":"British Airways","phoneNumber":"1-800-AIRWAYS","active":"true"},{"fs":"GF","iata":"GF","icao":"GFA","name":"Gulf Air","phoneNumber":"973 17 335 777","active":"true"}]},"airports":{"airport":[{"fs":"ORD","iata":"ORD","icao":"KORD","faa":"ORD","name":"O Hare International Airport","street1":"10000 West O Hare","city":"Chicago","cityCode":"CHI","stateCode":"IL","postalCode":"60666","countryCode":"US","countryName":"United States","regionName":"North America","timeZoneRegionName":"America/Chicago","weatherZone":"ILZ014","localTime":"2017-01-18T04:11:00.045","utcOffsetHours":"-6.0","latitude":"41.976912","longitude":"-87.904876","elevationFeet":"668","classification":"1","active":"true"},{"fs":"CLT","iata":"CLT","icao":"KCLT","faa":"CLT","name":"Charlotte Douglas International Airport","street1":"5501 Josh Birmingham Parkway","city":"Charlotte","cityCode":"CLT","stateCode":"NC","postalCode":"28204","countryCode":"US","countryName":"United States","regionName":"North America","timeZoneRegionName":"America/New_York","weatherZone":"NCZ071","localTime":"2017-01-18T05:11:00.045","utcOffsetHours":"-5.0","latitude":"35.219167","longitude":"-80.935833","elevationFeet":"748","classification":"1","active":"true"},{"fs":"JFK","iata":"JFK","icao":"KJFK","faa":"JFK","name":"John F. Kennedy International Airport","street1":"JFK Airport","city":"New York","cityCode":"NYC","stateCode":"NY","postalCode":"11430","countryCode":"US","countryName":"United States","regionName":"North America","timeZoneRegionName":"America/New_York","weatherZone":"NYZ178","localTime":"2017-01-18T05:11:00.045","utcOffsetHours":"-5.0","latitude":"40.642335","longitude":"-73.78817","elevationFeet":"13","classification":"1","active":"true"},{"fs":"LHR","iata":"LHR","icao":"EGLL","name":"London Heathrow Airport","city":"London","cityCode":"LON","stateCode":"EN","countryCode":"GB","countryName":"United Kingdom","regionName":"Europe","timeZoneRegionName":"Europe/London","localTime":"2017-01-18T10:11:00.045","utcOffsetHours":"0.0","latitude":"51.469603","longitude":"-0.453566","elevationFeet":"80","classification":"1","active":"true"}]}}}';
		
    	$txt = date("Y-m-d H:i:s")."\n". json_encode($_POST)."\n"."\n"."\n";
 		$myfile = file_put_contents('logs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
			
		switch ($arr->alert->flightStatus->status) {
			case 'C':
				$notification_msg = 'The flight '.$arr->alert->flightStatus->flightId.' has been canceled.';
				break;

			case 'D':
				$notification_msg = 'The flight '.$arr->alert->flightStatus->flightId.' has been diverted.';
				break;

			case 'S':
				$notification_msg = 'The flight '.$arr->alert->flightStatus->flightId.' has been Scheduled.';
				break;

			case 'L':
				$notification_msg = 'The flight '.$arr->alert->flightStatus->flightId.' has been Landed.';
				break;
			
			default:
				$notification_msg = '';
				break;
		}


		$dep_date =  date("Y-m-d 00:00:00",strtotime($arr->alert->rule->departure));
		$cond = array('flight_number' => $arr->alert->rule->flightNumber,'flight_date'=>$dep_date,'airport_code' => $arr->alert->rule->arrivalAirportFsCode,'depart_airport_code' => $arr->alert->rule->departureAirportFsCode, 'carrier' => $arr->alert->rule->carrierFsCode);
		$token_array = $this->common->selectQuery("device_token,".TB_USERS.".id AS id",TB_USERS,$cond,array(),array(TBL_FLIGHT_ALERT_REQUEST => 'user_id = '.TB_USERS.'.id'));
		//echo $this->db->last_query();
		//echo "<pre>"; print_r($token_array); exit;
		$token_array1[0] = array('id'=>'112','device_token' => 'cD8fouQyyl8:APA91bF-yyxK3nZrUuVCMF_IdbvnovHQWioBNGC5uamBXCyXZ0YO-o39nvsRky6LX8lf3cUhqGXuofxdvFF7ejeshMGL7GBwS4ZVyQDPXRRDhXg8ZP_l0o__g_Z56wivp3GAdVeI4_hC');
		if(count($token_array) > 0 && $notification_msg != "")
		{
			$r = sendAndroidNotification($token_array, $notification_msg);	
		}
		if($_POST){
			$r = sendAndroidNotification($token_array1, "Get data......");
		}
	}


	public function getAirLineCode(){
		$curl_handle=curl_init();	
		curl_setopt($curl_handle,CURLOPT_URL,"https://api.flightstats.com/flex/airlines/rest/v1/json/active?appId=".FLIGHT_STAT_appId."&appKey=".FLIGHT_STAT_appKey);
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
		curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		$buffer = curl_exec($curl_handle);

		curl_close($curl_handle);
		$dt=$this->common->update(TBL_AIRLINE_CODE,array("id"=>1),array('details'=>$buffer));
	}

	public function deailyActivity()
	{
		$today_date =  date("Y-m-d 23:59:59");
		$this->common->delete(TBL_FLIGHT_ALERT_REQUEST,array('flight_date <'=>$today_date));
		//Flight alert requests deleted.
	}

	public function ports()
	{
		$host = 'exceptionaire.co';
		$ports = array(2195, 21, 25, 80, 81, 110, 443, 3306);

		foreach ($ports as $port)
		{
		    $connection = @fsockopen($host, $port);

		    if (is_resource($connection))
		    {
		        echo '<h2>' . $host . ':' . $port . ' ' . '(' . getservbyport($port, 'tcp') . ') is open.</h2>' . "\n";

		        fclose($connection);
		    }

		    else
		    {
		        echo '<h2>' . $host . ':' . $port . ' is not responding.</h2>' . "\n";
		    }
		}
	}


	public function avd()
    {
    	//$postData = $this->input->post();

    	$headers[] = "From: Avadhut Yadav<avadhut@exceptionaire.co>";
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=iso-8859-1';
		$header=implode("\r\n", $headers);
		$message = json_encode($_POST);
		$r = mail("avadhut@exceptionaire.com","test",$message,$header);
		if($r){
			echo "send";
		}
    	$txt = date("Y-m-d H:i:s")."\n". json_encode($_POST)."\n"."\n"."\n";
 		$myfile = file_put_contents('avd_alert.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
	}
}
