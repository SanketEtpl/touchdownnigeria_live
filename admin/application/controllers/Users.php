<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,7);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($editRight || $deleteRight)
			{
				$widthArr=array('fname'=>27,'email'=>27,'phone'=>17,'country'=>25,'action'=>10);
			}
			else
			{
				$widthArr=array('fname'=>28,'email'=>28,'phone'=>20,'country'=>22);
			}
			$data['widthArr']=$widthArr;
			$data['pageTitle'] = 'Users - TDN';
			if($accessRight)
			{
				$this->load->view('users/listUsers',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	public function listUsers(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,7);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$postData = $this->input->post();
				
				$arrayColumn = array("user"=>"id","fname"=>"first_name","lname"=>"last_name","email"=>"email","phone"=>"phone","country"=>"country");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','id',TB_USERS,'*','listUsers');

				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                            
	                            <td class="text-left">'.$user['email'].'</td>
	                            <td class="text-left">'.$user['phone'].'</td>
								<td class="text-left">'.$user['country'].'</td>';
								if($edit || $delete)
								{
									$rows .= '<td class="text-left">';
								if($edit)
								{
									//<td class="text-left">'.$user['last_name'].'</td>
									//$rows .= '<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getUser(this)" title="Edit" href="javascript:void(0)">
									//			   <i class="fa fa-fw fa-edit"></i>
									//		   </a>';
								}
								if($delete)
								{
									$rows .= '<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteUser(this)" title="Delete" href="javascript:void(0)">
												   <i class="fa fa-fw fa-close"></i>
											   </a>';
								}		
									$rows .= ' </td>';
								}
								$rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function getUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("first_name,last_name,email as email_id,phone",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$insertArr = array('first_name'=>$postData["first_name"],'last_name' => $postData['last_name'],'email'=>$postData["email_id"],'phone' => $postData['phone']);
				if($postData["email_id"]!="")
				{
					if($postData["user_key"]){
						$userData = $this->common->selectQuery("*",TB_USERS,array('id !='=>$this->encrypt->decode($postData['user_key']),'email'=>$postData['email_id']));
					}
					else
					{
						$userData = $this->common->selectQuery("*",TB_USERS,array('email'=>$postData['email_id']));
					}
					if(count($userData))
					{
						echo json_encode(array("status"=>"error","msg"=>"Email id already exists.")); exit;	
					}
				}
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"User has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertId = $this->common->insert(TB_USERS,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	function deleteUser()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$subadminData = $this->common->delete(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($subadminData){
					echo json_encode(array("status"=>"success","msg"=>"User deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

}