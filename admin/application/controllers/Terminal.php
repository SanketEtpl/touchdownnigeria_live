<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terminal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,10);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$data['pageTitle']='Airport terminal - Airport management - TDN';

			$accessRight=$dtRight['access_privilege'];
			if($role==1)
			{
				$widthArr=array('terminal_name'=>45,'airport_name'=>45,'action'=>10);
			}
			else
			{
				if($editRight || $deleteRight)
				{
					$widthArr=array('terminal_name'=>90,'action'=>10);
				}
				else
				{
					$widthArr=array('terminal_name'=>100);
				}
			}
			$data['widthArr']=$widthArr;
			if($accessRight)
			{
				$data["airports"] = $this->common->selectQuery('DISTINCT '.TB_AIRPORTS.".id as airport_id,name as airport_name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));
				//echo $this->db->last_query();die();
				$this->load->view('airports/listTerminal',$data);
			}
			else
			{
				redirect("login");
				exit;
			}

		}else{
			redirect("login");
			exit;
		}
	}

	function listTerminal()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,10);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$postData = $this->input->post();
				$arrayColumn = array("terminal_id"=>"terminal_id","name"=>"name","terminal_name"=>"terminal_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array();
				if($role==2)
				{
					$where=array('airport_id'=>get_user_data('air_port_id'));
				}
				if($role==3)
				{
					$where=array('terminal_id'=>get_user_data('terminal_id'));
				}
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'terminal_id','terminal_id',TB_TERMINAL,'terminal_id,terminal_name,name','listTerminal',$where,array(TB_AIRPORTS=>TB_TERMINAL.".airport_id=".TB_AIRPORTS.".id"));
				/*echo $this->db->last_query();
				echo "<pre>";print_r($result);die;*/
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $terminal) {
						$terminal_id = $this->encrypt->encode($terminal['terminal_id']);
						$rows .= '<tr terminal_id="'.$terminal_id.'">
	                            <td class="text-left">'.$terminal['terminal_name'].'</td>';
	                            // if($terminal['terminal_desc'] !='')
	                            // {
	                            // 	'<td class="text-left">'.$terminal['terminal_desc'].'</td>';
	                            // }

								if($role==1)
								{
						$rows .= '<td class="text-left">'.$terminal['name'].'</td>';
									// if($terminal['terminal_desc'] !='')
		       //                      {
		       //                      	'<td class="text-left">'.$terminal['terminal_desc'].'</td>';
		       //                      }
								}
							if($edit || $delete)
							{
								$rows .= '<td class="text-left">';
								if($edit)
								{
									$rows .= '	<a data-id="'.$i.'" data-row-id="'.$terminal_id.'" class="" onclick="getTerminal(this)" title="Edit" href="javascript:void(0)">
													<i class="fa fa-fw fa-edit"></i>
												</a>';
								}
								if($delete)
								{
									$rows .= '    <a data-id="'.$i.'" data-row-id="'.$terminal_id.'" class="" onclick="deleteTerminal(this)" title="Delete" href="javascript:void(0)">
													<i class="fa fa-fw fa-close"></i>
												</a>';
								}
								$rows .= ' </td>';
							}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="2" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function getTerminal()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$terminalData = $this->common->selectQuery("*",TB_TERMINAL,array('terminal_id'=>$this->encrypt->decode($postData['key'])));
				if($terminalData){
					echo json_encode(array("status"=>"success","terminalData"=>$terminalData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities found.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveTerminal()
	{
		// echo "<pre>";print_r($_FILES);
		// echo "<pre>";print_r($_POST);die;
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{	$fac_images='';

				// ------ images ------
				$files=$_FILES['terminal_thumb'];
				$cpt = count($_FILES['terminal_thumb']['name']);

				$postData = $this->input->post();
				$insertArr = array('terminal_name'=>$postData["terminal_name"],'terminal_desc'=>$postData["terminal_desc"]);
				if(get_user_data('role')==1)
				{
					$insertArr["airport_id"]=$postData["airport_id"];
				}
				else
				{
					$insertArr["airport_id"]=get_user_data('air_port_id');
				}

						// -------- images code here------------
						if($cpt==0){
							foreach ($_POST['terminal_thumb_old'] as $key => $value) {
								$all_img_old[]=$value;
							}
							$fac_images.= implode(",", $all_img_old);
							$insertArr['terminal_images']=$fac_images;
						}else{
							$fac_images='';
							foreach ($_POSTsaveTerminal['terminal_thumb_old'] as $key => $value) {
								$all_img_old[]=$value;
							}
							if(!(empty($all_img_old))){
							$fac_images.= implode(",", $all_img_old).",";
						}else{
							$fac_images.='';
						}


						for($i=0; $i<$cpt; $i++){
						if($files['name'][$i])
						{
							$userfile_extn = explode(".", strtolower($files['name'][$i]));
							$ext=$userfile_extn[1];
							$allowed=array('gif','jpg','jpeg','png');
							if(!in_array($ext,$allowed))
							{
								echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"terminal_thumb"));	 exit;
							}
							if($files['size'][$i] > 5242880)//2097152
							{
								echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"terminal_thumb"));exit;
							}

							// ------ check image dimension ---------
							$image_info = getimagesize($_FILES["terminal_thumb"]['tmp_name'][$i]);
							$image_width = $image_info[0];
							$image_height = $image_info[1];
							if ($image_width > 300 || $image_height > 300)
							{
							echo json_encode(array("status"=>"error","msg"=> "Please upload image of 300X300.","field"=>"terminal_thumb"));exit;
							}

							$new_image_name=uniqid(rand(), true).time().'.'.$ext;
							$all_img[]=$new_image_name;

							$quality=40;
							$source=$files['tmp_name'][$i];
							$destination='./images/terminal/'.$new_image_name;
							$info = getimagesize($source);
							if ($info['mime'] == 'image/jpeg')
								$image = imagecreatefromjpeg($source);
							elseif ($info['mime'] == 'image/gif')
								$image = imagecreatefromgif($source);
							elseif ($info['mime'] == 'image/png')
								$image = imagecreatefrompng($source);
							imagejpeg($image, $destination, $quality);

						}
					}
					$fac_images.= implode(",", $all_img);
					$insertArr['terminal_images']=$fac_images;
				}



				if($postData["terminal_key"]){


					$insertcategory_id = $this->common->update(TB_TERMINAL,array('terminal_id'=>$this->encrypt->decode($postData['terminal_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Terminal has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{

					// $terminalExist = $this->common->select('*',TB_TERMINAL,array('terminal_name'=>$postData["terminal_name"]));
          //
					// if($terminalExist)
					// {
					// 	echo json_encode(array("status"=>"error","action"=>"exist","msg"=>"Terminal Name Alredy Exist.")); exit;
					// }
					$insertcategory_id = $this->common->insert(TB_TERMINAL,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Terminal has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteTerminal()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$terminal_id=$this->encrypt->decode($postData['key']);

				/*print_r($terminal_id);

				exit();*/
				/*$admin=$this->common->selectQuery("*",TB_TERMINAL,array(TB_TERMINAL.'.terminal_id'=>$terminal_id,TB_ADMIN.".role" => 2),array(),array(TB_ADMIN=>TB_TERMINAL.".airport_id=".TB_ADMIN.".air_port_id"));*/

				$admin = $this->common->select('*',TB_ADMIN,array('terminal_id' => $terminal_id));
				if(count($admin) > 0)
				{
					echo json_encode(array("status"=>"error","msg"=>"Can not delete terminal,this terminal is assign to a Terminal Admin."));

					exit();
				}
				$store=$this->common->selectQuery("count(store_id) as cnt",TB_STORES,array('terminal_id'=>$terminal_id));
				if($store[0]['cnt'])
				{
					echo json_encode(array("status"=>"error","msg"=>"Can not delete terminal,stores available under this terminal.")); exit;
				}
				else
				{
					$terminalData = $this->common->delete(TB_TERMINAL,array('terminal_id'=>$terminal_id));
					if($terminalData){
						echo json_encode(array("status"=>"success","msg"=>"Terminal deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
}
