<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,4);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($role==1)
			{
				$widthArr=array('store_name'=>25,'terminal_name'=>20,'airport_name'=>25,'store_thumb'=>20,'action'=>10);
			}
			else
			{
				if($role==2)
				{
					$widthArr=array('store_name'=>30,'terminal_name'=>30,'store_thumb'=>30,'action'=>10);
				}
				if($role==3)
				{
					$widthArr=array('store_name'=>45,'store_thumb'=>45,'action'=>10);
				}
				if($editRight || $deleteRight)
				{
					$widthArr['action']=10;
				}
			}
			$data['widthArr']=$widthArr;
			//$data["airports"] = $this->common->selectQuery("id as airport_id,name",TB_AIRPORTS,array());
			$data["airports"] = $this->common->selectQuery('DISTINCT '.TB_AIRPORTS.".id as airport_id,name",TB_AIRPORTS,array(TB_ADMIN.'.role'=>2),array(),array(TB_ADMIN=>TB_ADMIN.'.air_port_id = '.TB_AIRPORTS.'.id'));


			$data["category"] = $this->common->selectQuery("category_id,category",TB_STORE_CATEGORY,array());



			if($accessRight)
			{
				$this->load->view('store/listStore',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}
	function listStore()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){

				$role=get_user_data('role');
				$dtRight=checkRight($role,4);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];

				$where=array();
				if($role!=1)
				{
					$where=array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'));
					$terminal_id=get_user_data('terminal_id');
					if($role==3 && $terminal_id)
					{
						$where[TB_TERMINAL.'.terminal_id']=$terminal_id;
					}
				}
				$postData = $this->input->post();
				/*$arrayColumn = array("store_id"=>"store_id","store_name"=>"store_name","store_thumb"=>"store_thumb","name"=>"name","terminal_name"=>TB_TERMINAL.".terminal_name");*/

				$arrayColumn = array("store_id"=>"store_id","store_name"=>"store_name","store_thumb"=>"store_thumb","name"=>"name");

				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");


				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORES,'*','listStore',$where,array(TB_AIRPORTS=>TB_STORES.".airport_id=".TB_AIRPORTS.".id"));
				/*$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORES,TB_STORES.'*','listStore',$where,array(TB_AIRPORTS=>TB_STORES.".airport_id=".TB_AIRPORTS.".id"));*/

				

				/*echo $this->db->last_query();

				exit();*/


				//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_FACILITY,'store_facility_id,store_name,facility_name','listStoreFacility',array(),array(TB_STORES=>TB_STORE_FACILITY.".store_id=".TB_STORES.".store_id"));
				//echo "<pre>";print_r($result);die();
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;


					foreach ($result['rows'] as $store) {
						$store_id = $this->encrypt->encode($store['store_id']);

						$terminalId = $this->common->select('*',TB_TERMINAL,array('terminal_id' => $store['terminal_id']));
						// ---------- store multiple images ------------------
						if("" != $store['store_thumb'])
						{
							$imgarr = explode(",", $store['store_thumb']);
							$storeimg = $imgarr[0];
						}
						else
						{
							$storeimg = "";
						}

						$rows .= '<tr category_id="'.$store_id.'">
	                            <td class="text-left">'.$store['store_name'].'</td>';
								if($role!=3)
								{
									$rows .= '<td class="text-left">'.$terminalId[0]['terminal_name'].'</td>';
									if($role!=2)
									{
										$rows .= '<td class="text-left">'.$store['name'].'</td>';
									}
								}

								if("" != $storeimg)
								{

	                    			$rows .= '<td class="text-left"><img src="'.base_url().'images/store/'.$storeimg.'" width="100"></td>';
	                    		}
	                    		else
	                    		{
	                    			$rows .= '<td class="text-left">Image not available</td>';	
	                    		}
						if($edit || $delete)
						{
							$rows.='<td class="text-left">';
										if($edit){
											$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="getStore(this)" title="Edit" href="javascript:void(0)">
												<i class="fa fa-fw fa-edit"></i>
											</a>';
										}
										if($delete)
										{
											$rows .= '<a data-id="'.$i.'" data-row-id="'.$store_id.'" class="" onclick="deleteStore(this)" title="Delete" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
											</a>';
										}
							$rows .= '</td>';
						}
	                    $rows.='</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function getStore()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeData = $this->common->selectQuery("*",TB_STORES,array('store_id'=>$this->encrypt->decode($postData['key'])));
				// $storeData = $this->common->selectQuery("*,CONCAT('".base_url()."images/store/',store_thumb) as store_thumb",TB_STORES,array('store_id'=>$this->encrypt->decode($postData['key'])));
				if($storeData){
					echo json_encode(array("status"=>"success","storeData"=>$storeData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteStore()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$store_id=$this->encrypt->decode($postData['key']);

				$store_facility=$this->common->selectQuery("count(store_facility_id) as cnt",TB_STORE_FACILITY,array('store_id'=>$store_id));
				$store_offer=$this->common->selectQuery("count(store_offer_id) as cnt",TB_STORE_OFFER,array('store_id'=>$store_id));
				/*if($store_facility[0]['cnt'] || $store_offer[0]['cnt'])
				{
					echo json_encode(array("status"=>"error","msg"=>"Can not delete store, there are products or offers available under this store.")); exit;
				}*/
				/*else
				{*/
					$storeData = $this->common->delete(TB_STORES,array('store_id'=>$store_id));
					if($storeData){
						echo json_encode(array("status"=>"success","msg"=>"Store deleted successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
					}
				//}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	public function saveStore(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$store_images='';
				$postData = $this->input->post();
				$files=$_FILES['store_thumb'];
				$cpt = count($_FILES['store_thumb']['name']);
				$insertArr = array('store_name'=>$postData["store_name"],'store_desc'=>$postData["store_desc"],'offer_desc'=>$postData["offer_desc"],"store_category_id"=>$postData["store_category_id"],"terminal_id"=>$postData["terminal_id"]);
				if(get_user_data('role')==1)
				{
					$insertArr["airport_id"]=$postData["airport_id"];
				}
				else
				{
					$insertArr["airport_id"]=get_user_data('air_port_id');
				}

				if($cpt==0)
				{
					foreach ($_POST['store_thumb_old'] as $key => $value) {
						$all_img_old[]=$value;
					}
					$store_images.= implode(",", $all_img_old);
					$insertArr['store_thumb']=$store_images;
				}
				else
				{
					$store_images='';
					foreach ($_POST['store_thumb_old'] as $key => $value) {
						$all_img_old[]=$value;
					}
					if(!(empty($all_img_old)))
					{
						$store_images.= implode(",", $all_img_old).",";
					}
					else
					{
						$store_images.='';
					}


					for($i=0; $i<$cpt; $i++)
					{
						if($files['name'][$i])
						{
							// $userfile_extn = explode(".", strtolower($files['name'][$i]));
		         			// $ext=$userfile_extn[1];
							$allowed=array('gif','jpg','jpeg','png');
							// if(!in_array($ext,$allowed))
							// {
							// 	echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"store_thumb"));	 exit;
							// }
							if($files['size'][$i] > 5242880)//2097152
							{
								echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"store_thumb"));exit;
							}

							// ------ check image dimension ---------
							$image_info = getimagesize($_FILES["store_thumb"]['tmp_name'][$i]);

							// print_r($image_info);
		          			//
							// exit();
							$extension = image_type_to_extension($image_info[2]);

							$image_width = $image_info[0];
							$image_height = $image_info[1];
							if ($image_width > 300 || $image_height > 300)
							{
								echo json_encode(array("status"=>"error","msg"=> "Please upload image of 300X300.","field"=>"store_thumb"));exit;
							}

							$allowed=array('.gif','.jpg','.jpeg','.png');
							if(!in_array($extension,$allowed))
							{
								echo json_encode(array("status"=>"error","msg"=> "Invalid image type (.".$ext.").","field"=>"store_thumb"));	 exit;
							}
							// if($files['size'][$i] > 5242880)//2097152
							// {
							// 	echo json_encode(array("status"=>"error","msg"=> "Image size too big.","field"=>"store_thumb"));exit;
							// }


							//$new_image_name=uniqid(rand(), true).time().'.'.$ext;
							$new_image_name=uniqid(rand(), true).time().$extension;
							$all_img[]=$new_image_name;

							$quality=40;
							$source=$files['tmp_name'][$i];
							$destination='./images/store/'.$new_image_name;
							$info = getimagesize($source);
							if ($info['mime'] == 'image/jpeg')
								$image = imagecreatefromjpeg($source);
							elseif ($info['mime'] == 'image/gif')
								$image = imagecreatefromgif($source);
							elseif ($info['mime'] == 'image/png')
								$image = imagecreatefrompng($source);
							imagejpeg($image, $destination, $quality);

						}
					}
					$store_images.= implode(",", $all_img);
					$insertArr['store_thumb']=$store_images;
				}

				/*print_r($insertArr);

				exit();*/
				if($postData["store_key"]){
					$insertcategory_id = $this->common->update(TB_STORES,array('store_id'=>$this->encrypt->decode($postData['store_key'])),$insertArr);

					/*echo $this->db->last_query();

					exit();*/
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{

					/*print_r($insertArr);

					exit();*/
					$insertcategory_id = $this->common->insert(TB_STORES,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Store has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	//function getStoreFacility1()
	//{
	//	if(is_ajax_request())
	//	{
	//		if(is_user_logged_in()){
	//			$postData = $this->input->post();
	//			$storeFaciData = $this->common->selectQuery("*",TB_STORE_FACILITY,array('store_id'=>$this->encrypt->decode($postData['key'])));
	//			if($storeFaciData){
	//				echo json_encode(array("status"=>"success","storeFaciData"=>$storeFaciData)); exit;
	//			}else{
	//				echo json_encode(array("status"=>"success","msg"=>"No facilities added.")); exit;
	//			}
	//		}else{
	//			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
	//		}
	//	}
	//}

	function saveFaciStore()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				if($postData["fstore_key"]){
					$store_id=$this->encrypt->decode($postData['fstore_key']);
					$this->common->delete(TB_STORE_FACILITY,array('store_id'=>$store_id));
					$cnt=0;
					foreach($postData["facility"] as $k => $v)
					{
						if($v!="")
						{
							$insertArr = array('store_id'=>$store_id,'facility_name'=>$v);
							$this->common->insert(TB_STORE_FACILITY,$insertArr);
							$cnt++;
						}
					}
					if($cnt){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store facilities has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	//function getStoreOffer1()
	//{
	//	if(is_ajax_request())
	//	{
	//		if(is_user_logged_in()){
	//			$postData = $this->input->post();
	//			$storeOfferData = $this->common->selectQuery("*",TB_STORE_OFFER,array('store_id'=>$this->encrypt->decode($postData['key'])));
	//			if($storeOfferData){
	//				echo json_encode(array("status"=>"success","storeOfferData"=>$storeOfferData)); exit;
	//			}else{
	//				echo json_encode(array("status"=>"success","msg"=>"No offers added.")); exit;
	//			}
	//		}else{
	//			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
	//		}
	//	}
	//}

	function saveOfferStore()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				if($postData["ostore_key"]){
					$store_id=$this->encrypt->decode($postData['ostore_key']);
					$this->common->delete(TB_STORE_OFFER,array('store_id'=>$store_id));
					$cnt=0;
					foreach($postData["offer"] as $k => $v)
					{
						if($v!="")
						{
							$insertArr = array('store_id'=>$store_id,'offer_name'=>$v);
							$this->common->insert(TB_STORE_OFFER,$insertArr);
							$cnt++;
						}
					}
					if($cnt){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store offers has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function facility()
	{
		if(is_user_logged_in())
		{
			$role = get_user_data('role');
			$dtRight = checkRight($role,5);
			$data['addRight'] = $dtRight['add_privilege'];
			$data['editRight'] = $editRight=$dtRight['update_privilege'];
			$data['deleteRight'] = $deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($role==1)
			{
				$widthArr=array('facility_name'=>25,'store_name'=>25,"terminal_name"=>20,'airport_name'=>20,'action'=>10);
			}
			else
			{
				if($editRight || $deleteRight)
				{
					$widthArr=array('facility_name'=>45,'store_name'=>45,'action'=>10);
				}
				else
				{
					$widthArr=array('facility_name'=>50,'store_name'=>50);
				}
			}
			$data['widthArr']=$widthArr;
			if($accessRight)
			{
				$where=array();
				if($role!=1)
				{
					$where = array('airport_id'=>get_user_data('air_port_id'));
					if($role == 3){
						$where = array('airport_id'=>get_user_data('air_port_id'), 'terminal_id'=>get_user_data('terminal_id'));
					}
				}
				$data["store"] = $this->common->selectQuery("store_id,store_name",TB_STORES,$where);
				$this->load->view('store/listStoreFacility',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	function listStoreFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,5);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];

				$postData = $this->input->post();

				$where=array();
				if($role!=1)
				{
					$where=array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'));
					if($role == 3){
						$where = array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'), TB_STORES.'.terminal_id'=>get_user_data('terminal_id'));
					}
				}

				$arrayColumn = array("store_facility_id"=>"store_facility_id","store_id"=>TB_STORE_FACILITY.".store_id","store_name"=>"store_name","facility_name"=>"facility_name","name"=>"name","terminal_name"=>TB_TERMINAL.".terminal_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_FACILITY,'*','listStoreFacility',$where,array(TB_STORES=>TB_STORE_FACILITY.".store_id=".TB_STORES.".store_id",TB_AIRPORTS=>TB_STORES.".airport_id=".TB_AIRPORTS.".id",TB_TERMINAL=>TB_STORES.".terminal_id=".TB_TERMINAL.".terminal_id"));
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $facility) {
						$facility_id = $this->encrypt->encode($facility['store_facility_id']);
						$rows .= '<tr category_id="'.$facility_id.'">
								<td class="text-left">'.$facility['facility_name'].'</td>
								<td class="text-left">'.$facility['store_name'].'</td>';
								if($role==1)
								{
									$rows .= '<td class="text-left">'.$facility['terminal_name'].'</td>';
									$rows .= '<td class="text-left">'.$facility['name'].'</td>';
								}
						if($edit || $delete)
						{
						$rows .= '<td class="text-left">';
						if($edit)
						{
						$rows .= '	<a data-id="'.$i.'" data-row-id="'.$facility_id.'" class="" onclick="getStoreFacility(this)" title="Edit" href="javascript:void(0)">
										<i class="fa fa-fw fa-edit"></i>
									</a>';
						}
						if($delete)
						{
						$rows .= '	<a data-id="'.$i.'" data-row-id="'.$facility_id.'" class="" onclick="deleteStoreFacility(this)" title="Delete" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>';
						}
						$rows .= '</td>';
						}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function getStoreFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeFaciData = $this->common->selectQuery("*",TB_STORE_FACILITY,array('store_facility_id'=>$this->encrypt->decode($postData['key'])));
				if($storeFaciData){
					echo json_encode(array("status"=>"success","storeFaciData"=>$storeFaciData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities added.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveStoreFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr = array('store_id'=>$postData["store_id"],'facility_name'=>$postData["facility_name"]);
				if($postData["storefacility_key"]){
					$insertcategory_id = $this->common->update(TB_STORE_FACILITY,array('store_facility_id'=>$this->encrypt->decode($postData['storefacility_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store product category has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertcategory_id = $this->common->insert(TB_STORE_FACILITY,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Store product category has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteStoreFacility()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeData = $this->common->delete(TB_STORE_FACILITY,array('store_facility_id'=>$this->encrypt->decode($postData['key'])));
				if($storeData){
					echo json_encode(array("status"=>"success","msg"=>"Product category deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function offer()
	{
		if(is_user_logged_in())
		{
			$role=get_user_data('role');
			$dtRight=checkRight($role,6);
			$data['addRight']=$dtRight['add_privilege'];
			$data['editRight']=$editRight=$dtRight['update_privilege'];
			$data['deleteRight']=$deleteRight=$dtRight['delete_privilege'];
			$accessRight=$dtRight['access_privilege'];
			if($role==1)
			{
				$widthArr=array('offer_name'=>25,'store_name'=>25,'terminal_name'=>20,'airport_name'=>20,'action'=>10);
			}
			else
			{
				if($editRight || $deleteRight)
				{
					$widthArr=array('offer_name'=>45,'store_name'=>45,'action'=>10);
				}
				else
				{
					$widthArr=array('offer_name'=>50,'store_name'=>50);
				}
			}
			$data['widthArr']=$widthArr;

			if($accessRight)
			{
				$where=array();
				if($role!=1)
				{
					$where=array('airport_id'=>get_user_data('air_port_id'));
					if($role == 3){
						$where = array('airport_id'=>get_user_data('air_port_id'), 'terminal_id'=>get_user_data('terminal_id'));
					}
				}
				$data["store"] = $this->common->selectQuery("store_id,store_name",TB_STORES,$where);
				$this->load->view('store/listStoreOffer',$data);
			}
			else
			{
				redirect("login");
				exit;
			}
		}else{
			redirect("login");
			exit;
		}
	}

	function listStoreOffer()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$role=get_user_data('role');
				$dtRight=checkRight($role,6);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];
				$where=array();
				if($role!=1)
				{
					$where=array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'));
					if($role == 3){
						$where = array(TB_STORES.'.airport_id'=>get_user_data('air_port_id'), TB_STORES.'.terminal_id'=>get_user_data('terminal_id'));
					}
				}
				$postData = $this->input->post();
				$arrayColumn = array("store_offer_id"=>"store_offer_id","store_id"=>TB_STORE_OFFER.".store_id","store_name"=>"store_name","offer_name"=>"offer_name","name"=>"name","terminal_name"=>TB_TERMINAL.".terminal_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_OFFER,'store_offer_id,store_name,offer_name,name,'.TB_TERMINAL.'.terminal_name','listStoreOffer',$where,array(TB_STORES=>TB_STORE_OFFER.".store_id=".TB_STORES.".store_id",TB_AIRPORTS=>TB_STORES.".airport_id=".TB_AIRPORTS.".id",TB_TERMINAL=>TB_STORES.".terminal_id=".TB_TERMINAL.".terminal_id"));
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $offer) {
						$offer_id = $this->encrypt->encode($offer['store_offer_id']);
						$rows .= '<tr category_id="'.$offer_id.'">
	                            <td class="text-left">'.$offer['offer_name'].'</td>
								<td class="text-left">'.$offer['store_name'].'</td>';
								if($role==1)
								{
									$rows .= '<td class="text-left">'.$offer['terminal_name'].'</td>';
									$rows .= '<td class="text-left">'.$offer['name'].'</td>';
								}
	                    if($edit || $delete)
						{
							$rows .= ' <td class="text-left">';
						if($edit)
						{
							$rows .= '<a data-id="'.$i.'" data-row-id="'.$offer_id.'" class="" onclick="getStoreOffer(this)" title="Edit" href="javascript:void(0)">
											<i class="fa fa-fw fa-edit"></i>
										</a>';
						}
						if($delete)
						{
							$rows .= ' <a data-id="'.$i.'" data-row-id="'.$offer_id.'" class="" onclick="deleteStoreOffer(this)" title="Delete" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
										</a>';
						}
							$rows .= ' </td>';
						}
	                    $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);

			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function getStoreOffer()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeOfferData = $this->common->selectQuery("*",TB_STORE_OFFER,array('store_offer_id'=>$this->encrypt->decode($postData['key'])));
				if($storeOfferData){
					echo json_encode(array("status"=>"success","storeOfferData"=>$storeOfferData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities added.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveStoreOffer()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr = array('store_id'=>$postData["store_id"],'offer_name'=>$postData["offer_name"]);
				if($postData["storeoffer_key"]){
					$insertcategory_id = $this->common->update(TB_STORE_OFFER,array('store_offer_id'=>$this->encrypt->decode($postData['storeoffer_key'])),$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store offer has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertcategory_id = $this->common->insert(TB_STORE_OFFER,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Store offer has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function deleteStoreOffer()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeData = $this->common->delete(TB_STORE_OFFER,array('store_offer_id'=>$this->encrypt->decode($postData['key'])));
				if($storeData){
					echo json_encode(array("status"=>"success","msg"=>"Offer deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function tesdfst()
	{
		error_reporting(-1);
		//$hours=2;
		//$min=30;
		//$td=360;
		//$degreesinmin=360/60;
		//$minutehandmovesinmin=$degreesinmin;
		//$hourhandmovesinmin=(360/12)/60;
		//$hourpos=0;
		//$minpos=0;
		//if($min==0)
		//{
		//	$hourpos=($hours*5)*$minutehandmovesinmin;
		//	$minpos=0;
		//}
		//else
		//{
		//	$hourpos=(($hours*5)*$minutehandmovesinmin)+$min*$hourhandmovesinmin;
		//	$minpos=$min*$minutehandmovesinmin;
		//}
		//if($hourpos>$minpos)
		//{
		//	echo $hourpos-$minpos;
		//}
		//else
		//{
		//	echo $minpos-$hourpos;
		//}
		//$a=1;
		////echo "<pre>";print_r(__sleep());
		//$role=get_user_data('role');
		//$addRight=getRightArr($role);
		//echo "<pre>";print_r($addRight);
		//
		//$url = 'https://fcm.googleapis.com/fcm/send';
		//$fields = array(
		//	'registration_ids' => array("ej7tdc03jb8:APA91bG65qe8XOE9RUhsv_KQ2g9I5dDaO2c37QMm9AwMZ1ysaGyZ9nVcpyogEW9KAvX2uz1Hf8cci55TJCpFv5Thh2Vc1oyWm2k0qJ2MLWDyYWzEDo_2LRSmnJIk4i6WjzbH8MHuJ9gp"),
		//	'data' => "hii fcm",
		//);
		//$headers = array(
		//	'Authorization: key=' . "AIzaSyBo2dErFcdcgM1BvKNGAMAsRSdI_WVQxPo",
		//	'Content-Type: application/json'
		//);
		//$ch = curl_init();
		//curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_POST, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		//$result = curl_exec($ch);
		//if ($result === FALSE) {
		//	die('Curl failed: ' . curl_error($ch));
		//}
		//curl_close($ch);


		$url = 'https://fcm.googleapis.com/fcm/send';

		$fields = array (
				'registration_ids' =>array("eCSSBVlnpFM:APA91bGXxABnBUuGaE4sSqAqmavWNYxM0bJlEVzcGCLMFxdj8JuwR-FPkRkV_StFmuVWfu8K7KpGdLdxnjWn1s6CG1XXtIlWvKLPrATAl9Tctb81iVG61gPyuY99MK18Bhdm_KlHEPjK"),
				'data' => array ("message" => "Hii"),
		);
		$fields = json_encode ($fields);
	echo $fields;
		$headers = array (
				'Authorization: key='."AIzaSyBo2dErFcdcgM1BvKNGAMAsRSdI_WVQxPo",
				'Content-Type: application/json'
		);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		echo $result;

		echo "done";
		curl_close ( $ch );
	}
	public function sendroid($email='p@gmail.com',$msg1='hi')
	{
		// API access key from Google API's Console
		$storeOfferData = $this->common->selectQuery("*",TB_USERS,array('email'=>$email));
		define( 'API_ACCESS_KEY', 'AAAAvNe5vMI:APA91bENRuoVWFyho14Y44Vyqhm67iD_nNls2q9roroLRRE6K4TMyn5dtmdjsCqtw86WDWGtM7zZ7rMzJ_xeTazResXj9asfBCtSUfusU8FzcFMbCW3aDgsl4aZ3dK66TX1O2Mr9HvHMzzk-5kTzcR5T5ImhmlnJXw' );

		$registrationIds = array($storeOfferData[0]['device_token']);
		// prep the bundle
		$msg = array
		(
			'message' 	=> $msg1,
			'title'		=> 'title',
			'subtitle'	=> 'subtitle',
			//'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			'sound'		=> 1,
			//'largeIcon'	=> 'large_icon',
			//'smallIcon'	=> 'small_icon'
		);
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);

		$headers = array
		(
			'Authorization: key=' . "AIzaSyBo2dErFcdcgM1BvKNGAMAsRSdI_WVQxPo",
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		//curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		add_notification(array('userid'=>$storeOfferData[0]['id'],"not_title"=>$msg1));
		echo $result;
	}
	function sendme($msg1='hi')
	{
		$msg = array
		(
			'message' 	=> $msg1,
			'title'		=> 'title',
			'subtitle'	=> 'subtitle',
			//'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			'sound'		=> 1,
			//'largeIcon'	=> 'large_icon',
			//'smallIcon'	=> 'small_icon'
		);
		$fields = array
		(
			'registration_ids' 	=> array("fWNw9-mlR2I:APA91bFVQKpSbx9o34uqV4P1vjHCctWl6SNZwkyNehZAJl6qK8kTpdipcQtFTpJvNfjCvmeJIFFt7WuS7D5CWJGCvSVTIHG2wEmdaBpFroeCdEd4DZ8sgD1fQpoRPLvzEk2InJixBK_S"),
			'data'			=> $msg
		);

		$headers = array
		(
			'Authorization: key=' . "AIzaSyBo2dErFcdcgM1BvKNGAMAsRSdI_WVQxPo",
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		//curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		add_notification(array('userid'=>$storeOfferData[0]['id'],"not_title"=>$msg1));
		echo $result;
	}
	function geneateEnc()
	{
		echo md5('vardhaman8805027371');//614266cc2ca75d357d87bc33b00ecedf
	}
	function sendgattamsgs()
	{
		$token_array=$this->common->selectQuery("device_token",TB_USERS);
		//echo "<pre>";print_r($token_array);
		sendAndroidNotification($token_array,'hiiii');
		//echo "hi";
	}
	function img_save_to_file($fld=null)
	{
		$imagePath = realpath("images/airport/".$fld."/").'/';
//$response = Array(
//				"status" => 'error',
//				"message" => 'Can`t upload File; no write Access'.$imagePath
//			);
//			print json_encode($response);exit;
		$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
		$temp = explode(".", $_FILES["img"]["name"]);
		$extension = end($temp);

		//Check write Access to Directory

		if(!is_writable($imagePath)){
			$response = Array(
				"status" => 'error',
				"message" => 'Can`t upload File; no write Access'.$imagePath
			);
			print json_encode($response);
			return;
		}

		if ( in_array($extension, $allowedExts))
		  {
		  if ($_FILES["img"]["error"] > 0)
			{
				 $response = array(
					"status" => 'error',
					"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
				);
			}
		  else
			{

			  $filename = $_FILES["img"]["tmp_name"];
			  list($width, $height) = getimagesize( $filename );

			  move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

			  $response = array(
				"status" => 'success',
				"url" => base_url().'images/airport/'.$fld.'/'.$_FILES["img"]["name"],
				"width" => $width,
				"height" => $height
			  );

			}
		  }
		else
		  {
		   $response = array(
				"status" => 'error',
				"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
			);
		  }
		  print json_encode($response);
		}
		function img_crop_to_file($fld=null)
		{
			$imgUrl = $_POST['imgUrl'];
			// original sizes
			$imgInitW = $_POST['imgInitW'];
			$imgInitH = $_POST['imgInitH'];
			// resized sizes
			$imgW = $_POST['imgW'];
			$imgH = $_POST['imgH'];
			// offsets
			$imgY1 = $_POST['imgY1'];
			$imgX1 = $_POST['imgX1'];
			// crop box
			$cropW = $_POST['cropW'];
			$cropH = $_POST['cropH'];
			// rotation angle
			$angle = $_POST['rotation'];

			$jpeg_quality = 100;

			$rrtt="/croppedImg_".rand();
			$output_filename = base_url().'images/airport/'.$fld.$rrtt;
			$imagePath = realpath("images/airport/".$fld."/").'/'.$rrtt;
			// uncomment line below to save the cropped image in the same location as the original image.
			//$output_filename = dirname($imgUrl). "/croppedImg_".rand();

			$what = getimagesize($imgUrl);

			switch(strtolower($what['mime']))
			{
				case 'image/png':
					$img_r = imagecreatefrompng($imgUrl);
					$source_image = imagecreatefrompng($imgUrl);
					$type = '.png';
					break;
				case 'image/jpeg':
					$img_r = imagecreatefromjpeg($imgUrl);
					$source_image = imagecreatefromjpeg($imgUrl);
					error_log("jpg");
					$type = '.jpeg';
					break;
				case 'image/gif':
					$img_r = imagecreatefromgif($imgUrl);
					$source_image = imagecreatefromgif($imgUrl);
					$type = '.gif';
					break;
				default: die('image type not supported');
			}


			//Check write Access to Directory

			if(!is_writable(dirname($imagePath))){
				$response = Array(
					"status" => 'error',
					"message" => 'Can`t write cropped File'
				);
			}else
			{

				// resize the original image to size of editor
				$resizedImage = imagecreatetruecolor($imgW, $imgH);
				imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
				// rotate the rezized image
				$rotated_image = imagerotate($resizedImage, -$angle, 0);
				// find new width & height of rotated image
				$rotated_width = imagesx($rotated_image);
				$rotated_height = imagesy($rotated_image);
				// diff between rotated & original sizes
				$dx = $rotated_width - $imgW;
				$dy = $rotated_height - $imgH;
				// crop rotated image to fit into original rezized rectangle
				$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
				imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
				imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
				// crop image into selected area
				$final_image = imagecreatetruecolor($cropW, $cropH);
				imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
				imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
				// finally output png image
				//imagepng($final_image, $output_filename.$type, $png_quality);
				imagejpeg($final_image, $imagePath.$type, $jpeg_quality);
				$response = Array(
					"status" => 'success',
					"url" => $output_filename.$type
				);
		}
	print json_encode($response);
	}

	function category()
	{
			if(is_user_logged_in())
			{
				//$role = get_user_data('role');

				/*if($role==1 || $role==2)
				{*/
					$widthArr=array('category_name'=>25,'action'=>10);
					$this->load->view('store/listCategory');
				//}
			}
			else
			{
				redirect("login");
				exit;
			}

	}

	function listStoreCategory()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{

				$role=get_user_data('role');
				$dtRight=checkRight($role,5);
				$add=$dtRight['add_privilege'];
				$edit=$dtRight['update_privilege'];
				$delete=$dtRight['delete_privilege'];


				$postData = $this->input->post();


				$where=array();

				$arrayColumn = array("category_id"=>"category_id","category"=>"category");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'category_id','category_id',TB_STORE_CATEGORY,'*','listStoreCategory',$where);
				//$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'store_name','store_name',TB_STORE_FACILITY,'*','listStoreFacility',$where,array(TB_STORES=>TB_STORE_FACILITY.".store_id=".TB_STORES.".store_id",TB_AIRPORTS=>TB_STORES.".airport_id=".TB_AIRPORTS.".id",TB_TERMINAL=>TB_STORES.".terminal_id=".TB_TERMINAL.".terminal_id"));
				//echo $this->db->last_query();exit();
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $facility)
					{
						$category_id = $this->encrypt->encode($facility['category_id']);
						$rows .= '<tr category_id="'.$category_id.'">
								<td class="text-left">'.$facility['category'].'</td>';
								// if($role==1)
								// {
								// 	$rows .= '<td class="text-left">'.$facility['category'].'</td>';

								// }
						if($edit || $delete)
						{
						$rows .= '<td class="text-left">';
						if($edit)
						{
									$rows .= '	<a data-id="'.$i.'" data-row-id="'.$category_id.'" class="" onclick="getStoreCategory(this)" title="Edit" href="javascript:void(0)">
										<i class="fa fa-fw fa-edit"></i>
									</a>';
						}
						if($delete)
						{
								$rows .= '	<a data-id="'.$i.'" data-row-id="'.$category_id.'" class="" onclick="deleteStoreCategory(this)" title="Delete" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>';
						}
								$rows .= '</td>';
						}
	              $rows .= '</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="100%" align="center">No Record Found.</td></tr>';
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}



	function getStoreCategory()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeCatData = $this->common->selectQuery("*",TB_STORE_CATEGORY,array('category_id'=>$this->encrypt->decode($postData['key'])));
				if($storeCatData){
					echo json_encode(array("status"=>"success","storeFaciData"=>$storeCatData[0])); exit;
				}else{
					echo json_encode(array("status"=>"success","msg"=>"No facilities added.")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}

	function saveStoreCategory()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$insertArr = array('category'=>$postData["category_name"]);
				if($postData["storefacility_key"]){

					$insertcategory_id = $this->common->update(TB_STORE_CATEGORY,array('category_id'=>$this->encrypt->decode($postData['storefacility_key'])),$insertArr);

					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Store category has been updated successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}else{
					$insertcategory_id = $this->common->insert(TB_STORE_CATEGORY,$insertArr);
					if($insertcategory_id){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Store category has been added successfully.")); exit;
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;
					}
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}



	function deleteStoreCategory()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$storeData = $this->common->delete(TB_STORE_CATEGORY,array('category_id'=>$this->encrypt->decode($postData['key'])));
				if($storeData){
					echo json_encode(array("status"=>"success","msg"=>"Store category deleted successfully.")); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}





	function test()
	{
		//$this->load->view('store/test');

		//echo phpinfo();
		/*$arr=array('user_token' => "2",'airport_id' => array('0'=>1,'1'=>2));
		echo "<pre>";print_r($arr);
		echo json_encode($arr)*/;
		$subject="Welcome";
		$message=getMailTemplate('templat',array('name'=>"Vardhaman","msg"=>"Your account has been created successfully."));
		echo $message;
		//$res=sentEmail(array('lesFlammant@exceptionaire.co','TDN'), array("hhpcharles@gmail.com"),array(),$subject,$message);


		//$msg = "First line of text\nSecond line of text";

		// use wordwrap() if lines are longer than 70 characters
		//$msg = wordwrap($msg,70);

		// send email
		//$link=base_url();
		//$message1="Hello Vardhaman<br/>
		//<br/>
		//This is your website.
		//Url : $link
		//<br/>
		//<br/>
		//-----------------------------------------<br/>
		//TouchDown Nigeria.
		//";
		//$headers[] = "From: TDN<lesFlammant@exceptionaire.co>";
		//$headers[] = 'MIME-Version: 1.0';
		//$headers[] = 'Content-type: text/html; charset=iso-8859-1';
		//$header=implode("\r\n", $headers);
		//mail("hhpcharles@gmail.com",$subject,$message,$header);

		//echo $message;
		//echo "<pre>";print_r($res);
	}
}
