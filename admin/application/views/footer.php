  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Touchdown Nigeria</b>
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> Touchdown Nigeria.</strong> All rights reserved.
  </footer>
</div>
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/app.min.js"></script>
<script src="<?php echo base_url(); ?>js/demo.js"></script>
<script src="<?php echo base_url(); ?>js/toastr.min.js"></script>
<script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(".sidebar-menu a").click(function () {
  	$(".fa-angle-right",this).toggleClass("fa-angle-down");
    if (!$(".fa-angle-right").is(":visible")) {
        $('.fa-angle-right').toggleClass('fa-angle-down');      
    }  
    if (!$(".fa-angle-down").is(":visible")) {
        $('.fa-angle-down').addClass('fa-angle-right');      
    }   
    //$('.fa-angle-down').toggleClass('fa-angle-right');  
});    
	$(".modal .modal-footer .btn-default, .modal .modal-header .close").click(function(){
		toastr.warning("You have cancelled your request.","Info:");
	});
	$('#txt_search').keydown(function(e){
	  if(e.keyCode==13)
	  {
		$( "#btn_search" ).trigger( "click" );
	  }
	  });
</script>
</body>
</html>