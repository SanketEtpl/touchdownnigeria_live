<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Admin Permissions 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin Permissions </li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <!--!label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listRights(0,'category','ASC');">
                      Search
                    </a-->
                    Role : <select name="role_id" id="role_id" class="form-control" onchange="listRights(0,'category','ASC');">
                    <option value="">Select Role</option>
                    <?php echo generateOptions($roles,'role_id','role_title',2); ?>
                  </select>
                    <input type="hidden" id="admin" name="admin" value="<?php echo $id;?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listRights" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="40%">Module</th>
                        <th width="15%" class="">Access</th>
                        <th width="15%" class="">Add</th>
                        <th width="15%" class="">Edit</th>
                        <th width="15%" class="">Delete</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="permissionModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php base_url(); ?>permission/savePermission" id="permissionForm" name="permissionForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title">Manage Category</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="category_name">Category</label>
                  <input type="hidden" id="category_key" name="category_key">
                  <input type="text" placeholder="Category" id="category_name" name="category_name" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/rights.js"></script>
<?php $this->load->view('footer'); ?>