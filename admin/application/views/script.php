<?php $this->load->view('header'); ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        CSV Files
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airlines File Upload</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                </div>
                <div>
                  <?php if(isset($status) && 'success' == $status)
                  {?>
                    <div class="alert alert-success csvAlert"  role="alert">
                      <?php echo $message;?>
                    </div>
                  <?php }
                  if(isset($status) && 'error' == $status)
                  {?>
                    <div class="alert alert-danger csvAlert" role="alert">
                      <?php echo $message;?>
                    </div>
                <?php }?>

                 </div>
              <div class="row">
                  <div class="col-sm-12">
                      <div class="form-group">
                          <form action="<?php echo base_url();?>/script/uploadFile" method="post" enctype="multipart/form-data" id="uploadCSVScript" name="uploadCSVScript">
                            <label for="csvFileUpload">Select CSV file to upload</label>

                            <input type="file" name="fileToUpload" id="fileToUpload">
                            <!-- <input type="submit" value="Upload" name="submit"> -->

                            <button type="button" id="uploadData" class="btn btn-default">Upload</button>
                          </form>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box cust_body">
                      <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="dataTables_length" id="example1_length">
                              </div>
                            </div>
                          <div class="col-sm-6">
                            <div id="example1_filter" class="dataTables_filter">
                              <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                              <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listAirports(0,'fname','DESC');">
                                Search
                              </a>
                              <?php if($addRight){?>
                              <a class="btn btn-primary" href="javascript:void(0);" onclick="getAirport();">
                                Add New
                              </a>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <table class="table table-bordered table-striped dataTable" id="listCSVData" role="grid" aria-describedby="example1_info">
                              <thead>
                                <tr role="row">
                                  <th data-name="fname" data-order="DESC" class="sorting"><input type="checkbox" name="checkAirline" id="checkAirline"></th>
                                  <th data-name="fname" data-order="DESC" class="sorting">Name</th>
                                  <th data-name="fname" data-order="DESC" class="sorting">Date</th>
                                  <th width="<?php echo $widthArr['action']?>%" class="">Action</th>

                                </tr>
                              </thead>
                                <tbody>

                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-5">
                              <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                            </div>
                            <div class="col-sm-7">
                              <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-12">
                                <div id="paginate_entries" class="pull-right">
                                  <span ><label for="changeAllStatus">Delete Multiple CSV file &nbsp;&nbsp;&nbsp;</label></span>
                                  <button type="button" class="btn btn-primary" id="deleteAirline">Delete</button>
                                </div>
                              </div>
                          </div>



                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </section>



  </div>
<style>
.cust_body{border-top:0px !important;}
input#fileToUpload {margin-bottom: 10px;}
</style>

  <script src="<?php echo base_url();?>js/admin/script.js"></script>

  <?php $this->load->view('footer'); ?>
