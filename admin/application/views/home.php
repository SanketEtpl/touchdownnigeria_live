 <?php $this->load->view('header'); ?>
   <link rel="stylesheet" href="<?php echo base_url();?>css/plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard 
      </h1>
      <!--ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"></li>
      </ol-->
      </section>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      <?php if(get_user_data('role')==1){?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $airports;?></h3>

              <p>Airports</p>
            </div>
            <div class="icon">
              <i class="ion ion-plane"></i>
            </div>
            <a href="<?php echo base_url();?>airports" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php
        $rowadmin=4;
        $rowuser=4;
        }else{
        $rowadmin=6;
        $rowuser=6;
            }?>
        <!-- ./col -->
        <?php
        if(get_user_data('role')==1)
        {?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $admins;?><sup style="font-size: 20px"></sup></h3>

              <p>Airport Admins</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?php echo base_url();?>subadmin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php } ?>
      <?php
        if(get_user_data('role')==2){
        if(checkRight(get_user_data('role'),10,'access'))
        {?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $terminal;?><sup style="font-size: 20px"></sup></h3>

              <p>Terminals</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?php echo base_url();?>terminal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $admins;?><sup style="font-size: 20px"></sup></h3>

              <p>Terminal Admins</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?php echo base_url();?>subadmin/terminalAdmin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }} ?>
        <!-- ./col -->
        <?php if(get_user_data('role')==1){?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $notifications;?></h3>

              <p>Approve Notification</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-bell"></i>
            </div>
            <a href="<?php echo base_url();?>notification/approveNotification" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php } ?>
        
        <?php
        if(checkRight(get_user_data('role'),7,'access'))
        {?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $users;?></h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-phone-portrait"></i>
            </div>
            <a href="<?php echo base_url();?>users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php } ?>
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <?php if(get_user_data('role')==1){?>
        <section class="col-lg-12 connectedSortable">
                    <div class="tray tray-center pv40 ph30 va-t posr animated-delay animated-long" data-animate='["800","fadeIn"]'>
              <div class="mw1100 center-block">
                  <div id="donut-chart" style="height: 370px; width: 100%;"></div>
              </div>
          </div>

        </section>
        <?php } ?>
        <!--section class="col-lg-6 connectedSortable">


        </section-->

      </div>
    </section>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>
<script type="text/javascript">
Morris.Donut({
  element: 'donut-chart',
  data: <?php echo json_encode($charts["donut"],JSON_NUMERIC_CHECK); ?>
});
</script>
<?php $this->load->view('footer'); ?>