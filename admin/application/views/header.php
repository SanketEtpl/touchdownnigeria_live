<?php $segmentMenu = $this->uri->segment(1);
$user_auth=$this->session->userdata("auth_user");
$rightArr=getRightArr($user_auth['role']);

$titleName1 = $this->uri->segment(1);
$titleName2 = $this->uri->segment(2);

if(strcmp($titleName1, "notification") == 0 && strcmp($titleName2, "approveNotification") == 0)
  $pageTitle = 'Approve notification - TDN';
else if (strcmp($titleName1, "notification") == 0 && strcmp($titleName2, "emergency") == 0)
  $pageTitle = 'Emergency notification - TDN';
else if (strcmp($titleName1, "notification") == 0 && strcmp($titleName2, "sentEmergency") == 0)
  $pageTitle = 'Emergency notification history - TDN';
else if(strcmp($titleName1, "notification") == 0)
  $pageTitle = 'Notification list - TDN';

if(strcmp($titleName1, "store") == 0 && strcmp($titleName2, "category") == 0)
  $pageTitle = 'Store category - Store management- TDN';
else if (strcmp($titleName1, "store") == 0 && strcmp($titleName2, "offer") == 0)
  $pageTitle = 'Store offer - Store management - TDN';
else if (strcmp($titleName1, "store") == 0 && strcmp($titleName2, "facility") == 0)
  $pageTitle = 'Store product category - Store management - TDN';
else if(strcmp($titleName1, "store") == 0)
  $pageTitle = 'Stores - Store management - TDN';
    
if (strcmp($titleName1, "airportScript") == 0 && strcmp($titleName2, "csvFiles") == 0)
  $pageTitle = 'CSV file - Airport - TDN';
else if(strcmp($titleName1, "airportScript") == 0)
  $pageTitle = 'Upload csv file - Airport - TDN';

if (strcmp($titleName1, "subadmin") == 0 && strcmp($titleName2, "terminalAdmin") == 0)
  $pageTitle = 'Terminal admins - Airport management - TDN';

//print_r($pageTitle);exit();
//echo "<pre>";print_r($rightArr);die();
$airports = $this->common->selectQuery("id as airport,name",TB_AIRPORTS,array());
$admin = $this->common->selectQuery("first_name",TB_ADMIN,array("id"=>$user_auth['id']));
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $pageTitle; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/toastr.min.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    var BASEURL = '<?php echo base_url(); ?>';
    $.ajaxSetup({
		beforeSend:function(xhr, ajaxOptions, thrownError){
		  $(".loadingBackground, .loadingImg").show();
		},
		error: function (xhr, ajaxOptions, thrownError) {
		//window.location="";
        toastr.error("Something went wrong.","Error:");
		$(".loadingBackground, .loadingImg").hide();
        },
		success:function(xhr, ajaxOptions, thrownError){
		  $(".loadingBackground, .loadingImg").hide();
		},
		complete:function(xhr, ajaxOptions, thrownError){
		  $(".loadingBackground, .loadingImg").hide();
		},
    });
  </script>
  <style>
    .pagination li :hover{
      cursor: pointer;
    }
	.subadmin_airport_name{
	  display: block;
	  color: rgb(85, 85, 85);
	  border: 1px solid rgba(210, 214, 222, 0.12);
	  border-radius: 5px;
	  height: 37px;
	  padding: 7px 13px;
	  background: #EEE;
	}
    .loadingBackground{background-color: rgba(0, 0, 0, 0.8); display: none; height: 100%; left: 0; position: absolute; top: 0; width: 100%; z-index: 99999; }
    .loadingImg{position: fixed;top:50%;left: 50%;width: 5%;z-index: 1000000;}
  button#clearText {
    padding: 0px 4px 0px 4px;    
    position: absolute;
    z-index: 1;
    background-color: transparent;
    border: none;
    color: #e22828;
    font-size: 19px;
    right: 2px;
    top: 0px;
  }
  div#clear {
    position: relative;
    margin-top: -27px;
  }

  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="loadingBackground" style="display:none;"></div>
<img src="<?php echo base_url();?>images/loading.gif" class="loadingImg" style="display:none;">
<div class="wrapper">

  <header class="main-header">
    <a href="<?php echo base_url(); ?>" class="logo">
      <span class="logo-mini"><b>TDN</b> <!--Touchdown Nigeria--></span>
      <span class="logo-lg"><b>Touchdown</b> Nigeria</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

		<li>
            <a href="javascript:void(0);" onclick="getMyProfile();" title="My Profile">
              <!--img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"-->
              <i class="fa fa-fw fa-user"></i>
              <span class="hidden-xs"><span id="myname"><?php echo $admin[0]['first_name'];?></span></span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url(); ?>logout" title="Logout"><i class="fa fa-sign-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">

		<?php if($rightArr[1] || $rightArr[2] || $rightArr[3]){?>
		  <li class="treeview<?php if(in_array($segmentMenu, array("airports","subadmin","facility","terminal"))){echo " active";} ?>">
			<a href="#">
			  <i class="fa fa-fw fa-gears"></i> <span>Airport Management</span>
			  <span class="pull-right-container">
				<i class="fa <?php if(in_array($segmentMenu, array("airports","subadmin","facility","terminal"))){echo " fa-angle-down";} else { echo "fa-angle-right"; }  ?> pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu">
			  <?php if($rightArr[1]){?>
				<li><a href="<?php echo base_url(); ?>airports"><i class="fa fa-fw fa-plane"></i> <span>Airports</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[2]){?>
				<li><a href="<?php echo base_url(); ?>subadmin"><i class="fa fa-fw fa-users"></i> <span>Airport Admins</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[10]){?>
				<li><a href="<?php echo base_url(); ?>terminal"><i class="fa fa-fw fa-th-large"></i> <span>Airport Terminal</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[11]){?>
				<li><a href="<?php echo base_url(); ?>subadmin/terminalAdmin"><i class="fa fa-fw fa-users"></i> <span>Terminal Admins</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[3]){?>
				<li><a href="<?php echo base_url(); ?>facility"><i class="fa fa-fw fa-cubes"></i> <span>Airport Facilities</span></a></li><!--fa-bed-->
			  <?php } ?>
			</ul>
		  </li>
		<?php } ?>

        <?php if($rightArr[4] || $rightArr[5] || $rightArr[6]){?>
		  <li class="treeview<?php if(in_array($segmentMenu, array("store"))){echo " active";} ?>">
			<a href="#">
			  <i class="fa fa-fw fa-gears"></i> <span>Store Management</span>
			  <span class="pull-right-container">
				<i class="fa <?php if(in_array($segmentMenu, array("store"))){echo " fa-angle-down";} else { echo "fa-angle-right"; }  ?> pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu">
			  <!--li><a href="<?php echo base_url(); ?>store"><i class="fa fa-fw fa-code-fork"></i> <span>Store Category</span></a></li-->
			  <?php if($rightArr[4]){?>
				<li><a href="<?php echo base_url(); ?>store"><i class="fa fa-fw fa-university"></i> <span>Stores</span></a></li>
			  <?php } ?>
        <?php if($rightArr[5]){?>
				<li><a href="<?php echo base_url(); ?>store/category"><i class="fa fa-fw fa-certificate"></i> <span>Store Category</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[5]){?>
				<li><a href="<?php echo base_url(); ?>store/facility"><i class="fa fa-fw fa-shopping-cart"></i> <span>Store Product Categories</span></a></li>
			  <?php } ?>
			  <?php if($rightArr[6]){?>
				<li><a href="<?php echo base_url(); ?>store/offer"><i class="fa fa-fw fa-certificate"></i> <span>Store Offers</span></a></li>
			  <?php } ?>

			</ul>
		  </li>
		<?php } ?>
		<?php if($rightArr[7]){?>
		  <li><a href="<?php echo base_url(); ?>users"><i class="fa fa-fw fa-users"></i> <span>Users</span></a></li>
		<?php } ?>
		<?php if($rightArr[9]){?>
		  <li><a href="<?php echo base_url(); ?>pages"><i class="fa fa-fw fa-file-text-o"></i> <span>Pages</span></a></li>
		<?php } ?>
		<?php if($rightArr[8]){?>
		  <li><a href="<?php echo base_url(); ?>rights"><i class="fa fa-fw fa-gear"></i> <span>Settings</span></a></li>
		<?php } ?>

    <?php if($user_auth['role']==1){?>
		  <li><a href="<?php echo base_url(); ?>script"><i class="fa fa-fw fa-picture-o"></i> <span>Airlines</span></a></li>
		<?php } ?>

    <?php if($user_auth['role']==1){?>
      <!-- <li><a href="<?php //echo base_url(); ?>airportScript"><i class="fa fa-fw fa-picture-o"></i> <span>Airports</span></a></li> -->
    <?php } ?>

    

		<li class="treeview<?php if(in_array($segmentMenu, array("airportScript"))){echo " active";} ?>">
      <?php if($rightArr[12]){?>
			<a href="#">
			  <i class="fa fa-fw fa-gears"></i> <span>Airport</span>
			  <span class="pull-right-container">
				<i class="fa <?php if(in_array($segmentMenu, array("airportScript"))){echo " fa-angle-down";} else { echo "fa-angle-right"; }  ?> pull-right"></i>
			  </span>
			</a>
			<ul class="treeview-menu">
			  
			  	
				<li><a href="<?php echo base_url(); ?>airportScript"><i class="fa fa-fw fa-plane"></i> <span>Upload CSV File</span></a></li>
			  	 
				<li><a href="<?php echo base_url(); ?>airportScript/csvFiles"><i class="fa fa-fw fa-users"></i> <span>CSV Files</span></a></li>
        </ul>

				<?php } ?>
			 
			  
			
		  </li>


		<?php if($user_auth['role']==1){?>
		  <li><a href="<?php echo base_url(); ?>homescreen"><i class="fa fa-fw fa-picture-o"></i> <span>App Background</span></a></li>
		<?php } ?>
		<?php if($user_auth['role']==1 || $user_auth['role']==2){?>
		  <li class="treeview<?php if(in_array($segmentMenu, array("notification"))){echo " active";} ?>"><a href="#"><i class="fa fa-fw fa-gears"></i> <span>Notification Setting</span><span class="pull-right-container">
				<i class="fa <?php if(in_array($segmentMenu, array("notification"))){echo " fa-angle-down";} else { echo "fa-angle-right"; }  ?> pull-right"></i>
			  </span></a>
		  <ul class="treeview-menu">
		  <?php if($user_auth['role']==1 || $user_auth['role']==2){?>
		  <li><a href="<?php echo base_url(); ?>notification/emergency"><i class="fa fa-fw fa-paper-plane"></i> <span>Emergency Notification</span></a></li>
		  <?php } ?>
		  <?php if($user_auth['role']==1){?>
		  <li><a href="<?php echo base_url(); ?>notification/sentEmergency"><i class="fa fa-fw fa-history"></i> <span>Emergency Notification History</span></a></li>
		  <?php } ?>
		  <?php if($user_auth['role']==1 || $user_auth['role']==2){?>
		  <li><a href="<?php echo base_url(); ?>notification"><i class="fa fa-fw fa-bell"></i> <span>Notification List</span></a></li>
		  <?php } ?>
		  <?php if($user_auth['role']==1){?>
		  <li><a href="<?php echo base_url(); ?>notification/approveNotification"><i class="fa fa-fw fa-check-square-o"></i> <span>Approve Notifications</span></a></li>
		  <?php } ?>
		  </ul>
		  </li>
		<?php } ?>

    <?php if($user_auth['role']==1 || $user_auth['role']==2){?>
      <li><a href="<?php echo base_url(); ?>user_guide"><i class="fa fa-fw fa-gears"></i> <span>User Guide</span></a></li>
    <?php } ?>

   
      </ul>
    </section>
  </aside>

  <div id="profileModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>subadmin/saveProfile" id="profileForm" name="profileForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title">My Profile</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="myfirst_name">First Name</label>
                  <input type="text" placeholder="First Name" id="myfirst_name" name="myfirst_name" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="mylast_name">Last Name</label>
                  <input type="text" placeholder="Last Name" id="mylast_name" name="mylast_name" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="myusername">Username</label>
                  <input type="text" placeholder="Username" id="myusername" name="myusername" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="mypassword">Password</label>
                  <input type="password" placeholder="Password" id="mypassword" name="mypassword" class="form-control" autocomplete="off">
				  Enter only if you want to change password.
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="myemail_id">Email ID</label>
                  <input type="text" placeholder="Email ID" id="myemail_id" name="myemail_id" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="myphone">Phone</label>
                  <input type="text" placeholder="Phone" id="myphone" name="myphone" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>

            <!--div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="myairport">Airport</label>
                  <select name="myairport" id="myairport" class="form-control">
                    <option value="">Select Airport</option>
                    <?php echo generateOptions($airports,'airport','name'); ?>
                  </select>
                </div>
              </div>
            </div-->

          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function()
{
  $("#profileForm").validate({
      rules: {
        myfirst_name: {
              required: true,
              maxlength: 30,
          },
        mylast_name: {
              required: true,
              maxlength: 30,
          },
        myusername: {
              required: true,
              maxlength: 30,
          },
        password: {
              required: true,
              minlength: 5,
              maxlength: 10,
          },
        myemail_id: {
              required: true,
              email: true,
              maxlength: 50,
          },
        myphone: {
              required: true,
              maxlength: 15,
              number: true,
          },
        myairport: {
              required: true,
          },
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        myfirst_name: {
                required: "Please enter your first name.",
                maxlength: "First name is too long."
            },
        mylast_name: {
                required: "Please enter your last name.",
                maxlength: "Last name is too long."
            },
        myusername: {
                required: "Please enter your username.",
                maxlength: "Username is too long."
            },
        password: {
                required: "Please enter your password.",
                minlength: "Your password must be at least 5 characters long.",
                maxlength: "Password is too long."
            },
        myemail_id: {
                required: "Please enter email id.",
                email : "Please enter valid email id.",
                maxlength: "Email id is too long."
            },
        myphone: {
                required: "Please enter phone number.",
                number: "Please enter valid phone number.",
                maxlength: "Phone number is too long."
            },
        myairport: "Please select airport.",
      }
  });
  $("#profileForm").ajaxForm({
      dataType: 'json',
      beforeSend: function()
      {
          //$("#userForm").find('[type="submit"]')
          $("#save_btn").toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete)
      {

      },
      success: function(json)
      {
        if(json.status == "success"){
          jQuery('#profileModal').modal('hide');
          //if(json.action == "add"){
          //  listAdmins(0,'user','DESC');
          //}else{
          //  listAdmins(pagenumber,ordercolumn,orderby);
          //}
		  $("#myname").html(json.userData.first_name);
          toastr.success(json.msg,"Success:");
        }else{
          toastr.error(json.msg,"Error:");
		  if(json.status == "logout"){
			window.location="<?php echo base_url();?>logout";
          }
        }
        $("#save_btn").removeClass('sending').blur();
      },
      complete: function(json)
      {
         $("#save_btn").removeClass('sending').blur();
      },
      error: function()
      {
          $("#save_btn").removeClass('sending').blur();
          toastr.error("Something went wrong.","Error:");
      }
  });
});
function getMyProfile($this){
$("#profileForm")[0].reset();
$("#profileForm .form-control").removeClass('error-block');
$("#profileForm span.error-block").hide();
$.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"subadmin/getMyProfile",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          var alert_type='danger';
          if(json.status == "success"){
            $("#action").html('Edit');
              $("[name=password]").each(function(){
                $(this).rules("add", {
                required: false,
                 });
               });
            //$("#user_key").val($($this).attr("data-row-id"));
            $("#myfirst_name").val(json.userData.fname);
            $("#mylast_name").val(json.userData.lname);

            $("#myusername").val(json.userData.username);
            $("#myemail_id").val(json.userData.email_id);

            $("#myphone").val(json.userData.phone);
            $("#myairport").val(json.userData.airport);

            if(json.userData.is_active === 1){
              $('#active').prop('checked', true);
            }else{
              //console.log(json.userData.is_active);
              $('#active').prop('checked', false);
            }
            jQuery('#profileModal').modal('show', {backdrop: 'static'});
          }else if(json.status == "danger"){
          }else if(json.status == "logout"){
			window.location="<?php echo base_url();?>logout";
          }
      });
}
</script>
