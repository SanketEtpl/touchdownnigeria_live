<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Stores Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stores Category</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listStoreCategory(0,'category','DESC');">
                      Search
                    </a>

                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getStoreCategory();">
                      Add New
                    </a>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listStoreCategory" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['category_name']?>%" data-name="category_name" data-order="DESC" class="sorting">Category Name</th>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>

                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="storeFaciModal" class="modal fade" style="display: none;" aria-hidden="false">
      <form method="post" action="<?php echo base_url(); ?>store/saveStoreCategory" id="storeCatForm" name="storeCatForm">
          <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
      <h4 class="modal-title"><span id="action"></span> Store Facilities</h4>
    </div>
    <div class="modal-body">
      <input type="hidden" id="fstore_key" name="fstore_key">
    <div id="facilitis">
      <div class="row" id="row1">
        <div class="col-md-10">
          <div class="form-group">

            <input type="text" placeholder="Facility" id="facility" name="facility[]" class="form-control" autocomplete="off">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
              <!--button id="addmore1" class="btn btn-info" onclick="addone(1);" type="button"><i class="fa fa-fw fa-plus-square"></i></button-->
              <!--button id="remove1" class="btn btn-info" onclick="removeone(1);" style="display:none" type="button"><i class="fa fa-fw fa-times"></i></button-->
          </div>
        </div>
      </div>
      </div>
      <button id="addmore" class="btn btn-info" onclick="addone();" type="button"><i class="fa fa-fw fa-plus"></i></button>
      <span>  You can add only 10 facilities in each store.</span>
    </div>
    <div class="modal-footer">
      <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      <button class="btn btn-info" type="submit">Save</button>
    </div>
  </div>
</div>
      </form>
  </div>

      <div id="storeOfferModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>store/saveOfferStore" id="storeOfferForm" name="storeOfferForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Store Offers</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" id="ostore_key" name="ostore_key">
          <div id="offers">
            <div class="row" id="row1">
              <div class="col-md-10">
                <div class="form-group">

                  <input type="text" placeholder="Offer" id="offer" name="offer" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                    <!--button id="addmore1" class="btn btn-info" onclick="addone(1);" type="button"><i class="fa fa-fw fa-plus-square"></i></button-->
                    <!--button id="remove1" class="btn btn-info" onclick="removeone(1);" style="display:none" type="button"><i class="fa fa-fw fa-times"></i></button-->
                </div>
              </div>
            </div>
            </div>
            <button id="addmoreoffer" class="btn btn-info" onclick="addoneoffer();" type="button"><i class="fa fa-fw fa-plus"></i></button>
            <span>  You can add only 10 offers in each store.</span>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>

    <div id="storeCategoryModal" class="modal fade" style="display: none;" aria-hidden="false">
      <form method="post" action="<?php echo base_url(); ?>store/saveStoreCategory" id="storeCatiForm" name="storeCatiForm">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">

                <input type="hidden" id="storefacility_key" name="storefacility_key">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
              <h4 class="modal-title"><span id="action"></span> Store Category</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="store_thumb">Category Name</label>
                    <input type="input" placeholder="Category Name" id="category_name" name="category_name" class="form-control" autocomplete="off">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
            </div>
          </div>
        </div>
        </form>
      </div>


  <script src="<?php echo base_url(); ?>js/admin/storeCategory.js"></script>
<?php $this->load->view('footer'); ?>
