<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Stores
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stores</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listStore(0,'store_name','DESC');">
                      Search
                    </a>
                    <?php if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getStore();">
                      Add New
                    </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listStore" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['store_name']?>%" data-name="store_name" data-order="DESC" class="sorting">Store Name</th>
                        <?php if(get_user_data('role')!=3){?>
                        <th width="<?php echo $widthArr['terminal_name']?>%" data-name="terminal_name" data-order="DESC" class="sorting">Terminal</th>
                          <?php if(get_user_data('role')!=2){?>
                          <th width="<?php echo $widthArr['airport_name']?>%" data-name="name" data-order="DESC" class="sorting">Airport</th>
                        <?php }} ?>
                        <th width="<?php echo $widthArr['store_thumb']?>%" class="">Store Image</th>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="storeModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>store/saveStore" id="storeForm" name="storeForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Store</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="store_name">Store Name</label>
                  <input type="hidden" id="store_key" name="store_key">
                  <!-- <input type="hidden" id="img" name="img"> -->
                  <input type="hidden" id="imgcnt" name="imgcnt" value="1">
                  <input type="text" placeholder="Store Name" id="store_name" name="store_name" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="store_category_id">Store Category</label>
                  <select name="store_category_id" id="store_category_id" class="form-control">
                    <option value="">Select Category</option>
                    <?php echo generateOptions($category,'category_id','category'); ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <?php
              $row=6;
              if(get_user_data('role')==1){ ?>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="airport_id">Airport</label>
                  <select name="airport_id" id="airport_id" class="form-control" onchange="getTerminal();">
                    <option value="">Select Airport</option>
                    <?php echo generateOptions($airports,'airport_id','name'); ?>
                  </select>
                </div>
              </div>
              <?php
              $row=6;
              }else{ ?>
              <input type="hidden" id="airport_id" name="airport_id" value="<?php echo get_user_data('air_port_id');?>">
              <?php
              $row=12;
              } ?>
             <?php
              //$row=6;
              if(get_user_data('role')==1 || get_user_data('role')==2){ ?>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="airport">Terminal</label>
                  <select name="terminal_id" id="terminal_id" class="form-control">
                    <option value="">Select Terminal</option>
                    <!--<option value="1">Terminal A</option>
                    <option value="2">Terminal B</option>-->
                    <?php //echo generateOptions($airports,'airport','name'); ?>
                  </select>
                </div>
              </div>
            <?php }else{?>
            <input type="hidden" id="terminal_id" name="terminal_id" value="<?php echo get_user_data('terminal_id');?>">
              </div>

            <div class="row">
              <?php
              $row=12;
              } ?>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="store_desc">Store Detail</label>
                  <textarea placeholder="Store Detail" id="store_desc" name="store_desc" class="form-control"></textarea>
                </div>
              </div>
            </div>

            <div class="row">
             <div class="col-md-12">
                <div class="form-group">
                  <br/>
                 <a href="javascript:void(0)" onclick="addImage()"><label>Add More Images<a href="#" class="" data-toggle="tooltip" data-placement="right" title="Please select jpg | png | jpeg type image. Store Image max size is 5 MB.">(Image Guideline)</a></label></a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 store_one">
                <div class="form-group">
                  <label class="control-label" for="store_thumb">Store Image</label> &nbsp;(Size: 300X300)
                  <br/>
                  <img src="" id="store_thumb_img" width="120px" style="padding: 7px;">
                  <input type="file" placeholder="Store Image" id="store_thumb" name="store_thumb[]" class="" autocomplete="off">
                  Upload upto 5 Mb image.
                </div>
              </div>

               <div id="ImageHtml"></div>

            </div>
            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>

      <div id="storeFaciModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>store/saveFaciStore" id="storeFaciForm" name="storeFaciForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Store Facilities</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" id="fstore_key" name="fstore_key">
          <div id="facilitis">
            <div class="row" id="row1">
              <div class="col-md-10">
                <div class="form-group">

                  <input type="text" placeholder="Facility" id="facility" name="facility[]" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                    <!--button id="addmore1" class="btn btn-info" onclick="addone(1);" type="button"><i class="fa fa-fw fa-plus-square"></i></button-->
                    <!--button id="remove1" class="btn btn-info" onclick="removeone(1);" style="display:none" type="button"><i class="fa fa-fw fa-times"></i></button-->
                </div>
              </div>
            </div>
            </div>
            <button id="addmore" class="btn btn-info" onclick="addone();" type="button"><i class="fa fa-fw fa-plus"></i></button>
            <span>  You can add only 10 facilities in each store.</span>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>

      <div id="storeOfferModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>store/saveOfferStore" id="storeOfferForm" name="storeOfferForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Store Offers</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" id="ostore_key" name="ostore_key">
          <div id="offers">
            <div class="row" id="row1">
              <div class="col-md-10">
                <div class="form-group">

                  <input type="text" placeholder="Offer" id="offer" name="offer" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                    <!--button id="addmore1" class="btn btn-info" onclick="addone(1);" type="button"><i class="fa fa-fw fa-plus-square"></i></button-->
                    <!--button id="remove1" class="btn btn-info" onclick="removeone(1);" style="display:none" type="button"><i class="fa fa-fw fa-times"></i></button-->
                </div>
              </div>
            </div>
            </div>
            <button id="addmoreoffer" class="btn btn-info" onclick="addoneoffer();" type="button"><i class="fa fa-fw fa-plus"></i></button>
            <span>  You can add only 10 offers in each store.</span>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  <script src="<?php echo base_url(); ?>js/admin/store.js"></script>
<?php $this->load->view('footer'); ?>
