<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Store Offer
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Store Offer</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listStoreOffer(0,'Offer_name','DESC');">
                      Search
                    </a>
                     <?php if($addRight){?>
                     <?php //if(get_user_data('role')!=1){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getStoreOffer();">
                      Add New
                    </a>
                    <?php //} 
                  } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listStoreOffer" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['offer_name']?>%" data-name="offer_name" data-order="DESC" class="sorting">Offer Name</th>
                        <th width="<?php echo $widthArr['store_name']?>%" data-name="store_name" data-order="DESC" class="sorting">Store</th>
                        <?php if(get_user_data('role')==1){?>
                        <th width="<?php echo $widthArr['terminal_name']?>%" data-name="terminal_name" data-order="DESC" class="sorting">Terminal</th>
                        <th width="<?php echo $widthArr['airport_name']?>%" data-name="name" data-order="DESC" class="sorting">Airport</th>
                        <?php } ?>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="storeOfferModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>store/saveStoreOffer" id="storeOfferForm" name="storeOfferForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Store Offer</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="store_name">Store</label>
                  <input type="hidden" id="storeoffer_key" name="storeoffer_key">
                   <select name="store_id" id="store_id" class="form-control">
                    <option value="">Select Store</option>
                    <?php echo generateOptions($store,'store_id','store_name'); ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="store_thumb">Offer Name</label>
                  <input type="input" placeholder="Offer Name" id="offer_name" name="offer_name" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  <script src="<?php echo base_url(); ?>js/admin/storeoffer.js"></script>
<?php $this->load->view('footer'); ?>