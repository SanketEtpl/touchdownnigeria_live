<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Notifications
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Notifications</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listNotification(0,'notification_text','DESC');">
                      Search
                    </a>

                    <a class="btn btn-primary" href="javascript:void(0);" onclick="addNewNotifications();">
                      Add New
                    </a>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listNotification" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['notification']?>%" data-name="notification_text" data-order="DESC" class="sorting">Notification</th>
                        <?php //if(get_user_data('role')==1){?>
                        <!--th width="<?php echo $widthArr['airport_name']?>%" data-name="airport_name" data-order="DESC" class="sorting">Airport</th-->
                        <?php //} ?>
                        <?php //if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php //} ?>
                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

    <div id="notificationAlertModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>notification/sendNotificationAlert" id="notificationAlertForm" name="notificationAlertForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Notification Values</h4>

          </div>
          <div class="modal-body" id="htmldata">
            <!--<div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="notification_text">Notification</label>
                  <input type="hidden" id="notification_key" name="notification_key">
                    <textarea class="form-control" rows="2" id="notification_text" name="notification_text" placeholder="Notification"></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="notification_text"></label>
                   <div id="replace_array" class="col-md-4"></div>
                </div>
              </div>
            </div>-->
            *******************here is dynamic html*******************
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>


    <div id="notificationModal" class="modal fade" style="display: none;" aria-hidden="false">
      <form method="post" action="<?php echo base_url(); ?>notification/addNotification" id="notificationForm" name="notificationForm">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
              <h4 class="modal-title"><span id="action"></span> Notifications</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label" for="airport_name">Notification</label>
                    <input type="hidden" id="airport_key" name="airport_key">
                    <textarea class="form-control notificationBox" rows="2" id="notification_text" name="notification_text" placeholder="Notification" ></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info tiny" type="submit" id="save_notification">Send</button>

            </div>
          </div>
        </form>
      </div>
    </div>

    <script>
  var module = 'fix';
</script>
  <script src="<?php echo base_url(); ?>js/admin/notification.js"></script>
<?php $this->load->view('footer'); ?>
