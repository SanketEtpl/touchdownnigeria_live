<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Emergency Notification
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Emergency Notification</li>
      </ol>
    </section>
    <section class="content">
     
            <div class="box box-primary">
           
            <form method="post" action="<?php echo base_url(); ?>notification/sendNotification" id="notificationForm1" name="notificationForm1">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Notification</label>
                      <textarea class="form-control" rows="2" id="notification" name="notification" placeholder="Notification"></textarea>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <!--<div class="col-md-3">-->
                  <!--  <div class="form-group">-->
                  <!--    <label for="exampleInputEmail1"></label>-->
                  <!--    <input type="checkbox" id="all_user" name="all_user" value="all_user"> All users-->
                  <!--  </div>-->
                  <!--</div>-->
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <input type="checkbox" id="select_all" name="select_all" value="all"> All Airports
                    </div>
                  </div>
                </div>
                <?php
                //echo "<pre>";print_r($airports);
                $i=0;
                ?>
                <?php foreach($airports as $k=> $v){
                $i++;
                if($i==1){
                ?>
                <div class="row">
                <?php } ?>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <!--<div class="checkbox2">-->
                      <input type="checkbox" name="airport_id[]" class="checkbox1" id="airport_<?php echo $v['airport_id'];?>" id="<?php echo $v['airport_id'];?>"  value="<?php echo $v['airport_id'];?>"> <?php echo $v['airport_name'];?>
                      <!--</div>-->
                    </div>
                  </div>
                <?php if($i%4==0){?>
                </div>
                <div class="row">
                <?php
                }}?>
              </div>  

              <div class="box-footer">
                <div class="pull-right">
                  <button type="button" onclick="history.back();" class="btn btn-default">Back</button>
                  <button type="submit" id="save_btn" class="btn btn-info send_conf">Send</button>
                </div> 
              </div>
            </form>
          </div>
    </section>

    </div>
    
<script>
var module = 'em';
</script>
  <script src="<?php echo base_url(); ?>js/admin/notification.js"></script>
<?php $this->load->view('footer'); ?>