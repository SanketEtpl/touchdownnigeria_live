<?php $this->load->view('header'); ?>
<style>
  .btn {
    border-radius: 3px;
    -webkit-box-shadow: none;
    box-shadow: none;
    border: 1px solid transparent;
}
  .btn-block {
    display: block;
    /* width: 46%; */
    width: 50%;
    float: left;
    margin: 0PX 2PX;
}
.btn-group-sm>.btn, .btn-sm {
    padding: 5px 0px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
</style>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Approve Notifications
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Approve Notifications</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listApproveNotification(0,'notification_text','DESC');">
                      Search
                    </a>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listApproveNotification" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['notification']?>%" data-name="notification_text" data-order="DESC" class="sorting">Notification</th>
                        <?php //if(get_user_data('role')==1){?>
                        <th width="<?php echo $widthArr['first_name']?>%" data-name="first_name" data-order="DESC" class="sorting">Sender</th>
                        <th width="<?php echo $widthArr['created_date']?>%" data-name="created_date" data-order="DESC" class="sorting">Date</th>
                        <?php //} ?>
                        <?php //if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php //} ?>
                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="notificationModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>notification/saveNotification" id="notificationForm" name="notificationForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Notification</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="notification_text">Notification</label>
                  <input type="hidden" id="notification_key" name="notification_key">
                    <textarea class="form-control" rows="2" id="notification_text" name="notification_text" placeholder="Notification"></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="notification_text"></label>
                  <input type="hidden" id="notification_key" name="notification_key">
                    <!--textarea class="form-control" rows="2" id="notification_text" name="notification_text" placeholder="Notification"></textarea-->
                    <div id="replace_array">askdjkasd</div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>

    <div id="updateNotificationModel" class="modal fade" style="display: none;" aria-hidden="false">

      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title">Edit Notification</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <input type="hidden" id="editId">
              <div class="col-md-6">
                <div class="form-group">
                  <!-- <label class="control-label" for="myfirst_name">Notification</label> -->
                  <textarea cols="30" rows="5" style="width:500px;" id="updateExistingNotification"></textarea>
                </div>

                <span id="txtError" style="color: red ;display: none;"></span>
              </div>

            </div>

          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="button" id="editNotification">Save</button>
          </div>
        </div>

    </div>
  </div>




  <script src="<?php echo base_url(); ?>js/admin/notificationApprove.js"></script>
<?php $this->load->view('footer'); ?>
