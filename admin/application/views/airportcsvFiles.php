<div class="content-wrapper">
    <section class="content-header">
      <h1>
       Airport CSV Files
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airport CSV Files</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
              <div class="row">
                  <div class="col-xs-12">
                    <div class="box cust_body">
                      <div class="box-body">
                        <table class="table table-bordered table-striped dataTable" id="listAirportCsv" role="grid" aria-describedby="example1_info">
                          <thead>
                            <tr role="row">
                              <th data-name="fname" data-order="DESC" class="sorting">Name</th>
                              <th data-name="fname" data-order="DESC" class="sorting">Date</th>
                              <th width="<?php echo $widthArr['action']?>%" class="">Action</th>

                            </tr>
                          </thead>
                            <tbody>

                            </tbody>
                          </table>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5">
                        <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                      </div>
                      <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </section>
                
</div>
<style>
.cust_body{border-top:0px !important;}
input#fileToUpload {margin-bottom: 10px;}
</style>
<script src="<?php echo base_url();?>/js/admin/airportCsvFile.js" type="text/javascript" charset="utf-8" async defer></script>