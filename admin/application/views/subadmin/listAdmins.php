<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Airport Admins
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airport Admins</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" id="btn_search" href="javascript:void(0);" onclick="listAdmins(0,'user','DESC');">
                      Search
                    </a>
                    <?php if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getUser(this);">
                      Add New
                    </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listAdmins" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['fname']?>%" data-name="fname" data-order="DESC" class="sorting">First Name</th>
                        <th width="<?php echo $widthArr['lname']?>%" data-name="lname" data-order="ASC" class="sorting">Last Name</th>
                        <th width="<?php echo $widthArr['email']?>%" data-name="email" data-order="ASC" class="sorting">Email ID</th>
                        <th width="<?php echo $widthArr['phone']?>%" data-name="phone" data-order="ASC" class="sorting">Phone</th>
                        <?php if(get_user_data('role')==1){?>
                        <th width="<?php echo $widthArr['airport_name']?>%" data-name="name" data-order="ASC" class="sorting">Airport</th>
                        <th width="<?php echo $widthArr['status']?>%" data-name="is_active" data-order="ASC" class="sorting">Status</th>
                        <?php } ?>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>subadmin/saveUser" id="userForm" name="userForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Airport Admin</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="first_name">First Name</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="First Name" id="first_name" name="first_name" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="last_name">Last Name</label>
                  <input type="text" placeholder="Last Name" id="last_name" name="last_name" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="username">Username</label>
                  <input type="text" placeholder="Username" id="username" name="username" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="password">Password</label>
                  <input type="password" placeholder="Password" id="password" name="password" class="form-control" autocomplete="off">
                  <span id="password_note">Enter only if you want to change password.</span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_id">Email ID</label>
                  <input type="text" placeholder="Email ID" id="email_id" name="email_id" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="phone">Phone</label>
                  <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
            <?php if(get_user_data('role')==1){?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="airport">Airport</label>
                  <select name="airport" id="airport" class="form-control">
                    <option value="">Select Airport</option>
                    <?php echo generateOptions($airports,'airport','name'); ?>
                  </select>
                </div>
              </div>
            </div>
            <?php }else{ ?>
           <input type="hidden" id="airport" name="airport" value="<?php echo get_user_data('air_port_id');?>">
           <!--span class="subadmin_airport_name"><?php //echo getSelectedValue($airports,get_user_data('air_port_id'),'airport','name');?></span-->
           <?php } ?>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  
    <div id="userRights" class="modal fade" style="display:none;" aria-hidden="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Airport Admin Permissions</h4>
          </div>
          <div class="modal-body">
                <input type="hidden" id="admin" name="admin" value="<?php echo $id;?>">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listRights" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="40%">Module</th>
                        <th width="15%" class="">Access</th>
                        <th width="15%" class="">Add</th>
                        <th width="15%" class="">Edit</th>
                        <th width="15%" class="">Delete</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
               
            
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!--button class="btn btn-info tiny" type="submit" id="save_btn">Save</button-->
          </div>
        </div>
     
      </div>
    </div>


  <script src="<?php echo base_url(); ?>js/admin/subadmin.js"></script>
  <!--script src="<?php echo base_url(); ?>js/admin/rights.js"></script-->
<?php $this->load->view('footer'); ?>