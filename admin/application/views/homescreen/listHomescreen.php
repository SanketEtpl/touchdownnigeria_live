<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Background Images
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Background Images</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <!--<div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listHomescreen(0,'image','DESC');">
                      Search
                    </a>
                    <?php //if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getAirportFacility();">
                      Add New
                    </a>
                    <?php //} ?>
                  </div>-->
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listHomescreen" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['image']?>%" data-name="id" data-order="DESC" class="sorting">Image</th>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="homescreenModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>homescreen/saveHomescreen" id="homescreenForm" name="homescreenForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Background Image</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                  <input type="hidden" id="homescreen_key" name="homescreen_key">
              
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="facility_name">Image</label>
                  <!--<input type="input" placeholder="Facility" id="facility_name" name="facility_name" class="form-control" autocomplete="off">-->
                  <br/>
                  <img src="" id="image_thumb" width="90%" style="padding: 7px;">
                  <input type="file" placeholder="Store Image" id="image" name="image" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  <script src="<?php echo base_url(); ?>js/admin/homescreen.js"></script>
<?php $this->load->view('footer'); ?>