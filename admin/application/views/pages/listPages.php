<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pages
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pages</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listPages(0,'page_title','DESC');">
                      Search
                    </a>
                    <?php /* ?><a class="btn btn-primary" href="javascript:void(0);" onclick="getUser(this);">
                      Add New
                    </a>
                    <?php */ ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listPages" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['page_title']?>%" data-name="page_title" data-order="DESC" class="sorting">Title</th>
                        <th width="<?php echo $widthArr['page_detail']?>%" data-name="page_detail" data-order="ASC" class="sorting">Detail</th>
                        <?php if($editRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="pageModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>pages/savePage" id="pageForm" name="pageForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Page</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_title">Title</label>
                  <input type="hidden" id="page_key" name="page_key">
                  <input type="hidden" id="type" name="type">
                  <input type="hidden" placeholder="Title" id="page_title" name="page_title" class="form-control" autocomplete="off">
                  <br/>
                  <span id="page_title_txt"></span>
                </div>
              </div>
            </div>
            <div class="row" id="row_page_detail">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_detail">Detail</label>
                  <textarea placeholder="Detail" id="page_detail" name="page_detail" ></textarea>
                </div>
              </div>
            </div>
            <div class="row" id="row_office_address" style="display:none">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_detail3">Office Address</label>
                  <textarea placeholder="Office Address" id="office_address" name="office_address" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
            </div>
            <div class="row" id="row_call_us_on" style="display:none">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_detail1">Call Us On</label>
                  <input type="text" placeholder="Call Us On" id="call_us_on" name="call_us_on" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
            <div class="row" id="row_write_to_us_at" style="display:none">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_detail2">Write To Us At</label>
                  <input type="text" placeholder="Write To Us At" id="write_to_us_at" name="write_to_us_at" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>

<script src="<?php echo base_url();?>js/ckeditor-full/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>js/admin/pages.js"></script>
<?php $this->load->view('footer'); ?>
