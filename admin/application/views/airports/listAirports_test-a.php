<link href="<?php echo base_url();?>css/main.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/croppic.css" rel="stylesheet">
<?php $this->load->view('header'); ?>
<style>
  .pac-container {
    background-color: #FFF;
    z-index: 9999;
    position: fixed;
    display: inline-block;
    float: left;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDFg8ed5088vo8C3Wbg_x25hlX_6jOzzuc"></script>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Airports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airports</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listAirports(0,'fname','DESC');">
                      Search
                    </a>
                    <?php if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getAirport();">
                      Add New
                    </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listAirports" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['fname']?>%" data-name="fname" data-order="DESC" class="sorting">Airport Name</th>
                        <th width="<?php echo $widthArr['city']?>%" data-name="city" data-order="ASC" class="sorting">City</th>
                        <th width="<?php echo $widthArr['email']?>%" data-name="email" data-order="ASC" class="sorting">Email ID</th>
                        <th width="<?php echo $widthArr['phone']?>%" data-name="phone" data-order="ASC" class="sorting">Phone</th>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="airportModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>airports/saveAirport" id="airportForm" name="airportForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Airport</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="airport_name">Airport Name</label>
                  <input type="hidden" id="airport_key" name="airport_key">
                  <input type="text" placeholder="Airport Name" id="airport_name" name="airport_name" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Phone</label>
                  <input type="text" placeholder="Phone" id="contact_number" name="contact_number" class="form-control">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email ID</label>
                  <input type="text" placeholder="Email ID" id="email_address" name="email_address" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Address</label>
                  <input type="text" placeholder="Address" id="address" name="address" class="form-control" autocomplete="off">
                  <!--label class="control-label" for="latitude"></label-->
                  <input type="hidden" id="latitude" name="latitude" value="" class="form-control">
                  <!--label class="control-label" for="longitude"></label-->
                  <input type="hidden" id="longitude" name="longitude" value="" class="form-control">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="city">City</label>
                  <input type="text" placeholder="City" id="city" name="city" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="state">State</label>
                  <input type="text" placeholder="State" id="state" name="state" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="country">Country</label>
                  <input type="text" placeholder="Country" id="country" name="country" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="zipcode">Zip Code</label>
                  <input type="text" placeholder="Zip Code" id="zipcode" name="zipcode" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="interior">Interior</label>
                  <div id="cropContainerPreload"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="exterior">Exterior</label>
                  <div id="cropContainerEyecandy"></div>
                </div>
              </div>
            </div>  
              
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="bio">Details</label>
                  <textarea placeholder="Details" rows="3" name="bio" id="bio" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
            <div id="data"></div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  <script src="<?php echo base_url(); ?>js/admin/airport.js"></script>
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url();?>js/jquery.mousewheel.min.js"></script>
   	<script src="<?php echo base_url();?>js/croppic.min.js"></script>
    <script src="<?php echo base_url();?>js/main.js"></script>
    <script>
		var croppicContainerPreloadOptions = {
				uploadUrl:'<?php echo base_url()?>store/img_save_to_file/interior',
				cropUrl:'<?php echo base_url()?>store/img_crop_to_file/interior',
				//loadPicture:'assets/img/night.jpg',
				enableMousescroll:true,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
        var cropContainerPreload = new Croppic('cropContainerPreload', croppicContainerPreloadOptions);
        
       var croppicContainerEyecandyOptions = {
				uploadUrl:'<?php echo base_url()?>store/img_save_to_file/exterior',
				cropUrl:'<?php echo base_url()?>store/img_crop_to_file/exterior',
				//loadPicture:'assets/img/night.jpg',
				enableMousescroll:true,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		
		var cropContainerEyecandy = new Croppic('cropContainerEyecandy', croppicContainerEyecandyOptions);
        
</script>