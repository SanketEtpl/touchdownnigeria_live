<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Airport Terminal
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airport Terminal</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listTerminal(0,'terminal_name','DESC');">
                      Search
                    </a>
                    <?php if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getTerminal();">
                      Add New
                    </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listTerminal" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['terminal_name']?>%" data-name="terminal_name" data-order="DESC" class="sorting">Terminal</th>
                         <!-- <th width="<?php echo $widthArr['terminal_desc']?>%" data-name="terminal_desc" data-order="DESC" class="sorting">Terminal Description</th> -->
                        <?php if(get_user_data('role')==1){?>
                        <th width="<?php echo $widthArr['airport_name']?>%" data-name="airport_name" data-order="DESC" class="sorting">Airport</th>
                        <?php } ?>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="terminalModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>terminal/saveTerminal" id="terminalForm" name="terminalForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Airport Terminal</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <input type="hidden" id="terminal_key" name="terminal_key">
               <input type="hidden" id="imgcnt" name="imgcnt" value="1">
              <?php
              $row=6;
              if(get_user_data('role')==1){ ?>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="airport_id">Airport</label>
                  <select name="airport_id" id="airport_id" class="form-control">
                    <option value="">Select Airport</option>
                    <?php echo generateOptions($airports,'airport_id','airport_name'); ?>
                  </select>
                </div>
              </div>
              <?php
              $row=6;
              }else{ ?>
                  <input type="hidden" id="airport_id" name="airport_id" value="<?php echo get_user_data('air_port_id');?>">
              <?php
              $row=12;
              } ?>
              <div class="col-md-<?php echo $row;?>">
                <div class="form-group">
                  <label class="control-label" for="terminal_name">Terminal</label>
                  <input type="input" placeholder="Terminal" id="terminal_name" name="terminal_name" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-<?php echo $row;?>">
                <div class="form-group">
                  <label class="control-label" for="ug_desc">Terminal Description</label>
                  <textarea placeholder="Terminal Description" id="terminal_desc" name="terminal_desc" class="form-control"></textarea>
                </div>
              </div>
            </div>

              <div class="row">

                <div class="col-md-12">
                  <div class="form-group">
                    <br/>
                   <a href="javascript:void(0)" onclick="addFacImage()"><label>Add More Images <a href="#" class="" data-toggle="tooltip" data-placement="right" title="Please select jpg | png | jpeg type image. Terminal Image max size is 5 MB.">(Image Guideline)</a></label></a>
                  </div>
                </div>
              </div>

              <div class="row">
              
              <div class="col-md-6 fac_one">
                <div class="form-group">
                  <label class="control-label" for="terminal_thumb">Terminal Image</label> (300X300)
                  <br/>
                  <img src="" id="terminal_thumb_img" width="120" style="padding: 7px;display:none">
                  <input type="file" placeholder="Terminal Image" name="terminal_thumb[]" class="" autocomplete="off">
                  Upload upto 5 Mb image.
                </div>
              </div>             

              <div id="ImageHtml" class="ImageHtml"></div>

            </div>


            <!-- <div class="row">
              <div class="col-md-4 fac_one">
                <div class="form-group">
                  <label class="control-label" for="terminal_thumb">Terminal Image</label> (300X300)
                  <br/>
                  <img src="" id="terminal_thumb_img" width="120" style="padding: 7px;display: none">
                  <input type="file" placeholder="Terminal Image" name="terminal_thumb[]" class="form-control" autocomplete="off">
                  Upload upto 5 Mb image.
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <br/>
                 <a href="javascript:void(0)" onclick="addFacImage()"><label>Add More</label></a>
                </div>
              </div>

              <div id="ImageHtml"></div>

            </div> -->
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  <script src="<?php echo base_url(); ?>js/admin/airportTerminal.js"></script>
<?php $this->load->view('footer'); ?>
