<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Airport Facilities
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Airport Facilities</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" id="btn_search" onclick="listAirportFacility(0,'facility_name','DESC');">
                      Search
                    </a>
                    <?php if($addRight){?>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getAirportFacility();">
                      Add New
                    </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listAirportFacility" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="<?php echo $widthArr['facility_name']?>%" data-name="facility_name" data-order="DESC" class="sorting">Facility</th>
                        <th width="<?php echo $widthArr['facility_desc']?>%" data-name="facility_desc" data-order="DESC" class="sorting">Facility description</th>

                        <?php if(get_user_data('role')==1){?>
                        <th width="<?php echo $widthArr['airport_name']?>%" data-name="airport_name" data-order="DESC" class="sorting">Airport</th>
                         <th width="<?php echo $widthArr['terminal_name']?>%" data-name="terminal_name" data-order="DESC" class="sorting">Terminal Name</th>
                         <th width="<?php echo $widthArr['terminal_images']?>%" data-name="terminal_images" data-order="DESC" class="sorting">Facility Images</th>
                        <?php } ?>
                        <?php if($editRight || $deleteRight){?>
                        <th width="<?php echo $widthArr['action']?>%" class="">Action</th>
                        <?php } ?>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="airportFacilityModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>facility/saveAirportFacility" id="airportFaciForm" name="airportFaciForm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>
            <h4 class="modal-title"><span id="action"></span> Airport Facility</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                  <input type="hidden" id="airportfacility_key" name="airportfacility_key">
                  <input type="hidden" id="imgcnt" name="imgcnt" value="1">
              <?php
              $row=6;
              if(get_user_data('role')==1){ ?>
              <div class="col-md-6">
                <div class="form-group" style="height:87px;margin-bottom:0; ">
                  <label class="control-label" for="airport_id">Airport</label>
                  <select name="airport_id" id="airport_id" class="form-control" onchange="getTerminal();">
                    <option value="">Select Airport</option>
                    <?php echo generateOptions($airports,'airport_id','airport_name'); ?>
                  </select>
                </div>
              </div>
              <?php
              $row=6;
              }else{ ?>
                  <input type="hidden" id="airport_id" name="airport_id" value="<?php echo get_user_data('air_port_id');?>">
              <?php
              $row=12;
              } ?>
              <?php if(get_user_data('role')==1 || get_user_data('role')==2){?>
           <div class="col-md-6">
                <div class="form-group" style="height:87px;margin-bottom:0;">
                  <label class="control-label" for="airport">Terminal</label>
                  <select name="terminal_id" id="terminal_id" class="form-control">
                    <option value="">Select Terminal</option>
                    <!--<option value="1">Terminal A</option>
                    <option value="2">Terminal B</option>-->
                    <?php //echo generateOptions($airports,'airport','name'); ?>
                  </select>
                </div>
              </div>
           <?php }else{ ?>
           <input type="hidden" id="terminal_id" name="terminal_id" value="<?php echo get_user_data('terminal_id');?>">
           <?php } ?>

              <div class="col-md-<?php echo $row;?>">
               
                  <div class="form-group" style="height:87px;margin-bottom:0;">
                    <label class="control-label" for="facility_name">Facility</label>
                    <input type="input" placeholder="Facility" id="facility_name" name="facility_name" class="form-control" autocomplete="off">
                  </div>
                </div>              
            
              <div class="col-md-<?php echo $row;?>">
                <div class="form-group" style="height:87px;margin-bottom:0;">
                  <!-- <label class="control-label" for="ug_desc">Facility Description</label>
                  <textarea placeholder="Facility Description" id="facility_desc" name="facility_desc" class="form-control"></textarea> -->
                </div>
              </div>
              </div>
              <div class="row">

                <div class="class-md-12">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label" for="ug_desc">Facility Description </label>
                      <textarea placeholder="Facility Description" id="facility_desc" name="facility_desc" class="form-control"></textarea>

                      <span id="faciDescError" style="color:red;font-size: 14px;font-weight: bold;"> </span>
                    </div>
                  </div>
                  
                </div>
                

              </div>
              <div class="row">

                <div class="col-md-12">
                  <div class="form-group">
                    <br/>
                   <a href="javascript:void(0)" onclick="addFacImage()"><label>Add More Images <a href="#" class="" data-toggle="tooltip" data-placement="right" title="Please select jpg | png | jpeg type image. Facility Image max size is 5 MB.">(Image Guideline)</a></label></a>
                  </div>
                </div>
              </div>
             <div class="row">
              
              <div class="col-md-6 fac_one">
                <div class="form-group">
                  <label class="control-label" for="facility_thumb">Facility Image</label> (300X300)
                  <br/>
                  <img src="" id="facility_thumb_img" width="120" style="padding: 7px;display:none">
                  <input type="file" placeholder="Facility Image" name="facility_thumb[]" class="" autocomplete="off">
                  Upload upto 5 Mb image.
                </div>
              </div>

              

              <div id="ImageHtml" class="ImageHtml"></div>

            </div>

          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit" id="save_btn">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  <script src="<?php echo base_url();?>js/ckeditor-full/ckeditor.js"></script>
  <script src="<?php echo base_url(); ?>js/admin/airportFacility.js"></script>
<?php $this->load->view('footer'); ?>