<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Touchdown Nigeria | Admin Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/square/blue.css">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url(); ?>"><b>Touchdown</b>Nigeria</a>
  </div>
  <div id="login" class="login-box-body">
    <p class="login-box-msg">Sign in</p>
    <div id="message"></div>
    <form action="<?php echo base_url(); ?>login/checkLogin" method="post" id="loginForm" name="loginForm">
      <div class="form-group has-feedback">
        <input type="text" name="username" id="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="tiny btn btn-primary">Sign In</button>
        </div>
        <div class="col-xs-8">
        	<a href="javascript:void(0)" onclick="gotoForgetPassword()" class="pull-right">I forgot my password</a>
        </div>
      </div>
    </form>
  </div>

  <div id="fogetPassword" class="login-box-body" style="display:none;">
    <p class="login-box-msg">Forgot Password</p>
    <div id="message_f"></div>
    <form action="<?php echo base_url(); ?>login/forgetPassword" method="post" id="forgetPasswordForm" name="forgetPasswordForm">
      <div class="form-group has-feedback">
        <input type="text" name="username_f" id="username_f" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <button type="submit" class="tiny btn btn-primary">Retrieve Password</button>
        </div>
        <div class="col-xs-6">
          <a href="javascript:void(0)" onclick="gotoLogin()" class="pull-right">Sign In</a>
        </div>
      </div>
    </form>
  </div>
  
  <div id="changePassword" class="login-box-body" style="display:none;">
    <p class="login-box-msg">Change password</p>
    <div id="message_c"></div>
    <form action="<?php echo base_url(); ?>login/changepass" method="post" id="changePass" name="changePass">
      <input type="hidden" name="verification_code" id="verification_code" value="<?php echo $verification_code;?>">
	  <div class="form-group has-feedback">
        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="conf_password" id="conf_password" class="form-control" placeholder="Confirm Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="tiny btn btn-primary">Save</button>
        </div>
		<div class="col-xs-8">
          <a href="<?php echo base_url();?>" onclick="" class="pull-right">Sign In</a>
        </div>
      </div>
    </form>
  </div>

  <div id="changePassword_err" class="login-box-body" style="display:none;">
    <p class="login-box-msg">Change password</p>
    <div id="message_e"><div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button" onclick="gotoLogin();"><i class="fa fa-fw fa-close"></i></button>Your change password link has been expired.</div></div>
	 <div class="row">
        <div class="col-xs-4">
          <!--button type="submit" class="tiny btn btn-primary">Save</button-->
        </div>
		<div class="col-xs-8">
          <a href="<?php echo base_url();?>" onclick="" class="pull-right">Sign In</a>
        </div>
      </div>
  </div>
  
</div>
<script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript">
 
    function gotoForgetPassword(){
      $("#login").hide();
      $("#fogetPassword").show();
	  $("#changePassword").hide();
	  $("#changePassword_err").hide();
    }
    function gotoLogin(){
      $("#fogetPassword").hide();
      $("#login").show();
	  $("#changePassword").hide();
	  $("#changePassword_err").hide();
    }
	function gotoChangePass(){
      $("#fogetPassword").hide();
      $("#login").hide();
	  $("#changePassword").show();
	  $("#changePassword_err").hide();
    }
	function gotoChangePass_err(){
      $("#fogetPassword").hide();
      $("#login").hide();
	  $("#changePassword").hide();
	  $("#changePassword_err").show();
    }
	<?php if($show_change_password==1){ ?>
	gotoChangePass();
	<?php }else if($show_change_password==2){?>
	gotoChangePass_err();
	<?php } ?>
    $(document).ready(function()
    {

        var options = { 
          beforeSend: function() 
          {
             $("#loginForm").find('[type="submit"]').toggleClass('sending').blur();
          },
          uploadProgress: function(event, position, total, percentComplete) 
          {
              
          },
          success: function(json) 
          {
            if(json.status == "success"){           
                window.location.href = '<?php echo base_url(); ?>index';
            }else{
                $("#message").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>'+json.message+'</div>');
            }
            $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          },
          complete: function(json) 
          {
            $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          },
          error: function()
          {
              $("#message").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>Something went wrong.</div>');
			  $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          }
      };
      
      options = $.extend(true, {"dataType":"json"}, options);
      $("#loginForm").ajaxForm(options);
     
      var options_f = { 
          beforeSend: function() 
          {
             $("#forgetPasswordForm").find('[type="submit"]').toggleClass('sending').blur();
          },
          uploadProgress: function(event, position, total, percentComplete) 
          {
              
          },
          success: function(json) 
          {
            if(json.status == "success"){           
                $("#message_f").html('<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>'+json.message+'</div>');
            }else{
                $("#message_f").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>'+json.message+'</div>');
            }
            $("#forgetPasswordForm").find('[type="submit"]').removeClass('sending').blur();
          },
          complete: function(json) 
          {
            $("#forgetPasswordForm").find('[type="submit"]').removeClass('sending').blur();
          },
          error: function()
          {
			$("#message_f").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>Something went wrong.</div>');
              $("#forgetPasswordForm").find('[type="submit"]').removeClass('sending').blur();
          }
      };
      
      options_f = $.extend(true, {"dataType":"json"}, options_f);
      $("#forgetPasswordForm").ajaxForm(options_f);
	  
	  
	  var options_c = { 
          beforeSend: function() 
          {
             $("#changePassword").find('[type="submit"]').toggleClass('sending').blur();
          },
          uploadProgress: function(event, position, total, percentComplete) 
          {
              
          },
          success: function(json) 
          {
            if(json.status == "success"){           
                //window.location.href = '<?php echo base_url(); ?>users';
				$("#new_password").val('');
				$("#conf_password").val('');
				$("#message_c").html('<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>'+json.message+'</div>');
            }else{
                $("#message_c").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>'+json.message+'</div>');
            }
            $("#changePassword").find('[type="submit"]').removeClass('sending').blur();
          },
          complete: function(json) 
          {
            $("#changePassword").find('[type="submit"]').removeClass('sending').blur();
          },
          error: function()
          {
              $("#message_c").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-fw fa-close"></i></button>Something went wrong.</div>');
			  $("#changePassword").find('[type="submit"]').removeClass('sending').blur();
          }
      };
      
      options_c = $.extend(true, {"dataType":"json"}, options_c);
      $("#changePassword").ajaxForm(options_c);

    });
</script>
</body>
</html>
