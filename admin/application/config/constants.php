<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define("PER_PAGE",10);

define("TB_ADMIN","tbl_admin");
define("TB_USERS","tbl_users");
define("TB_AIRPORTS","tbl_airports");
define("TB_FACILITY","tbl_facility");
define("TB_STORE_CATEGORY","tb_store_category");
define("TB_ACCESS_PERMISSION","tb_access_permissions");
define("TB_STORES","tb_stores");
define("TB_STORE_FACILITY","tb_store_facility");
define("TB_STORE_OFFER","tb_store_offer");
define("TB_PAGES","tb_pages");
define("TB_RIGHTS","tbl_privilege");
define("TB_TERMINAL","tb_terminal");
define("TB_NOTIFICATION_ALERT", "tbl_notification_alert");
define("TB_NOTIFICATION", "tbl_notification");
define("TBL_PENDING_NOTIFICATION","tbl_pending_notification");
define("TBL_SENT_NOTIFICATION","tbl_sent_notification");
define("TB_FAQ","tb_faq");
define("TBL_HOMESCREEN","tbl_homescreen");
define("TBL_AIRPORT_ALERT_REQUEST","tbl_airport_alert_request");
define("TBL_FLIGHT_ALERT_REQUEST","tbl_flight_alert_request");
define("TBL_AIRLINE_CODE","tb_aireline_code");
define("TBL_AIRPORT_COUNT","tb_airport_hit_count");
define("TBL_ADDCONTACT","tb_addContact");
define("TBL_FEEDBACK","tbl_feedback");
define("TB_USER_GUIDE","tb_user_guide");
define("TB_INTEREST","tbl_interest");
define("TB_AIRLINE_CSV_UPLOAD","tbl_airline_csv_upload");
define("TB_CSV_AIRLINE","tbl_csv_airline");
define("TB_AIRPORT_CSV_UPLOAD","tbl_airport_csv_upload");
define("TB_CSV_AIRPORT","tbl_csv_airport");
define("TB_FLIGHT_STATUS","tbl_flight_status");
define("TB_CSV_DATE","tbl_csv_date");
define("TB_USER_NOTIFICATION","tbl_user_notification");

/*define("FLIGHT_STAT_appKey","593fb499032fe3cf713481a4fb6dc9d9");
define("FLIGHT_STAT_appId","60daedd8");*/


/*define("FLIGHT_STAT_appKey","9f6dfc6985a40481c605c0646a7679cc");
define("FLIGHT_STAT_appId","aca42f18");*/

/*define("FLIGHT_STAT_appKey","1dbb482fc9b0c2a1c9fd5e9b98f92c58");
define("FLIGHT_STAT_appId","380ed3e7");*/

/*define("FLIGHT_STAT_appKey","e877880ba9f14f66aeb8d390d2f76078");
define("FLIGHT_STAT_appId","4b11c75b");
*/

define("FLIGHT_STAT_appKey","03587ef13c4b58f8c87b82d307649a7d");
define("FLIGHT_STAT_appId","065902ad");

define("ADMIN_EMAIL","admin@touchdownnigeria.net");
//define("ADMIN_EMAIL","saritam.test@gmail.com");
define("ADMIN_NAME","Touchdown Nigeria");
