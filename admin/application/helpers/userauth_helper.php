<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function generateOTP(){
	return rand(1001,9999);
}

function is_user_logged_in()
{
	$CI = & get_instance();
	$sessionArr = $CI->session->userdata("auth_user");
	$CI->load->model("Common_model");
	$arrResult = $CI->Common_model->selectQuery("*",TB_ADMIN,array('id'=>$sessionArr['id'],'is_active'=>'1'));
	if(count($arrResult))
	{
		if($arrResult[0]["role"] == 3){
			$airport_admin_result = $CI->Common_model->select("*",TB_ADMIN,array("air_port_id"=>$arrResult[0]["air_port_id"],'is_active'=>'1',"role"=>2));
			if(!$airport_admin_result[0]["is_active"]){
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

function get_user_data($var="")
{
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}

function get_user_permission($moduleId,$roleId)
{
	if(is_numeric($moduleId))
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			if($roleId !="")
			{
				if($roleId == 1)
				{
					// $CI = get_instance();
					// $CI->load->model("common_model");
					// $arrResult = $CI->common_model->selectQuery("*",TB_EMPLOYEE_PRIVILEGE,array('employee_role_id'=>$roleId,'module_id'=>$moduleId));
					// if(isset($arrResult[0])){
					// 	return $arrResult[0];
					// }else{
					// 	return false;
					// }

					return array("add_privilege"=>1,"update_privilege"=>1,"delete_privilege"=>1,"access_privilege"=>1);
				}
				else
				{
					$CI = get_instance();
					$CI->load->model("common_model");
					$arrResult = $CI->common_model->selectQuery("*",TB_EMPLOYEE_PRIVILEGE,array('employee_role_id'=>$roleId,'module_id'=>$moduleId));
					if(isset($arrResult[0])){
						return $arrResult[0];
					}else{
						return false;
					}
				}

			}else
			{
				return false;//return array("permission_add"=>1,"permission_update"=>1,"permission_delete"=>1,"access"=>1);
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function is_ajax_request()
{
	$CI = & get_instance();
	if(!$CI->input->is_ajax_request()) {
	  exit('No direct script access allowed');
	}
	else
	{
		return true;
	}

}

function getValidUser($token)
{
	$CI = get_instance();
	$CI->load->model("Common_model");
	$user = $CI->common_model->select("userid",TB_USERS,array("user_token" => $token));
	return $user;
}

function formatDate($date, $format='Y-m-d'){
	return date($format,strtotime(str_replace("/", "-", $date)));
}

function sentEmail($from = array(ADMIN_EMAIL,ADMIN_NAME), $to = array(ADMIN_EMAIL), $reply_to = array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message){


	$CI = get_instance();
	$config['charset'] = 'utf-8';
	$CI->load->library('email', $config);
	
	$CI->email->set_mailtype('html');
	$CI->email->from($from[0], $from[1]);
	$CI->email->to($to[0]);
	$CI->email->reply_to($reply_to[0], $reply_to[1]);
	$CI->email->subject($subject);
	$CI->email->message($message);

	if ($CI->email->send()) {
        return array("status" => "success");
    } else {
       return array("status" => "error", "msg"=>$CI->email->print_debugger());
    }

	/*$headers[] = "From: ".$from[1]."<".$from[0].">";
	$headers[] = "Reply-To: ".$reply_to[1]."<".$reply_to[0].">";
	$headers[] = "Return-Path: ".$from[1]."<".$from[0].">";
	$headers[] = "X-Priority: 3";
	$headers[] = "X-Mailer: PHP/" . phpversion();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/html; charset=iso-8859-1"	;
	$header=implode("\r\n", $headers);
	mail($to[0],$subject,$message,$header);*/

	
}


function sentAdminEmail($from = array(ADMIN_EMAIL,ADMIN_NAME), $to = array(ADMIN_EMAIL), $reply_to = array(ADMIN_EMAIL,ADMIN_NAME),$subject,$message){


	$CI = get_instance();
	$config['charset'] = 'utf-8';
	$CI->load->library('email', $config);
	
	$CI->email->set_mailtype('html');
	$CI->email->from($from[0], $from[1]);
	$CI->email->to($to[0]);
	$CI->email->reply_to($reply_to[0], $reply_to[1]);
	$CI->email->subject($subject);
	$CI->email->message($message);
	if ($CI->email->send()) {
        return array("status" => "success");
    } 
    else
    {
       return array("status" => "error", "msg"=>$CI->email->print_debugger());
    }

	// $headers[] = "From: ".$from[1]."<".$from[0].">";
	// $headers[] = "Reply-To: ".$reply_to[1]."<".$reply_to[0].">";
	// $headers[] = "Return-Path: ".$from[1]."<".$from[0].">";
	// $headers[] = "X-Priority: 3";
	// $headers[] = "X-Mailer: PHP/" . phpversion();
	// $headers[] = "MIME-Version: 1.0";
	// $headers[] = "Content-type: text/html; charset=iso-8859-1"	;
	// $header=implode("\r\n", $headers);
	// mail($to[0],$subject,$message,$header);

	//if ($CI->email->send()) {
        //return array("status" => "success");
    //} else {
    //    return array("status" => "error", "msg"=>$CI->email->print_debugger());
    //}
}
function getMailTemplate($mail,$data)
{
	$CI = get_instance();
	$temp=$CI->load->view('mails/'.$mail,$data,true);
	return $temp;
}
function generateOptions($arr=array(),$value="",$op="",$select=""){
	$options = "";
	if($value != "" && $op != ""){
		foreach ($arr as $key) {
			if($key[$value] == $select){$select = ' selected="selected"';}else{$select = "";}
			$options .= '<option value="'.$key[$value].'" '.$select.'>'.$key[$op].'</option>';
		}
	}
	return $options;
}
function getSelectedValue($list,$select,$check,$get)
{
	foreach($list as $k=> $v)
	{
		if($v[$check]==$select)
		{
			return $v[$get];
		}
	}
}
function getModuleData($moduleArr,$arr)
{
	$CI = get_instance();
	$CI->load->model("common_model");
	$select='';
	$where=$moduleArr['where'];
	$order=$moduleArr['order'];
	if(count($moduleArr['field']))
	{
		foreach($moduleArr['field'] as $k => $v)
		{
			if($k)
			{
				$select.=','.$v;
			}
			else
			{

				$select.=$v;
			}
		}
	}
	else
	{
		$select="*";
	}

	$dataArr = $CI->Common_model->select($select,$moduleArr['table'],$where);
	return $dataArr;
}

function checkRight($role,$module,$privilege)
{
	$CI = get_instance();
	$CI->load->model("Common_model");
	$adminArr = $CI->Common_model->select('*',TB_ADMIN,array('id'=>$id));
	if($role==1)
	{
		return array('add_privilege'=>1,'update_privilege'=>1,'delete_privilege'=>1,'access_privilege'=>1);
	}
	$where=array('role_id'=>$role,'module_id'=>$module);
	if($privilege!="")
	{
		$where[$privilege.'_privilege']='1';
	}
	$dataArr = $CI->Common_model->select('add_privilege,update_privilege,delete_privilege,access_privilege',TB_RIGHTS,$where);

	/*print_r($CI->db->last_query());

	exit();*/
	if(count($dataArr))
	{
		return $dataArr[0];
	}
	else
	{
		return array('add_privilege'=>0,'update_privilege'=>0,'delete_privilege'=>0,'access_privilege'=>0);
	}
}

function getRightArr($role)
{
	$CI = get_instance();
	$CI->load->model("Common_model");
	$moduleArr=lang('MODULE_RIGHTS');
	$moduleArr[8]="Admin permmission";
	$arr=array();
	foreach($moduleArr as $k => $v)
	{
		//$dataRights=$CI->common->select('*',TB_RIGHTS,array('role_id'=>$role,'module_id'=>$k,"access_privilege"=>'1'));
		//$arr[$k]=$dataRights['access_privilege'];
		$dt=checkRight($role,$k,'access');
		if($dt['access_privilege'])
		{
			$arr[$k]=1;
		}
		else
		{
			$arr[$k]=0;
		}
	}
	if(count($arr))
	{
		return $arr;
	}
	else
	{
		return false;//'no_right_to_access';
	}
}
function add_notification($dt=array())
{
	$CI = get_instance();
	$CI->load->model("Common_model");
	$d = new DateTime("now", new DateTimeZone("Europe/Athens"));
	$today= $d->format("Y-m-d H:i");
	$dt['create_date']=date('Y-m-d H:i:s',strtotime($today));
	$id = $CI->Common_model->insert(TB_NOTIFICATION_ALERT,$dt);
	return $id;
}
function sendAndroidNotification($token_array=array(),$message='',$flag='')
{
	//echo "tyuhiii";
	$msg = array
	(
		'message' 	=> $message,
		'title'		=> 'title',
		'subtitle'	=> 'subtitle',
		//'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
		'vibrate'	=> 1,
		'sound'		=> 1,
		//'largeIcon'	=> 'large_icon',
		//'smallIcon'	=> 'small_icon'
	);
	if($flag!="")
	{
		$msg['flag']=$flag;
	}
	//echo $message;
	$arr=array();
	//if(count($token_array))
	//{
	//	return;
	//}
	//echo $message;
	//$arr=array_chunk($token_array,5);
	//echo "<pre>";
	//print_r($token_array);

	/*print_r($token_array);

	exit();*/
	foreach($token_array as $v)
	{
		//echo $v['device_token'];//die();

		if($v['device_token']!="")
		{
			$fields = array
			(
				'registration_ids' 	=> array($v['device_token']),//array("fWNw9-mlR2I:APA91bFVQKpSbx9o34uqV4P1vjHCctWl6SNZwkyNehZAJl6qK8kTpdipcQtFTpJvNfjCvmeJIFFt7WuS7D5CWJGCvSVTIHG2wEmdaBpFroeCdEd4DZ8sgD1fQpoRPLvzEk2InJixBK_S"),
				'data'			=> $msg
			);

			$headers = array
			(
				'Authorization: key=' . "AIzaSyBo2dErFcdcgM1BvKNGAMAsRSdI_WVQxPo",
				'Content-Type: application/json'
			);

			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			//curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode( $fields ) );
			$result = curl_exec($ch );

			/*print_r($result);

			exit();*/


			//echo $result."<br/>";
			curl_close( $ch );
		}
		add_notification(array('userid'=>$v['id'],"not_title"=>$message,"not_flag"=>$flag));
	}//die('hi');
	//add_notification(array('userid'=>$storeOfferData[0]['id'],"not_title"=>$msg1));
	return $result;
	//echo $result;
}
?>
