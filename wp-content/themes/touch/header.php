<?php

global $wpdb;

$results_data = $wpdb->get_results("select wp_abcfic_images.filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name='Logo') order by wp_abcfic_images.added_dt desc limit 0,1 ");


$image_data = $wpdb->get_results("select wp_abcfic_images.filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name='Header Image') order by wp_abcfic_images.added_dt desc limit 0,1 ");

//print_r($results_data);
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package touch
 */

?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <!--====== USEFULL META ======-->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <!--====== TITLE TAG ======-->
    <title>Touchdown Nigeria</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="<?php echo get_template_directory_uri();?>/img/favicon.png" />

    <!--====== STYLESHEETS ======-->
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
       <!-- <div class="top_img"><img src="<?php //echo site_url();?>/wp-content/themes/touch/img/top_back.jpg" /></div>-->
	    <div class="top_img" style="background-image:url(<?php echo site_url();?>/wp-content/uploads/img-collections/header-image/<?php echo $image_data[0]->filename; ?>);"></div>
		<div class="black_overlay"></div>
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <button class="collapsed navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-scrollspy">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="#home" class="navbar-brand"><img src="<?php echo site_url();?>/wp-content/uploads/img-collections/header/<?php echo $results_data[0]->filename; ?>" alt="logo"></a>
                        </div>
                       <!--<div class="app-download-button"><a href="#download"><i class="fa fa-mobile"></i> get this app</a></div>-->
                        <div class="collapse navbar-collapse bs-example-js-navbar-scrollspy">
                            <!--<ul id="nav" class="nav navbar-nav cl-effect-11">
                                <li><a href="#home">home</a></li>
                                <li><a href="#features">Features</a></li>
                                <li><a href="#about">about</a></li>
                                <li><a href="#screenshot">Screenshort</a></li>
                                <li><a href="#price">Pricing</a></li>
                                <li><a href="#testmonial">Testmonial</a></li>
                                <li><a href="#subscriber">subscribe</a></li>
                                <li class="visible-xs"><a href="#download">download</a></li>
                            </ul>-->
								<?php  
									$defaults = array('theme_location' => 'Menu 1',
										'menu' => 'Menu 1',
										'menu_id' => 'nav',
										'menu_class'      => 'nav navbar-nav cl-effect-11',
										'container'         => 'ul',
										
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									);
								wp_nav_menu( $defaults )
							?>
                        </div>
                    </div>
                </nav>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
        <div class="welcome-text-area">
            <!--
			<div class="welcome-background-layer hidden-sm hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="layer-one animated wow fadeInUp" data-wow-duration="2s"><img src="<?php echo get_template_directory_uri();?>/img/home/blue_shape.png" alt=""></div>
                        <div class="layer-two animated wow fadeInLeft" data-wow-duration="2s"><img src="<?php echo get_template_directory_uri();?>/img/home/red_shape.png" alt=""></div>
                    </div>
                </div>
            </div>
            <div class="welcome-img hidden-xs">
                <div class="welcome-mockup-one animated wow slideInRight" data-wow-duration="2.5s"><img src="<?php echo get_template_directory_uri();?>/img/home/mockup_top_img.png" alt=""></div>
                <div class="welcome-mockup-two animated wow slideInRight" data-wow-duration="1s"><img src="<?php echo get_template_directory_uri();?>/img/home/mockup_main_phone_img.png" alt=""></div>
            </div>
			-->
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                        <div class="welcome-text wel_sec">
                            <h1>Welcome to Touchdown Nigeria</h1>
                           <!-- <h3><span>in beautiful way</span> with Us</h3>-->
                            <p>This app is available on Google Play Store</p>
                            <div class="home-button">
								<div class="app-download-button"><a href="https://play.google.com/store/apps/details?id=com.exceptionaire.root.tdn&hl=en" target="_blank"><i class="fa fa-mobile"></i> get this app</a></div>
                                <!--<a href="#"><i class="fa fa-android"></i> Play Store</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </header>
    <!--END TOP AREA-->