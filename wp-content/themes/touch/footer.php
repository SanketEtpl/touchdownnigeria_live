<?php

global $wpdb;

$results_data = $wpdb->get_results("select wp_abcfic_images.filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name='Logo') order by wp_abcfic_images.added_dt desc limit 0,1 ");
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package touch
 */

?>

    <!--FOOER AREA-->
    <div id="bgvid">
        
    </div>
    <div class="footer-area">
        <div class="container">
            <div class="row wow fadeIn">
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                    <div class="footer-logo">
                        <a href="#"><img src="<?php echo site_url();?>/wp-content/uploads/img-collections/header/<?php echo $results_data[0]->filename; ?>" alt=""></a>
                    </div>
                </div>
                <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                    <div class="footer-copyright text-center">
                        <p><?php footer_text(); ?></p>
<a href="http://www.exceptionaire.com/" title="Web Design &amp; Development by Exceptionaire" target="_blank">Web Design &amp; Development by Exceptionaire</a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <div class="footer-social-bookmark">
                        <ul>
                            <li><a href="https://www.facebook.com/TouchDownNigeria/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/TDNigeria" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/13366599/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.instagram.com/touchdownnigeria/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <!-- <li><button id="theD">Button</button></li> -->
                            <!-- <li><a href="#"><i class="fa fa-dribbble"></i></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOER AREA END-->
<?php wp_footer(); ?>

<script type="text/javascript">

    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";

    
    
if($( window ).width() != 1287)
{
    $('.menu-item').click(function(){

        $('.navbar-collapse').removeClass('in');
       /* $('.navbar-collapse').attr('aria-expanded','false');
        $('.navbar-collapse').css({'height','1px'});*/

    });
}

jQuery('#addContact').click(function(){

    var name = jQuery('#name').val();
    var email = jQuery('#email').val();
    var subject = jQuery('#subject').val();
    var comments = jQuery('#comments').val();
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

    if(name == "")
    {
        jQuery("#name").css({'border':'1px solid red'});
    }
    if(email == "" || !pattern.test(email))
    {
        jQuery("#email").css({'border':'1px solid red'});
    }
    else
    {
        jQuery("#name").css({'border':'1px solid #ccc'});
        jQuery("#email").css({'border':'1px solid #ccc'});
        jQuery.ajax({

                    type : "POST",
                    url : 'http://touchdownnigeria.net/admin/api/addContact',
                    data :
                    {
                        name : name,
                        email : email,
                        subject : subject,
                        comments : comments
                        
                    },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Basic ' + btoa('LesFlammant:614266cc2ca75d357d87bc33b00ecedf'));
                    },
                    success : function(response)
                    {
                        var obj = JSON.parse(response);

                        console.log(obj);

                        if(obj.status == 'success')
                        {
                            jQuery('#successMessage').show();
                            setTimeout(function(){
                               
                                jQuery('#successMessage').hide();
                            },2000);
                            
                        }
                        else
                        {
                            jQuery('#errorMessage').show();
                            setTimeout(function(){

                                jQuery('#errorMessage').hide();
                            },2000);
                            
                        }
                    }


        });
    }

        
});


</script>

</body>
</html>
