<?php /* Template Name: privacy */ ?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="http://touchdownnigeria.net/wp-content/themes/touch/css/bootstrap.min.css">
<head>
    <title></title>
</head>
<body>
<style type="text/css">
    
    *{margin:0;}
body{margin:0; padding:0!important; color:#333; font-size:14px; font-weight:normal; background:#fff;font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;}
html{margin:0;padding:0}
a{margin:0;padding:0;outline:0;text-decoration:none;outline:none!important}
a:hover{margin:0;padding:0;outline:0;text-decoration:none;outline:none!important}
a:focus{margin:0;padding:0;outline:0;text-decoration:none;outline:none!important}
input, button{outline:none!important}
input, button:focus{outline:none!important}
textarea{outline:none!important;resize:none!important}
input::-ms-clear{display:none}
select::-ms-expand{display:none}
select{appearance:none; -moz-appearance:none; -webkit-appearance:none;}
::-moz-selection{background:#000;color:#fff}
::selection{background:#ccc;color:#333}
.wrapper{position: relative;width:100%;float: left;}

header {width: 100%; float: left; background-image: url(../images/bg.jpg); background-size: cover; background-position: 50% 40%; padding: 10px; }
.logo {display: block; width: 100%; float: left; text-align: center; }
.logo img {width: 100px; float: none; margin: 0 auto; display: inline-block; }
.privacy-policy{float: left; width:100%; padding: 45px 0; }
.main-head {font-size: 26px; margin-top: 0; margin-bottom: 25px; }
.privacy-policy p {font-size: 14px; line-height: 23px; }
.privact-subhead {font-size: 16px; font-weight: 600; }
.privact-subhead1 {margin-bottom: 0; font-size: 15px !important; font-weight: 600; }
footer{width: 100%; float: left; background: #333; }
footer p {color: #FFF; width: 100%; float: left; font-size: 14px; margin-bottom: 0; text-align: center; padding: 8px; }
.content2 .privact-subhead {margin-bottom: 0; }
.content2 .privact-subhead1,.content3 .privact-subhead1,.content4 .privact-subhead1 {margin-top: 15px; color: #353535; }
.content2 p,.content3 p,.content4 p,.content5 p,.content1 p,.content6 p {color: #555555; }
.content2,.content3,.content4,.content5,.content1,.content6 {width:100%; float: left; padding: 20px 0 0px; }

/*13-04-2017*/
.content7 .privact-subhead1 {margin-top: 15px; color: #353535; }
.content7 p {color: #555555; }
.content7 {width:100%; float: left; padding: 20px 0 0px; }
/*13-04-2017*/
</style>

    <div class="wrapper" id="top">

  <header>
      <div class="logo">
        <img src="http://touchdownnigeria.net/wp-content/uploads/img-collections/header/tdn-logo.jpg">
      </div>
  </header>

  <section class="privacy-policy">
    <div class="container">

    <h1 class="main-head">Privacy Policy</h1>    

      <p>Touch Down Nigeria. (“Touch Down Nigeria” or “we”) is committed to protecting the privacy of users of our applications (“you”). We have prepared this privacy policy to describe to you our practices regarding the Personal Data (as defined below) that we collect from users of our applications (“App”) and online services (the “Services”). This policy does not apply to the practices of companies that we do not own or control.</p>
      <p> By using our Site and our Services you understand and agree to the terms of this privacy policy. use and disclosure of your Personal Data in accordance with this policy. If you do not agree to this policy, do not install, use, or access our Services. We may update this policy from time to time and will post updates to this policy on this web page. 
      </p>

    <div class="content1">
        <h1 class="privact-subhead">1. INFORMATION COLLECTION AND USE</h1>
        <p>“Personal Data” means data that allows someone to identify or contact you, including, for example, your name, address, phone number, email address, as well as any other non-public information about you that is associated with or linked to any of the foregoing data, including audio content that you create using our App. </p>
    </div>

    <div class="content2">
        <h1 class="privact-subhead">2. INFORMATION THAT YOU PROVIDE TO US DIRECTLY</h1>
        <p class="privact-subhead1">Basic account information</p>
        <p>In order to register for a Smule account on the App, you are required to provide Touch Down Nigeria with certain Personal Data, such as your email address, phone number, username and password. Your user name is listed publicly on the Services, including in search results. We may use your email address and/or phone number to send you information about the Services.</p>

       

        <p class="privact-subhead1">Additional profile information</p>
        <p>You may also choose to provide Touch Down Nigeria with additional Personal Data for your profile, which we will make public (your “Public Profile”), such as a picture, The information that you provide in your Public Profile will be visible to others, including anonymous visitors to the Site.</p>

       

        <p class="privact-subhead1">Third party Integration</p>
        <p>FlightStats, Inc. (“FlightStats”) respects your individual privacy. This privacy policy documents our adherence to the highest industry standards for the protection of your personal information. This policy applies to all FlightStats web sites and covers the following areas: </p>
        <ul>
            <li>What personally identifiable information is collected by FlightStats or by any third party through our websites. </li>


            <li>How FlightStats uses this information.</li>
            <li>With whom FlightStats may share subscriber information.</li>
            <li>What choices are available you as a subscriber with respect to collection, use and distribution of the information.</li>
            <li>What types of security procedures are in place to protect the loss, misuse or alteration of information under our control.</li>
            <li>How subscribers can correct any inaccuracies in the information.</li>

        </ul>




       
        <p class="privact-subhead1">Content, following and other public information</p>
        <p>Touch Down Nigeria's Services are designed to help you create and share Content. Most of the information you provide Touch Down Nigeria is information you are asking Touch Down Nigeria to make public. This includes the Content you create and the metadata associated with it, such as when you created it, the people you follow, </p>
      
        <p class="privact-subhead1">Camera</p>
        <p>This permission allows this application to capture the images and/or record video and upload it to the timeline in the app.</p>

    </div>

    <div class="content3">
        <h1 class="privact-subhead">3. INFORMATION THAT WE GET FROM YOUR USE OF THE SERVICES</h1>
        <p class="privact-subhead1">Log data and location information</p>
        <p>When you use the Services, we automatically collect certain log data, including your IP address, browser type, hardware type, unique device ID, the version of your operating system, your activity within the Services, and your location. We use this information to administer, troubleshoot and improve the Services, and to help us understand how you and our other users use the Services. </p>

   
        <p class="privact-subhead1">Third-party platforms</p>
        <p>Third-party platforms and marketplaces through which Touch Down Nigeria's application are made available may collect information about you, or receive information from Touch Down Nigeria, in order to provide or analyze their services. These third-party platforms are neither developed nor administered by us and we are not responsible for the actions of those third parties. </p>
 
    </div>



    <div class="content4">
        <h1 class="privact-subhead">4. INFORMATION SHARING AND DISCLOSURE</h1>
        <p>We do not sell or disclose your Personal Data to unaffiliated third parties except under the circumstances described in this Privacy Policy.</p>
    
        <p class="privact-subhead1">Other users</p>
        <p>Any information in your Public Profile (e.g., your username and any profile image, location or additional information that you provide as part of your Public Profile, but not your phone number, or email address) will be accessible by other users of the App. Content that you create through or upload to the App or the Services will by default be public and available to users of the App and the Services. </p>

   

        <p class="privact-subhead1">With your consent</p>
        <p>We may disclose your Personal Data when we have your express permission.</p>
    </div>


    <div class="content5">
        <h1 class="privact-subhead">5. SECURITY</h1>
        <p>Our App have certain security measures in place in effort to protect the loss, misuse, and alteration of the Personal Data under our control. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we use reasonable efforts to protect your Personal Data, we cannot guarantee its absolute security.</p>
    </div>

    <div class="content6">
        <h1 class="privact-subhead">6. OTHER WEBSITES AND APPLICATIONS</h1>
        <p>The Services contain links to third party websites and applications. When you click on a link to any other website or location, you will leave the App and go to another site and another entity may collect Personal Data, Anonymous Data, and/or Aggregated Data from you. We have no control over, do not review, and cannot be responsible for, these outside websites or their content. </p>
    </div>


    


    </div><!--container-->
  </section>


  <footer>
      <p>© 2017 - All Rights Reserved​</p>
  </footer>


</div><!--wrapper--> 


</body>
</html>
