<?php

/*
 * Template Name: template
 * 
 */
?>

<!-- Featured-->
<?php
global $wpdb;
get_header();

?>

<!-- Features-->
<?php 
$post_id = 84;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
//echo do_shortcode( $content );//executing shortcodes

$posts = [31,34,40,43]; 
$icons = ['fa fa-television','fa fa-diamond','fa fa-pencil-square-o','fa fa-cogs'];

?>

<section class="features-area gray-bg section-padding" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(84); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>

                        <?php echo do_shortcode($content); ?>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow fadeIn">
                    <div class="features-image-slider">
                        
                    <?php for($i=0;$i<count($posts);$i++){ 

                    	$image = wp_get_attachment_image_src( get_post_thumbnail_id($posts[$i]), 'single-post-thumbnail' );
                    	?>
                           <div class="single-image-slide"><img src="<?php echo $image[0]; ?>" alt=""></div>
                        <?php  } ?>     

                    </div>
                </div>
                <div class="col-md-8 col-lg-8 col-sm-6 col-xs-12 wow fadeIn">
                    <div class="features-menu-triger">
                    <?php for($i=0;$i<count($posts);$i++){ ?>

                    	<div class="single-menu-items">
                            <h4><?php echo get_the_title($posts[$i]); ?></h4>
                            <div class="features-icon">
                                <i class="<?php echo $icons[$i];?>"></i>
                            </div>
                        </div>

                    <?php  } ?>

                        
                    </div>
                    
                    <div class="features-details-slider">
                    <?php for($i=0;$i<count($posts);$i++){ 


						$post_id = $posts[$i];// example post id
						$post_content = get_post($post_id);
						$content = $post_content->post_content;
						//executing shortcodes
					?>
                        <div class="single-features-details">
                            <?php echo do_shortcode( $content );?>
                        </div>
                    <?php  } ?>
                    </div>    
                    <div class="features-arrow"></div>
                </div>
            </div>
        </div>
</section>






<section class="about-area dark-bg section-padding" id="about">
	 <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">

                    
                        <h2><?php echo get_the_title(6); ?></h2>
                        <?php 

						$post_id = 6;// example post id
						$post_content = get_post($post_id);
						$content = $post_content->post_content;
						echo do_shortcode( $content );//executing shortcodes
					?>
                        
                    </div>
                </div>
            </div>

<?php 

$post_id = 111;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
echo do_shortcode( $content );//executing shortcodes
?>

</div>
</section>


<section id="fact">
<?php 
$post_id = 94;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
echo do_shortcode( $content );//executing shortcodes

?>
</section>

<section id="screenshot">
<?php 
$post_id = 98;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
//echo do_shortcode( $content );//executing shortcodes

 global $wpdb;
  //echo site_url();
  $results = $wpdb->get_results('select wp_abcfic_images.filename as filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name = "Screenshot") ');
  /*echo '<pre>';
  print_r($results);*/
   ?>
    <section class="screenshot-area section-padding" id="screenshot">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Screenshots</h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 wow fadeIn ">
                    <div class="screenshot-slider wow fadeIn">

                        <?php foreach($results as $data){ ?>
                        <div class="single-screenshot">
                            <a href="http://<?php echo $_SERVER['REQUEST_URI']?>Touch/wp-content/uploads/img-collections/screenshot/<?php echo $data->filename; ?>" data-effect="mfp-zoom-in"><img src="<?php echo site_url();?>/wp-content/uploads/img-collections/screenshot/<?php echo $data->filename; ?>" alt=""></a>
                        </div>

                        <?php  } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>




<section id="download">
<?php 
$post_id = 96;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
echo do_shortcode( $content );//executing shortcodes

?>
</section>


<section id="testimonial">
<?php 
$post_id = 88;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
echo do_shortcode( $content );//executing shortcodes

?>
</section>



<?php
get_footer();
?>
