<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

?>
  <!--SCREENSHOT AREA-->

  <?php global $wpdb;

    $post_id = 130;// example post id
    $post_content = get_post($post_id);
    $content = $post_content->post_content;
  //echo site_url();
  $results = $wpdb->get_results('select wp_abcfic_images.filename as filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name = "Screenshot") ');
  /*echo '<pre>';
  print_r($results);*/
   ?>
    <section class="screenshot-area section-padding" id="screenshot">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(130); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 wow fadeIn ">
                    <div class="screenshot-slider wow fadeIn">
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_1.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_1.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_2.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_2.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_3.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_3.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_4.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_4.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_5.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_5.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_6.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_6.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_1.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_1.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_2.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_2.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_3.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_3.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_4.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_4.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_5.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_5.png" alt=""></a>
                        </div>
                        <div class="single-screenshot">
                            <a href="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_6.png" data-effect="mfp-zoom-in"><img src="<?php echo get_template_directory_uri();?>/img/screenshot/screenshot_6.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 wow fadeIn ">
                    <div class="screenshot-slider wow fadeIn">

                        <?php foreach($results as $data){ ?>
                        <div class="single-screenshot">
                            <a href="<?php echo site_url();?>/wp-content/uploads/img-collections/screenshot/<?php echo $data->filename; ?>" data-effect="mfp-zoom-in"><img src="<?php echo site_url();?>/wp-content/uploads/img-collections/screenshot/<?php echo $data->filename; ?>" alt=""></a>
                        </div>

                        <?php  } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SCREENSHOT AREA END-->