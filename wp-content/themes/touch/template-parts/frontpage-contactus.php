<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

global $wpdb;

$post_id = 155;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;


?>
 <!--ABOUT AREA-->
    <section class="about-area section-padding" id="contactus">
	    <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(155); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>

                <!-- <div class="contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="253" title="Contact form 1"]'); ?>
                </div> --> 

                <div id="successMessage" style="display:none;">
                    <div class="alert alert-success">Thank's for Contacting us</div>
                </div>

                <div id="errorMessage" style="display:none;">
                    <div class="alert alert-danger">Oops something went wrong. !!</div>
                </div>

                <div class="contact-form">
                    <div role="form" class="wpcf7" id="wpcf7-f253-o1" dir="ltr" lang="en-US">
                        <div class="screen-reader-response"></div>
                        <form  method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                            <input name="_wpcf7" value="253" type="hidden">
                            <input name="_wpcf7_version" value="4.9" type="hidden">
                            <input name="_wpcf7_locale" value="en_US" type="hidden">
                            <input name="_wpcf7_unit_tag" value="wpcf7-f253-o1" type="hidden">
                            <input name="_wpcf7_container_post" value="0" type="hidden">
                            </div>
                            <p><label> Your Name (required)<br>
                                <span class="wpcf7-form-control-wrap your-name"><input id="name" name="your-name" value="" size="40"  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" type="text"></span> </label></p>
                            <p><label> Your Email (required)<br>
                                <span class="wpcf7-form-control-wrap your-email"><input id="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" type="email"></span> </label></p>
                            <p><label> Subject<br>
                                <span class="wpcf7-form-control-wrap your-subject"><input id="subject" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" type="text"></span> </label></p>
                            <p><label> Questions / Comments<br>
                                <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" id="comments" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
                            <p><input value="Send" class="wpcf7-form-control wpcf7-submit" type="button" id="addContact"><span class="ajax-loader"></span></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>                
                </div>

            </div>
            
        </div>
    </section>
    <!--ABOUT AREA END-->