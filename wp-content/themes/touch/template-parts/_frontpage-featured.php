<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

?>
<!--FEATURES AREA-->
<?php global $wpdb;
  //echo site_url();
  //$results = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE post_id IN ( SELECT wp_postmeta.meta_value FROM wp_posts JOIN wp_postmeta WHERE (wp_posts.id = wp_postmeta.post_id AND wp_posts.post_title IN ('Responsive', 'Simple', 'Documentation','Customize') AND wp_postmeta.meta_key = '_thumbnail_id')) AND meta_key = '_wp_attached_file'");
  $results_data = $wpdb->get_results("select * from wp_posts where post_title IN('Responsive','Simple','Documentation','Customize') and post_status ='publish'");
    
  $results = $wpdb->get_results("select wp_postmeta.meta_value from wp_posts join wp_postmeta where wp_posts.id = wp_postmeta.post_id and wp_posts.post_title IN('Responsive','Simple','Documentation','Customize') and wp_postmeta.meta_key='_thumbnail_id'");

  for($i=0;$i<count($results);$i++)
  {
        $res[] = $wpdb->get_results("select * from wp_postmeta where post_id = ".$results[$i]->meta_value ." and meta_key = '_wp_attached_file'");
  }
  
 /* echo '<pre>';
  print_r($res);*/
   ?>
    <section class="features-area gray-bg section-padding" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Amazing Features</h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow fadeIn">
                    <div class="features-image-slider">
                        <!-- <div class="single-image-slide"><img src="<?php echo get_template_directory_uri();?>/img/features/features_mockup_1.png" alt=""></div>
                        <div class="single-image-slide"><img src="<?php echo get_template_directory_uri();?>/img/features/features_mockup_2.png" alt=""></div>
                        <div class="single-image-slide"><img src="<?php echo get_template_directory_uri();?>/img/features/features_mockup_3.png" alt=""></div>
                        <div class="single-image-slide"><img src="<?php echo get_template_directory_uri();?>/img/features/features_mockup_4.png" alt=""></div> -->

                    <?php foreach($res as $data){ ?>
                           <div class="single-image-slide"><img src="<?php echo site_url();?>/wp-content/uploads/<?php echo $data[0]->meta_value; ?>" alt=""></div>
                        <?php  } ?>     

                    </div>
                </div>
                <div class="col-md-8 col-lg-8 col-sm-6 col-xs-12 wow fadeIn">
                    <div class="features-menu-triger">
                        <div class="single-menu-items">
                            <h4>Responsive</h4>
                            <div class="features-icon">
                                <i class="fa fa-television"></i>
                            </div>
                        </div>
                        <div class="single-menu-items">
                            <h4>Simple</h4>
                            <div class="features-icon">
                                <i class="fa fa-diamond"></i>
                            </div>
                        </div>
                        <div class="single-menu-items">
                            <h4>Documentation</h4>
                            <div class="features-icon">
                                <i class="fa fa-pencil-square-o"></i>
                            </div>
                        </div>
                        <div class="single-menu-items">
                            <h4>Customize</h4>
                            <div class="features-icon">
                                <i class="fa fa-cogs"></i>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="features-details-slider">
                        <div class="single-features-details">
                            <h3>Responsive Media</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui maiores sequi tempora obcaecati optio, quas molestiae laudantium fugiat sapiente nisi, pariatur, odio id, architecto non ipsum aliquam itaque molestias recusandae?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia omnis maxime blanditiis libero in. Numquam, dolorum, sunt. Omnis vitae modi, similique, alias eligendi beatae ad asperiores ipsam magnam eveniet possimus!</p>
                        </div>
                        <div class="single-features-details">
                            <h3>Very Simple and Dianamic</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia omnis maxime blanditiis libero in. Numquam, dolorum, sunt. Omnis vitae modi, similique, alias eligendi beatae ad asperiores ipsam magnam eveniet possimus!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui maiores sequi tempora obcaecati optio, quas molestiae laudantium fugiat sapiente nisi, pariatur, odio id, architecto non ipsum aliquam itaque molestias recusandae?</p>
                        </div>
                        <div class="single-features-details">
                            <h3>Well Documented and Organized Code</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui maiores sequi tempora obcaecati optio, quas molestiae laudantium fugiat sapiente nisi, pariatur, odio id, architecto non ipsum aliquam itaque molestias recusandae?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia omnis maxime blanditiis libero in. Numquam, dolorum, sunt. Omnis vitae modi, similique, alias eligendi beatae ad asperiores ipsam magnam eveniet possimus!</p>
                        </div>
                        <div class="single-features-details">
                            <h3>Easy To Customize All Section</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia omnis maxime blanditiis libero in. Numquam, dolorum, sunt. Omnis vitae modi, similique, alias eligendi beatae ad asperiores ipsam magnam eveniet possimus!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui maiores sequi tempora obcaecati optio, quas molestiae laudantium fugiat sapiente nisi, pariatur, odio id, architecto non ipsum aliquam itaque molestias recusandae?</p>
                        </div>
                    </div> -->
                    <div class="features-details-slider">
                    <?php foreach($results_data as $val){ ?>
                        <div class="single-features-details">
                            <?php echo $val->post_content;?>
                        </div>
                    <?php  } ?>
                    </div>    
                    <div class="features-arrow"></div>
                </div>
            </div>
        </div>
    </section>
    <!--FEATURES AREA END-->