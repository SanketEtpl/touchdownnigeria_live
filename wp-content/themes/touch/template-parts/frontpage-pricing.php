<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

?>
   <!--PRICEING AREA-->
    <section class="priceing-area gray-bg section-padding" id="price">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Chose Plan</h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-price text-center wow fadeIn">
                        <div class="price-title-and-rate">
                            <p class="price-title">Basic</p>
                            <h3>$12.99</h3>
                            <p>Per Month</p>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li><i class="fa fa-check"></i> One User</li>
                                <li><i class="fa fa-check"></i> Unlimited Style</li>
                                <li><i class="fa fa-check"></i> Advanced Protection</li>
                                <li><i class="fa fa-check"></i> Offline work</li>
                                <li><i class="fa fa-check"></i> Backup Service</li>
                                <li><i class="fa fa-check"></i> 24x7 Support</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <a href="#">Chose Plan</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-price active text-center wow fadeIn">
                        <div class="price-title-and-rate">
                            <p class="price-title">Standard</p>
                            <h3>$18.99</h3>
                            <p>Per Month</p>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li><i class="fa fa-check"></i> 10 User</li>
                                <li><i class="fa fa-check"></i> Unlimited Style</li>
                                <li><i class="fa fa-check"></i> Advanced Protection</li>
                                <li><i class="fa fa-check"></i> Offline work</li>
                                <li><i class="fa fa-check"></i> Backup Service</li>
                                <li><i class="fa fa-check"></i> 24x7 Support</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <a href="#">Chose Plan</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-price text-center wow fadeIn">
                        <div class="price-title-and-rate">
                            <p class="price-title">Pupular</p>
                            <h3>$35.99</h3>
                            <p>Per Month</p>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li><i class="fa fa-check"></i> 100 User</li>
                                <li><i class="fa fa-check"></i> Unlimited Style</li>
                                <li><i class="fa fa-check"></i> Advanced Protection</li>
                                <li><i class="fa fa-check"></i> Offline work</li>
                                <li><i class="fa fa-check"></i> Backup Service</li>
                                <li><i class="fa fa-check"></i> 24x7 Support</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <a href="#">Chose Plan</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-price text-center wow fadeIn">
                        <div class="price-title-and-rate">
                            <p class="price-title">Business</p>
                            <h3>$49.99</h3>
                            <p>Per Month</p>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li><i class="fa fa-check"></i> Unlimited User</li>
                                <li><i class="fa fa-check"></i> Unlimited Style</li>
                                <li><i class="fa fa-check"></i> Advanced Protection</li>
                                <li><i class="fa fa-check"></i> Offline work</li>
                                <li><i class="fa fa-check"></i> Backup Service</li>
                                <li><i class="fa fa-check"></i> 24x7 Support</li>
                            </ul>
                        </div>
                        <div class="price-button">
                            <a href="#">Chose Plan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--PRICEING AREA END-->