<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */
global $wpdb;

$post_id = 134;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;

$image_data = $wpdb->get_results("select wp_abcfic_images.filename from wp_abcfic_collections join wp_abcfic_images where (wp_abcfic_collections.coll_id = wp_abcfic_images.coll_id and wp_abcfic_collections.coll_name='Download Image') order by wp_abcfic_images.added_dt desc limit 0,1 ");

/*echo '<pre>';
print_r($image_data);*/

 

?>
 <!--DOWNLOAD AREA-->
    <section class="download-area section-padding wow fadeIn" id="download">
        <div class="download-area-bg">
            
            <div class="app_download-area" style="background-image:url(<?php echo site_url();?>/wp-content/uploads/img-collections/download-image/<?php echo $image_data[0]->filename; ?>);"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(134); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <ul class="download-button wow fadeIn">
                        <!-- <li><a href="#"><i class="fa fa-apple"></i>apple store</a></li> -->
                        <li><a href="https://play.google.com/store/apps/details?id=com.exceptionaire.root.tdn&hl=en" target="_blank"><i class="fa fa-android" ></i>Google Play Store</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--DOWNLOAD AREA END-->