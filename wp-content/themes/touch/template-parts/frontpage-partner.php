<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

global $wpdb;

$post_id = 119;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;


$results_data = $wpdb->get_results("select post_content from wp_posts where post_title ='Partnering' and post_status = 'publish'");



?>
 <!--ABOUT AREA-->
    <section class="about-area section-padding" id="partner">
	    <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(119); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!--ABOUT AREA END-->