<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

$post_id = 127;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;


global $wpdb;

$results_data = $wpdb->get_results("select post_content from wp_posts where post_title ='Feedback' and post_status = 'publish'");

?>
 <!--TESTMONIAL AREA-->
    <section class="testmonial-area gray-bg section-padding" id="testmonial">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(127); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>
            </div>
        <!--    <div class="row testmonial-slider wow fadeIn">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="testmonial-content tab-content">
                        <div class="single-testmonial tab-pane fade in active" id="testmonial_one">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <div class="testmonial-author-name-title-img">
                                        <div class="author-img"><img src="<?php echo get_template_directory_uri();?>/img/client/author-1.jpg" alt=""></div>
                                        <h4>Alexa Jolli</h4>
                                        <h5>Web Developer</h5>
                                        <div class="testmonial-author-social-bookmark">
                                            <ul>
                                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                                <li><a href=""><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12 ">
                                    <div class="testmonial-author-details">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorecat</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-testmonial tab-pane fade" id="testmonial_two">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <div class="testmonial-author-name-title-img">
                                        <div class="author-img"><img src="<?php echo get_template_directory_uri();?>/img/client/author-2.jpg" alt=""></div>
                                        <h4>Gonder Ullah</h4>
                                        <h5>Web Designer</h5>
                                        <div class="testmonial-author-social-bookmark">
                                            <ul>
                                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                                <li><a href=""><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12 ">
                                    <div class="testmonial-author-details">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorecat</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-testmonial tab-pane fade" id="testmonial_three">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <div class="testmonial-author-name-title-img">
                                        <div class="author-img"><img src="<?php echo get_template_directory_uri();?>/img/client/author-3.jpg" alt=""></div>
                                        <h4>Miss Fata Kopali</h4>
                                        <h5>Web Developer</h5>
                                        <div class="testmonial-author-social-bookmark">
                                            <ul>
                                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                                <li><a href=""><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12 ">
                                    <div class="testmonial-author-details">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorecat</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-testmonial tab-pane" id="testmonial_four">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <div class="testmonial-author-name-title-img">
                                        <div class="author-img"><img src="<?php echo get_template_directory_uri();?>/img/client/author-4.jpg" alt=""></div>
                                        <h4>Mr Tare Nare</h4>
                                        <h5>Web Developer</h5>
                                        <div class="testmonial-author-social-bookmark">
                                            <ul>
                                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                                <li><a href=""><i class="fa fa-skype"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12 ">
                                    <div class="testmonial-author-details">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorecat</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-xs-12 client-slider wow fadeIn">
                    <div class="single-client active">
                        <a data-toggle="tab" href="#testmonial_one">
                            <img src="<?php echo get_template_directory_uri();?>/img/client/client_1.png" alt="">
                        </a>
                    </div>
                    <div class="single-client">
                        <a data-toggle="tab" href="#testmonial_two">
                            <img src="<?php echo get_template_directory_uri();?>/img/client/client_2.png" alt="">
                        </a>
                    </div>
                    <div class="single-client">
                        <a data-toggle="tab" href="#testmonial_three">
                            <img src="<?php echo get_template_directory_uri();?>/img/client/client_3.png" alt="">
                        </a>
                    </div>
                    <div class="single-client">
                        <a data-toggle="tab" href="#testmonial_four">
                            <img src="<?php echo get_template_directory_uri();?>/img/client/client_4.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div> -->
        <?php echo do_shortcode('[tpsscode]'); ?>
    </section>
    <!--TESTMONIAL AREA END-->