<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

?>
 <!--FUN FACT AREA-->
    <section class="fact-area red-bg section-padding wow fadeIn" id="fact">
        <div class="fact-area-bg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact text-center wow fadeIn">
                        <div class="fact-icon"><i class="fa fa-download"></i></div>
                        <h3 class="counter">3250</h3>
                        <p>Downloads</p>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact text-center wow fadeIn">
                        <div class="fact-icon"><i class="fa fa-thumbs-up"></i></div>
                        <h3 class="counter">4270</h3>
                        <p>likes</p>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact text-center wow fadeIn">
                        <div class="fact-icon"><i class="fa fa-star"></i></div>
                        <h3 class="counter">3250</h3>
                        <p>Rated</p>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact text-center wow fadeIn">
                        <div class="fact-icon"><i class="fa fa-clock-o"></i></div>
                        <h3 class="counter">1140</h3>
                        <p>Time Saved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FUN FACT AREA END-->