<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */


global $wpdb;

$post_id = 6;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;


$results_data = $wpdb->get_results("select post_content from wp_posts where post_title ='About Us' and post_status = 'publish'");

?>
 <!--ABOUT AREA-->
    <section class="about-area dark-bg section-padding" id="about">
	 <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(6); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p><?php  echo do_shortcode( $content );?></p>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="single-about wow fadeIn">
                        <div class="about-icon"><i class="fa fa-television"></i></div>
                        <div class="about-details">
                            <h4>fully responsive design</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="single-about wow fadeIn">
                        <div class="about-icon"><i class="fa fa-map-o"></i></div>
                        <div class="about-details">
                            <h4>fully responsive design</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="single-about wow fadeIn">
                        <div class="about-icon"><i class="fa fa-picture-o"></i></div>
                        <div class="about-details">
                            <h4>fully responsive design</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="about-bottom-video wow fadeIn">
                         <img src="<?php echo get_template_directory_uri();?>/img/video/ipad_main_bg.png" alt="">
                        <div class="video-area">
                            <video id="bgvid" loop>
                                <source src="<?php echo get_template_directory_uri();?>/img/video.mp4" type="video/mp4">
                            </video>
                            <button><i class="fa fa-play wow fadeIn"></i></button>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!--ABOUT AREA END-->