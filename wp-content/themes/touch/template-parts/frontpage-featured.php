<?php 

global $wpdb;
$post_id = 84;// example post id
$post_content = get_post($post_id);
$content = $post_content->post_content;
//echo do_shortcode( $content );//executing shortcodes

$posts = [31,34,40,43]; 
$icons = ['fa fa-television','fa fa-diamond','fa fa-pencil-square-o','fa fa-cogs'];

?>

<section class="features-area gray-bg section-padding" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2><?php echo get_the_title(84); ?></h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>

                        <p><?php echo do_shortcode($content); ?></p>
                        
                    </div>
                </div>
            </div>
            
        </div>
</section>