<?php
/** FEATURES AREA
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

?>
    <!--SUBSCRIBER AREA-->
    <section class="subscriber-area section-padding" id="subscriber">
        <div class="subscriber-area-bg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Subscribe For Newsletter</h2>
                        <div class="area-title-after"><span class="area-icon"><i class="fa fa-angle-down"></i></span></div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="subscriber-form wow fadeIn">
                        <!--<form action="#">
                            <input type="email" name="subscriber_email" id="subscriber_email">
                            <button type="submit">Submit</button>
                        </form>-->

                        <form id="mc-form">
                            <label class="mt10" for="mc-email"></label>
                            <input type="email" id="mc-email" placeholder="Enter Your Email">
                            <button type="submit" class="plus-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SUBSCRIBER AREA END-->