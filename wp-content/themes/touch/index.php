<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package touch
 */

get_header();

	get_template_part( 'template-parts/frontpage', 'featured');
	get_template_part( 'template-parts/frontpage', 'funfact');
	get_template_part( 'template-parts/frontpage', 'aboutus');
	get_template_part( 'template-parts/frontpage', 'partner');
	get_template_part( 'template-parts/frontpage', 'privacy-policy');
	//get_template_part( 'template-parts/frontpage', 'testmonial');
	get_template_part( 'template-parts/frontpage', 'screenshot');
	get_template_part( 'template-parts/frontpage', 'download');
	get_template_part( 'template-parts/frontpage', 'terms');
	//get_template_part( 'template-parts/frontpage', 'testmonial');
	get_template_part( 'template-parts/frontpage', 'contactus');
	


get_footer();
